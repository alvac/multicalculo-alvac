﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum Qualifier
    {
        Other = 0,
        
        /// <summary>
        /// Sindicaliado
        /// </summary>
        Unionised = 1,

        /// <summary>
        /// Parceiro garantia.
        /// </summary>
        WarrantyPartner = 2,

        /// <summary>
        /// Parceiro cristão.
        /// </summary>
        ChristPartner = 3,




    }
}
