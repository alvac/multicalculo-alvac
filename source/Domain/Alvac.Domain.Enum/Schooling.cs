﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum Schooling
    {
        None = 0,
        PostGraduate = 1,
        TertiaryEducation = 2,
        TechinicalSecondaryEducation = 3,
        PrimaryEducation = 4,
        IncompletePrimaryEducation = 5
    }
}
