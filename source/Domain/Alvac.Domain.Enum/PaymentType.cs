﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    /// <summary>
    /// Tipo de pagamento.
    /// </summary>
    public enum PaymentType
    {
        None = 0,
        /// <summary>
        /// Carne
        /// </summary>
        PaymentBook = 1,
        /// <summary>
        /// Débito bancário
        /// </summary>
        BankDebit = 2,
        /// <summary>
        /// Cartão de credito
        /// </summary>
        CreditCard = 3,
        /// <summary>
        /// Cheque
        /// </summary>
        BankCheck = 4
    }
}
