﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{

    public enum DescriptionCode
    {
        /// <summary>
        /// Código de Informação
        /// 100 - 199
        /// </summary>
        Continue = 100,

        /// <summary>
        /// Códigos de Sucessos
        /// 200 - 299
        /// </summary>
        Successfully = 200,

        /// <summary>
        /// Códigos de Redirecionamentos / Retentativas
        /// 300 - 399
        /// </summary>
        TemporaryRedirect = 307,

        /// <summary>
        /// Códigos de Erros no Cliente / Retentáveis
        /// 400 - 499
        /// </summary>
        BadRequest = 400,
        Unauthorized = 401,
        NotFound = 404,
        TimeOut = 408,
        Conflict = 409,

        /// <summary>
        /// Códigos de Erros do Servidor
        /// 500 - 599
        /// </summary>
        InternalServerError = 500,
        InvalidRequest = 501,
        UserError = 502
    }
}
