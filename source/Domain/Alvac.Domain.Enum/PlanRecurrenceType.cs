﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum PlanRecurrenceType
    {
        [Description("Mês")]
        Month = 0,

        [Description("Dias")]
        Day = 1
    }
}
