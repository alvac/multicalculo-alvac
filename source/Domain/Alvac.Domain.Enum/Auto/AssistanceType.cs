﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    /// <summary>
    /// Tipo de assistrncia.
    /// </summary>
    public enum AssistanceType
    {
        None = 0,
        Essential = 1,
        Full = 2,
        DayAndNight = 3,
        Executive = 4,
        More = 5
    }
}
