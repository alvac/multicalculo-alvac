﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    /// <summary>
    /// Coberturas
    /// </summary>
    public enum CoveragePlanType
    {
        None = 0,
        Comprehensive = 1,
        FireTheft = 2,
        Theft = 3
    }
}
