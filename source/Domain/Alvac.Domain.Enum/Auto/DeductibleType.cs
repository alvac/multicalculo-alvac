﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    /// <summary>
    /// Tipo de Franquia
    /// </summary>
    public enum DeductibleType
    {
        None = 0,
        Required = 1,
        Reduced50Percent = 2,
        Reduced25Percent = 3,
        Optional50Percent = 4,
        Optional25Percent = 5
    }
}
