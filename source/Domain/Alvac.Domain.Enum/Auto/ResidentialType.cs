﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    public enum ResidentialType
    {
        None = 0,
        Home = 1,
        GatedCommunity = 2,
        Apartment = 3,
        Other = 4
    }
}
