﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    /// <summary>
    /// Tipo de utilização do veiculo.
    /// </summary>
    [Flags]
    public enum UseType
    {
        None = 0,
        GoToJob = 1,
        GoToSchool = 2,
        Commercial = 4,
        Leisure = 8
    }
}
