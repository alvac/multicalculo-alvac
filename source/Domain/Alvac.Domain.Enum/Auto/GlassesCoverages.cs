﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    /// <summary>
    /// Coberturas de vidros
    /// </summary>
    public enum GlassesCoverages
    {
        None = 0,
        FullWindows = 1,
        FlashlightFullWindowsRearviewMirror = 2
    }
}
