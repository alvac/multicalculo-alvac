﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    public enum CommercialUsePeriod
    {
        None = 0,
        Daytime = 1,
        Nightly = 2,
        Undetermined = 3
    }
}
