﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    [Flags]
    public enum Garage
    {
        None = 0,
        Residence = 1,
        Job = 2,
        School = 4
    }
}
