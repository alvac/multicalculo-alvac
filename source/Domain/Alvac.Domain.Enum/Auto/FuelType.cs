﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    public enum FuelType
    {
        None = 0,
        Flex = 1,
        Diesel = 2,
        Gasoline = 3,
        Alcohol = 4
        // Alcool, gasolina, diesel, gnv.
    }
}
