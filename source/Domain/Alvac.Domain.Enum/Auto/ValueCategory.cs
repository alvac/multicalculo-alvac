﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    /// <summary>
    /// Categoria do valor de cobertura
    /// </summary>
    public enum ValueCategory
    {
        None = 0,
        Referenced = 1,
        Determined = 2,
    }
}
