﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum.Auto
{
    public enum CommercialUseType
    {
        None = 0,
        Visits = 1,
        Deliveries = 2
    }
}
