﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    /// <summary>
    /// Os tipos de assinaturas que podem ter no pedido
    /// </summary>
    public enum SubscriptionType
    {
        /// <summary>
        /// Experiência
        /// </summary>
        Trial = 1,

        /// <summary>
        /// Normal
        /// </summary>
        Normal = 2,

        /// <summary>
        /// Gratuito
        /// </summary>
        [Description("Gratuito")]
        Free = 3
    }
}
