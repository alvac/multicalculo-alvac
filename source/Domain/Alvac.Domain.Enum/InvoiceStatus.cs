﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum InvoiceStatus
    {
        Open = 1,
        Closed = 2,
        Canceled = 3
    }
}
