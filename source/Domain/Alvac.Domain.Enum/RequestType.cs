﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    /// <summary>
    /// Os tipo de requisição que podem ter no pedido
    /// </summary>
    public enum RequestType
    {
        None = 0,

        /// <summary>
        /// Cotação de automóvel
        /// </summary>
        [Description("Cotação de automóvel")]
        AutoQuotation = 1,

        /// <summary>
        /// Cotação de vida
        /// </summary>
        [Description("Cotação de vida")]
        LifeQuotation = 2,

        /// <summary>
        /// Cotação de residencia
        /// </summary>
        [Description("Cotação de residencia")]
        ResidenceQuotation = 3
    }
}
