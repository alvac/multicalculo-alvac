﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum FileType
    {
        Pdf = 0,
        Html = 1,
        Xml = 2,
        Json = 3
    }
}
