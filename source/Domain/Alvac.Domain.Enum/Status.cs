﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum Status
    {
        Starting = 1,
        Enqueued = 2,
        Waiting = 3,
        Processed = 4,
        Success = 5,
        Failure = 6
    }
}
