﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    /// <summary>
    /// Tipo de pessoa
    /// </summary>
    public enum PersonType
    {
        None = 0,

        /// <summary>
        /// Física
        /// </summary>
        Natural = 1,

        /// <summary>
        /// Jurídica
        /// </summary>
        Legal = 2
    }
}
