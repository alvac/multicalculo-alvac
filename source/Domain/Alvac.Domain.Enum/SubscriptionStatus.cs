﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum SubscriptionStatus
    {
        [Description("Pendente")]
        Pending = 1,

        [Description("Ativo")]
        Active = 2,

        [Description("Cancelado")]
        Canceled = 3
    }
}
