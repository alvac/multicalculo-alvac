﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public class Role
    {
        public const string Master = "8a93527e-2f20-4889-a34b-c27448b87498";
        public const string Accounting = "0c9ce124-5fe3-4c89-bc18-729995850705";
        public const string Internet = "63f3a8ef-5baf-469d-8ab9-edd0afbd8431";
        public const string Alvac = "EAFC4BD2-70A1-46C9-BB47-44A3A191B799";

        public const string MasterString = "Master";
        public const string AccountingString = "Accounting";
        public const string InternetString = "Internet";
        public const string AlvacString = "Alvac";
    }
}
