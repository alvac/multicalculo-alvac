﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum Relationship
    {
        None = 0,
        Child = 1,
        Married = 2,
        Parent = 3,
        Driver = 4,
        Employee = 5,
        Employer = 6,
        Partner = 7,
        Self = 8,
        OtherIndividual = 9
    }
}
