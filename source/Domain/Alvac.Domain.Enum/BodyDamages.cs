﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    /// <summary>
    /// Danos Corporais
    /// </summary>
    public enum BodyDamages
    {
        None = 0,
        ThirtyThousand = 30000,
        FortyThousand = 40000,
        FiftyThousand = 50000,
        SixtyThousand = 60000,
        HundredThousand = 10000,
        OneHundredAndTwentyFiveThousand = 12500,
        HundredAndFiftyThousand = 15000,
        OneHundredAndSeventyFiveThousand = 17500,
        TwoHundredThousand = 20000,
        TwoHundredFiftyThousand = 25000,
        ThreeHundredThousand = 30000,
        ThreeHundredAndFiftyThousand = 35000,
        FourHundredThousand = 40000
    }
}
