﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum MaritalStatus
    {
        None = 0,
        Single = 1,
        Married = 2,
        Divorced = 3,
        Widow = 4,
        Separated = 5
    }
}
