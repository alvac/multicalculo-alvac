﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum CallType
    {
        None = 0,
        Post = 1,
        Get = 2
    }
}
