﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Enum
{
    public enum Technology
    {
        None = 0,
        [Description("Web service")]
        WebService = 1,
        [Description("Robo")]
        Robotics = 2,
    }
}
