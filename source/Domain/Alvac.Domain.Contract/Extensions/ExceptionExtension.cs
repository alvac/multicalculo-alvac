﻿using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Extensions
{
    public static class ExceptionExtension
    {
        public static OperationResult ToOperationResult(this Exception exception, string message)
        {
            var descriptionCode = DescriptionCode.InternalServerError;

            if (exception is AlvacException)
            {
                var AlvacException = exception as AlvacException;
                descriptionCode = AlvacException.DescriptionCode;
            }

            return new OperationResult()
            {
                Code = Code.Failure,
                DescriptionCode = descriptionCode,
                Description = message
            };
        }

        public static OperationResult ToOperationResult(this Exception exception)
        {
            return ToOperationResult(exception, string.Empty);
        }
    }
}
