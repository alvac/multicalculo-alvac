﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Extensions
{
    public static class BaseResponseExtension
    {
        public static NotificationRequest ToNotificationRequest(this BaseResponse baseResponse, string correlationId)
        {
            return new NotificationRequest
            {
                CorrelationId = correlationId,
                Response = baseResponse
            };
        }
    }
}
