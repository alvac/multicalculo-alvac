﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Extensions
{
    public static class TaskExtension
    {
        public static async Task TimeoutAfter(this Task task, int millisecondsTimeout)
        {
            if (millisecondsTimeout == 0)
            {
                await task;
                return;
            }

            var tokenSource = new CancellationTokenSource();

            if (task == await Task.WhenAny(task, Task.Delay(millisecondsTimeout, tokenSource.Token)))
            {
                tokenSource.Cancel();
                await task;
            }
            else
            {
                throw new TimeoutException();
            }
        }
    }
}
