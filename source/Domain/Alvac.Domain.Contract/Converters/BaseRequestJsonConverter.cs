﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Enum;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Converters
{
    public class BaseRequestJsonConverter : JsonCreationConverter<BaseRequest>
    {
        protected override BaseRequest Create(Type objectType, JObject jsonObject)
        {
            RequestType requestType = (RequestType)System.Enum.Parse(typeof(RequestType), jsonObject["requestType"].ToString());

            switch (requestType)
            {
                case RequestType.AutoQuotation:
                    return new AutoQuotationRequest();

                default:
                    return null;
            }
        }
    }
}
