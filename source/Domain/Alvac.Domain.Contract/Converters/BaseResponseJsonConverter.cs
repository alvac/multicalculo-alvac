﻿
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Contract.Response.Auto;
using Alvac.Domain.Enum;
using Newtonsoft.Json.Linq;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Converters
{
    public class BaseResponseJsonConverter : JsonCreationConverter<BaseResponse>
    {
        protected override BaseResponse Create(Type objectType, JObject jsonObject)
        {
            RequestType requestType = (RequestType)System.Enum.Parse(typeof(RequestType), jsonObject["requestType"].ToString());

            switch (requestType)
            {
                case RequestType.AutoQuotation:
                    return new AutoQuotationResponse();

                default:
                    return null;
            }
        }
    }
}
