﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract
{
    public class AlvacJson<T> where T : class
    {
        private static JsonSerializerSettings _settings = new JsonSerializerSettings()
                {
                    NullValueHandling = NullValueHandling.Ignore,
                    ContractResolver = new CamelCasePropertyNamesContractResolver(),
                    Formatting = Formatting.Indented
                };

        static AlvacJson()
        {
            _settings.Converters.Add(new StringEnumConverter());
        }

        public static string Serialize(T t)
        {
            return JsonConvert.SerializeObject(t, _settings);
        }

        public static T Deserialize(string json)
        {
            return JsonConvert.DeserializeObject<T>(json, _settings);
        }
    }
}
