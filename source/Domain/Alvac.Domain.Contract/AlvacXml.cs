﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using System.Xml.Serialization;

namespace Alvac.Domain.Contract
{
    /// <summary>
    /// Classe util Alvac xml.
    /// </summary>
    public class AlvacXml<T> where T : class
    {
        public static string Serialize(object t)
        {
            var serializer = new XmlSerializer(t.GetType());

            using (var stringWriter = new StringWriter())
            {
                var ns = new XmlSerializerNamespaces();
                ns.Add("", "");

                serializer.Serialize(stringWriter, t, ns);

                var xml = stringWriter.ToString();
                return Regex.Replace(xml, " encoding=[\"a-zA-Z0-9_-]*", string.Empty);
            }
        }

        public static string SerializeRemoveXmlTag(object t)
        {
            return Serialize(t).Replace("<?xml version=\"1.0\"?>", "").Trim();
        }

        

        public static T Deserialize(string xml)
        {
            var serializer = new XmlSerializer(typeof(T));

            // Retira atributos vazios do XML, para evitar problemas de serialização.
            xml = Regex.Replace(xml, " [a-zA-Z0-9_-]*=\"\"", string.Empty);
            xml = Regex.Replace(xml, " encoding=[\"a-zA-Z0-9_-]*", string.Empty);
            xml = xml.Replace(" xmlns:xsd=\"http://www.w3.org/2001/XMLSchema\"", string.Empty);
            xml = xml.Replace(" xmlns:xsi=\"http://www.w3.org/2001/XMLSchema-instance\"", string.Empty);
            xml = xml.Replace(" xsi:nil=\"true\"", string.Empty);

            using (var reader = new StringReader(xml))
            {
                return serializer.Deserialize(reader) as T;
            }
        }

        public static T Deserialize(Stream stream)
        {
            var serializer = new XmlSerializer(typeof(T));

            try
            {
                return serializer.Deserialize(stream) as T;
            }
            catch (Exception except)
            {
                throw new InvalidOperationException(string.Format("Failed to create object of type {0} from Stream.", typeof(T).FullName), except);
            }
        }
    }
}
