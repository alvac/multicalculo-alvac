﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract
{
    public interface IMapper
    {
        TResult Map<TResult>(object source);

        IQueryable<TResult> Project<TSource, TResult>(IQueryable<TSource> source);

        TResult Map<TSource, TResult>(TSource source, TResult destination);
    }
}
