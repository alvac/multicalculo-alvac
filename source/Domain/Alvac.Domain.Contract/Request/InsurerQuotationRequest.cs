﻿using Alvac.Domain.Contract.Converters;
using Alvac.Domain.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Request
{
    public class InsurerQuotationRequest
    {
        public int Insurer { get; set; }

        public IDictionary<string, object> Parameters { get; set; }

        /// <summary>
        /// Pedido a ser processado pela plataforma.
        /// </summary>      
        [JsonConverter(typeof(BaseRequestJsonConverter))]
        public BaseRequest Request { get; set; }
        
    }
}
