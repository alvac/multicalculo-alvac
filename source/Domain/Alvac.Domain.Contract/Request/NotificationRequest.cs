﻿using Alvac.Domain.Contract.Converters;
using Alvac.Domain.Contract.Response;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Request
{
    public class NotificationRequest
    {
        public int InsurerQuotationId { get; set; }

        public string CorrelationId { get; set; }

        [JsonConverter(typeof(BaseResponseJsonConverter))]
        public BaseResponse Response { get; set; }
    }
}
