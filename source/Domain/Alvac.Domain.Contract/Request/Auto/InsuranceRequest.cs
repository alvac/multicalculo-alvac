﻿using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Request.Auto
{
    /// <summary>
    /// Seguro a ser contratado / atual.
    /// </summary>
    public class InsuranceRequest
    {
        #region Constructor

        public InsuranceRequest()
        {

        }

        public InsuranceRequest(bool initializerMembers)
        {
            if (initializerMembers)
            {
                this.Renewal = new RenewalRequest();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Tipo cobertura da apólice que deseja contratar
        /// </summary>
        public CoveragePlanType CoveragePlanType { get; set; }

        /// <summary>
        /// Tipo de franquia que deseja contratar.
        /// </summary>
        public DeductibleType DeductibleType { get; set; }

        /// <summary>
        /// É uma renovação ou gostaria de transferir o seguro de outro veículo.
        /// </summary>
        public RenewalRequest Renewal { get; set; }

        /// <summary>
        /// Data de início da apólice
        /// </summary>
        public DateTime InsurancePolicyInitialDate { get; set; }

        /// <summary>
        /// Data de início da apólice
        /// </summary>
        public DateTime InsurancePolicyFinalDate { get; set; }

        #endregion

    }
}
