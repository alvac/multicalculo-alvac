﻿using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alvac.Domain.Contract.Request.Auto
{
    public class MainDriverRequest
    {
        #region Contructor

        public MainDriverRequest()
        {

        }

        public MainDriverRequest(bool initializerMembers)
        {
            if(initializerMembers)
            {
                this.DependentList = new List<DependentRequest>();
            }
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Cliente que esta realizando a contação é o segurado.
        /// </summary>
        public bool IsInsured { get; set; }

        /// <summary>
        /// Cliente que esta realizando a contação é o dono do veículo.
        /// </summary>
        public bool IsVehicleOwner { get; set; }

        /// <summary>
        /// Nome completo
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Telefone celular do cliente que esta realizando a contação.
        /// </summary>
        public string MobilePhoneNumber { get; set; }

        /// <summary>
        /// Telefone fixo do cliente que esta realizando a contação.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Email do cliente que esta realizando a contação.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Sexo
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Data de nascimento.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Cpf.
        /// </summary>
        public string Document { get; set; }

        /// <summary>
        /// Estado civíl.
        /// </summary>
        public MaritalStatus MaritalStatus { get; set; }

        /// <summary>
        /// Cep
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Lista de dependentes
        /// </summary>
        public List<DependentRequest> DependentList { get; set; }

        /// <summary>
        /// Tempo de habilitação.
        /// </summary>
        public int DriverLicenseYears { get; set; }

        /// <summary>
        /// Número da carteira de habilitação
        /// </summary>
        public string DriverLicenseNumber { get; set; }

        /// <summary>
        /// Identificador descrição de atividade de trabalho.
        /// </summary>
        public int WorkActivityId { get; set; }

        /// <summary>
        /// Escolaridade
        /// </summary>
        public Schooling Schooling { get; set; }

        /// <summary>
        /// Relação com o segurado.
        /// </summary>
        public Relationship RelationshipWithInsured { get; set; }

        /// <summary>
        /// Vítima de roubo de veículos nos últimos 24 meses.
        /// </summary>
        public bool HasBeenStolenInTheLast24Months { get; set; }

        /// <summary>
        /// Tipo de residencia onde fica o veiculo.
        /// </summary>
        public ResidentialType ResidentialType { get; set; }

        #endregion
    }
   
}
