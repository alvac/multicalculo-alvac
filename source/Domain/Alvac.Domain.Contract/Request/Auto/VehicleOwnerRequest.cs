﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alvac.Domain.Contract.Request.Auto
{
    public class VehicleOwnerRequest
    {
        #region Constructor

        public VehicleOwnerRequest()
        {

        }
        
        #endregion

        #region Public Properties

        /// <summary>
        /// Nome completo
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Sexo
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Data de nascimento.
        /// </summary>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Tipo de pessoa
        /// </summary>
        public PersonType PersonType { get; set; }

        /// <summary>
        /// Cpf / Cnpj
        /// </summary>
        public string Document { get; set; }

        /// <summary>
        /// Estado civíl.
        /// </summary>
        public MaritalStatus MaritalStatus { get; set; }

        /// <summary>
        /// Cep
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Número de veículos que possui.
        /// </summary>
        public int VehiclesNumbers { get; set; }

        
        #endregion
    }
}
