﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alvac.Domain.Contract.Request.Auto
{
    /// <summary>
    /// Condutor Adicional. Outras pessoas que dirigem o veículo regularmente ou eventualmente como filhos, 
    /// enteados, cônjuge, namorados, pais, irmãos, entre outros.
    /// </summary>
    public class AdditionalDriverRequest
    {

        #region Public Properties

        /// <summary>
        /// Nome completo
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Sexo
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Data de nascimento.
        /// </summary>
        public DateTime BirthDate { get; set; }

        /// <summary>
        /// Estado civíl.
        /// </summary>
        public MaritalStatus MaritalStatus { get; set; }

        /// <summary>
        /// Tempo de habilitação.
        /// </summary>
        public int DriverLicenseYears { get; set; }

        /// <summary>
        /// Relação com o segurado.
        /// </summary>
        public Relationship RelationshipWithInsured { get; set; }

        /// <summary>
        /// Possui veículo em seu nome
        /// </summary>
        public bool HasRegisteredVehicle { get; set; }

        /// <summary>
        /// Mora com o condutor principal.
        /// </summary>
        public bool LivesWithMainDriver { get; set; }

        /// <summary>
        /// Número de dias que utiliza o veículo na semana.
        /// </summary>
        public int NumberOfUseDaysPerWeek { get; set; }

        #endregion
    }
}
