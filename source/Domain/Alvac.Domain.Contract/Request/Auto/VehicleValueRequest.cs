﻿using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Request.Auto
{
    /// <summary>
    /// Dados referente ao tipo / cobertura do valor do veículo.
    /// </summary>
    public class VehicleValueRequest
    {
        /// <summary>
        /// Categria do valor de cobertura
        /// </summary>
        public ValueCategory ValueCategory { get; set; }

        /// <summary>
        /// Valor determinado da cobertura
        /// </summary>
        public decimal? DeterminedValue { get; set; }

        /// <summary>
        /// Valor referenciado da cobertura
        /// </summary>
        public decimal? ReferencedValue { get; set; }

        /// <summary>
        /// Porcentagem do valor de cobertura.
        /// </summary>
        public int? ReferencedPercentage { get; set; }

        /// <summary>
        /// Identifica se o veículo é financiado.
        /// </summary>
        public bool IsFinanced { get; set; }
    }
}
