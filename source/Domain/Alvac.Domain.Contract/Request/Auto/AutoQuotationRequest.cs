﻿using Alvac.Domain.Contract.Response;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum.Auto;
using Alvac.Domain.Contract.Response.Auto;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Commands;

namespace Alvac.Domain.Contract.Request.Auto
{
    /// <summary>
    /// Contrato de cotação
    /// </summary>
    public class AutoQuotationRequest : BaseRequest, ICommand<AutoQuotation>
    {
        #region Contructor

        public AutoQuotationRequest()
        {
            this.RequestType = RequestType.AutoQuotation;
        }

        public AutoQuotationRequest(bool initializerProperties)
        {
            if (initializerProperties)
            {
                this.Notification = new Notification();
                this.Retry = new RetryRequest();

                this.VehicleInfo = new VehicleInfoRequest(true);
                this.VehicleUse = new VehicleUseRequest();
                this.Insured = new InsuredRequest();
                this.MainDriver = new MainDriverRequest(true);
                this.VehicleOwner = new VehicleOwnerRequest();
                this.Insurance = new InsuranceRequest(true);
                this.AdditionalDriverList = new List<AdditionalDriverRequest>();
            }

            this.RequestType = RequestType.AutoQuotation;
        }

        #endregion

        #region Public Properties

        public VehicleInfoRequest VehicleInfo { get; set; }

        public VehicleUseRequest VehicleUse { get; set; }

        public MainDriverRequest MainDriver { get; set; }

        public InsuredRequest Insured { get; set; }

        public VehicleOwnerRequest VehicleOwner { get; set; }

        public InsuranceRequest Insurance { get; set; }

        public List<AdditionalDriverRequest> AdditionalDriverList { get; set; }

        #endregion

        #region RequestBase Members

        public override BaseResponse CreateResponse(Code code, DescriptionCode descriptionCode, string description)
        {
            return new AutoQuotationResponse
            {
                Identifier = this.Identifier,
                DeductibleType = this.Insurance.DeductibleType,
                ResponseDateTime = DateTimeOffset.Now,
                Result = new OperationResult { Code = code, DescriptionCode = descriptionCode, Description = description }
            };
        }

        public override BaseResponse CreateResponse(Code code, string description)
        {
            var descriptionCode = code == Code.Failure ? DescriptionCode.InternalServerError : DescriptionCode.Successfully;
            return CreateResponse(code, descriptionCode, description);
        }

        public override void SetDefaultRequestType()
        {
            this.RequestType = RequestType.AutoQuotation;
        }

        #endregion
    }
}
