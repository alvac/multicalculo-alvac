﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum.Auto;
using Alvac.Domain.Enum;
using Alvac.Domain.Contract.Exceptions;

namespace Alvac.Domain.Contract.Request.Auto
{
    /// <summary>
    /// Contrato de entrada da API.
    /// </summary>
    public class AutoQuotationApiRequest : QuotationApiRequest
    {
        #region Constructor

        public AutoQuotationApiRequest()
        {

        }

        public AutoQuotationApiRequest(bool initializerMembers)
        {
            if (initializerMembers)
            {
                this.InsurerRequestList = new List<InsurerRequest>();
                this.DeductibleTypeList = new List<DeductibleType>();
            }
        }

        #endregion

        #region Public Properties

        public List<DeductibleType> DeductibleTypeList { get; set; }

        #endregion

        #region Override Methods

        public override IEnumerable<InsurerQuotationRequest> ToInsurerQuotations(IMapper mapper)
        {
            var result = new List<InsurerQuotationRequest>();

            if (this.Request is AutoQuotationRequest)
            {
                var autoQuotationRequest = this.Request as AutoQuotationRequest;

                if (autoQuotationRequest.Insurance != null)
                {
                    if (this.DeductibleTypeList != null && this.DeductibleTypeList.Count > 0)
                    {
                        foreach (var deductibleType in this.DeductibleTypeList)
                        {
                            var currentRequest = mapper.Map<AutoQuotationRequest>(autoQuotationRequest);

                            currentRequest.Insurance.DeductibleType = deductibleType;

                            foreach (var insurerRequest in this.InsurerRequestList)
                            {
                                var insurerQuotationRequest = new InsurerQuotationRequest
                                {
                                    Insurer = insurerRequest.Insurer,
                                    Parameters = insurerRequest.Parameters,
                                    Request = currentRequest
                                };

                                result.Add(insurerQuotationRequest);
                            }
                        }
                    }
                    else
                    {
                        throw new AlvacException("DeductibleTypeList not found.", DescriptionCode.InvalidRequest);
                    }
                }
                else
                {
                    throw new AlvacException("Invalid AutoQuotationRequest - Insurance is null or empty.", DescriptionCode.InvalidRequest);
                }
            }
            else
            {
                throw new AlvacException("Request not is valid AutoQuotationRequest", DescriptionCode.InvalidRequest);
            }

            return result;
        }

        #endregion
    }
}
