﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Request.Auto
{
    public class DependentRequest
    {
        public int Age { get; set; }

        public Gender Gender { get; set; }

        public bool LivesWithMainDriver { get; set; }
    }
}
