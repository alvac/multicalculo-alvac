﻿using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alvac.Domain.Contract.Request.Auto
{
    public class VehicleInfoRequest
    {

        #region Contructor

        public VehicleInfoRequest()
        {

        }

        public VehicleInfoRequest(bool initializerMembers)
        {
            if (initializerMembers)
            {
                this.License = new VehicleLicenseRequest();
                this.Value = new VehicleValueRequest();
            }
        }

        #endregion

        #region Public Properties
        
        /// <summary>
        /// Marca do veículo. 
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// Modelo do veículo.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Ano de fabricação veículo.
        /// </summary>
        public int ManufacturingYear { get; set; }

        /// <summary>
        /// Ano do modelo do veiculo.
        /// </summary>
        public int ModelYear { get; set; }

        /// <summary>
        /// Código fipe da versão do veículo.
        /// </summary>
        public string FipeCode { get; set; }

        /// <summary>
        /// Tipo de combutível do veículo.
        /// </summary>
        public FuelType Fuel { get; set; }

        /// <summary>
        /// Descrição do modelo do veículo.
        /// </summary>
        public string ModelDescription { get; set; }

        /// <summary>
        /// Veículo zero KM
        /// </summary>
        public bool IsNew { get; set; }

        /// <summary>
        /// Dispositivos de alarme.
        /// </summary>
        public bool DevicesAlarm { get; set; }

        /// <summary>
        /// Dispositivos de bloquio de ingnição.
        /// </summary>
        public int[] DevicesIgnitionBlocker { get; set; }

        /// <summary>
        /// Dispositivos rastreador.
        /// </summary>
        public int[] DevicesTracker { get; set; }

        /// <summary>
        /// Veículo blindado.
        /// </summary>
        public bool IsArmored { get; set; }

        /// <summary>
        /// Valor covertura blindagem.
        /// </summary>
        public decimal? ArmoryCoverage { get; set; }

        /// <summary>
        /// Possui kit gás
        /// </summary>
        public bool HasNaturalGasKit { get; set; }

        /// <summary>
        /// Valor covertura kit gas.
        /// </summary>
        public decimal? NaturalGasKitCoverage { get; set; }

        /// <summary>
        /// Dados de licença do veículo
        /// </summary>
        public VehicleLicenseRequest License { get; set; }

        /// <summary>
        /// Dados referente ao tipo / cobertura do valor do veículo.
        /// </summary>
        public VehicleValueRequest Value { get; set; }

        #endregion
    }
}
