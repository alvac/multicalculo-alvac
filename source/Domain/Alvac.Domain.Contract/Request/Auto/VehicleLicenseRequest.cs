﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Request.Auto
{
    /// <summary>
    /// Dados de licença do veículo
    /// </summary>
    public class VehicleLicenseRequest
    {
        public string PlateNumber { get; set; }

        public string ChassisNumber { get; set; }

    }
}
