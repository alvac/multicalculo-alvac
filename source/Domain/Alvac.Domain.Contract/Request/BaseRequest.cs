﻿using Alvac.Domain.Contract.Converters;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Request
{
    [Serializable]
    public abstract class BaseRequest 
    {
        #region Contructor

        #endregion

        #region Private methods

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            SetDefaultRequestType();
        }

        [OnSerialized]
        private void OnSerialized(StreamingContext context)
        {
            SetDefaultRequestType();
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// IP do originador do pedido.
        /// </summary>
        public string Ip { get; set; }

        /// <summary>
        /// Identificador da conta que esta realizando a requisição.
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// Identificador do usuario que esta realizando a requisição.
        /// </summary>
        public string UserId { get; set; }

        /// <summary>
        /// Identificador.
        /// </summary>
        public int Identifier { get; set; }

        /// <summary>
        /// Data da requisição.
        /// </summary>
        public DateTimeOffset RequestDateTime { get { return DateTimeOffset.Now; } }

        /// <summary>
        /// Contem as informações para notificar o resultado da operação.
        /// </summary>
        public Notification Notification { get; set; }

        /// <summary>
        /// Classe que representa o numero de retentavias a serem feitas em caso de falha.
        /// </summary>
        public RetryRequest Retry { get; set; }

        /// <summary>
        /// Representa o tipo da requisição.
        /// 1. AutoQuotation
        /// 2. LifeQuotation
        /// 3. ResidenceQuotation
        /// 
        /// 4. AutoTransmission
        /// 5. LifeTransmission
        /// 6. ResidenceTransmission 
        /// </summary>
        public RequestType RequestType { get; set; }

        #endregion

        #region Public abstract methods

        public abstract BaseResponse CreateResponse(Code code, DescriptionCode descriptionCode, string description);

        public abstract BaseResponse CreateResponse(Code code, string description);

        public abstract void SetDefaultRequestType();

        #endregion
    }

    /// <summary>
    /// Representa os dados para enviar notificação.
    /// </summary>
    public class Notification
    {
        #region Contructor

        public Notification()
        {

        }

        public Notification(Notification notificationRequest)
        {
            if (notificationRequest != null)
            {
                notificationRequest.CallType = notificationRequest.CallType;
                notificationRequest.Path = notificationRequest.Path;
            }
        }

        #endregion

        /// <summary>
        /// Identificador passado pela aplicação para corelação.
        /// </summary>
        public string CorrelationId { get; set; }

        /// <summary>
        /// Tipo de chamada a ser realizada para entregar a notificação.
        /// </summary>
        public CallType CallType { get; set; }

        /// <summary>
        /// Caminho de entrega da notificação.
        /// </summary>
        public string Path { get; set; }
        
        public override string ToString()
        {
            return string.Format("CorrelationId: {0} - NotificationCallType: {1} - NotificationPath: {2}", CorrelationId, CallType, Path);
        }
    }
}
