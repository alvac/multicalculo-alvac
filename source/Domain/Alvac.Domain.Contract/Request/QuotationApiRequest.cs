﻿using Alvac.Domain.Contract.Converters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using Alvac.Domain.Contract.Exceptions;

namespace Alvac.Domain.Contract.Request
{

    /// <summary>
    /// Contrato base de cotação de entrada da API
    /// </summary>
    [Serializable]
    public abstract class QuotationApiRequest
    {
        #region Public properties

        public List<InsurerRequest> InsurerRequestList { get; set; }

        /// <summary>
        /// Pedidos a serem processados pela plataforma.
        /// </summary>    
        [JsonConverter(typeof(BaseRequestJsonConverter))]
        public BaseRequest Request { get; set; }

        #endregion

        #region Public Methods

        public virtual IEnumerable<InsurerQuotationRequest> ToInsurerQuotations(IMapper mapper)
        {
            IEnumerable<InsurerQuotationRequest> result = new List<InsurerQuotationRequest>();

            if (InsurerRequestList != null && InsurerRequestList.Count > 0)
            {
                foreach (var insurerRequest in this.InsurerRequestList)
                {
                    var insurerQuotationRequest = new InsurerQuotationRequest
                    {
                        Insurer = insurerRequest.Insurer,
                        Parameters = insurerRequest.Parameters,
                        Request = mapper.Map<BaseRequest>(this.Request)
                    };
                }
            }
            else
            {
                throw new AlvacException("InsurerRequestList is null or empty.", DescriptionCode.InvalidRequest);
            }

            return result;
        }

        #endregion
    }
}
