﻿using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Request
{
    /// <summary>
    /// É uma renovação ou gostaria de transferir o seguro
    /// </summary>
    public class RenewalRequest
    {
        /// <summary>
        /// Seguradora atual
        /// </summary>
        public int? CurrentInsuranceCompany { get; set; }

        /// <summary>
        /// Tipo cobertura da apólice atual
        /// </summary>
        public CoveragePlanType CurrentCoveragePlanType { get; set; }

        /// <summary>
        /// Classe de Bônus da apólice atual
        /// </summary>
        public int CurrentInsuranceBonus { get; set; }

        /// <summary>
        /// Número da apólice atual
        /// </summary>
        public string CurrentInsuranceNumber { get; set; }

        /// <summary>
        /// Sinistros ocorreram na apólice
        /// </summary>
        public int ClaimQuantity { get; set; }
        
        /// <summary>
        /// Data do início da apólice atual
        /// </summary>
        public DateTime? CurrentInsurancePolicyDate { get; set; }

        /// <summary>
        /// Data do vencimento da apólice atual
        /// </summary>
        public DateTime? CurrentInsurancePolicyExpirationDate { get; set; }

        /// <summary>
        /// Corretor detem a apólice.
        /// </summary>
        public bool IsPolicyOwner { get; set; }
    }
}
