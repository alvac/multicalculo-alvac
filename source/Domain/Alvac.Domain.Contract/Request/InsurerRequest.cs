﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;

namespace Alvac.Domain.Contract.Request
{
    public class InsurerRequest
    {
        /// <summary>
        /// Seguradora
        /// </summary>
        public int Insurer { get; set; }

        /// <summary>
        /// Dicionário de paradmetros de coniuração da seguradora.
        /// </summary>
        public IDictionary<string, object> Parameters { get; set; }
    }
}
