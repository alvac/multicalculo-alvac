﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Request
{
    /// <summary>
    /// Representa as informações para retentativa.
    /// </summary>
    public class RetryRequest
    {
        #region Contructor

        public RetryRequest()
        {

        }

        public RetryRequest(RetryRequest retryRequest)
        {
            if (retryRequest != null)
            {
                retryRequest.Max = retryRequest.Max;
                retryRequest.Attempts = retryRequest.Attempts;
                retryRequest.Next = retryRequest.Next;
            }
        }

        #endregion

        #region Public Properties

        public int? Max { get; set; }

        public int? Attempts { get; set; }

        public int? Interval { get; set; }

        public DateTimeOffset? Next { get; set; }

        #endregion
    }
}
