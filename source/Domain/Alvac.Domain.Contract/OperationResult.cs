﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract
{
    public class OperationResult
    {
        /// <summary>
        /// Indica o resultado completo da operação. Valores:
        /// </summary>
        public Code Code { get; set; }

        /// <summary>
        /// Indica o codigo de descrição da operação. 
        /// </summary>
        public DescriptionCode DescriptionCode { get; set; }

        /// <summary>
        /// Descrição sobre o resultado da operação completa.
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Mensagem "amigável" descritiva da operação. 
        /// </summary>
        public string Message { get; set; }


        public override string ToString()
        {
            return string.Format("Code: {0} - DescriptionCode: {1} - Description: {2} - Message: {3}", Code, DescriptionCode, Description, Message);
        }
    }
}
