﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Response
{
    /// <summary>
    ///  Arquivo do orçamento de saída da seguradora
    /// </summary>
    public class BudgetFileResponse
    {
        /// <summary>
        /// Tipo do arquivo
        /// </summary>
        public FileType Type { get; set; }

        /// <summary>
        /// Conteúdo binario do arquivo.
        /// </summary>
        public byte[] Content { get; set; }
    }
}
