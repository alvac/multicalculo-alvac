﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Response
{
    [Serializable]
    public abstract class BaseResponse
    {
        #region Constructor

        public BaseResponse()
        {
            this.Result = new OperationResult();
        }

        #endregion

        #region Private methods

        [OnDeserialized]
        private void OnDeserialized(StreamingContext context)
        {
            SetDefaultRequestType();
        }

        [OnSerialized]
        private void OnSerialized(StreamingContext context)
        {
            SetDefaultRequestType();
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Identificador da requisição.
        /// </summary>
        public int Identifier { get; set; }
        
        /// <summary>
        /// Valor do prêmio do seguro.
        /// </summary>
        public decimal PremiumValue { get; set; }

        /// <summary>
        /// Lista de formas de pagamento
        /// </summary>
        public List<PaymentResponse> Payment { get; set; }

        /// <summary>
        /// Data de validade do prêmio.
        /// </summary>
        public DateTimeOffset? ValidUntil { get; set; }

        /// <summary>
        /// Identificador da transação no gateway.
        /// </summary>
        public string GatewayId { get; set; }

        /// <summary>
        /// Código do orçamento no sistema da seguradora
        /// </summary>
        public string InsurerBudgetCode { get; set; }

        /// <summary>
        /// Indica o resultado da operação realizada.
        /// </summary>
        public OperationResult Result { get; set; }

        /// <summary>
        /// Hora/data que a resposta foi processada
        /// </summary>
        public DateTimeOffset ResponseDateTime { get; set; }

        /// <summary>
        /// Arquivo do orçamento de saída da seguradora
        /// </summary>
        public BudgetFileResponse BudgetFile { get; set; }

        /// <summary>
        /// Representa o tipo da requisição.
        /// </summary>
        public RequestType RequestType { get; set; }

        #endregion

        #region Public abstract methods

        public abstract void SetDefaultRequestType();

        #endregion
    }  

}
