﻿using Alvac.Domain.Contract.Converters;
using Alvac.Domain.Enum;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Contract.Request;

namespace Alvac.Domain.Contract.Response
{
    public class InsurerQuotationResponse
    {
        public Notification Notification { get; set; }
                  
        [JsonConverter(typeof(BaseResponseJsonConverter))]
        public BaseResponse Response { get; set; }
    }
}
