﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Response
{
    public class PaymentResponse
    {
        /// <summary>
        /// Código identificação do pagamento.
        /// </summary>
        public string Code { get; set; }

        /// <summary>
        /// Descrição da forma de pagamento
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Valor do primeiro pagamento.
        /// </summary>
        public decimal FirstInstallmentValue { get; set; }

        /// <summary>
        /// Valor dos demais pagamentos
        /// </summary>
        public decimal OtherInstallmentsValue { get; set; }

        /// <summary>
        /// Valor total pagamento a prazo.
        /// </summary>
        public decimal TotalInstallmentsValue { get; set; }
    }
}
