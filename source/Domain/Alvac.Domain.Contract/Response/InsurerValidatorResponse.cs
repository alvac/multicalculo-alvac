﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Response
{
    public class InsurerValidatorResponse
    {
        public Code Code { get; set; }

        public List<InsurerValidatorResponseDescription> Descriptions { get; set; }      
    }

    public class InsurerValidatorResponseDescription
    {
        public DescriptionCode DescriptionCode { get; set; }

        public string Description { get; set; }

        public string Message { get; set; }
    }
}
