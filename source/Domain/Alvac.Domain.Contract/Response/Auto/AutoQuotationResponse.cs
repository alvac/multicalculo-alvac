﻿using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Response.Auto
{
    public class AutoQuotationResponse : BaseResponse, ICommand<Domain.Entities.Auto.InsurerAutoQuotation>
    {
        #region Contructor

        public AutoQuotationResponse()
            : base()
        {
            this.RequestType = RequestType.AutoQuotation;
        }

        public AutoQuotationResponse(int identifier)
            : base()
        {
            this.Identifier = identifier;
            this.RequestType = RequestType.AutoQuotation;
        }

        #endregion

        #region Public properties

        /// <summary>
        /// Categoria do valor do premio.
        /// </summary>
        public ValueCategory PremiumValueCategory { get; set; }

        /// <summary>
        /// Tipo de Franquia
        /// </summary>
        public DeductibleType DeductibleType { get; set; }

        /// <summary>
        /// Valor da franquia.
        /// </summary>
        public decimal DeductibleValue { get; set; }

        /// <summary>
        /// Coberturas
        /// </summary>
        public CoverageResponse Coverage { get; set; }

        /// <summary>
        /// Guincho
        /// </summary>
        public bool HasWinch { get; set; }

        /// <summary>
        /// Km de cobertura
        /// </summary>
        public decimal WinchKm { get; set; }

        /// <summary>
        /// Possui carro cortezia/reserva
        /// </summary>
        public bool HasCourtesyCar { get; set; }

        /// <summary>
        /// Dias carro cortezia/reserva
        /// </summary>
        public int CourtesyCarDays { get; set; }

        #endregion

        #region ResponseBase Members

        public override void SetDefaultRequestType()
        {
            this.RequestType = RequestType.AutoQuotation;
        }

        #endregion
    }
}
