﻿using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Response.Auto
{
    /// <summary>
    /// Resposta de coberturas.
    /// </summary>
    public class CoverageResponse
    {
        /// <summary>
        /// Tipo de assistencia.
        /// </summary>
        public AssistanceType AssistanceType { get; set; }

        /// <summary>
        /// Tipo de coberturas
        /// </summary>
        public CoveragePlanType CoveragePlanType { get; set; }

        /// <summary>
        /// Coberturas de vidros
        /// </summary>
        public GlassesCoverages GlassesCoverages { get; set; }

        /// <summary>
        /// Valor cobertura danos materias.
        /// </summary>
        public decimal MaterialDamages { get; set; }

        /// <summary>
        /// Valor cobertura danos morais.
        /// </summary>
        public decimal MoralDamages { get; set; }

        /// <summary>
        /// Valor cobertura danos corporais.
        /// </summary>
        public decimal BodyDamages { get; set; }

        /// <summary>
        /// Acidentes pessoais por passageiro
        /// </summary>
        public decimal PersonalAccidentPassenger { get; set; }

        /// <summary>
        /// Despesas médicas hospitalares
        /// </summary>
        public decimal MedicalExpenses { get; set; }

        /// <summary>
        /// Despesas extras
        /// </summary>
        public decimal ExtraExpenses { get; set; }

    }
}
