﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Response
{
    public class PremiumInstallments
    {
        public int Numbers { get; set; }

        public decimal Value { get; set; }

        public override string ToString()
        {
            return AlvacJson<PremiumInstallments>.Serialize(this);
        }
    }
}
