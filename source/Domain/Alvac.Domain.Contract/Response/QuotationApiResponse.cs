﻿using Alvac.Domain.Contract.Converters;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.Serialization;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Response
{
    [Serializable]
    /// <summary>
    /// Representa uma resposta a um pedido feito à plataforma Alvac. 
    /// </summary>
    public class QuotationApiResponse
    {
        #region Constructor

        public QuotationApiResponse()
        {

        }

        #endregion

        #region Public properties

        /// <summary>
        /// Indica o resultado da operação realizada.
        /// </summary>
        public OperationResult Result { get; set; }

        /// <summary>
        /// Identificador da cotação
        /// </summary>
        public int QuotationId { get; set; }

        #endregion

        #region Public methods

        /// <summary>
        /// Converte a instância atual em uma string Json
        /// </summary>
        public override string ToString()
        {
            return AlvacJson<QuotationApiResponse>.Serialize(this);
        }

        #endregion

    }

}
