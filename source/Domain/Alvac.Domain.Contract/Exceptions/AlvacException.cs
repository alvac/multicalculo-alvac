﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Contract.Exceptions
{
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2237:MarkISerializableTypesWithSerializable"),
    System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Usage", "CA2240:ImplementISerializableCorrectly")]
    public class AlvacException : Exception
    {
        #region Public Properties

        public Code Code
        {
            get
            {
                return Code.Failure;
            }
        }

        public DescriptionCode DescriptionCode { get; set; }

        #endregion

        #region Contructor

        public AlvacException(string message)
            : base(message)
        {

        }

        public AlvacException(string message, DescriptionCode descriptionCode)
            : base(message)
        {
            this.DescriptionCode = descriptionCode;
        }

        public AlvacException(string message, DescriptionCode descriptionCode, Exception innerException)
            : base(message, innerException)
        {
            this.DescriptionCode = descriptionCode;
        }

        #endregion
    }
}
