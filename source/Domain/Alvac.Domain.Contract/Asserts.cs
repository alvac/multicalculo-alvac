﻿using Alvac.Domain.Contract.Exceptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alvac.Domain.Contract
{
    public static class Asserts
    {
        public static void AreEquals(object obj, object obj2)
        {
            if(!obj.Equals(obj2))
            {
                throw new AlvacException(string.Format("Objects are not equals. {0} != {1}", obj.ToString(), obj2.ToString()), Domain.Enum.DescriptionCode.Conflict);
            }
        }
    }
}