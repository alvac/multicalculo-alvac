﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class SkipMapIfNullResolver : IValueResolver
    {
        public ResolutionResult Resolve(ResolutionResult source)
        {
            if (source.Context.PropertyMap.SourceMember == null)
                source.ShouldIgnore = true;

            if (source.Value == null)
                source.ShouldIgnore = true;

            if (source.Type.IsValueType && source.Value == Activator.CreateInstance(source.Type))
                source.ShouldIgnore = true;

            return source;
        }
    }
}
