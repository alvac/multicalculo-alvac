﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class AccountInsurerConfigurationParameters
    {
        /// <summary>
        /// Identificador da configuração
        /// </summary>
        public int AccountInsurerConfigurationId { get; set; }

        public int AccountInsurerTemplateId { get; set; }

        public int InsurerConfigurationId { get; set; }

        public int Insurer { get; set; }

        /// <summary>
        /// Parâmetro obrigatório.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Nome de exibição do campo.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Tipo do parametro.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Chave
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Preenchido em caso de campo 'select'
        /// </summary>
        public string Options { get; set; }

        /// <summary>
        /// Valor
        /// </summary>
        public string Value { get; set; }       
    }

    public class AccountInsurerConfigurationParametersMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.AccountInsurerConfiguration, Alvac.Domain.DataTransferObject.AccountInsurerConfigurationParameters>()
                .ForMember(dest => dest.AccountInsurerConfigurationId, opt => opt.MapFrom(src => src.AccountInsurerConfigurationId))
                .ForMember(dest => dest.InsurerConfigurationId, opt => opt.MapFrom(src => src.InsurerConfigurationId))
                .ForMember(dest => dest.AccountInsurerTemplateId, opt => opt.MapFrom(src => src.AccountInsurerTemplateId))
                .ForMember(dest => dest.Insurer, opt => opt.MapFrom(src => src.InsurerConfiguration.Insurer))
                .ForMember(dest => dest.Required, opt => opt.MapFrom(src => src.InsurerConfiguration.Required))
                .ForMember(dest => dest.DisplayName, opt => opt.MapFrom(src => src.InsurerConfiguration.DisplayName))
                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.InsurerConfiguration.Type))
                .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.InsurerConfiguration.Key))
                .ForMember(dest => dest.Options, opt => opt.MapFrom(src => src.InsurerConfiguration.Options))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value));
        }
    }
}
