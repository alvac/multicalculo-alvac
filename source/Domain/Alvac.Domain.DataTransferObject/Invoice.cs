﻿using AutoMapper;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Invoice
    {
        /// <summary>
        /// Identificador da fatura
        /// </summary>
        public int InvoiceId { get; set; }

        /// <summary>
        /// Identificador da assinatura
        /// </summary>
        public int SubscriptionId { get; set; }

        /// <summary>
        /// Data de fechamento da fatura
        /// </summary>
        [DisplayName("Data de fechamento")]
        public DateTimeOffset Closing { get; set; }

        /// <summary>
        /// Data de criação da fatura
        /// </summary>
        [DisplayName("Data de criação")]
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Ultima atualização
        /// </summary>
        public DateTimeOffset LastUpdate { get; set; }

        /// <summary>
        /// Status da fatura
        /// </summary>
        [DisplayName("Status")]
        public InvoiceStatus Status { get; set; }

        /// <summary>
        /// Valor total da fatura
        /// </summary>
        [DisplayName("Valor total")]
        public decimal Value { get; set; }
    }

    public class InvoiceMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.Invoice, Invoice>();
        }
    }
}
