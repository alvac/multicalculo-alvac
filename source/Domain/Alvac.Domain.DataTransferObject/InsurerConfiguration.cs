﻿using AutoMapper;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class InsurerConfiguration
    {
        public int InsurerConfigurationId { get; set; }

        /// <summary>
        /// Seguradora
        /// </summary>
        [DisplayName("Seguradora")]
        public int InsurerId { get; set; }
        public string InsurerName { get; set; }

        /// <summary>
        /// Parâmetro obrigatório.
        /// </summary>
        [DisplayName("Obrigatório")]
        public bool Required { get; set; }

        /// <summary>
        /// Nome de exibição do campo.
        /// </summary>
        [DisplayName("Nome de exibição")]
        public string DisplayName { get; set; }

        /// <summary>
        /// Tipo do parametro.
        /// </summary>
        [DisplayName("Tipo do parametro")]
        public string Type { get; set; }

        /// <summary>
        /// Chave
        /// </summary>
        [DisplayName("Chave")]
        public string Key { get; set; }

        /// <summary>
        /// Preenchido em caso de campo 'select'
        /// </summary>
        [DisplayName("Opções")]
        public string Options { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        [DisplayName("Tipo de requisição")]
        public RequestType RequestType { get; set; }
    }

    public class InsurerConfigurationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.InsurerConfiguration, Alvac.Domain.DataTransferObject.InsurerConfiguration>();
        }
    }
}
