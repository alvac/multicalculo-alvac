﻿using AutoMapper;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Plan
    {
        /// <summary>
        /// Identificador do plano.
        /// </summary>
        public int PlanId { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        [DisplayName("Nome")]
        public string Name { get; set; }

        /// <summary>
        /// Recorrência do plano
        /// </summary>
        [DisplayName("Recorrência")]
        public int Recurrence { get; set; }

        /// <summary>
        /// Tipo de recorrência do plano.
        /// </summary>
        [DisplayName("Tipo de recorrência")]
        public PlanRecurrenceType RecurrenceType { get; set; }

        /// <summary>
        /// Quantidade cotação
        /// </summary>
        [DisplayName("Quantidade de cotação")]
        public int Amount { get; set; }

        /// <summary>
        /// Valor de cada cotação
        /// </summary>
        [DisplayName("Valor da cotação")]
        public decimal Value { get; set; }

        /// <summary>
        /// Valor adicional de cada cotação
        /// </summary>
        [DisplayName("Valor adicional da cotação")]
        public decimal AdditionalValue { get; set; }

        /// <summary>
        /// Dias de degustação
        /// </summary>
        public int? Trial { get; set; }

        /// <summary>
        /// Determina se o plano esta ativo
        /// </summary>
        [DisplayName("Ativo")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Ultima atualização
        /// </summary>
        [DisplayName("Ultima atualização")]
        public DateTimeOffset LastUpdate { get; set; }
    }

    public class ConnectionMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.Plan, Alvac.Domain.DataTransferObject.Plan>()
                .ForMember(dest => dest.PlanId, opt => opt.MapFrom(src => src.PlanId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.RecurrenceType, opt => opt.MapFrom(src => src.RecurrenceType))
                .ForMember(dest => dest.Recurrence, opt => opt.MapFrom(src => src.Recurrence))
                .ForMember(dest => dest.Amount, opt => opt.MapFrom(src => src.Amount))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value))
                .ForMember(dest => dest.AdditionalValue, opt => opt.MapFrom(src => src.AdditionalValue))
                .ForMember(dest => dest.Trial, opt => opt.MapFrom(src => src.Trial))
                .ForMember(dest => dest.IsActive, opt => opt.MapFrom(src => src.IsActive))
                .ForMember(dest => dest.LastUpdate, opt => opt.MapFrom(src => src.LastUpdate));

        }
    }
}
