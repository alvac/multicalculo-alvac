﻿using AutoMapper;
using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class SimpleAutoQuotation
    {
        public int QuotationId { get; set; }

        public DateTimeOffset RequestDateTime { get; set; }

        public string FullName { get; set; }

        public string UserName { get; set; }

        public string Document { get; set; }

        public List<InsurerAutoQuotation> InsurerQuotation { get; set; }
    }

    public class SimpleAutoQuotationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AutoQuotation, SimpleAutoQuotation>()
                .ForMember(dest => dest.QuotationId, opt => opt.MapFrom(src => src.QuotationId))
                .ForMember(dest => dest.FullName, opt => opt.MapFrom(src => src.Insured.FullName))
                .ForMember(dest => dest.Document, opt => opt.MapFrom(src => src.Insured.Document))
                .ForMember(dest => dest.RequestDateTime, opt => opt.MapFrom(src => src.RequestDateTime))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.User.UserName))
                .ForMember(dest => dest.InsurerQuotation, opt => 
                    opt.MapFrom(src => src.InsurerQuotations.AsQueryable().OfType<Domain.Entities.Auto.InsurerAutoQuotation>()));
        }
    }
}
