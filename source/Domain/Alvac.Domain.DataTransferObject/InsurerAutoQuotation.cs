﻿using AutoMapper;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class InsurerAutoQuotation : InsurerQuotation
    {
        public ValueCategory PremiumValueCategory { get; set; }

        public DeductibleType DeductibleType { get; set; }

        public decimal DeductibleValue { get; set; }

        public AssistanceType AssistanceType { get; set; }

        public CoveragePlanType CoveragePlanType { get; set; }

        public GlassesCoverages GlassesCoverages { get; set; }

        public decimal MaterialDamages { get; set; }

        public decimal MoralDamages { get; set; }

        public decimal BodyDamages { get; set; }

        public decimal PersonalAccidentPassenger { get; set; }

        public decimal MedicalExpenses { get; set; }

        public decimal ExtraExpenses { get; set; }

        public bool HasWinch { get; set; }

        public decimal WinchKm { get; set; }

        public bool HasCourtesyCar { get; set; }

        public int CourtesyCarDays { get; set; }
    }

    public class InsurerAutoQuotationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Entities.Auto.InsurerAutoQuotation, InsurerAutoQuotation>()
                .IncludeBase<Entities.InsurerQuotation, InsurerQuotation>();
        }
    }
}
