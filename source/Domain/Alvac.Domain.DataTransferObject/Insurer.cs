﻿using AutoMapper;
using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;

namespace Alvac.Domain.DataTransferObject
{
    public class Insurer
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }

    public class InsurerMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.Insurer, Insurer>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.InsurerId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name));

            Mapper.CreateMap<Alvac.Domain.Entities.AccountConnection, Insurer>()
                .ForMember(dest => dest.Id, opt => opt.MapFrom(src => src.InsurerId))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Insurer.Name));

        }
    }
}
