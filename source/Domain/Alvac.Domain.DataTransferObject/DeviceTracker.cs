﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class DeviceTracker
    {
        public int DeviceTrackerId { get; set; }

        public string Name { get; set; }
    }

    public class DeviceTrackerMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.DeviceTracker, DeviceTracker>();
        }
    }
}
