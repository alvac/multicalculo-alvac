﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Audit
    {
        /// <summary>
        /// Usuario que realizou a operação
        /// </summary>
        public string UserName { get; set; }

        /// <summary>
        /// Endereço IP
        /// </summary>
        public string IPAddress { get; set; }

        /// <summary>
        /// Area acessada
        /// </summary>
        public string AreaAccessed { get; set; }

        /// <summary>
        /// Objeto enviado
        /// </summary>
        public string Data { get; set; }

        /// <summary>
        /// Resultado da operação
        /// </summary>
        public int HttpStatusCode { get; set; }

        /// <summary>
        /// Data da operação
        /// </summary>
        public DateTimeOffset Timestamp { get; set; }
    }

    public class AuditMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.Audit, Alvac.Domain.DataTransferObject.Audit>();
        }
    }
}
