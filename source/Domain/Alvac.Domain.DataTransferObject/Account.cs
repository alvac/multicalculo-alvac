﻿using AutoMapper;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Account
    {
        public int AccountId { get; set; }

        /// <summary>
        /// Nome do representante legal.
        /// </summary>
        [DisplayName("Representante legal")]
        public string LegalRepresentative { get; set; }

        /// <summary>
        /// SUSEP
        /// </summary>
        [Required]
        [DisplayName("SUSEP")]
        public string Susep { get; set; }

        /// <summary>
        /// Tipo de pessoa
        /// </summary>
        [Required]
        [DisplayName("Tipo de pessoa")]
        public PersonType PersonType { get; set; }

        /// <summary>
        /// Documento RG/CPF
        /// </summary>
        [Required]
        [DisplayName("RG/CPF")]
        public string Document { get; set; }

        /// <summary>
        /// Nome e/ou razao social
        /// </summary>
        [DisplayName("Razão social")]
        public string Name { get; set; }

        /// <summary>
        /// CEP
        /// </summary>
        [DisplayName("CEP")]
        public string ZipCode { get; set; }

        /// <summary>
        /// Rua
        /// </summary>
        [DisplayName("Rua")]
        public string Street { get; set; }

        /// <summary>
        /// Bairro
        /// </summary>
        [DisplayName("Bairro")]
        public string Neighborhood { get; set; }

        /// <summary>
        /// Estado
        /// </summary>
        [DisplayName("Estado")]
        public string State { get; set; }

        /// <summary>
        /// Cidade
        /// </summary>
        [DisplayName("Cidade")]
        public string City { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Email de cobrança.
        /// </summary>
        [Required]
        [DisplayName("Email de cobrança")]
        public string ChargeEmail { get; set; }

        /// <summary>
        /// Telefone
        /// </summary>
        [DisplayName("Telefone")]
        public string Phone { get; set; }

        /// <summary>
        /// Celular
        /// </summary>
        [DisplayName("Celular")]
        public string MobilePhone { get; set; }

        /// <summary>
        /// Site da empresa / pessoa.
        /// </summary>
        public string Site { get; set; }
    }

    public class AccountMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.Account, Alvac.Domain.DataTransferObject.Account>()
                .ForMember(dest => dest.AccountId, opt => opt.MapFrom(src => src.AccountId))
                .ForMember(dest => dest.LegalRepresentative, opt => opt.MapFrom(src => src.LegalRepresentative))
                .ForMember(dest => dest.Susep, opt => opt.MapFrom(src => src.Susep))
                .ForMember(dest => dest.PersonType, opt => opt.MapFrom(src => src.PersonType))
                .ForMember(dest => dest.Document, opt => opt.MapFrom(src => src.Document))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.ZipCode, opt => opt.MapFrom(src => src.ZipCode))
                .ForMember(dest => dest.Street, opt => opt.MapFrom(src => src.Street))
                .ForMember(dest => dest.Neighborhood, opt => opt.MapFrom(src => src.Neighborhood))
                .ForMember(dest => dest.State, opt => opt.MapFrom(src => src.State))
                .ForMember(dest => dest.City, opt => opt.MapFrom(src => src.City))
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.ChargeEmail, opt => opt.MapFrom(src => src.ChargeEmail))
                .ForMember(dest => dest.Phone, opt => opt.MapFrom(src => src.Phone))
                .ForMember(dest => dest.MobilePhone, opt => opt.MapFrom(src => src.MobilePhone))
                .ForMember(dest => dest.Site, opt => opt.MapFrom(src => src.Site));

        }
    }
}
