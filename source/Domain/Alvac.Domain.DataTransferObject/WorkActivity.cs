﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class WorkActivity
    {
        /// <summary>
        /// Identificador atividade de trabalho
        /// </summary>
        public int WorkActivityId { get; set; }

        /// <summary>
        /// Nome da atividade de trabalho.
        /// </summary>
        public string Name { get; set; }
    }

    public class WorkActivityMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.WorkActivity, WorkActivity>();
        }
    }
}
