﻿using AutoMapper;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class InsurerQuotation
    {
        public int InsurerQuotationId { get; set; }

        public int InsurerId { get; set; }

        public string InsurerName { get; set; }

        public Status Status { get; set; }

        public string OperationDescription { get; set; }

        public decimal PremiumValue { get; set; }

        public string InsurerBudgetCode { get; set; }

        public DateTimeOffset? ValidUntil { get; set; }
    }

    public class InsurerQuotationBaseMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.InsurerQuotation, InsurerQuotation>();
        }
    }
}
