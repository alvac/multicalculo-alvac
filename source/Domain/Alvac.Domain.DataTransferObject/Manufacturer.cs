﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Manufacturer
    {
        public int ManufacturerId { get; set; }

        public string Name { get; set; }

        public int? InsurerId { get; set; }
        public string InsurerName { get; set; }

        public string CodeManufacturerInsurer { get; set; }
    }

    public class ManufacturerMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.Manufacturer, Manufacturer>();
        }
    }
}
