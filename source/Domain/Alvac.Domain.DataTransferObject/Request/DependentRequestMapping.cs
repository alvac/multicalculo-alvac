﻿using AutoMapper;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Request
{
    public class DependentRequestMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<DependentRequest, MainDriverDependent>()
                .ForMember(dest => dest.MainDriverDependentId, opt => opt.Ignore())
                .ForMember(dest => dest.MainDriverId, opt => opt.Ignore())
                .ForMember(dest => dest.MainDriver, opt => opt.Ignore());
        }
    }
}
