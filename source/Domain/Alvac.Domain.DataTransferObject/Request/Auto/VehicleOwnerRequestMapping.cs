﻿using AutoMapper;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Request.Auto
{
    public class VehicleOwnerRequestMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<VehicleOwnerRequest, VehicleOwner>()
                .ForMember(dest => dest.QuotationId, opt => opt.Ignore())
                .ForMember(dest => dest.AutoQuotation, opt => opt.Ignore());

            Mapper.CreateMap<VehicleOwnerRequest, VehicleOwnerRequest>();
        }
    }
}
