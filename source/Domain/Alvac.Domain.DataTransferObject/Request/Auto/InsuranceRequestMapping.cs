﻿using AutoMapper;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Request.Auto
{
    public class InsuranceRequestMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<InsuranceRequest, Insurance>()
                .ForMember(dest => dest.QuotationId, opt => opt.Ignore())
                .ForMember(dest => dest.AutoQuotation, opt => opt.Ignore())
                .ForMember(dest => dest.CurrentInsuranceCompany, opt => opt.MapFrom(src => src.Renewal.CurrentInsuranceCompany))
                .ForMember(dest => dest.CurrentCoveragePlanType, opt => opt.MapFrom(src => src.Renewal.CurrentCoveragePlanType))
                .ForMember(dest => dest.CurrentInsuranceBonus, opt => opt.MapFrom(src => src.Renewal.CurrentInsuranceBonus))
                .ForMember(dest => dest.ClaimQuantity, opt => opt.MapFrom(src => src.Renewal.ClaimQuantity))
                .ForMember(dest => dest.CurrentInsurancePolicyDate, opt => opt.MapFrom(src => src.Renewal.CurrentInsurancePolicyDate))
                .ForMember(dest => dest.CurrentInsuranceNumber, opt => opt.MapFrom(src => src.Renewal.CurrentInsuranceNumber))
                .ForMember(dest => dest.CurrentInsurancePolicyExpirationDate, opt => opt.MapFrom(src => src.Renewal.CurrentInsurancePolicyExpirationDate))
                .ForMember(dest => dest.IsPolicyOwner, opt => opt.MapFrom(src => src.Renewal.IsPolicyOwner));

            Mapper.CreateMap<InsuranceRequest, InsuranceRequest>();

        }
    }
}
