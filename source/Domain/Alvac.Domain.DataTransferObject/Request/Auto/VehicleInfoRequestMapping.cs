﻿using AutoMapper;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Request.Auto
{
    public class VehicleInfoRequestMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<VehicleInfoRequest, VehicleInfo>()
                .ForMember(dest => dest.QuotationId, opt => opt.Ignore())
                .ForMember(dest => dest.AutoQuotation, opt => opt.Ignore())
                .ForMember(dest => dest.PlateNumber, opt => opt.MapFrom(src => src.License.PlateNumber))
                .ForMember(dest => dest.ChassisNumber, opt => opt.MapFrom(src => src.License.ChassisNumber))
                .ForMember(dest => dest.ValueCategory, opt => opt.MapFrom(src => src.Value.ValueCategory))
                .ForMember(dest => dest.DeterminedValue, opt => opt.MapFrom(src => src.Value.DeterminedValue))
                .ForMember(dest => dest.ReferencedValue, opt => opt.MapFrom(src => src.Value.ReferencedValue))
                .ForMember(dest => dest.ReferencedPercentage, opt => opt.MapFrom(src => src.Value.ReferencedPercentage))
                .ForMember(dest => dest.IsFinanced, opt => opt.MapFrom(src => src.Value.IsFinanced))
                .ForMember(dest => dest.DevicesTracker, opt => opt.Ignore())
                .ForMember(dest => dest.DevicesIgnitionBlocker, opt => opt.Ignore());

            Mapper.CreateMap<VehicleInfoRequest, VehicleInfoRequest>();
        }
    }
}
