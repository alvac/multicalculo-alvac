﻿using AutoMapper;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Request.Auto
{
    public class AutoQuotationRequestMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AutoQuotationRequest, AutoQuotation>()
                .ForMember(dest => dest.Insured, opt => opt.MapFrom(src => src.Insured))
                .ForMember(dest => dest.AdditionalDrivers, opt => opt.MapFrom(src => src.AdditionalDriverList))
                .IncludeBase<BaseRequest, Quotation>();

            Mapper.CreateMap<AutoQuotationRequest, AutoQuotationRequest>();
        }
    }
}
