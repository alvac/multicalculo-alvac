﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Request
{
    public class NotificationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.Notification, Alvac.Domain.Contract.Request.Notification>()
                .ForMember(dest => dest.CorrelationId, opt => opt.MapFrom(src => src.CorrelationId))
                .ForMember(dest => dest.CallType, opt => opt.MapFrom(src => src.CallType))
                .ForMember(dest => dest.Path, opt => opt.MapFrom(src => src.Path));
        }
    }
}
