﻿using AutoMapper;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Request
{
    public class InsuredRequestMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<InsuredRequest, Insured>()
                .ForMember(dest => dest.QuotationId, opt => opt.Ignore())
                .ForMember(dest => dest.Quotation, opt => opt.Ignore())
                .ForMember(dest => dest.WorkActivity, opt => opt.Ignore());

        }
    }
}
