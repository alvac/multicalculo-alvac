﻿using AutoMapper;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Request
{
    public class BaseRequestMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<BaseRequest, Quotation>()
                .ForMember(dest => dest.QuotationId, opt => opt.Ignore())
                .ForMember(dest => dest.Account, opt => opt.Ignore())
                .ForMember(dest => dest.User, opt => opt.Ignore())
                .ForMember(dest => dest.Processed, opt => opt.Ignore())
                .ForMember(dest => dest.OperationDescriptionCode, opt => opt.Ignore())
                .ForMember(dest => dest.OperationDescription, opt => opt.Ignore())
                .ForMember(dest => dest.LastUpdate, opt => opt.Ignore())
                .ForMember(dest => dest.NotificationCorrelationId, opt => opt.MapFrom(src => src.Notification.CorrelationId))
                .ForMember(dest => dest.NotificationCallType, opt => opt.MapFrom(src => src.Notification.CallType))
                .ForMember(dest => dest.NotificationPath, opt => opt.MapFrom(src => src.Notification.Path))
                .ForMember(dest => dest.Insured, opt => opt.Ignore())
                .ForMember(dest => dest.InsurerQuotations, opt => opt.Ignore());
        }
    }
}
