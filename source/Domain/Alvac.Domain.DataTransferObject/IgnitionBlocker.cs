﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class IgnitionBlocker
    {
        public int IgnitionBlockerId { get; set; }

        public string Name { get; set; }
    }

    public class IgnitionBlockerMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.IgnitionBlocker, IgnitionBlocker>();
        }
    }
}
