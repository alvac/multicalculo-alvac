﻿using AutoMapper;
using Alvac.Domain.DataTransferObject.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public static class AutoMapperConfiguration
    {
        public static void Configure()
        {
            AutoMapper.Mapper.Initialize(x => GetConfiguration(AutoMapper.Mapper.Configuration));
        }

        private static void GetConfiguration(IConfiguration configuration)
        {
            var profiles = typeof(BaseRequestMapping).Assembly.GetTypes().Where(x => typeof(Profile).IsAssignableFrom(x));

            var baseProfiles = profiles.Where(x => x.Name.Contains("Base"));

            foreach (var baseProfile in baseProfiles)
            {
                configuration.AddProfile(Activator.CreateInstance(baseProfile) as Profile);
            }

            foreach (var profile in profiles.Where(x => !x.Name.Contains("Base")))
            {
                configuration.AddProfile(Activator.CreateInstance(profile) as Profile);
            }
        }
    }
}
