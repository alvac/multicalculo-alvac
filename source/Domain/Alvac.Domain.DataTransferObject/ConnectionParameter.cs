﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class ConnectionParameter
    {
        /// <summary>
        /// Identificador do parametro
        /// </summary>
        public int ConnectionParameterId { get; set; }

        /// <summary>
        /// Identificador da conexão
        /// </summary>
        public int ConnectionId { get; set; }

        /// <summary>
        /// Chave
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Valor
        /// </summary>
        public string Value { get; set; }
    }

    public class ConnectionParameterMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.ConnectionParameter, Alvac.Domain.DataTransferObject.ConnectionParameter>()
                .ForMember(dest => dest.ConnectionParameterId, opt => opt.MapFrom(src => src.ConnectionParameterId))
                .ForMember(dest => dest.ConnectionId, opt => opt.MapFrom(src => src.ConnectionId))
                .ForMember(dest => dest.Key, opt => opt.MapFrom(src => src.Key))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value));

        }
    }
}
