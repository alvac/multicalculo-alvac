﻿using AutoMapper;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Role
    {
        public string Id { get; set; }
        public string Name { get; set; }
    }

    public class RoleMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<IdentityRole, Role>();
        }
    }
}
