﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class VehiclePrice
    {
        public int VehiclePriceId { get; set; }
        public int VehicleId { get; set; }
        public int Year { get; set; }
        public decimal Price { get; set; }
    }

    public class VehiclePriceMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.VehiclePrice, VehiclePrice>();
        }
    }
}
