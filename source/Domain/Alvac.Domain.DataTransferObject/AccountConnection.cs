﻿using AutoMapper;
using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;
using System.ComponentModel.DataAnnotations;


namespace Alvac.Domain.DataTransferObject
{
    public class AccountConnection
    {
        public int AccountId { get; set; }
        public int ConnectionId { get; set; }

        [Display(Name = "Conexão")]
        public string ConnectionName { get; set; }

        [Display(Name = "Seguradora")]
        public int InsurerId { get; set; }
        public string InsurerName { get; set; }

        [Display(Name = "Tecnologia")]
        public Technology Technology { get; set; }

        [Display(Name = "Tipo de requisição")]
        public RequestType RequestType { get; set; }

        
    }

    public class AccountConnectionMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.AccountConnection, AccountConnection>()
                .ForMember(dest => dest.InsurerId, opt => opt.MapFrom(src => src.InsurerId))
                .ForMember(dest => dest.InsurerName, opt => opt.MapFrom(src => src.Insurer.Name))
                .ForMember(dest => dest.Technology, opt => opt.MapFrom(src => src.Connection.Dispatcher.Technology))
                .ForMember(dest => dest.RequestType, opt => opt.MapFrom(src => src.Connection.Dispatcher.RequestType))
                .ForMember(dest => dest.ConnectionName, opt => opt.MapFrom(src => src.Connection.Name));

        }
    }
}
