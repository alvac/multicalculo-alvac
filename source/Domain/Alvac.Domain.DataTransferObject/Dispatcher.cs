﻿using AutoMapper;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Dispatcher
    {
        /// <summary>
        /// Identificador do despachante
        /// </summary>
        public int DispatcherId { get; set; }

        /// <summary>
        /// Seguradora
        /// </summary>
        [DisplayName("Seguradora")]
        public int InsurerId { get; set; }
        public string InsurerName { get; set; }

        /// <summary>
        /// Tecnologia
        /// </summary>
        [DisplayName("Tecnologia")]
        public Technology Technology { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        [DisplayName("Tipo de requisição")]
        public RequestType RequestType { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        [DisplayName("Nome")]
        public string Name { get; set; }

        /// <summary>
        /// Descrição
        /// </summary>
        [DisplayName("Descrição")]
        public string Description { get; set; }

        /// <summary>
        /// Despachante ativo
        /// </summary>
        [DisplayName("Ativo")]
        public bool IsActive { get; set; }
    }

    public class DispatcherMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.Dispatcher, Alvac.Domain.DataTransferObject.Dispatcher>();

        }
    }
}
