﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    /// <summary>
    /// Representa uma transação em uma fatura
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Identificador da cotação na seguradora
        /// </summary>
        public int InsurerQuotationId { get; set; }

        /// <summary>
        /// Identificador da fatura
        /// </summary>
        public int InvoiceId { get; set; }

        /// <summary>
        /// Valor da transação
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Data de criação da transação
        /// </summary>
        public DateTimeOffset Created { get; set; }
    }

    public class TransactionMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.Transaction, Transaction>();
        }
    }
}
