﻿using AutoMapper;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Contract.Response.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Response.Auto
{
    public class AutoQuotationResponseMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<AutoQuotationResponse, Domain.Entities.Auto.InsurerAutoQuotation>()
                
                .ForMember(dest => dest.PremiumValueCategory, opt => opt.MapFrom(src => src.PremiumValueCategory))
                .ForMember(dest => dest.DeductibleType, opt => opt.MapFrom(src => src.DeductibleType))
                .ForMember(dest => dest.DeductibleValue, opt => opt.MapFrom(src => src.DeductibleValue))
                .ForMember(dest => dest.AssistanceType, opt => opt.MapFrom(src => src.Coverage.AssistanceType))
                .ForMember(dest => dest.CoveragePlanType, opt => opt.MapFrom(src => src.Coverage.CoveragePlanType))
                .ForMember(dest => dest.GlassesCoverages, opt => opt.MapFrom(src => src.Coverage.GlassesCoverages))
                .ForMember(dest => dest.MaterialDamages, opt => opt.MapFrom(src => src.Coverage.MaterialDamages))
                .ForMember(dest => dest.MoralDamages, opt => opt.MapFrom(src => src.Coverage.MoralDamages))
                .ForMember(dest => dest.BodyDamages, opt => opt.MapFrom(src => src.Coverage.BodyDamages))
                .ForMember(dest => dest.PersonalAccidentPassenger, opt => opt.MapFrom(src => src.Coverage.PersonalAccidentPassenger))
                .ForMember(dest => dest.MedicalExpenses, opt => opt.MapFrom(src => src.Coverage.MedicalExpenses))
                .ForMember(dest => dest.ExtraExpenses, opt => opt.MapFrom(src => src.Coverage.ExtraExpenses))                
                .ForMember(dest => dest.HasWinch, opt => opt.MapFrom(src => src.HasWinch))
                .ForMember(dest => dest.WinchKm, opt => opt.MapFrom(src => src.WinchKm))
                .ForMember(dest => dest.HasCourtesyCar, opt => opt.MapFrom(src => src.HasCourtesyCar))
                .ForMember(dest => dest.CourtesyCarDays, opt => opt.MapFrom(src => src.CourtesyCarDays))

                .ForMember(dest => dest.InsurerAutoQuotationParameter, opt => opt.Ignore())              
                
                .IncludeBase<BaseResponse, Domain.Entities.InsurerQuotation>();

        }
    }
}
