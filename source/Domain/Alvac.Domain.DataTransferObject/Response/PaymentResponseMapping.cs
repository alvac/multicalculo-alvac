﻿//using AutoMapper;
//using Alvac.Domain.Contract.Response;
//using Alvac.Domain.Entities;
//using System;
//using System.Collections.Generic;
//using System.Linq;
//using System.Text;
//using System.Threading.Tasks;

//namespace Alvac.Domain.DataTransferObject.Response
//{
//    public class PaymentResponseMapping : Profile
//    {
//        protected override void Configure()
//        {
//            Mapper.CreateMap<Payment, PaymentResponse>()
//                .ForMember(dest => dest.CashValue, opt => opt.MapFrom(src => src.Value))
//                .ForMember(dest => dest.InstallmentsValue, opt => opt.MapFrom(src => src.InstallmentsTotalValue))
//                .ForMember(dest => dest.PremiumInstallments, opt => opt.MapFrom(src => new PremiumInstallments
//                {
//                    Numbers = src.InstallmentsNumber,
//                    Value = src.InstallmentsValue
//                }))
//                .ForMember(dest => dest.Type, opt => opt.MapFrom(src => src.Type));
//        }
//    }
//}
