﻿using AutoMapper;
using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Entities;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Response
{
    public class BaseResponseMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.InsurerQuotation, Domain.Entities.InsurerQuotation>()
                .ForMember(dest => dest.Connection, opt => opt.Ignore())
                .ForMember(dest => dest.Quotation, opt => opt.Ignore())
                .ForMember(dest => dest.Transaction, opt => opt.Ignore())
                .ForMember(dest => dest.Notification, opt => opt.Ignore())
                .ForMember(dest => dest.Payments, opt => opt.Ignore())
                .ForAllMembers(o => o.Condition(c => c.PropertyMap.SourceMember != null && !c.IsSourceValueNull && c.SourceType.IsValueType && c.SourceValue != Activator.CreateInstance(c.SourceType)));

            Mapper.CreateMap<Domain.Entities.Auto.InsurerAutoQuotation, Domain.Entities.Auto.InsurerAutoQuotation>()
                .ForMember(dest => dest.InsurerAutoQuotationParameter, opt => opt.Ignore())
                .IncludeBase<Domain.Entities.InsurerQuotation, Domain.Entities.InsurerQuotation>()
                .ForAllMembers(o => o.Condition(c => c.PropertyMap.SourceMember != null && !c.IsSourceValueNull && c.SourceType.IsValueType && c.SourceValue != Activator.CreateInstance(c.SourceType)));

            Mapper.CreateMap<BaseResponse, Domain.Entities.InsurerQuotation>()
                .ForMember(dest => dest.InsurerQuotationId, opt => opt.MapFrom(src => src.Identifier))
                .ForMember(dest => dest.QuotationId, opt => opt.Ignore())
                .ForMember(dest => dest.Quotation, opt => opt.Ignore())
                .ForMember(dest => dest.ConnectionId, opt => opt.Ignore())
                .ForMember(dest => dest.Connection, opt => opt.Ignore())
                .ForMember(dest => dest.Insurer, opt => opt.Ignore())

                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Result.Code == Code.Success ? Status.Success : Status.Failure))
                .ForMember(dest => dest.OperationDescriptionCode, opt => opt.MapFrom(src => src.Result.DescriptionCode))
                .ForMember(dest => dest.OperationDescription, opt => opt.MapFrom(src => src.Result.Description))
                .ForMember(dest => dest.PremiumValue, opt => opt.MapFrom(src => src.PremiumValue))
                .ForMember(dest => dest.ValidUntil, opt => opt.MapFrom(src => src.ValidUntil))
                .ForMember(dest => dest.GatewayId, opt => opt.MapFrom(src => src.GatewayId))
                .ForMember(dest => dest.InsurerBudgetCode, opt => opt.MapFrom(src => src.InsurerBudgetCode))
                .ForMember(dest => dest.BudgetFile, opt => opt.MapFrom(src => src.BudgetFile))
                .ForMember(dest => dest.LastUpdate, opt => opt.MapFrom(src => DateTimeOffset.Now))

                .ForMember(dest => dest.Transaction, opt => opt.Ignore())
                .ForMember(dest => dest.Notification, opt => opt.Ignore())
                .ForMember(dest => dest.Payments, opt => opt.Ignore());
        }
    }
}

