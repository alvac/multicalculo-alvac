﻿using AutoMapper;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Vehicle
    {
        public int VehicleId { get; set; }

        /// <summary>
        /// Modelo do veículo.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Ano do modelo.
        /// </summary>
        public int ModelYear { get; set; }

        /// <summary>
        /// Código fipe da versão do veículo.
        /// </summary>
        public string FipeCode { get; set; }

        /// <summary>
        /// Tipo de combutível do veículo.
        /// </summary>
        public FuelType Fuel { get; set; }

        /// <summary>
        /// Preços
        /// </summary>
        public IEnumerable<VehiclePrice> VehiclePrices { get; set; }
    }

    public class VehicleMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.Vehicle, Vehicle>();
        }
    }
}
