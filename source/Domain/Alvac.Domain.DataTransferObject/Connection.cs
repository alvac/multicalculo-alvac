﻿using AutoMapper;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Connection
    {
        /// <summary>
        /// Identificador da conexão
        /// </summary>
        public int ConnectionId { get; set; }

        /// <summary>
        /// Identificador do despachante
        /// </summary>
        public int DispatcherId { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Vazão maxima
        /// </summary>
        public int MaxThroughput { get; set; }

        /// <summary>
        /// Tempo em milisegundos para vazão maxima
        /// </summary>
        public int LengthTimeUnitThroughput { get; set; }

        /// <summary>
        /// Tamanho da janela
        /// </summary>
        public int WindowSize { get; set; }

        /// <summary>
        /// Quantidade
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Nome da fila de entrada
        /// </summary>
        public string InputQueueName { get; set; }

        /// <summary>
        /// Nome da fila de saída
        /// </summary>
        public string OutputQueueName { get; set; }

        /// <summary>
        /// Nome da fila de notificação
        /// </summary>
        public string NotificationQueueName { get; set; }

        /// <summary>
        /// Máximo de retentativa desta conexão
        /// </summary>
        public int? RetryMax { get; set; }

        /// <summary>
        /// Intervalo de retentativa em segundos.
        /// </summary>
        public int? RetryInterval { get; set; }

        /// <summary>
        /// Determina se a conexão esta ativa
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Determina se a conexão é a padrão para criação automatica 
        /// quando uma nova conta for cadastrada
        /// </summary>
        public bool IsDefault { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        public RequestType RequestType { get; set; }

        /// <summary>
        /// Seguradora
        /// </summary>
        public int InsurerId { get; set; }
        public string InsurerName { get; set; }
    }

    public class PlanMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.Connection, Alvac.Domain.DataTransferObject.Connection>()
                .ForMember(dest => dest.RequestType, opt => opt.MapFrom(src => src.Dispatcher.RequestType))
                .ForMember(dest => dest.InsurerId, opt => opt.MapFrom(src => src.Dispatcher.InsurerId))
                .ForMember(dest => dest.InsurerName, opt => opt.MapFrom(src => src.Dispatcher.Insurer.Name));

        }
    }
}
