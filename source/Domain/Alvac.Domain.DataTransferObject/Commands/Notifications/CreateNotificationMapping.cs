﻿using AutoMapper;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands.Notifications;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Notifications
{
    public class CreateNotificationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateNotification, Notification>()
                .ForMember(dest => dest.InsurerQuotationId, opt => opt.MapFrom(src => src.InsurerQuotationId))
                .ForMember(dest => dest.Status, opt => opt.MapFrom(src => src.Status))
                .ForMember(dest => dest.OperationDescriptionCode, opt => opt.MapFrom(src => src.OperationDescriptionCode))
                .ForMember(dest => dest.OperationDescription, opt => opt.MapFrom(src => src.OperationDescription))

                .ForMember(dest => dest.CorrelationId, opt => opt.MapFrom(src => src.CorrelationId))
                .ForMember(dest => dest.CallType, opt => opt.MapFrom(src => src.CallType))
                .ForMember(dest => dest.Path, opt => opt.MapFrom(src => src.Path))

                .ForMember(dest => dest.Created, opt => opt.Ignore())
                .ForMember(dest => dest.LastUpdate, opt => opt.Ignore())
                .ForMember(dest => dest.InsurerQuotation, opt => opt.Ignore());
        }
    }
}
