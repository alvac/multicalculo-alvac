﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.AccountConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.AccountConfigurations
{
    public class CreateEmailConfigurationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateEmailConfiguration, Domain.Entities.AccountConfiguration>()
                .ForMember(dest => dest.Account, opt => opt.Ignore())
                .ForMember(dest => dest.ReportComments, opt => opt.Ignore())
                .ForMember(dest => dest.ReportFontSize, opt => opt.Ignore())
                .ForMember(dest => dest.ReportFooter, opt => opt.Ignore())
                .ForMember(dest => dest.ReportLogo, opt => opt.Ignore())
                .ForMember(dest => dest.ReportSubtitle, opt => opt.Ignore())
                .ForMember(dest => dest.ReportTitle, opt => opt.Ignore())
                .ForMember(dest => dest.ReportUserMessage, opt => opt.Ignore());
        }
    }
}
