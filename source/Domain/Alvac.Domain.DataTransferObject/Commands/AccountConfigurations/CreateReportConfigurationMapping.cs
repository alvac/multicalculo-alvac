﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.AccountConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.AccountConfigurations
{
    public class CreateReportConfigurationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateReportConfiguration, Domain.Entities.AccountConfiguration>()
                 .ForMember(dest => dest.Account, opt => opt.Ignore())
                 .ForMember(dest => dest.EmailBody, opt => opt.Ignore())
                 .ForMember(dest => dest.EmailReceipt, opt => opt.Ignore())
                 .ForMember(dest => dest.EmailSendToInsured, opt => opt.Ignore());
        }
    }
}
