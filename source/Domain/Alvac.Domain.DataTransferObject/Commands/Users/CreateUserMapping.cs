﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Users
{
    public class CreateUserMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateUser, Domain.Entities.User>()
                .ForAllMembers(x => x.Ignore());

            Mapper.CreateMap<CreateUser, Domain.Entities.User>()
                .ForMember(dest => dest.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.Name, opt => opt.MapFrom(src => src.Name))
                .ForMember(dest => dest.UserName, opt => opt.MapFrom(src => src.Email))
                .ForMember(dest => dest.AccountId, opt => opt.MapFrom(src => src.AccountId))
                .ForMember(dest => dest.IsActive, opt => opt.UseValue(true));
        }
    }
}
