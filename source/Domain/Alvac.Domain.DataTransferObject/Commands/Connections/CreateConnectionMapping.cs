﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Connections
{
    public class CreateConnectionMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateConnection, Domain.Entities.Connection>()
                .ForMember(dest => dest.ConnectionId, opt => opt.Ignore())
                .ForMember(dest => dest.AccountConnections, opt => opt.Ignore())
                .ForMember(dest => dest.Dispatcher, opt => opt.Ignore())
                .ForMember(dest => dest.ConnectionParameters, opt => opt.Ignore());
        }
    }
}
