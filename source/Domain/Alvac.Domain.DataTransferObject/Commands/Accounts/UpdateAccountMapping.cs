﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Accounts
{
    public class UpdateAccountMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.Commands.Accounts.UpdateAccount, Alvac.Domain.Entities.Account>()
                .ForMember(dest => dest.AccountConfiguration, opt => opt.Ignore())
                .ForMember(dest => dest.Quotations, opt => opt.Ignore())
                .ForMember(dest => dest.Users, opt => opt.Ignore())
                .ForMember(dest => dest.Subscriptions, opt => opt.Ignore())
                .ForMember(dest => dest.AccountInsurerTemplates, opt => opt.Ignore());

        }
    }
}
