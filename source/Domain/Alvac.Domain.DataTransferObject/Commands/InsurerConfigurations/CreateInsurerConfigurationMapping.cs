﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.InsurerConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.InsurerConfigurations
{
    public class CreateInsurerConfigurationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateInsurerConfiguration, Domain.Entities.InsurerConfiguration>()
                .ForMember(dest => dest.InsurerConfigurationId, opt => opt.Ignore())
                .ForMember(dest => dest.AccountInsurerConfigurations, opt => opt.Ignore());
        }
    }
}
