﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.InsurerConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.InsurerConfigurations
{
    public class UpdateInsurerConfigurationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UpdateInsurerConfiguration, Domain.Entities.InsurerConfiguration>()
                .ForMember(dest => dest.AccountInsurerConfigurations, opt => opt.Ignore())
                .ForMember(dest => dest.RequestType, opt => opt.Ignore());
        }
    }
}
