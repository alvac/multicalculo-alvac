﻿using AutoMapper;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands.Transactions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Transactions
{
    public class CreateTransactionMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateTransaction, Domain.Entities.Transaction>()
                .ForMember(dest => dest.InvoiceId, opt => opt.MapFrom(src => src.InvoiceId))
                .ForMember(dest => dest.Invoice, opt => opt.Ignore())
                .ForMember(dest => dest.InsurerQuotationId, opt => opt.MapFrom(src => src.InsurerQuotationId))
                .ForMember(dest => dest.InsurerQuotation, opt => opt.Ignore())
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value))
                .ForMember(dest => dest.Created, opt => opt.Ignore());
        }
    }
}
