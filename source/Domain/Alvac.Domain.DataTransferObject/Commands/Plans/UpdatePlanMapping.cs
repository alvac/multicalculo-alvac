﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.Plans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Plans
{
    public class UpdatePlanMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UpdatePlan, Domain.Entities.Plan>()
                .ForMember(dest => dest.LastUpdate, opt => opt.Ignore())
                .ForMember(dest => dest.Subscriptions, opt => opt.Ignore());
        }
    }
}
