﻿using AutoMapper;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Commands.Auto.InsurerAutoQuotationParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Auto.InsurerAutoQuotationParameters
{
    public class InsurerAutoQuotationParameterMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateInsurerAutoQuotationParameter, InsurerAutoQuotationParameter>()
                .ForMember(dest => dest.InsurerAutoQuotation, opt => opt.Ignore());
        }
    }
}
