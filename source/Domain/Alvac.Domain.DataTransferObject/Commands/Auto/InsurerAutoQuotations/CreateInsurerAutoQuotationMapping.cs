﻿using AutoMapper;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Commands.Auto.InsurerAutoQuotations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Auto.InsurerAutoQuotations
{
    public class CreateInsurerAutoQuotationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateInsurerAutoQuotation, Entities.Auto.InsurerAutoQuotation>()
                .ForMember(dest => dest.InsurerQuotationId, opt => opt.Ignore())
                .ForMember(dest => dest.Quotation, opt => opt.Ignore())
                .ForMember(dest => dest.Connection, opt => opt.Ignore())
                .ForMember(dest => dest.PremiumValue, opt => opt.Ignore())
                .ForMember(dest => dest.Notification, opt => opt.Ignore())
                .ForMember(dest => dest.ValidUntil, opt => opt.Ignore())
                .ForMember(dest => dest.GatewayId, opt => opt.Ignore())
                .ForMember(dest => dest.InsurerBudgetCode, opt => opt.Ignore())
                .ForMember(dest => dest.BudgetFile, opt => opt.Ignore())
                .ForMember(dest => dest.Transaction, opt => opt.Ignore())
                .ForMember(dest => dest.Payments, opt => opt.Ignore())
                .ForMember(dest => dest.PremiumValueCategory, opt => opt.Ignore())
                .ForMember(dest => dest.DeductibleValue, opt => opt.Ignore())
                .ForMember(dest => dest.AssistanceType, opt => opt.Ignore())
                .ForMember(dest => dest.CoveragePlanType, opt => opt.Ignore())
                .ForMember(dest => dest.GlassesCoverages, opt => opt.Ignore())
                .ForMember(dest => dest.MaterialDamages, opt => opt.Ignore())

                .ForMember(dest => dest.MoralDamages, opt => opt.Ignore())
                .ForMember(dest => dest.BodyDamages, opt => opt.Ignore())
                .ForMember(dest => dest.PersonalAccidentPassenger, opt => opt.Ignore())
                .ForMember(dest => dest.MedicalExpenses, opt => opt.Ignore())
                .ForMember(dest => dest.ExtraExpenses, opt => opt.Ignore())
                .ForMember(dest => dest.HasWinch, opt => opt.Ignore())
                .ForMember(dest => dest.WinchKm, opt => opt.Ignore())
                .ForMember(dest => dest.HasCourtesyCar, opt => opt.Ignore())
                .ForMember(dest => dest.CourtesyCarDays, opt => opt.Ignore())
                .ForMember(dest => dest.InsurerAutoQuotationParameter, opt => opt.Ignore())
                .ForMember(dest => dest.LastUpdate, opt => opt.Ignore());
        }
    }
}
