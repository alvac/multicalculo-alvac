﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.Subscriptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Subscriptions
{
    public class UpdateSubscriptionMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UpdateSubscription, Domain.Entities.Subscription>()
                .ForMember(dest => dest.Account, opt => opt.Ignore())
                .ForMember(dest => dest.AccountId, opt => opt.Ignore())
                .ForMember(dest => dest.Created, opt => opt.Ignore())
                .ForMember(dest => dest.Invoices, opt => opt.Ignore())
                .ForMember(dest => dest.LastUpdate, opt => opt.Ignore())
                .ForMember(dest => dest.Plan, opt => opt.Ignore());
        }
    }
}
