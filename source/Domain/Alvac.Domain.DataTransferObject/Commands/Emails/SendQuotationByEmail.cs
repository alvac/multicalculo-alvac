﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Emails
{
    public class SendQuotationByEmail
    {
        public string Email { get; set; }
        public int[] InsurerQuotations { get; set; }
    }
}
