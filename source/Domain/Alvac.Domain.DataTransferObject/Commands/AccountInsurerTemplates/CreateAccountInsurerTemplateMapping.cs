﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.AccountInsurerTemplates;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.AccountInsurerTemplates
{
    public class CreateAccountInsurerTemplateMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateAccountInsurerTemplate, Domain.Entities.AccountInsurerTemplate>()
                .ForMember(dest => dest.IsActive, opt => opt.UseValue(true))
                .ForMember(dest => dest.AccountInsurerTemplateId, opt => opt.Ignore())
                .ForMember(dest => dest.Created, opt => opt.Ignore())
                .ForMember(dest => dest.Account, opt => opt.Ignore())
                .ForMember(dest => dest.AccountInsurerConfigurations, opt => opt.Ignore());

        }
    }
}
