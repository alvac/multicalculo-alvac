﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.Dispatchers;

namespace Alvac.Domain.DataTransferObject.Commands.Dispatchers
{
    public class CreateDispatcherMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<CreateDispatcher, Entities.Dispatcher>()
                .ForMember(dest => dest.Connections, opt => opt.Ignore())
                .ForMember(dest => dest.Insurer, opt => opt.Ignore())
                .ForMember(dest => dest.DispatcherId, opt => opt.Ignore());
        }
    }
}
