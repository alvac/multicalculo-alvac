﻿using AutoMapper;
using Alvac.Domain.Entities.Commands.Dispatchers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject.Commands.Dispatchers
{
    public class UpdateDispatcherMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<UpdateDispatcher, Entities.Dispatcher>()
                .ForMember(dest => dest.Insurer, opt => opt.Ignore())
                .ForMember(dest => dest.Connections, opt => opt.Ignore());
        }
    }
}
