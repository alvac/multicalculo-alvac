﻿using AutoMapper;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class Subscription
    {
        /// <summary>
        /// Identificador da assinatura
        /// </summary>
        public int SubscriptionId { get; set; }

        /// <summary>
        /// Identificador da conta
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// Nome da conta
        /// </summary>
        public string AccountName { get; set; }

        /// <summary>
        /// Identificador do plano
        /// </summary>
        public int PlanId { get; set; }

        /// <summary>
        /// Nome do plano
        /// </summary>
        [DisplayName("Nome do plano")]
        public string PlanName { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        [DisplayName("Tipo de requisição")]
        public RequestType RequestType { get; set; }

        /// <summary>
        /// Tipo de assinatura
        /// </summary>
        [DisplayName("Tipo de assinatura")]
        public SubscriptionType Type { get; set; }

        /// <summary>
        /// Status da assinatura
        /// </summary>
        public SubscriptionStatus Status { get; set; }

        /// <summary>
        /// Data que à assinatura expira
        /// </summary>
        public DateTimeOffset? Expiration { get; set; }

        /// <summary>
        /// Data de criação
        /// </summary>
        [DisplayName("Data de criação")]
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Ultima atualização
        /// </summary>
        public DateTimeOffset LastUpdate { get; set; }
    }

    public class SubscriptionMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.Subscription, Subscription>();
        }
    }
}
