﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class InsurerValidator
    {
        public int InsurerValidatorId { get; set; }

        public int Insurer { get; set; }

        public RequestType RequestType { get; set; }

        public string Description { get; set; }

        public string FullAssemblyName { get; set; }
    }
}
