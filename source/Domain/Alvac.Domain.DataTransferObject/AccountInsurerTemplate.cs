﻿using AutoMapper;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class AccountInsurerTemplate
    {
        public int AccountInsurerTemplateId { get; set; }

        /// <summary>
        /// Identificador da conta
        /// </summary>
        public int AccountId { get; set; }

        public string Name { get; set; }

        public string Description { get; set; }

        public bool IsActive { get; set; }

        public DateTimeOffset Created { get; set; }

        public RequestType RequestType { get; set; }
    }

    public class AccountInsurerTemplateMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.AccountInsurerTemplate, Alvac.Domain.DataTransferObject.AccountInsurerTemplate>();
        }
    }
}
