﻿using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AutoMapper.QueryableExtensions;

namespace Alvac.Domain.DataTransferObject
{
    public class MapperService : IMapper
    {
        public TResult Map<TResult>(object source)
        {
            return AutoMapper.Mapper.Map<TResult>(source);
        }

        public IQueryable<TResult> Project<TSource, TResult>(IQueryable<TSource> source)
        {
            return ((IQueryable<TSource>)source).Project<TSource>().To<TResult>();
        }

        public TResult Map<TSource, TResult>(TSource source, TResult destination)
        {
            return AutoMapper.Mapper.Map<TSource, TResult>(source, destination);
        }
    }
}
