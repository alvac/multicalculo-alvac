﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class AccountInsurerConfiguration
    {
        /// <summary>
        /// Identificador da configuração
        /// </summary>
        public int AccountInsurerConfigurationId { get; set; }

        public int InsurerConfigurationId { get; set; }

        public int AccountInsurerTemplateId { get; set; }
        
        /// <summary>
        /// Valor
        /// </summary>
        public string Value { get; set; }
    }

    public class AccountInsurerConfigurationMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Alvac.Domain.Entities.AccountInsurerConfiguration, Alvac.Domain.DataTransferObject.AccountInsurerConfiguration>()
                .ForMember(dest => dest.AccountInsurerConfigurationId, opt => opt.MapFrom(src => src.AccountInsurerConfigurationId))
                .ForMember(dest => dest.InsurerConfigurationId, opt => opt.MapFrom(src => src.InsurerConfigurationId))
                .ForMember(dest => dest.AccountInsurerTemplateId, opt => opt.MapFrom(src => src.AccountInsurerTemplateId))
                .ForMember(dest => dest.Value, opt => opt.MapFrom(src => src.Value));
        }
    }
}
