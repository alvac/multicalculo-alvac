﻿using AutoMapper;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.DataTransferObject
{
    public class User
    {
        public string Id { get; set; }
        public string Name { get; set; }
        public string Email { get; set; }
        public bool IsActive { get; set; }
        public string Role { get; set; }
        public string RoleName { get; set; }
        public DateTimeOffset Created { get; set; }
    }

    public class UserMapping : Profile
    {
        protected override void Configure()
        {
            Mapper.CreateMap<Domain.Entities.User, User>()
                .ForMember(dest => dest.Role, opt => opt.MapFrom(src => src.Roles.First().RoleId))
                .ForMember(dest => dest.RoleName, opt => opt.ResolveUsing(src =>
                    {
                        if (src.Roles.Any())
                        {
                            switch (src.Roles.First().RoleId)
                            {
                                case Enum.Role.Master:
                                    return Enum.Role.MasterString;
                                case Enum.Role.Accounting:
                                    return Enum.Role.AccountingString;
                                case Enum.Role.Internet:
                                    return Enum.Role.InternetString;
                            }
                        }
                        return "";
                    }));
        }
    }
}
