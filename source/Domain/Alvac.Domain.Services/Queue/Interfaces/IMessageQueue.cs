﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Queue.Interfaces
{
    public interface IMessageQueue<T> : IInitializer<string>, IQueue<QueueMessage<T>> where T : class
    {
        Task<string> PushAsync(string correlationId, string body);

        Task<string> PushAsync(string correlationId, string label, string body);
    }
}
