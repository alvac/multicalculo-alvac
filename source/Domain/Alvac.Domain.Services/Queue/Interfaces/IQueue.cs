﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Queue.Interfaces
{
    public interface IQueue<TMessage> : IDisposable
    {
        void Initializer(string queueName);

        Task PushAsync(TMessage message);

        Task<TMessage> ReceiveAsync();

        Task<TMessage> ReceiveAsync(TimeSpan timeout);

        Task<TMessage> PeekAsync();

        Task<TMessage> PeekAsync(long sequenceNumber);

        void Close();

        string QueueName { get; }
    }
}
