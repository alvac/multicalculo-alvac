﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IApiClient<DataTransferObject> : IInitializer<string>
        where DataTransferObject : class
    {
        string Url { get; }

        void Initializer(string url);

        Task<IEnumerable<DataTransferObject>> GetAsync();

        Task<DataTransferObject> GetAsync(int id);

        Task PostAsync(DataTransferObject dataTransferObject);

        Task PutAsync(int id, DataTransferObject dataTransferObject);

        Task DeleteAsync(int id);
    }
}
