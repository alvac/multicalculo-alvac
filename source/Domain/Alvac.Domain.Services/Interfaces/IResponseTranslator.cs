﻿using Alvac.Domain.Contract.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IResponseTranslator<TDispatcherResponse>
    {
        void ToDispatcherResponse(TDispatcherResponse dispatcherResponse, ref BaseResponse baseResponse);
    }
}
