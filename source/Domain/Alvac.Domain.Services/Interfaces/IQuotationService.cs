﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Enum;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IQuotationService
    {
        /// <summary>
        /// Regisra as cotações da requisição no banco de dados.
        /// </summary>
        /// <param name="quotationRequestList"></param>
        /// <returns></returns>
        Task<Alvac.Domain.Entities.Quotation> RegisterQuotationAsync(BaseRequest baseRequest);

        /// <summary>
        /// Busca a conexão referente aos parametros de configuração.
        /// </summary>
        /// <param name="accountId"></param>
        /// <param name="insurer"></param>
        /// <param name="requestType"></param>
        /// <returns></returns>
        Task<Alvac.Domain.Entities.Connection> GetConnectionAsync(int accountId, int insurer, RequestType requestType);

        /// <summary>
        /// Regisra as cotações seguradoras da requisição no banco de dados.
        /// </summary>
        /// <param name="quotationRequestList"></param>
        /// <returns></returns>
        Task RegisterInsurerQuotationAsync(InsurerQuotationRequest insurerQuotationRequest, int autoQuotationId, int connectionId);

        /// <summary>
        /// Enfileira as requisições de cada seguradora na fila correspondente.
        /// </summary>
        /// <param name="quotationRequestList"></param>
        /// <returns></returns>
        Task EnqueueRequestAsync(InsurerQuotationRequest insurerQuotationRequest, string inputQueueName);

        /// <summary>
        /// Atualiza o status da cotação seguradora.
        /// </summary>
        /// <param name="insurerQuotationId"></param>
        /// <param name="status"></param>
        /// <returns></returns>
        Task UpdateInsurerQuotationStatusAsync(int insurerQuotationId, int connectionId, Status status, DescriptionCode descriptionCode, string description);
    }
}
