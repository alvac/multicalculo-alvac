﻿using Alvac.Domain.Contract.Response;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IBillingService
    {
        Task<int> DebitAsync(int insurerQuotationId);
    }
}
