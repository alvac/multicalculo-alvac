﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IHttpClient : IDisposable
    {
        TimeSpan Timeout { get; set; }

        Task<HttpResponseMessage> GetAsync(Uri uri);

        Task<HttpResponseMessage> PostAsync(HttpRequestMessage requestMessage);

        Task<TResult> GetAsync<TResult>(string uri);

        Task<HttpResponseMessage> PostAsync(string uri, object content);

        Task<TResult> PostAsync<TResult>(string uri, object content);

        Task<HttpResponseMessage> PutAsync(string uri, object content);

        Task<TResult> PutAsync<TResult>(string uri, object content);

        Task<HttpResponseMessage> DeleteAsync(string uri);
    }
}
