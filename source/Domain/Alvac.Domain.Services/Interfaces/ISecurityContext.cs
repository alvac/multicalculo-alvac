﻿using Alvac.Domain.Entities;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface ISecurityContext
    {
        Task<User> GetUserAsync();
        bool IsInRole(string role);
    }
}
