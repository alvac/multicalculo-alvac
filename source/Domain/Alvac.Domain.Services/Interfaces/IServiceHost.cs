﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IServiceHost
    {
        Task StartAsync();
        Task StopAsync();
    }
}
