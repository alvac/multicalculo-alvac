﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IRequestTranslator<TDispatcherRequest>
    {
        Task ToDispatcherRequestAsync(BaseRequest baseRequest, IDictionary<string, object> insurerParameters, TDispatcherRequest dispatcherRequest);
    }
}
