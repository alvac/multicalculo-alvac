﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Services.Notification;
using Alvac.Domain.Services.Pipeline;
using Alvac.Domain.Services.Pipeline.Producer;
using Alvac.Domain.Services.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace Alvac.Domain.Services.Interfaces
{
    public interface INotificationProducerPipeline : IPipeline, IInitializer<NotificationParameter>
    {
        Func<QueueMessage<NotificationRequest>, Task<PipelineMessageNotificationProducer>> BuilderPipelineMessageAsync();

        Func<PipelineMessageNotificationProducer, Task<PipelineMessageNotificationProducer>> GetCallNotificationParametersAsync();
        
        Task DeliveryNotificationRequestAsync(PipelineMessageNotificationProducer pipelineMessage);
    }
}
