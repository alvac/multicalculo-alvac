﻿using Alvac.Domain.Contract.Request;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface INotificationProvider
    {
        Task RegisterNotificationAsync(Alvac.Domain.Contract.Request.Notification notification, NotificationRequest notificationRequest);

        Task EnqueueAsync(NotificationRequest notificationRequest, string notificationQueueName);

        Task<Alvac.Domain.Contract.Request.Notification> GetNotificationAsync(int insurerQuotationId);

        Task DeliveryNotificationAsync(Alvac.Domain.Contract.Request.Notification notification, NotificationRequest notificationRequest);

    }
}
