﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Notifications;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Notifications;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Notification
{
    public class NotificationProvider : INotificationProvider
    {
        #region Private Fields

        protected readonly ICommandProcessor _commandProcessor;
        protected readonly IQueryProcessor _queryProcessor;
        protected readonly IMapper _mapper;
        protected readonly IMessageQueueFactory _messageQueueFactory;
        private readonly ILogger _logger;
        private readonly IHttpClient _httpClient;

        #endregion

        #region Contructor

        public NotificationProvider(ICommandProcessor commandProcessor, IQueryProcessor queryProcessor, IMapper mapper, IMessageQueueFactory messageQueueFactory,
            ILogger logger, IHttpClient httpClient)
        {
            _commandProcessor = commandProcessor;
            _queryProcessor = queryProcessor;
            _mapper = mapper;
            _messageQueueFactory = messageQueueFactory;
            _logger = logger;
            _httpClient = httpClient;
        }

        #endregion

        #region INotificationProvider Members

        public async Task RegisterNotificationAsync(Alvac.Domain.Contract.Request.Notification notification, NotificationRequest notificationRequest)
        {
            if (notificationRequest != null)
            {
                if (notificationRequest.Response != null)
                {
                    var createNotificationCommand = new CreateNotification
                    {
                        InsurerQuotationId = notificationRequest.Response.Identifier,
                        Status = Status.Enqueued,
                        OperationDescriptionCode = DescriptionCode.Successfully,
                        OperationDescription = "Notificação enfileirada com sucesso.",
                        CorrelationId = notification.CorrelationId,
                        CallType = notification.CallType,
                        Path = notification.Path
                    };

                    var registeredNotification = await _commandProcessor.ProcessAsync<Entities.Notification>(createNotificationCommand);

                    if (registeredNotification != null)
                    {
                        notificationRequest.InsurerQuotationId = registeredNotification.InsurerQuotationId;

                        await _logger.WriteVerboseAsync(notificationRequest.Response.Identifier.ToString(), "NotificationProvider", "RegisterNotificationAsync",
                            string.Format("Notificação '{0}' registrada com sucesso.", registeredNotification.InsurerQuotationId));
                    }
                    else
                    {
                        throw new Exception("Erro ao registra notificacao. CreateNotificationCommand é vazio ou null.");
                    }
                }
                else
                {
                    throw new AlvacException("NotificationRequest erro. Response é vazio ou null", DescriptionCode.InvalidRequest);
                }
            }
            else
            {
                throw new AlvacException("NotificationRequest é vazio ou null", DescriptionCode.InvalidRequest);
            }

        }

        public async Task EnqueueAsync(NotificationRequest notificationRequest, string notificationQueueName)
        {
            if (notificationRequest != null)
            {
                if (notificationRequest.Response != null)
                {
                    var queueMessage = _messageQueueFactory.Create<NotificationRequest>(notificationQueueName);
                    await queueMessage.PushAsync(notificationRequest.Response.Identifier.ToString(), AlvacJson<NotificationRequest>.Serialize(notificationRequest));

                    await _logger.WriteVerboseAsync(notificationRequest.InsurerQuotationId.ToString(), "NotificationProvider", "RegisterNotificationAsync",
                            string.Format("NotificationRequest '{0}' enfileirado com sucesso.", notificationRequest.InsurerQuotationId));

                }
                else
                {
                    throw new AlvacException("NotificationRequest erro. Responseé vazio ou null", DescriptionCode.InvalidRequest);
                }
            }
            else
            {
                throw new AlvacException("NotificationRequest é vazio ou null", DescriptionCode.InvalidRequest);
            }
        }

        public async Task<Contract.Request.Notification> GetNotificationAsync(int insurerQuotationId)
        {
            var findNotificationById = new GetNotificationById
            {
                InsurerQuotationId = insurerQuotationId
            };

            var entityNotification = await _queryProcessor.ProcessAsync<Alvac.Domain.Entities.Notification>(findNotificationById);

            if (entityNotification != null)
            {
                return _mapper.Map<Contract.Request.Notification>(entityNotification);
            }
            else
            {
                throw new AlvacException(string.Format("Notification not found. InsurerQuotationId: {0}", insurerQuotationId), DescriptionCode.NotFound);
            }

        }

        public async Task DeliveryNotificationAsync(Contract.Request.Notification notification, NotificationRequest notificationRequest)
        {
            _httpClient.Timeout = TimeSpan.FromSeconds(80);

            var httpRequestMessage = new HttpRequestMessage(HttpMethod.Post, notification.Path);
            httpRequestMessage.Content = new StringContent(AlvacJson<NotificationRequest>.Serialize(notificationRequest), Encoding.UTF8, "application/json");

            var httpResponseMessage = await _httpClient.PostAsync(httpRequestMessage);

            if (httpResponseMessage != null)
            {
                if (httpResponseMessage.IsSuccessStatusCode)
                {
                    await _logger.WriteVerboseAsync(notificationRequest.Response.Identifier.ToString(), "NotificationProvider", "DeliveryNotificationAsync",
                          string.Format("Notification '{0}' entregue com sucesso.", notificationRequest.InsurerQuotationId));
                }
                else
                {
                    await _logger.WriteErrorAsync(notificationRequest.Response.Identifier.ToString(), "NotificationProvider", "DeliveryNotificationAsync",
                          string.Format("Notification '{0}' erro na entrega. StatusCode: {1}", notificationRequest.InsurerQuotationId, httpResponseMessage.StatusCode));
                }
            }
            else
            {
                await _logger.WriteErrorAsync(notificationRequest.Response.Identifier.ToString(), "NotificationProvider", "DeliveryNotificationAsync",
                          string.Format("Notification '{0}' erro na entrega. HttpResponseMessage é vazio ou null.", notificationRequest.InsurerQuotationId));
            }
        }

        #endregion
    }
}
