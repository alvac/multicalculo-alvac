﻿using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Notification
{
    public class NotificationParameter
    {
         #region Contructor

        public NotificationParameter()
        {
            this.MaxWaitTimeSec = 60;
            this.ReceiveTimeoutMiliSec = TimeSpan.FromMilliseconds(5000);
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Valor máximo espera da thread de Throughput.
        /// </summary>
        public int MaxWaitTimeSec { get; set; }

        /// <summary>
        /// Valor de Throughput.
        /// </summary>
        public int MaxThroughput { get; set; }

        /// <summary>
        /// Janela de envio data flow.
        /// </summary>
        public int WindowSize { get; set; }
        
        /// <summary>
        /// Nome da fila de Notificação.
        /// </summary>
        public string NotificationQueueName { get; set; }

        /// <summary>
        /// Timeout de espera para leitura da fila.
        /// </summary>
        public TimeSpan ReceiveTimeoutMiliSec { get; set; }

        #endregion
    }
}
