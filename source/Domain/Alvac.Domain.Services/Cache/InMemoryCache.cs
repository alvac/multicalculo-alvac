﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Runtime.Caching;
using Alvac.Domain.Services.Interfaces;

namespace Alvac.Domain.Services.Cache
{
    public class InMemoryCache : ICache, IDisposable
    {
        #region Private Fields

        private MemoryCache _memoryCache;

        #endregion

        #region Constructor

        public InMemoryCache()
        {
            _memoryCache = new MemoryCache("MemoryCache");
        }

        #endregion


        #region ICache Members

        public Task<T> Get<T>(string key)
        {
            throw new NotImplementedException();
        }

        public Task Set(string key, object value, TimeSpan duration)
        {
            throw new NotImplementedException();
        }

        #endregion

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _memoryCache.Dispose();
            }
        }
    }
}
