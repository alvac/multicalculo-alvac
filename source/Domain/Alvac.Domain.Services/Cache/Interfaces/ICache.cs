﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface ICache
    {
        Task<T> Get<T>(string key);

        Task Set(string key, object value, TimeSpan duration);
    }
}
