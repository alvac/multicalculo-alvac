﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services
{
    public class TimeProvider : ITimeProvider
    {
        public DateTimeOffset Now
        {
            get
            {
                return DateTimeOffset.UtcNow;
            }
        }
    }
}
