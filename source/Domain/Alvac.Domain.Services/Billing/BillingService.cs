﻿using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Transactions;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.InsurerQuotations;
using Alvac.Domain.Entities.Queries.Invoices;
using Alvac.Domain.Entities.Queries.Plans;
using Alvac.Domain.Entities.Queries.Quotations;
using Alvac.Domain.Entities.Queries.Subscriptions;
using Alvac.Domain.Entities.Queries.Transactions;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Billing
{
    public class BillingService : IBillingService
    {
        #region Private Fields

        private readonly ICommandProcessor _commandProcessor;
        private readonly ILogger _logger;
        private readonly IQueryProcessor _queryProcessor;

        #endregion Private Fields

        #region Contructor

        public BillingService(IQueryProcessor queryProcessor, ICommandProcessor commandProcessor, ILogger logger)
        {
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
            _logger = logger;
        }

        #endregion Contructor


        #region IBillingService Members

        public async Task<int> DebitAsync(int insurerQuotationId)
        {
            var subscription = await GetBillingSubscriptionAsync(insurerQuotationId);

            var transactionId = 0;

            if (subscription != null)
            {
                if (subscription.Type == SubscriptionType.Normal)
                {
                    var invoice = await GetOpenInvoiceAsync(subscription.SubscriptionId);

                    if (invoice != null)
                    {
                        var transactionValue = await GetValueAsync(subscription, invoice);

                        if (transactionValue > 0)
                        {
                            var createTransactionCommand = new CreateTransaction
                            {
                                InvoiceId = invoice.InvoiceId,
                                InsurerQuotationId = insurerQuotationId,
                                Value = transactionValue
                            };

                            var transaction = await _commandProcessor.ProcessAsync(createTransactionCommand);

                            transactionId = transaction.InsurerQuotationId;
                        }
                    }
                    else
                    {
                        throw new AlvacException(string.Format("Fatura não encontrada - SubscriptionId: {0} Status: Open", subscription.SubscriptionId), DescriptionCode.NotFound);
                    }
                }
                else
                {
                    await _logger.WriteVerboseAsync("BillingService", "GetValueAsync", string.Format("Transação não foi registrada - SubscriptionType: {0}",
                        subscription.Type.ToString()));
                }
            }
            else
            {
                throw new AlvacException(string.Format("Assinatura não encontrada - InsurerQuotationId: {0}", insurerQuotationId), DescriptionCode.NotFound);
            }

            return transactionId;
        }

        #endregion IBillingService Members

        #region Private Methods

        private async Task<Subscription> GetBillingSubscriptionAsync(int insurerQuotationId)
        {
            var findInsurerQuotationById = new GetInsurerQuotationById
            {
                InsurerQuotationId = insurerQuotationId
            };

            var insurerQuotation = await _queryProcessor.ProcessAsync(findInsurerQuotationById);

            if (insurerQuotation != null)
            {
                var findQuotationById = new GetQuotationById
                {
                    QuotationId = insurerQuotation.QuotationId
                };

                var quotation = await _queryProcessor.ProcessAsync(findQuotationById);

                if (quotation != null)
                {
                    var findSubscriptionByAccountIdRequestType = new GetSubscriptionByAccountIdRequestType
                    {
                        AccountId = quotation.AccountId,
                        RequestType = insurerQuotation.GetRequestType(),
                        Status = SubscriptionStatus.Active
                    };

                    return await _queryProcessor.ProcessAsync(findSubscriptionByAccountIdRequestType);
                }
                else
                {
                    throw new AlvacException(string.Format("Cotação não encontrada - QuotationId: {0}", insurerQuotation.QuotationId), DescriptionCode.NotFound);
                }
            }
            else
            {
                throw new AlvacException(string.Format("Cotação da seguradora não encontrada - insurerQuotationId: {0}", insurerQuotationId), DescriptionCode.NotFound);
            }
        }

        private async Task<Invoice> GetOpenInvoiceAsync(int subscriptionId)
        {
            var findInvoiceBySubscriptionIdStatus = new GetInvoiceBySubscriptionIdStatus
            {
                SubscriptionId = subscriptionId,
                Status = InvoiceStatus.Open
            };

            return await _queryProcessor.ProcessAsync(findInvoiceBySubscriptionIdStatus);
        }

        private async Task<decimal> GetValueAsync(Subscription subscription, Invoice invoice)
        {
            decimal result = 0;

            var findPlanById = new GetPlanById
            {
                PlanId = subscription.PlanId
            };

            var plan = await _queryProcessor.ProcessAsync(findPlanById);

            if (plan != null)
            {
                if (plan.IsActive)
                {
                    var getTransactionCountByInvoiceId = new GetTransactionCountByInvoiceId
                    {
                        InvoiceId = invoice.InvoiceId
                    };

                    var totalTransaction = await _queryProcessor.ProcessAsync(getTransactionCountByInvoiceId);

                    if (totalTransaction <= plan.Amount)
                    {
                        result = plan.Value;
                    }
                    else
                    {
                        result = plan.AdditionalValue;
                    }
                }
                else
                {
                    throw new AlvacException(string.Format("Plano esta desativado - Id: {0}", subscription.PlanId), DescriptionCode.NotFound);
                }
            }
            else
            {
                throw new AlvacException(string.Format("Plano não encontrado - Id: {0}", subscription.PlanId), DescriptionCode.NotFound);
            }

            return result;
        }

        #endregion Private Methods
    }
}