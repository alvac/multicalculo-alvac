﻿using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Contract.Response.Auto;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Entities.Commands.InsurerQuotations;

namespace Alvac.Domain.Services.UpdateResponse
{
    public class UpdateResponseAuto : IUpdateResponse
    {
        #region Private Fields

        private readonly ICommandProcessor _commandProcessor;

        #endregion

        #region Constructor

        public UpdateResponseAuto(ICommandProcessor commandProcessor)
        {
            _commandProcessor = commandProcessor;
        }

        #endregion

        #region IUpdateResponse Members

        public async Task UpdateAsync(BaseResponse response)
        {
            if (response != null)
            {
                if (response is AutoQuotationResponse)
                {
                    var insurerAutoQuotation = await _commandProcessor.ProcessAsync<InsurerAutoQuotation>(response as AutoQuotationResponse);
                }
                else
                {
                    throw new AlvacException("Resposta base não é um pedido de auto valido.", DescriptionCode.InvalidRequest);
                }
            }
            else
            {
                throw new AlvacException("Resposta base vazio ou null.", DescriptionCode.InvalidRequest);
            }
        }

        public async Task UpdateStatusAsync(BaseResponse response, Status status, DescriptionCode descriptionCode, string operationDescription)
        {
            if (response != null)
            {
                if (response is AutoQuotationResponse)
                {
                    var updateInsurerQuotationStatus = new UpdateStatusInsurerQuotation
                    {
                        InsurerQuotationId = response.Identifier,
                        Status = status,
                        OperationDescriptionCode = descriptionCode,
                        OperationDescription = operationDescription,
                        Gateway = response.GatewayId
                    };

                    var insurerAutoQuotation = await _commandProcessor.ProcessAsync(updateInsurerQuotationStatus);
                }
                else
                {
                    throw new AlvacException("Resposta base não é um pedido de auto valido.", DescriptionCode.InvalidRequest);
                }
            }
            else
            {
                throw new AlvacException("Resposta base vazio ou null.", DescriptionCode.InvalidRequest);
            }
        }

        #endregion

    }
}
