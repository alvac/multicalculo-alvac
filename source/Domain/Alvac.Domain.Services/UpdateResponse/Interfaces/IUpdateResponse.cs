﻿using Alvac.Domain.Contract.Response;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IUpdateResponse
    {
        Task UpdateAsync(BaseResponse response);

        Task UpdateStatusAsync(BaseResponse response, Status status, DescriptionCode descriptionCode, string operationDescription);
    }
}
