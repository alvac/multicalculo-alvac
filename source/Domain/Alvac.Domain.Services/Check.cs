﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services
{
    public static class Check
    {
        public static T NotNull<T>(T value, string parameterName)
        {
            if (ReferenceEquals(value, null))
            {
                throw new ArgumentNullException(parameterName);
            }

            return value;
        }

        public static T NotEquals<T>(T source, T destination)
        {
            if (typeof(T).IsValueType)
            {
                if (!ReferenceEquals(source, destination))
                {
                    throw new ArgumentException(string.Format("Valores não são iguais, {0} {1}", source.ToString(), destination.ToString()));
                }
            }
            else
            {
                if (!Equals(source, destination))
                {
                    throw new ArgumentException(string.Format("Valores não são iguais, {0} {1}", source.ToString(), destination.ToString()));
                }
            }

            return source;
        }

        public static T NotSameType<T>(T value, Type type)
        {
            if (!value.GetType().IsAssignableFrom(type))
            {
                throw new ArgumentException(string.Format("A variavel não é do mesmo tipo"));
            }

            return value;
        }
    }
}
