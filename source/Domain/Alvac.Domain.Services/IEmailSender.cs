﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services
{
    public interface IEmailSender
    {
        Task SendAsync(string from, string to, string subject, string html);
        Task SendAsync(string host, int port, string user, string password, bool enableSsl, string from, string to, string subject, string html);
    }
}
