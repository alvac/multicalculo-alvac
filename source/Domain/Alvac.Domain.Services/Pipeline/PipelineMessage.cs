﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Services.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Pipeline
{
    public class PipelineMessage<TMessage> where TMessage : class
    {
        #region Public Properties

        public QueueMessage<TMessage> Message { get; set; }

        public OperationResult Result { get; set; }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return string.Format("Result: {0} - Message: {1}", Result, Message);
        }

        #endregion
    }
}
