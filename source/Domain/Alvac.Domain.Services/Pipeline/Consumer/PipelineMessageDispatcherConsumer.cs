﻿using Alvac.Domain.Contract.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Enum;

namespace Alvac.Domain.Services.Pipeline.Consumer
{
    public class PipelineMessageDispatcherConsumer : PipelineMessage<InsurerQuotationResponse>
    {
        #region Public Properties
        
        public Alvac.Domain.Contract.Request.Notification Notification { get; set; }
        
        public BaseResponse Response { get; set; }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return string.Format("Result: {0} - Message: {1} - {2} - Response: {3}", Result, Message, Notification, Response);
        }

        #endregion
    }
}
