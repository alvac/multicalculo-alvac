﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Alvac.Domain.Services.Pipeline
{
    public class PipelineLink<PipelineMessage> where PipelineMessage : class
    {
        #region Private Fields

        private ITargetBlock<PipelineMessage> _workerBlock;

        private ISourceBlock<PipelineMessage> _sourceBlock;

        private IDisposable _link;

        #endregion

        #region Public Properties

        private bool _isLinked;

        public bool IsLinked
        {
            get
            {
                return _isLinked;
            }
        }

        #endregion

        #region Contructor

        public PipelineLink(ISourceBlock<PipelineMessage> bufferBlock, ITargetBlock<PipelineMessage> workerBlock)
        {
            _sourceBlock = bufferBlock;
            _workerBlock = workerBlock;
            _isLinked = false;
        }

        #endregion

        #region Public Abstract Methods

        public void Link()
        {
            if (!IsLinked)
            {
                _link = _sourceBlock.LinkTo(_workerBlock);
                _isLinked = true;
            }
        }

        public void UnLink()
        {
            if (IsLinked)
            {
                _link.Dispose();
                _isLinked = false;
            }
        }

        #endregion
    }
}
