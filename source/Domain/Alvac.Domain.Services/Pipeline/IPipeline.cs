﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Pipeline
{
    public interface IPipeline
    {
        Task StartAsync();
        
        Task StopAsync();
    }


}
