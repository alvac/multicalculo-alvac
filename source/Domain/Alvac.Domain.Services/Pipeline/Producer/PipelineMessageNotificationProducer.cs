﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Pipeline.Producer
{
    public class PipelineMessageNotificationProducer : PipelineMessage<NotificationRequest>
    {
        #region Public Properties

        public int InsurerQuotationId { get; set; }

        public string CorrelationId { get; set; }

        public Alvac.Domain.Contract.Request.Notification Notification { get; set; }

        public BaseResponse Response { get; set; }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return string.Format("Result: {0} - Message: {1} - InsurerQuotationId: {2} - CorrelationId: {3} - Notification: {4} - Response: {5}", 
                Result, Message, InsurerQuotationId, CorrelationId, Notification, Response);
        }

        #endregion
    }
}
