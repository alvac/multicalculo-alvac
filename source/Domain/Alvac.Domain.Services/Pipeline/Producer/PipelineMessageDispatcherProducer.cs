﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Pipeline.Producer
{
    public class PipelineMessageDispatcherProducer : PipelineMessage<InsurerQuotationRequest>
    {
        #region Public Properties

        public Technology Technology { get; set; }

        public IDictionary<string, object> Parameters { get; set; }

        public BaseRequest Request { get; set; }

        public BaseResponse Response { get; set; }

        #endregion

        #region Public Methods

        public override string ToString()
        {
            return string.Format("Result: {0} - Message: {1} - Request: {2} - Response: {3}", Result, Message, Request, Response);
        }

        #endregion
    }
}
