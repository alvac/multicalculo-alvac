﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Enum;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Runtime.Remoting;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Factories
{
    public class DispatcherFactory : IDispatcherFactory
    {
        private IContainerProvider _containerProvider;

        public DispatcherFactory(IContainerProvider containerProvider)
        {
            _containerProvider = containerProvider;
        }

        #region IDispatcherFactory Members

        public IDispatcher Create(string fullAssemblyName, IDictionary<string, object> parameters, IEnumerable<InsurerConfiguration> insurerConfigurations)
        {
            Type type = Type.GetType(fullAssemblyName);

            if (type == null)
            {
                throw new Exception(string.Format("Assembly {0} não foi encontrada.", fullAssemblyName));
            }

            var dispatcher = _containerProvider.GetService(type) as DispatcherBase;

            if (dispatcher is IInitializer<DispatcherData>)
            {
                ((IInitializer<DispatcherData>)dispatcher).Initializer(new DispatcherData(parameters));
            }

            return new DispatcherInsurerConfigurationDecorator(dispatcher, insurerConfigurations);
        }

        #endregion
    }
}
