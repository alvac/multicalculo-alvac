﻿using Alvac.Domain.Enum;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Factories
{
    public interface IQuotationServiceFactory
    {
        IQuotationService Create (RequestType requestType);
    }
}
