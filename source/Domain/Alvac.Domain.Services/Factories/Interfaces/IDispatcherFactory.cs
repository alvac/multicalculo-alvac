﻿using Alvac.Domain.Entities;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Factories
{
    public interface IDispatcherFactory
    {
        IDispatcher Create(string fullAssemblyName, IDictionary<string, object> parameters, IEnumerable<InsurerConfiguration> insurerConfigurations);
    }
}
