﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Services.Queue.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Factories
{
    public interface IMessageQueueFactory
    {
        IMessageQueue<T> Create<T>(string queueName) where T : class;
    }
}
