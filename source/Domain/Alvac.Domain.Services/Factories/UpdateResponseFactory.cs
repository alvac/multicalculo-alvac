﻿using Alvac.Domain.Enum;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.UpdateResponse;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Factories
{
    public class UpdateResponseFactory : IUpdateResponseFactory
    {
        #region Private Fields

        private readonly IContainerProvider _containerProvider;

        #endregion

        #region Contructor

        public UpdateResponseFactory(IContainerProvider containerProvider)
        {
            _containerProvider = containerProvider;
        }

        #endregion

        #region IUpdateResponseFactory Members

        public IUpdateResponse Create(RequestType requestType)
        {
            switch (requestType)
            {
                case RequestType.AutoQuotation:

                    return _containerProvider.GetService<UpdateResponseAuto>();

                default:
                    throw new NotImplementedException(string.Format("IUpdateResponse não implementado - RequestType: {0}", requestType));
            }
        }

        #endregion

    }
}
