﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.InsurerQuotations;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.AccountConnections;
using Alvac.Domain.Entities.Queries.Connections;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System.Threading.Tasks;

namespace Alvac.Service.Domain.Quotation
{
    public abstract class QuotationService : IQuotationService
    {
        #region Private Fields

        protected readonly ICommandProcessor _commandProcessor;
        protected readonly ILogger _logger;
        protected readonly IMessageQueueFactory _messageQueueFactory;
        protected readonly IQueryProcessor _queryProcessor;

        #endregion Private Fields

        #region Constructors

        public QuotationService(IMessageQueueFactory messageQueueFactory, 
            IQueryProcessor queryProcessor, 
            ICommandProcessor commandProcessor, 
            ILogger logger)
        {
            _messageQueueFactory = messageQueueFactory;
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
            _logger = logger;
        }

        #endregion Constructors

        #region IQuotationService Members

        public virtual async Task EnqueueRequestAsync(InsurerQuotationRequest insurerQuotationRequest, string inputQueueName)
        {
            await _logger.WriteVerboseAsync("QuotationService", "EnqueueRequestAsync", "Inicio EnqueueRequestAsync.");

            if (insurerQuotationRequest.Request != null)
            {
                var messageQueue = _messageQueueFactory.Create<InsurerQuotationRequest>(inputQueueName);

                await messageQueue.PushAsync(insurerQuotationRequest.Request.Identifier.ToString(), AlvacJson<InsurerQuotationRequest>.Serialize(insurerQuotationRequest));

                await _logger.WriteVerboseAsync("QuotationService", "EnqueueRequestAsync", 
                    string.Format("ensagem enfileirada com sucesso. CorrelationId:{0}", 
                    insurerQuotationRequest.Request.Identifier));
            }
            else
            {
                throw new AlvacException("Pedido de cotação não encontrado.", DescriptionCode.InvalidRequest);
            }
        }

        public async virtual Task<Connection> GetConnectionAsync(int accountId, int insurer, RequestType requestType)
        {
            Connection result = null;

            await _logger.WriteVerboseAsync("QuotationService", "EnqueueRequestAsync",
                string.Format("Inicio GetConnectionAsync. Parametros - AccountId:{0} Insurer:{1} RequestType:{2}", accountId,
                insurer, requestType));

            var findAccountConnection = new FindAccountConnectionByAccountIdInsurerRequestType
            {
                AccountId = accountId,
                Insurer = insurer,
                RequestType = requestType
            };

            var accountConnection = await _queryProcessor.ProcessAsync(findAccountConnection);

            if (accountConnection != null)
            {
                await _logger.WriteVerboseAsync("QuotationService", "EnqueueRequestAsync", 
                    string.Format("Conexão de uma conta encontrada com sucesso. Parametros - AccountId:{0} Insurer:{1} RequestType:{2}", accountId,
                insurer, requestType));

                await _logger.WriteVerboseAsync("QuotationService", "EnqueueRequestAsync",
                    string.Format("Inicio FindConnectionById. Parametros - ConnectionId:{0} Insurer:{1} RequestType:{2}",
                    accountConnection.ConnectionId, insurer, requestType));

                var findConnection = new FindConnectionById
                {
                    ConnectionId = accountConnection.ConnectionId
                };

                result = await _queryProcessor.ProcessAsync(findConnection);

                if (result == null)
                {
                    throw new AlvacException(string.Format("Conexão não foi encontrada. Parametros: ConnectionId = {0}",
                        accountConnection.ConnectionId), DescriptionCode.NotFound);
                }

                await _logger.WriteVerboseAsync("QuotationService", "EnqueueRequestAsync", 
                    string.Format("Conexão encontrada com sucesso. Parametros - ConnectionId:{0}", accountConnection.ConnectionId));
            }
            else
            {
                throw new AlvacException(string.Format("Nenhuma conexão configurada para a conta. Parametros: AccountId = {0} Insurer = {1} RequestType = {2}",
                    accountId, insurer, requestType), DescriptionCode.NotFound);
            }

            return result;
        }
        
        public abstract Task RegisterInsurerQuotationAsync(InsurerQuotationRequest insurerQuotationRequest, int autoQuotationId, int connectionId);

        public abstract Task<Alvac.Domain.Entities.Quotation> RegisterQuotationAsync(BaseRequest baseRequest);

        public async Task UpdateInsurerQuotationStatusAsync(int insurerQuotationId, int connectionId, Status status, DescriptionCode descriptionCode, string description)
        {
            var updateStatusInsurerQuotationCommand = new UpdateStatusInsurerQuotation
            {
                InsurerQuotationId = insurerQuotationId,
                ConnectionId = connectionId,
                Status = status,
                OperationDescriptionCode = descriptionCode,
                OperationDescription = description
            };

            await _commandProcessor.ProcessAsync<InsurerQuotation>(updateStatusInsurerQuotationCommand);
        }

        #endregion IQuotationService Members
    }
}