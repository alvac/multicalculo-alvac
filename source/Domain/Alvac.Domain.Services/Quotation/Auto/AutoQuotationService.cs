﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Auto.InsurerAutoQuotationParameters;
using Alvac.Domain.Entities.Commands.Auto.InsurerAutoQuotations;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Logger;
using Alvac.Service.Domain.Quotation;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Quotation.Auto
{
    public class AutoQuotationService : QuotationService
    {
        #region Private Fields

        #endregion Private Fields

        #region Constructors

        public AutoQuotationService(IMessageQueueFactory messageQueueFactory, IQueryProcessor queryProcessor, ICommandProcessor commandProcessor, ILogger logger)
            : base(messageQueueFactory, queryProcessor, commandProcessor, logger)
        {
        }

        #endregion Constructors

        #region QuotationService Members

        public override async Task RegisterInsurerQuotationAsync(InsurerQuotationRequest insurerQuotationRequest, int autoQuotationId, int connectionId)
        {
            if (insurerQuotationRequest.Request != null)
            {
                await _logger.WriteVerboseAsync("AutoQuotationService", "RegisterInsurerQuotationAsync",
                    string.Format("Inicio RegisterInsurerQuotationAsync - QuotationId: {0} ConnectionId: {1}",
                    insurerQuotationRequest.Request.Identifier, connectionId));

                var autoQuotationRequest = insurerQuotationRequest.Request as AutoQuotationRequest;

                var createInsurerAutoQuotationCommand = new CreateInsurerAutoQuotation
                {
                    QuotationId = autoQuotationId,
                    ConnectionId = connectionId,
                    Insurer = insurerQuotationRequest.Insurer,
                    Status = Status.Starting,
                    OperationDescriptionCode = DescriptionCode.Successfully,
                    OperationDescription = "Begin quotation enqueue process.",
                    DeductibleType = autoQuotationRequest.Insurance.DeductibleType
                };

                if (autoQuotationRequest.Retry != null && autoQuotationRequest.Retry.Max.HasValue && autoQuotationRequest.Retry.Max.Value > 0)
                {
                    createInsurerAutoQuotationCommand.RetryMax = autoQuotationRequest.Retry.Max.Value;
                    createInsurerAutoQuotationCommand.RetryInterval = autoQuotationRequest.Retry.Interval.HasValue ? autoQuotationRequest.Retry.Interval.Value : 0;

                    if (autoQuotationRequest.Retry.Next.HasValue)
                    {
                        createInsurerAutoQuotationCommand.RetryNext = autoQuotationRequest.Retry.Next.Value;
                    }

                }

                var insurerAutoQuotation = await _commandProcessor.ProcessAsync(createInsurerAutoQuotationCommand);

                if (insurerAutoQuotation != null && insurerAutoQuotation.InsurerQuotationId > 0)
                {
                    insurerQuotationRequest.Request.Identifier = insurerAutoQuotation.InsurerQuotationId;

                    var createInsurerAutoQuotationParameterCommand = new CreateInsurerAutoQuotationParameter
                    {
                        InsurerAutoQuotationId = insurerAutoQuotation.InsurerQuotationId,
                        Parameters = AlvacJson<IDictionary<string, object>>.Serialize(insurerQuotationRequest.Parameters)
                    };

                    await _commandProcessor.ProcessAsync(createInsurerAutoQuotationParameterCommand);

                    await _logger.WriteVerboseAsync("AutoQuotationService", "RegisterInsurerQuotationAsync",
                        string.Format("CommandProcessor 'CreateInsurerAutoQuotationCommand' executado com sucesso -  InsurerAutoQuotation registrado. - QuotationId: {0} ConnectionId: {1}",
                        autoQuotationRequest.Identifier, connectionId));
                }
                else
                {
                    throw new Exception("CreateInsurerAutoQuotation é nulo.");
                }
            }
            else
            {
                throw new AlvacException("Nenhum pedido encontrado.", DescriptionCode.InvalidRequest);
            }
        }

        public override async Task<Entities.Quotation> RegisterQuotationAsync(BaseRequest baseRequest)
        {
            Entities.Quotation result = null;

            await _logger.WriteVerboseAsync("AutoQuotationService", "RegisterQuotationAsync", "Iniciando RegisterQuotationAsync.");

            if (baseRequest is AutoQuotationRequest)
            {
                result = await _commandProcessor.ProcessAsync(baseRequest as AutoQuotationRequest);

                await _logger.WriteVerboseAsync("AutoQuotationService", "RegisterQuotationAsync", "RegisterQuotationAsync executado com sucesso - Cotação registrada.");
            }
            else
            {
                throw new AlvacException("Pedido não é um autoQuotationRequest valido.", DescriptionCode.InvalidRequest);
            }

            return result;
        }
        #endregion QuotationService Members
    }
}