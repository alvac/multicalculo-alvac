﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Parameters
{
    public interface IParameterInsurer 
    {
        IDictionary<string, object> Parameters { get; }

        T GetValue<T>(string key);
    }
}
