﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Parameters
{
    public class ParameterBase : IParameterInsurer
    {
        #region Private Fields

        public readonly IDictionary<string, object> _parameters;

        #endregion
        
        public ParameterBase(IDictionary<string, object> parameters)
        {
            _parameters = parameters;
        }

        #region Public Properties

        public IDictionary<string, object> Parameters
        {
            get
            {
                return _parameters;
            }
        }

        #endregion
        
        public  T GetValue<T>(string key)
        {
            object objValue;
            if (!Parameters.TryGetValue(key.ToLower(), out objValue))
            {
                return default(T);
            }

            return (T)Convert.ChangeType(objValue, typeof(T), new CultureInfo("pt-BR"));
        }
    }
}
