﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Reports
{
    public class ReportQuotationInsurer
    {
        public int Insurer { get; set; }
        public string InsurerDisplay { get; set; }


        public int Count { get; set; }

        public List<ReportQuotationStatus> Status { get; set; }
    }
}
