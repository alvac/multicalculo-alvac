﻿using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Reports.Queries
{
    public class ReportGeneralQuery : IQuery<ReportGeneral>
    {
        [Required]
        public DateTimeOffset StartDate { get; set; }

        [Required]
        public DateTimeOffset EndDate { get; set; }

        public int AccountId { get; set; }
    }
}
