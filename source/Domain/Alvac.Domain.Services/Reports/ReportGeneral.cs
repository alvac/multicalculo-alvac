﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Reports
{
    public class ReportGeneral
    {
        public List<ReportQuotationInsurer> InsurerQuotations { get; set; }
        public List<ReportQuotationStatus> InsurerQuotationsStatus { get; set; }
    }
}
