﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Reports
{
    public class ReportQuotationStatus
    {
        public DescriptionCode DescriptionCode { get; set; }
        public int Count { get; set; }
    }
}
