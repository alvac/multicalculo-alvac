﻿using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Reports
{
    public class ReportQuotationGroupByUser
    {
        public string UserName { get; set; }

        public int Count
        {
            get
            {
                return Quotations.Sum(x => x.Count);
            }
        }

        public IEnumerable<ReportQuotationInsurer> Quotations { get; set; }
    }
}
