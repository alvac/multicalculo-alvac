﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Dispatchers;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IDispatcher : IInitializer<DispatcherData>
    {
        Task<BaseResponse> ProcessAsync(BaseRequest request, IDictionary<string, object> insurerParameters);

        Task UploadInsurerResponseStorage(object insurerResponse);
    }
}
