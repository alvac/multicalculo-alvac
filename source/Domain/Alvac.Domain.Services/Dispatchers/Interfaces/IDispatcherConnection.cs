﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IDispatcherConnection
    {
        /// <summary>
        /// Inicia uma conexão com a seguradora.
        /// </summary>
        void Start();

        /// <summary>
        /// Para uma conexão com a seguradora.
        /// </summary>
        /// <returns></returns>
        Task StopAsync();
    }
}
