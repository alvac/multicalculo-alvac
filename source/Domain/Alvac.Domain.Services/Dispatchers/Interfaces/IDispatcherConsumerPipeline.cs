﻿using Alvac.Domain.Contract.Response;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Pipeline;
using Alvac.Domain.Services.Pipeline.Consumer;
using Alvac.Domain.Services.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IDispatcherConsumerPipeline : IPipeline, IInitializer<DispatcherParameter>
    {
        Func<QueueMessage<InsurerQuotationResponse>, Task<PipelineMessageDispatcherConsumer>> BuilderPipelineMessageAsync();

        Func<PipelineMessageDispatcherConsumer, Task<PipelineMessageDispatcherConsumer>> RegisterTransactionAsync();

        Func<PipelineMessageDispatcherConsumer, Task<PipelineMessageDispatcherConsumer>> UpdateResponseAsync();
        
        Task CreateNotificationAsync(PipelineMessageDispatcherConsumer pipelineMessage);
    }
}
