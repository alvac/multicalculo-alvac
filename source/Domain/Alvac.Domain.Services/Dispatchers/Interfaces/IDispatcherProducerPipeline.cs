﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Pipeline;
using Alvac.Domain.Services.Pipeline.Producer;
using Alvac.Domain.Services.Queue;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Interfaces
{
    public interface IDispatcherProducerPipeline : IPipeline, IInitializer<DispatcherParameter>
    {
        Func<QueueMessage<InsurerQuotationRequest>, Task<PipelineMessageDispatcherProducer>> BuilderPipelineMessageAsync();

        Func<PipelineMessageDispatcherProducer, Task<PipelineMessageDispatcherProducer>> CreateThroughputLimiterAsync();

        Func<PipelineMessageDispatcherProducer, Task<PipelineMessageDispatcherProducer>> ProcessRequestAsync(IDispatcher dispatcher);
        
        Task EnqueueResponseAsync(PipelineMessageDispatcherProducer pipelineMessage);
    }
}
