﻿using Alvac.Domain.Entities;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace Alvac.Domain.Services.Dispatchers
{
    public class DispatcherData
    {
        #region Contructor

        public DispatcherData(IDictionary<string, object> parameters)
        {
            this.Id = Guid.NewGuid();
            this.Parameters = parameters;
        }

        #endregion

        #region Pubic Properties

        public Guid Id { get; set; }

        public IDictionary<string, object> Parameters { get; set; }

        #endregion
    }
}
