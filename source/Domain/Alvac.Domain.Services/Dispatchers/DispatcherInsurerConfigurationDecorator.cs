﻿using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Entities;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Dispatchers
{
    public class DispatcherInsurerConfigurationDecorator : DispatcherBase
    {
        private readonly IDispatcher _dispatcher;
        private readonly IEnumerable<InsurerConfiguration> _insurerConfigurations;

        public DispatcherInsurerConfigurationDecorator(IDispatcher dispatcher, IEnumerable<InsurerConfiguration> insurerConfigurations)
        {
            _insurerConfigurations = insurerConfigurations;
            _dispatcher = dispatcher;
        }

        public override Task<Contract.Response.BaseResponse> ProcessAsync(Contract.Request.BaseRequest request, IDictionary<string, object> insurerParameters)
        {
            insurerParameters = ConvertKeysToLowerCase(insurerParameters);

            foreach (var item in _insurerConfigurations.Where(x => x.RequestType == request.RequestType))
            {
                if (item.Required && !insurerParameters.Any(x => x.Key.Equals(item.Key, StringComparison.OrdinalIgnoreCase)))
                {
                    throw new AlvacException("Configuração '" + item.Key + "' da seguradora nulo.", Enum.DescriptionCode.InvalidRequest);
                }
            }

            return _dispatcher.ProcessAsync(request, insurerParameters);
        }

        public override Task UploadInsurerResponseStorage(object insurerResponse)
        {
            return _dispatcher.UploadInsurerResponseStorage(insurerResponse);
        }

        private IDictionary<string, object> ConvertKeysToLowerCase(IDictionary<string, object> dictionaries)
        {
            var convertedDictionatry = new Dictionary<string, object>();
            foreach (string key in dictionaries.Keys)
            {
                convertedDictionatry.Add(key.ToLower(), dictionaries[key]);
            }

            return convertedDictionatry;
        }
    }
}
