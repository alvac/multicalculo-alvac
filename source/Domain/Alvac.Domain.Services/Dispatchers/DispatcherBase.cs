﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.InsurerConfigurations;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Dispatchers
{
    public abstract class DispatcherBase : IDispatcher
    {
        #region Public Properties

        public DispatcherData DispatcherData { get; set; }

        #endregion

        #region IDispatcher Members

        public abstract Task<BaseResponse> ProcessAsync(BaseRequest request, IDictionary<string, object> insurerParameters);

        public virtual Task UploadInsurerResponseStorage(object insurerResponse)
        {
            throw new NotImplementedException();
        }

        public void Initializer(DispatcherData dispatcherData)
        {
            DispatcherData = dispatcherData;
        }

        #endregion



    }
}
