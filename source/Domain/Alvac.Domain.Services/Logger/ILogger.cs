﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services.Logger
{
    public interface ILogger
    {
        Task WriteVerboseAsync(string source, string eventName, string message);
        Task WriteVerboseAsync(string correlationId, string source, string eventName, string message);

        Task WriteInformationAsync(string source, string eventName, string message);
        Task WriteInformationAsync(string correlationId, string source, string eventName, string message);

        Task WriteWarningAsync(string source, string eventName, string message, Exception ex = null);
        Task WriteWarningAsync(string source, string eventName, string message, string correlationId, Exception ex = null);
        Task WriteWarningAsync(string source, string eventName, Exception ex);

        Task WriteErrorAsync(string source, string eventName, string message, Exception ex = null);
        Task WriteErrorAsync(string source, string eventName, string message, string correlationId, Exception ex = null);
        Task WriteErrorAsync(string source, string eventName, Exception ex);

        Task WriteCriticalAsync(string source, string eventName, string message, Exception ex = null);
        Task WriteCriticalAsync(string source, string eventName, string message, string correlationId, Exception ex = null);
        Task WriteCriticalAsync(string source, string eventName, Exception ex);
    }
}
