﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Services
{
    public interface ITimeProvider
    {
        DateTimeOffset Now { get; }
    }
}
