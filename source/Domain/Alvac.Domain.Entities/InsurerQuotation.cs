﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public abstract class InsurerQuotation
    {
        #region Public Properties

        /// <summary>
        /// Identificador da cotação na seguradora
        /// </summary>
        public int InsurerQuotationId { get; set; }

        /// <summary>
        /// Identificador da cotação
        /// </summary>
        public int QuotationId { get; set; }
        public virtual Quotation Quotation { get; set; }

        /// <summary>
        /// Identificador da conexão
        /// </summary>
        public int ConnectionId { get; set; }
        public virtual Connection Connection { get; set; }

        /// <summary>
        /// Seguradora
        /// </summary>
        public int InsurerId { get; set; }
        public virtual Insurer Insurer { get; set; }

        /// <summary>
        /// Indica o status da operação.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Indica o codigo de descrição da operação. 
        /// </summary>
        public DescriptionCode OperationDescriptionCode { get; set; }

        /// <summary>
        /// Descrição sobre o resultado da operação completa.
        /// </summary>
        public string OperationDescription { get; set; }

        /// <summary>
        /// Valor do prêmio do seguro.
        /// </summary>
        public decimal PremiumValue { get; set; }

        /// <summary>
        /// Data de validade do prêmio.
        /// </summary>
        public DateTimeOffset? ValidUntil { get; set; }

        /// <summary>
        /// Identificador da transação no gateway.
        /// </summary>
        public string GatewayId { get; set; }

        /// <summary>
        /// Código do orçamento no sistema da seguradora
        /// </summary>
        public string InsurerBudgetCode { get; set; }

        /// <summary>
        /// Nome do arquivo retornado pela seguradora
        /// </summary>
        public string BudgetFile { get; set; }

        /// <summary>
        /// Número máximo de retentativa
        /// </summary>
        public int? RetryMax { get; set; }

        /// <summary>
        /// Número de retentativa
        /// </summary>
        public int? RetryAttempts { get; set; }

        /// <summary>
        /// Intervalo deretentativa
        /// </summary>
        public int? RetryInterval { get; set; }

        /// <summary>
        /// Data da próxima retentativa
        /// </summary>
        public DateTimeOffset? RetryNext { get; set; }

        /// <summary>
        /// Última data de atualização da cotação seguradora.
        /// </summary>
        public DateTimeOffset LastUpdate { get; set; }

        public virtual Transaction Transaction { get; set; }

        public virtual Notification Notification { get; set; }

        public virtual ICollection<Payment> Payments { get; set; }

        #endregion

        #region Public Methods

        public abstract RequestType GetRequestType();

        #endregion
    }

}
