﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa os parametros de uma conexão
    /// </summary>
    public class ConnectionParameter
    {
        /// <summary>
        /// Identificador do parametro
        /// </summary>
        public int ConnectionParameterId { get; set; }

        /// <summary>
        /// Identificador da conexão
        /// </summary>
        public int ConnectionId { get; set; }
        public virtual Connection Connection { get; set; }

        /// <summary>
        /// Chave
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Valor
        /// </summary>
        public string Value { get; set; }
    }
}
