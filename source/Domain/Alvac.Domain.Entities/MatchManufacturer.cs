﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class MatchManufacturer
    {
        public int MatchManufacturerId { get; set; }

        public Insurer Insurer { get; set; }

        public int ManufacturerAlvacId { get; set; }
        public virtual Manufacturer ManufacturerAlvac { get; set; }

        public int ManufacturerInsurerId { get; set; }
        public virtual Manufacturer ManufacturerInsurer { get; set; }
    }
}
