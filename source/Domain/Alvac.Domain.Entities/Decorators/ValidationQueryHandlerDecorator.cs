﻿using Alvac.Domain.Entities.Queries;
using System.ComponentModel.DataAnnotations;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Decorators
{
    public class ValidationQueryHandlerDecorator<TQuery, TResult> : IQueryHandler<TQuery, TResult> where TQuery : IQuery<TResult>
    {
        private readonly IQueryHandler<TQuery, TResult> _handler;

        public ValidationQueryHandlerDecorator(IQueryHandler<TQuery, TResult> handler)
        {
            _handler = handler;
        }

        public async Task<TResult> HandleAsync(TQuery query)
        {
            var validationContext = new ValidationContext(query, null, null);
            Validator.ValidateObject(query, validationContext, validateAllProperties: true);

            return await this._handler.HandleAsync(query);
        }
    }
}
