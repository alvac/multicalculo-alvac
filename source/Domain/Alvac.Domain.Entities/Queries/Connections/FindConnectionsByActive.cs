﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Connections
{
    public class FindConnectionsByActive : IQuery<IEnumerable<Entities.Connection>>
    {
        [Required]
        public bool IsActive { get; set; }
    }
}
