﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Connections
{
    public class FindConnectionById : IQuery<Entities.Connection>
    {
        [Required]
        public int ConnectionId { get; set; }
    }
}
