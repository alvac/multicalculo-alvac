﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.InsurerConfigurations
{
    public class FindInsurerConfigurationById : IQuery<Entities.InsurerConfiguration>
    {
        [Required]
        public int InsurerConfigurationId { get; set; }
    }
}
