﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.InsurerConfigurations
{
    public class FindInsurerConfigurationByInsurer : IQuery<IEnumerable<Entities.InsurerConfiguration>>
    {
        [Required]
        public int Insurer { get; set; }

        [Required]
        public RequestType RequestType { get; set; }
    }
}
