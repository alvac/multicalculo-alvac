﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.InsurerQuotations
{
    public class GetInsurerQuotationsById : IQuery<IEnumerable<InsurerQuotation>>
    {
        [Required]
        public int[] InsurerQuotationId { get; set; }
    }
}
