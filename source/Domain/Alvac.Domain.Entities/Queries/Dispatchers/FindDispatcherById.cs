﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Dispatchers
{
    public class FindDispatcherById : IQuery<Entities.Dispatcher>
    {
        [Required]
        public int DispatcherId { get; set; }
    }
}
