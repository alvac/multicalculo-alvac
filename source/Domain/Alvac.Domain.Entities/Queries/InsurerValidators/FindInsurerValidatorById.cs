﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.InsurerValidators
{
    public class FindInsurerValidatorById : IQuery<Entities.InsurerValidator>
    {
        [Required]
        public int InsurerValidatorId { get; set; }
    }
}
