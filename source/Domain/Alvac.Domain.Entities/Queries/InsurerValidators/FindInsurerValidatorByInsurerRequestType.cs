﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.InsurerValidators
{
    public class FindInsurerValidatorByInsurerRequestType : IQuery<IEnumerable<Entities.InsurerValidator>>
    {
        [Required]
        public int Insurer { get; set; }

        [Required]
        public RequestType RequestType { get; set; }
    }
}
