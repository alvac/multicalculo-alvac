﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Vehicles
{
    public class GetVehicleById : IQuery<Vehicle>
    {
        [Required]
        public int VehicleId { get; set; }
    }
}
