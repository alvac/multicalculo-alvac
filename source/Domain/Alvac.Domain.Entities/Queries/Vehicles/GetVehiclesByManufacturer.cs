﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Vehicles
{
    public class GetVehiclesByManufacturer : IQuery<IQueryable<Vehicle>>
    {
        [Required]
        public int ManufacturerId { get; set; }

        [Required]
        public int ModelYear { get; set; }
    }
}
