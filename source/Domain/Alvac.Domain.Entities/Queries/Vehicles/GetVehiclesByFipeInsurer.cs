﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Vehicles
{
    public class GetVehiclesByFipeInsurer : IQuery<IEnumerable<Vehicle>>
    {
        [Required]
        public string FipeCode { get; set; }

        [Required]
        public int Insurer { get; set; }
    }
}
