﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.AccountConnections
{
    public class FindAccountConnectionsByAccountRequestType : IQuery<IEnumerable<AccountConnection>>
    {
        [Required]
        public int AccountId { get; set; }

        [Required]
        public RequestType RequestType { get; set; }
    }
}
