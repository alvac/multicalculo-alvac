﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.AccountInsurerTemplates
{
    public class FindAccountInsurerTemplateByAccountId : IQuery<IQueryable<Entities.AccountInsurerTemplate>>
    {
        [Required]
        public int AccountId { get; set; }

        [Required]
        public bool IsActive { get; set; }
    }
}
