﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.ConnectionParameters
{
    public class FindConnectionParametersById : IQuery<IEnumerable<ConnectionParameter>>
    {
        [Required]
        public int ConnectionParameterId { get; set; }
    }
}
