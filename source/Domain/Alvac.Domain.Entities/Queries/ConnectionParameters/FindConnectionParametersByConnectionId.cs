﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.ConnectionParameters
{
    public class FindConnectionParametersByConnectionId : IQuery<IQueryable<ConnectionParameter>>
    {
        [Required]
        public int ConnectionId { get; set; }
    }
}
