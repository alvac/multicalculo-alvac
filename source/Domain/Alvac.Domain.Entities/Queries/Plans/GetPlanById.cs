﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Plans
{
    public class GetPlanById : IQuery<Plan>
    {
        [Required]
        public int PlanId { get; set; }
    }
}
