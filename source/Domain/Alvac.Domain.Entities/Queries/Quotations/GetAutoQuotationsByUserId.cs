﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Quotations
{
    public class GetAutoQuotationsByUserId : IQuery<IQueryable<AutoQuotation>>
    {
        [Required]
        public string UserId { get; set; }
    }
}
