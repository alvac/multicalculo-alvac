﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Quotations
{
    public class GetQuotationById : IQuery<Quotation>
    {
        [Required]
        public int QuotationId { get; set; }
    }
}
