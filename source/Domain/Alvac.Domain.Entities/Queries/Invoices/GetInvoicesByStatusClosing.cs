﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Invoices
{
    public class GetInvoicesByStatusClosing : IQuery<IQueryable<Invoice>>
    {
        [Required]
        public InvoiceStatus Status { get; set; }

        [Required]
        public DateTimeOffset Closing { get; set; }
    }
}
