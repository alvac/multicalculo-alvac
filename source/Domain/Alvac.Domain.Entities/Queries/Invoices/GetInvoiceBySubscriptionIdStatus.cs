﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Invoices
{
    public class GetInvoiceBySubscriptionIdStatus : IQuery<Invoice>
    {
        [Required]
        public int SubscriptionId { get; set; }

        [Required]
        public InvoiceStatus Status { get; set; }
    }
}
