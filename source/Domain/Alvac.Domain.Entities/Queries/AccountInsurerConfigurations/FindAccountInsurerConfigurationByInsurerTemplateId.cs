﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.AccountInsurerConfigurations
{
    public class FindAccountInsurerConfigurationByInsurerTemplateId : IQuery<IEnumerable<Entities.AccountInsurerConfiguration>>
    {
        [Required]
        public int AccountInsurerTemplateId { get; set; }

        [Required]
        public int Insurer { get; set; }
    }
}
