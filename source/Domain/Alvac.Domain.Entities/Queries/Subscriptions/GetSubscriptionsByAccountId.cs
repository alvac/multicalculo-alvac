﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Subscriptions
{
    public class GetSubscriptionsByAccountId : IQuery<IQueryable<Subscription>>
    {
        [Required]
        public int AccountId { get; set; }
    }
}
