﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Subscriptions
{
    public class GetSubscriptionById : IQuery<Subscription>
    {
        [Required]
        public int SubscriptionId { get; set; }
    }
}
