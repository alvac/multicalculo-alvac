﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Accounts
{
    public class FindAccountById : IQuery<Entities.Account>
    {
        [Required]
        public int AccountId { get; set; }
    }
}
