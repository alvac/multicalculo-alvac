﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Users
{
    public class GetUserById : IQuery<User>
    {
        [Required]
        public string Id { get; set; }
    }
}
