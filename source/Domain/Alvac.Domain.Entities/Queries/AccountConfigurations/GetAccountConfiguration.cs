﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.AccountConfigurations
{
    public class GetAccountConfiguration : IQuery<AccountConfiguration>
    {
        [Required]
        public int AccountId { get; set; }
    }
}
