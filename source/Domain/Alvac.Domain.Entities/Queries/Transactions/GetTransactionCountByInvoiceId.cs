﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Queries.Transactions
{
    public class GetTransactionCountByInvoiceId : IQuery<int>
    {
        [Required]
        public int InvoiceId { get; set; }
    }
}
