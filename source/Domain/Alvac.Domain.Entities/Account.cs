﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Auto;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa uma conta da corretora
    /// </summary>
    public class Account
    {
        /// <summary>
        /// Identificador da conta.
        /// </summary>
        public int AccountId { get; set; }

        /// <summary>
        /// Nome do representante legal.
        /// </summary>
        public string LegalRepresentative { get; set; }

        /// <summary>
        /// Nível qualificador do cliente.
        /// </summary>
        public Qualifier Qualifier { get; set; }
        
        /// <summary>
        /// SUSEP
        /// </summary>
        public string Susep { get; set; }

        /// <summary>
        /// Tipo de pessoa
        /// </summary>
        public PersonType PersonType { get; set; }

        /// <summary>
        /// Documento RG/CPF
        /// </summary>
        public string Document { get; set; }

        /// <summary>
        /// Nome e/ou razao social
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// CEP
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Rua
        /// </summary>
        public string Street { get; set; }

        /// <summary>
        /// Bairro
        /// </summary>
        public string Neighborhood { get; set; }

        /// <summary>
        /// Estado
        /// </summary>
        public string State { get; set; }

        /// <summary>
        /// Cidade
        /// </summary>
        public string City { get; set; }        

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Email de cobrança.
        /// </summary>
        public string ChargeEmail { get; set; }

        /// <summary>
        /// Telefone
        /// </summary>
        public string Phone { get; set; }

        /// <summary>
        /// Celular
        /// </summary>
        public string MobilePhone { get; set; }

        /// <summary>
        /// Site da empresa / pessoa.
        /// </summary>
        public string Site { get; set; }

        /// <summary>
        /// Determina se a conta esta ativa
        /// </summary>
        public bool IsActive { get; set; }

        #region Relationship

        public virtual AccountConfiguration AccountConfiguration { get; set; }

        public virtual ICollection<Quotation> Quotations { get; set; }

        public virtual ICollection<User> Users { get; set; }

        public virtual ICollection<Subscription> Subscriptions { get; set; }

        public virtual ICollection<AccountInsurerTemplate> AccountInsurerTemplates { get; set; }

        #endregion
    }
}
