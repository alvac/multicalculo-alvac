﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Repositories
{
    public interface IRepository<TEntity> where TEntity : class
    {
        // Summary:
        //     Adds a TEntity to the repository
        //
        // Parameters:
        //   entity:
        //     Entity instance to add on repository
        Task AddAsync(TEntity entity);

        // Summary:
        //     Adds a range TEntity to the repository
        //
        // Parameters:
        //   entity:
        //     Entity instance to add on repository
        Task AddRangeAsync(IEnumerable<TEntity> entities);

        // Summary:
        //     Updates a TEntity to the repository
        //
        // Parameters:
        //   entity:
        //     Entity instance to update on repository
        Task UpdateAsync(TEntity entity);

        //
        // Summary:
        //     Removes a existing TEntity from the repository
        //
        // Parameters:
        //   entity:
        //     Entity instance to remove from repository
        Task RemoveAsync(TEntity entity);

        //
        // Summary:
        //     Gets a collection of TEntity with all entities on the repository
        Task<IEnumerable<TEntity>> GetAllAsync();

        //
        // Summary:
        //     Get a instance of TEntity by entity key
        //
        // Parameters:
        //   id:
        //     Entity key
        Task<TEntity> GetByIdAsync(object id);     

        //
        // Summary:
        //     Gets a generic IQueryable member.  AsQueryable is not async because it not
        //     represents an IO operation, since the query is built in memory. The submission
        //     of an IQueryable object (i.e. calling ToList() method) should be async.
        IQueryable<TEntity> AsQueryable();

        Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);

        Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate);     
       
    }
}
