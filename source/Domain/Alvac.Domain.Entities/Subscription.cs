﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa uma assinatura de uma conta
    /// </summary>
    public class Subscription
    {
        /// <summary>
        /// Identificador da assinatura
        /// </summary>
        public int SubscriptionId { get; set; }

        /// <summary>
        /// Identificador da conta
        /// </summary>
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Identificador do plano
        /// </summary>
        public int PlanId { get; set; }
        public virtual Plan Plan { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        public RequestType RequestType { get; set; }

        /// <summary>
        /// Tipo de assinatura
        /// </summary>
        public SubscriptionType Type { get; set; }

        /// <summary>
        /// Status da assinatura
        /// </summary>
        public SubscriptionStatus Status { get; set; }

        /// <summary>
        /// Data que à assinatura expira
        /// </summary>
        public DateTimeOffset? Expiration { get; set; }

        /// <summary>
        /// Data de criação
        /// </summary>
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Ultima atualização
        /// </summary>
        public DateTimeOffset LastUpdate { get; set; }

        #region Relationship

        public virtual ICollection<Invoice> Invoices { get; set; }

        #endregion
    }
}
