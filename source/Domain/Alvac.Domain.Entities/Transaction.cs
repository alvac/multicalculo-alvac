﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;
using System.ComponentModel.DataAnnotations.Schema;
using Alvac.Domain.Entities.Auto;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa uma transação em uma fatura
    /// </summary>
    public class Transaction
    {
        /// <summary>
        /// Identificador da cotação na seguradora
        /// </summary>
        public int InsurerQuotationId { get; set; }
        public virtual InsurerQuotation InsurerQuotation { get; set; }

        /// <summary>
        /// Identificador da fatura
        /// </summary>
        public int InvoiceId { get; set; }
        public virtual Invoice Invoice { get; set; }

        /// <summary>
        /// Valor da transação
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Data de criação da transação
        /// </summary>
        public DateTimeOffset Created { get; set; }
    }
}
