﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class Insurer
    {
        /// <summary>
        /// Identificador da seguradora
        /// </summary>
        public int InsurerId { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }

        public virtual ICollection<Manufacturer> Manufacturers { get; set; }

        public virtual ICollection<AccountConnection> AccountConnections { get; set; }

        public virtual ICollection<Dispatcher> Dispatcher { get; set; }

        public virtual ICollection<InsurerConfiguration> InsurerConfigurations { get; set; }

        public virtual ICollection<InsurerQuotation> InsurerQuotations { get; set; }

        public virtual ICollection<InsurerValidator> InsurerValidators { get; set; }

    }
}
