﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class VehiclePrice
    {
        public int VehiclePriceId { get; set; }

        public int VehicleId { get; set; }
        public virtual Vehicle Vehicle { get; set; }

        public int Year { get; set; }

        public decimal Price { get; set; }
    }
}
