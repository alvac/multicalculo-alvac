﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class AccountInsurerConfiguration
    {
        /// <summary>
        /// Identificador da configuração
        /// </summary>
        public int AccountInsurerConfigurationId { get; set; }

        public int InsurerConfigurationId { get; set; }
        public virtual InsurerConfiguration InsurerConfiguration { get; set; }

        public int AccountInsurerTemplateId { get; set; }
        public virtual AccountInsurerTemplate AccountInsurerTemplate { get; set; }
        
        /// <summary>
        /// Valor
        /// </summary>
        public string Value { get; set; }
 
    }
}
