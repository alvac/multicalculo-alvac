﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;

namespace Alvac.Domain.Entities
{
    public class InsurerValidator
    {
        public int InsurerValidatorId { get; set; }

        public int InsurerId { get; set; }
        public virtual Insurer Insurer { get; set; }

        public RequestType RequestType { get; set; }

        public string Description { get; set; }

        public string FullAssemblyName { get; set; }
    }
}
