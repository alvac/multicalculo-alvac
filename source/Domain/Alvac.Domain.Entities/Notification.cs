﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class Notification
    {
        /// <summary>
        /// Identificador.
        /// </summary>
        public int InsurerQuotationId { get; set; }
        public virtual InsurerQuotation InsurerQuotation { get; set; }
        
        /// <summary>
        /// Indica o status da operação.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Indica o codigo de descrição da operação. 
        /// </summary>
        public DescriptionCode OperationDescriptionCode { get; set; }

        /// <summary>
        /// Descrição sobre o resultado da operação completa.
        /// </summary>
        public string OperationDescription { get; set; }

        /// <summary>
        /// Identificador passado pela aplicação para corelação.
        /// </summary>
        public string CorrelationId { get; set; }

        /// <summary>
        /// Tipo de chamada a ser realizada para entregar a notificação.
        /// </summary>
        public CallType CallType { get; set; }

        /// <summary>
        /// Caminho de entrega da notificação.
        /// </summary>
        public string Path { get; set; }

        /// <summary>
        /// Data de criação da notificação.
        /// </summary>
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Hora/data da ultima atualização.
        /// </summary>
        public DateTimeOffset? LastUpdate { get; set; }


        
    }
}
