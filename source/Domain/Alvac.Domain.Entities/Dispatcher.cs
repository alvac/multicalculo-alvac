﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa uma implementação da seguradora
    /// </summary>
    public class Dispatcher
    {
        /// <summary>
        /// Identificador do despachante
        /// </summary>
        public int DispatcherId { get; set; }

        /// <summary>
        /// Seguradora
        /// </summary>
        public int InsurerId { get; set; }
        public virtual Insurer Insurer { get; set; }

        /// <summary>
        /// Tecnologia
        /// </summary>
        public Technology Technology { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        public RequestType RequestType { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descrição
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Despachante ativo
        /// </summary>
        public bool IsActive { get; set; }

        #region Relationship

        public virtual ICollection<Connection> Connections { get; set; }

        #endregion
    }
}
