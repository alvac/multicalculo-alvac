﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Auto
{
    public class AdditionalDriver
    {
        /// <summary>
        /// Identificador do condutor adicional
        /// </summary>
        public int AdditionalDriverId { get; set; }

        /// <summary>
        /// Identificador da cotação de automóvel
        /// </summary>
        public int QuotationId { get; set; }
        public AutoQuotation AutoQuotation { get; set; }

        /// <summary>
        /// Nome completo
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Sexo
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Data de nascimento.
        /// </summary>
        public DateTime? BirthDate { get; set; }

        /// <summary>
        /// Estado civíl.
        /// </summary>
        public MaritalStatus MaritalStatus { get; set; }

        /// <summary>
        /// Tempo de habilitação.
        /// </summary>
        public int? DriverLicenseYears { get; set; }

        /// <summary>
        /// Relação com o segurado.
        /// </summary>
        public Relationship RelationshipWithInsured { get; set; }

        /// <summary>
        /// Possui veículo em seu nome
        /// </summary>
        public bool HasRegisteredVehicle { get; set; }

        /// <summary>
        /// Mora com o condutor principal.
        /// </summary>
        public bool LivesWithMainDriver { get; set; }

        /// <summary>
        /// Número de dias que utiliza o veículo na semana.
        /// </summary>
        public int NumberOfUseDaysPerWeek { get; set; }
    }
}
