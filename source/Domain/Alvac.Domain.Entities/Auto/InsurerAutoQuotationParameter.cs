﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Auto
{
    public class InsurerAutoQuotationParameter
    {
        /// <summary>
        /// Identificador da cotação seguradora.
        /// </summary>
        public int InsurerAutoQuotationId { get; set; }
        public virtual InsurerAutoQuotation InsurerAutoQuotation { get; set; }

        /// <summary>
        /// Parametros da seguradora usado na cotação.
        /// </summary>
        public string Parameters { get; set; }
    }
}
