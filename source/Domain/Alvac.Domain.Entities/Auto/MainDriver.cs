﻿using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Auto
{
    public class MainDriver
    {
        public int QuotationId { get; set; }
        public virtual AutoQuotation AutoQuotation { get; set; }

        /// <summary>
        /// Identificador descrição de atividade de trabalho.
        /// </summary>
        public int WorkActivityId { get; set; }
        public WorkActivity WorkActivity { get; set; }

        /// <summary>
        /// Cliente que esta realizando a contação é o segurado.
        /// </summary>
        public bool IsInsured { get; set; }

        /// <summary>
        /// Cliente que esta realizando a contação é o dono do veículo.
        /// </summary>
        public bool IsVehicleOwner { get; set; }

        /// <summary>
        /// Nome completo
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Telefone celular do cliente que esta realizando a contação.
        /// </summary>
        public string MobilePhoneNumber { get; set; }

        /// <summary>
        /// Telefone fixo do cliente que esta realizando a contação.
        /// </summary>
        public string PhoneNumber { get; set; }

        /// <summary>
        /// Email do cliente que esta realizando a contação.
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Sexo
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Data de nascimento.
        /// </summary>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Cpf.
        /// </summary>
        public string Document { get; set; }

        /// <summary>
        /// Estado civíl.
        /// </summary>
        public MaritalStatus MaritalStatus { get; set; }

        /// <summary>
        /// Cep
        /// </summary>
        public string ZipCode { get; set; }

        /// <summary>
        /// Tempo de habilitação.
        /// </summary>
        public int DriverLicenseYears { get; set; }

        /// <summary>
        /// Número da carteira de habilitação
        /// </summary>
        public string DriverLicenseNumber { get; set; }

        /// <summary>
        /// Escolaridade
        /// </summary>
        public Schooling Schooling { get; set; }

        /// <summary>
        /// Relação com o segurado.
        /// </summary>
        public Relationship RelationshipWithInsured { get; set; }

        /// <summary>
        /// Vítima de roubo de veículos nos últimos 24 meses.
        /// </summary>
        public bool HasBeenStolenInTheLast24Months { get; set; }

        /// <summary>
        /// Tipo de residencia ondse fica o veiculo.
        /// </summary>
        public ResidentialType ResidentialType { get; set; }

        #region Relationship

        public virtual ICollection<MainDriverDependent> MainDriverDependents { get; set; }

        #endregion
    }
}
