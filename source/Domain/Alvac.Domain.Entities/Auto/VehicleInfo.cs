﻿
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Auto
{
    /// <summary>
    /// Informações do veiculo
    /// </summary>
    public class VehicleInfo
    {
        public int QuotationId { get; set; }
        public virtual AutoQuotation AutoQuotation { get; set; }

        /// <summary>
        /// Marca do veículo. 
        /// </summary>
        public string Brand { get; set; }

        /// <summary>
        /// Modelo do veículo.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Ano de fabricação veículo.
        /// </summary>
        public int ManufacturingYear { get; set; }

        /// <summary>
        /// Ano do modelo do veiculo.
        /// </summary>
        public int ModelYear { get; set; }

        /// <summary>
        /// Código fipe da versão do veículo.
        /// </summary>
        public string FipeCode { get; set; }

        /// <summary>
        /// Tipo de combutível do veículo.
        /// </summary>
        public FuelType Fuel { get; set; }

        /// <summary>
        /// Descrição do modelo do veículo.
        /// </summary>
        public string ModelDescription { get; set; }

        /// <summary>
        /// Veículo zero KM
        /// </summary>
        public bool IsNew { get; set; }

        /// <summary>
        /// Dispositivos de alarme.
        /// </summary>
        public bool DevicesAlarm { get; set; }

        /// <summary>
        /// Veículo blindado.
        /// </summary>
        public bool IsArmored { get; set; }

        /// <summary>
        /// Valor covertura blindagem.
        /// </summary>
        public decimal? ArmoryCoverage { get; set; }

        /// <summary>
        /// Possui kit gás
        /// </summary>
        public bool HasNaturalGasKit { get; set; }

        /// <summary>
        /// Valor covertura kit gas.
        /// </summary>
        public decimal? NaturalGasKitCoverage { get; set; }

        /// <summary>
        /// Dados de licença do veículo
        /// </summary>
        public string PlateNumber { get; set; }

        /// <summary>
        /// Número do CHASSI
        /// </summary>
        public string ChassisNumber { get; set; }

        /// <summary>
        /// Categria do valor de cobertura
        /// </summary>
        public ValueCategory ValueCategory { get; set; }

        /// <summary>
        /// Valor determinado da cobertura
        /// </summary>
        public decimal? DeterminedValue { get; set; }

        /// <summary>
        /// Valor referenciado da cobertura
        /// </summary>
        public decimal? ReferencedValue { get; set; }

        /// <summary>
        /// Porcentagem do valor de cobertura.
        /// </summary>
        public int? ReferencedPercentage { get; set; }

        /// <summary>
        /// Identifica se o veículo é financiado.
        /// </summary>
        public bool IsFinanced { get; set; }

        /// <summary>
        /// Dispositivos de bloquio de ingnição.
        /// </summary>
        public virtual ICollection<IgnitionBlocker> DevicesIgnitionBlocker { get; set; }

        /// <summary>
        /// Dispositivos rastreador.
        /// </summary>
        public virtual ICollection<DeviceTracker> DevicesTracker { get; set; }
    }
}
