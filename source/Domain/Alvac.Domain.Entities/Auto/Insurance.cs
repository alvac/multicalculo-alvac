﻿using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Auto
{
    /// <summary>
    /// Representa um seguro
    /// </summary>
    public class Insurance
    {
        public int QuotationId { get; set; }
        public virtual AutoQuotation AutoQuotation { get; set; }

        /// <summary>
        /// Tipo cobertura da apólice que deseja contratar
        /// </summary>
        public CoveragePlanType CoveragePlanType { get; set; }
        
        /// <summary>
        /// Seguradora atual
        /// </summary>
        public Insurer CurrentInsuranceCompany { get; set; }

        /// <summary>
        /// Tipo cobertura da apólice atual
        /// </summary>
        public CoveragePlanType CurrentCoveragePlanType { get; set; }

        /// <summary>
        /// Classe de Bônus da apólice atual
        /// </summary>
        public int CurrentInsuranceBonus { get; set; }

        /// <summary>
        /// Número da apólice atual
        /// </summary>
        public string CurrentInsuranceNumber { get; set; }

        /// <summary>
        /// Sinistros ocorreram na apólice
        /// </summary>
        public int ClaimQuantity { get; set; }

        /// <summary>
        /// Data do início da apólice atual
        /// </summary>
        public DateTime? CurrentInsurancePolicyDate { get; set; }

        /// <summary>
        /// Data do vencimento da apólice atual
        /// </summary>
        public DateTime? CurrentInsurancePolicyExpirationDate { get; set; }

        /// <summary>
        /// Corretor detem a apólice.
        /// </summary>
        public bool IsPolicyOwner { get; set; }

        /// <summary>
        /// Data de início da apólice
        /// </summary>
        public DateTime InsurancePolicyInitialDate { get; set; }

        /// <summary>
        /// Data de início da apólice
        /// </summary>
        public DateTime InsurancePolicyFinalDate { get; set; }
    }
}
