﻿using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Auto
{
    /// <summary>
    /// Representa o resultado de uma cotação de automovel em uma seguradora
    /// </summary>
    public class InsurerAutoQuotation : InsurerQuotation
    {
        #region Public Properties

        /// <summary>
        /// Categoria do valor do premio.
        /// </summary>
        public ValueCategory PremiumValueCategory { get; set; }

        /// <summary>
        /// Tipo de Franquia
        /// </summary>
        public DeductibleType DeductibleType { get; set; }

        /// <summary>
        /// Valor da franquia.
        /// </summary>
        public decimal DeductibleValue { get; set; }

        /// <summary>
        /// Tipo de assistencia.
        /// </summary>
        public AssistanceType AssistanceType { get; set; }

        /// <summary>
        /// Tipo de coberturas
        /// </summary>
        public CoveragePlanType CoveragePlanType { get; set; }

        /// <summary>
        /// Coberturas de vidros
        /// </summary>
        public GlassesCoverages GlassesCoverages { get; set; }

        /// <summary>
        /// Valor cobertura danos materias.
        /// </summary>
        public decimal MaterialDamages { get; set; }

        /// <summary>
        /// Valor cobertura danos morais.
        /// </summary>
        public decimal MoralDamages { get; set; }

        /// <summary>
        /// Valor cobertura danos corporais.
        /// </summary>
        public decimal BodyDamages { get; set; }

        /// <summary>
        /// Acidentes pessoais por passageiro
        /// </summary>
        public decimal PersonalAccidentPassenger { get; set; }

        /// <summary>
        /// Despesas médicas hospitalares
        /// </summary>
        public decimal MedicalExpenses { get; set; }

        /// <summary>
        /// Despesas extras
        /// </summary>
        public decimal ExtraExpenses { get; set; }

        /// <summary>
        /// Guincho
        /// </summary>
        public bool HasWinch { get; set; }

        /// <summary>
        /// Km de cobertura
        /// </summary>
        public decimal WinchKm { get; set; }

        /// <summary>
        /// Possui carro cortezia/reserva
        /// </summary>
        public bool HasCourtesyCar { get; set; }

        /// <summary>
        /// Dias carro cortezia/reserva
        /// </summary>
        public int CourtesyCarDays { get; set; }

        public virtual InsurerAutoQuotationParameter InsurerAutoQuotationParameter { get; set; }

        #endregion

        #region InsurerQuotation Members

        public override RequestType GetRequestType()
        {
            return RequestType.AutoQuotation;
        }

        #endregion



    }
}
