﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Auto
{
    public class MainDriverDependent
    {
        public int MainDriverDependentId { get; set; }

        public int MainDriverId { get; set; }
        public virtual MainDriver MainDriver { get; set; }

        public int Age { get; set; }

        public Gender Gender { get; set; }

        public bool LivesWithMainDriver { get; set; }
    }
}
