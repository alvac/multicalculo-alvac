﻿using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Auto
{
    public class VehicleUse
    {
        public int QuotationId { get; set; }
        public virtual AutoQuotation AutoQuotation { get; set; }

        /// <summary>
        /// Lista tipo de utilização do veiculo.
        /// </summary>
        public UseType UseType { get; set; }

        /// <summary>
        /// Cep de pernoite
        /// </summary>
        public string OvernightZipCode { get; set; }

        /// <summary>
        /// Cep de circulação
        /// </summary>
        public string MovementZipCode { get; set; }

        /// <summary>
        /// Tipo de utilização comercial.
        /// </summary>
        public CommercialUseType CommercialUseType { get; set; }

        /// <summary>
        /// Periodo de us comercial.
        /// </summary>
        public CommercialUsePeriod CommercialUsePeriod { get; set; }

        /// <summary>
        /// Onde o segurado possui garagem.
        /// </summary>
        public Garage GarageLocal { get; set; }

        /// <summary>
        /// Quilometragem diária do veículos
        /// </summary>
        public int DailyDistance { get; set; }
    }
}
