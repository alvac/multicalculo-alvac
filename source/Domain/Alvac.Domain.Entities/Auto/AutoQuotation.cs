﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Auto
{
    public class AutoQuotation : Quotation
    {
        public virtual VehicleInfo VehicleInfo { get; set; }

        public virtual VehicleUse VehicleUse { get; set; }
        
        public virtual MainDriver MainDriver { get; set; }

        public virtual VehicleOwner VehicleOwner { get; set; }

        public virtual Insurance Insurance { get; set; }
                
        #region Relationship
        
        public virtual ICollection<AdditionalDriver> AdditionalDrivers { get; set; }

        #endregion
    }
}
