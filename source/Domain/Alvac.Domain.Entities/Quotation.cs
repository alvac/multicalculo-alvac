﻿using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public abstract class Quotation
    {
        public int QuotationId { get; set; }

        /// <summary>
        /// Iidentificador da conta.
        /// </summary>
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Iidentificador do usuário.
        /// </summary>
        public string UserId { get; set; }
        public virtual User User { get; set; }

        /// <summary>
        /// IP do originador do pedido.
        /// </summary>
        public string Ip { get; set; }

        /// <summary>
        /// Indica se a(s) cotações na seguradoras foram processadas.
        /// </summary>
        public bool Processed { get; set; }

        /// <summary>
        /// Indica o codigo de descrição da operação. 
        /// </summary>
        public DescriptionCode OperationDescriptionCode { get; set; }

        /// <summary>
        /// Descrição sobre o resultado da operação completa.
        /// </summary>
        public string OperationDescription { get; set; }

        /// <summary>
        /// Data de requisição
        /// </summary>
        public DateTimeOffset RequestDateTime { get; set; }

        /// <summary>
        /// Hora/data da ultima atualização.
        /// </summary>
        public DateTimeOffset LastUpdate { get; set; }

        /// <summary>
        /// Identificador passado pela aplicação para corelação.
        /// </summary>
        public string NotificationCorrelationId { get; set; }

        /// <summary>
        /// Tipo de chamada para notificação.
        /// </summary>
        public CallType NotificationCallType { get; set; }

        /// <summary>
        /// Caminho de chamada para notificação.
        /// </summary>
        public string NotificationPath { get; set; }

        public virtual Insured Insured { get; set; }

        public virtual ICollection<InsurerQuotation> InsurerQuotations { get; set; }
    }
}
