﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;
using System.ComponentModel.DataAnnotations;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa uma conexão de uma conta
    /// </summary>
    public class AccountConnection
    {
        /// <summary>
        /// Identificador da conta
        /// </summary>
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Seguradora
        /// </summary>
        public int InsurerId { get; set; }
        public virtual Insurer Insurer { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        public RequestType RequestType { get; set; }

        /// <summary>
        /// Identificador da conexão
        /// </summary>
        public int ConnectionId { get; set; }
        public virtual Connection Connection { get; set; }
    }
}
