﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;

namespace Alvac.Domain.Entities
{
    public class InsurerConfiguration
    {
        public int InsurerConfigurationId { get; set; }

        /// <summary>
        /// Seguradora
        /// </summary>
        public int InsurerId { get; set; }
        public virtual Insurer Insurer { get; set; }

        /// <summary>
        /// Parâmetro obrigatório.
        /// </summary>
        public bool Required { get; set; }

        /// <summary>
        /// Nome de exibição do campo.
        /// </summary>
        public string DisplayName { get; set; }

        /// <summary>
        /// Tipo do parametro.
        /// </summary>
        public string Type { get; set; }

        /// <summary>
        /// Chave
        /// </summary>
        public string Key { get; set; }

        /// <summary>
        /// Preenchido em caso de campo 'select'
        /// </summary>
        public string Options { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        public RequestType RequestType { get; set; }

        public ICollection<AccountInsurerConfiguration> AccountInsurerConfigurations { get; set; }
        
    }
}
