﻿using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class Insured
    {
        public int QuotationId { get; set; }
        public virtual Quotation Quotation { get; set; }

        /// <summary>
        /// Identificador descrição de atividade de trabalho.
        /// </summary>
        public int WorkActivityId { get; set; }
        public virtual WorkActivity WorkActivity { get; set; }

        /// <summary>
        /// Nome completo
        /// </summary>
        public string FullName { get; set; }

        /// <summary>
        /// Sexo
        /// </summary>
        public Gender Gender { get; set; }

        /// <summary>
        /// Data de nascimento.
        /// </summary>
        public DateTime Birthdate { get; set; }

        /// <summary>
        /// Tipo de pessoa
        /// None = 0,
        /// Natural = 1,
        /// Legal = 2
        /// </summary>
        public PersonType PersonType { get; set; }

        /// <summary>
        /// Cpf / Cnpj
        /// </summary>
        public string Document { get; set; }

        /// <summary>
        /// Estado civíl.
        /// </summary>
        public MaritalStatus MaritalStatus { get; set; }

        /// <summary>
        /// Cep
        /// </summary>
        public string ZipCode { get; set; }

    }
}
