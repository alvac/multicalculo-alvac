﻿using Alvac.Domain.Entities.Auto;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa um usuario no sistema
    /// </summary>
    public class User : IdentityUser
    {
        /// <summary>
        /// Identificador da conta.
        /// </summary>
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }

        /// <summary>
        /// Determina se o usuário esta ativo.
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Nome do usuário
        /// </summary>
        public string Name { get; set; }
        
        /// <summary>
        /// Data de criação
        /// </summary>
        public DateTimeOffset Created { get; set; }

        #region Relationship

        /// <summary>
        /// Cotacoes realizadas pelo usuário. 
        /// </summary>
        public virtual ICollection<Quotation> Quotations { get; set; }

        #endregion

        #region Methods
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<User> manager, string authenticationType)
        {
            // Observe que o AuthenticationType deve corresponder ao definido em CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, authenticationType);
            // Adicionar user claims aqui
            return userIdentity;
        }
        #endregion
    }
}
