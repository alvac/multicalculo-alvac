﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Transactions
{
    public class CreateTransaction : ICommand<Transaction>
    {
        [Required]
        public int InvoiceId { get; set; }

        /// <summary>
        /// Identificador da cotação na seguradora
        /// </summary>
        [Required]
        public int InsurerQuotationId { get; set; }

        /// <summary>
        /// Valor da transação
        /// </summary>
        [Required]
        public decimal Value { get; set; }
    }
}
