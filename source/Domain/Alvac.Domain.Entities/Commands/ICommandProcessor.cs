﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands
{
    public interface ICommandProcessor
    {
        Task<TResult> ProcessAsync<TResult>(ICommand<TResult> command);
    }
}
