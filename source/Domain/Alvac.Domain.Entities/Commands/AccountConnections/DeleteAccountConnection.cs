﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.AccountConnections
{
    public class DeleteAccountConnection : ICommand<bool>
    {
        [Required]
        public int AccountId { get; set; }

        [Required]
        public int ConnectionId { get; set; }
    }
}
