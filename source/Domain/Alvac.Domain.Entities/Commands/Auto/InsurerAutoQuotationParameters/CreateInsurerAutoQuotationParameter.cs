﻿using Alvac.Domain.Entities.Auto;
using System.ComponentModel.DataAnnotations;

namespace Alvac.Domain.Entities.Commands.Auto.InsurerAutoQuotationParameters
{
    public class CreateInsurerAutoQuotationParameter : ICommand<InsurerAutoQuotationParameter>
    {
        /// <summary>
        /// Identificador da cotação seguradora.
        /// </summary>
        [Required]
        public int InsurerAutoQuotationId { get; set; }

        /// <summary>
        /// Parametros da seguradora usado na cotação.
        /// </summary>
        [Required]
        public string Parameters { get; set; }
    }
}