﻿using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.ComponentModel.DataAnnotations;

namespace Alvac.Domain.Entities.Commands.Auto.InsurerAutoQuotations
{
    public class CreateInsurerAutoQuotation : ICommand<InsurerAutoQuotation>
    {
        /// <summary>
        /// Identificador da cotação
        /// </summary>
        [Required]
        public int QuotationId { get; set; }

        /// <summary>
        /// Identificador da conexão
        /// </summary>
        [Required]
        public int ConnectionId { get; set; }

        /// <summary>
        /// Seguradora
        /// </summary>
        [Required]
        public int Insurer { get; set; }

        /// <summary>
        /// Indica o status da operação.
        /// </summary>
        public Status Status { get; set; }

        /// <summary>
        /// Indica o codigo de descrição da operação.
        /// </summary>
        public DescriptionCode OperationDescriptionCode { get; set; }

        /// <summary>
        /// Descrição sobre o resultado da operação completa.
        /// </summary>
        public string OperationDescription { get; set; }

        /// <summary>
        /// Tipo de Franquia
        /// </summary>
        [Required]
        public DeductibleType DeductibleType { get; set; }

        /// <summary>
        /// Número máximo de retentativa
        /// </summary>
        public int? RetryMax { get; set; }

        /// <summary>
        /// Número de retentativa
        /// </summary>
        public int? RetryAttempts { get; set; }

        /// <summary>
        /// Intervalo deretentativa
        /// </summary>
        public int? RetryInterval { get; set; }

        /// <summary>
        /// Data da próxima retentativa
        /// </summary>
        public DateTimeOffset? RetryNext { get; set; }
    }
}