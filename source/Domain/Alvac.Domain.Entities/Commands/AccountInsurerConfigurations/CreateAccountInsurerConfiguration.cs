﻿using System.ComponentModel.DataAnnotations;

namespace Alvac.Domain.Entities.Commands.AccountInsurerConfigurations
{
    public class CreateAccountInsurerConfiguration : ICommand<AccountInsurerConfiguration>
    {
        [Required]
        public int InsurerConfigurationId { get; set; }

        [Required]
        public int AccountInsurerTemplateId { get; set; }

        public string Value { get; set; }
    }
}