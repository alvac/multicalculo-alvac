﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.AccountInsurerTemplates
{
    public class DeleteAccountInsurerTemplate : ICommand<bool>
    {
        [Required]
        public int AccountInsurerTemplateId { get; set; }
    }
}
