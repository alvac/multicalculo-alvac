﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.AccountInsurerTemplates
{
    public class CreateAccountInsurerTemplate : ICommand<AccountInsurerTemplate>
    {
        [Required]
        public string Name { get; set; }

        [Required]
        public string Description { get; set; }

        [Required]
        public int AccountId { get; set; }

        [Required]
        public RequestType RequestType { get; set; }
    }
}
