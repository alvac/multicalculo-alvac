﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Notifications
{
    public class CreateNotification : ICommand<Notification>
    {
        /// <summary>
        /// Identificador.
        /// </summary>
        [Required]
        public int InsurerQuotationId { get; set; }

        /// <summary>
        /// Indica o status da operação.
        /// </summary>
        [Required]
        public Status Status { get; set; }

        /// <summary>
        /// Indica o codigo de descrição da operação. 
        /// </summary>
        [Required]
        public DescriptionCode OperationDescriptionCode { get; set; }

        /// <summary>
        /// Descrição sobre o resultado da operação completa.
        /// </summary>
        public string OperationDescription { get; set; }

        /// <summary>
        /// Identificador passado pela aplicação para corelação.
        /// </summary>
        [Required]
        public string CorrelationId { get; set; }

        /// <summary>
        /// Tipo de chamada a ser realizada para entregar a notificação.
        /// </summary>
        [Required]
        public CallType CallType { get; set; }

        /// <summary>
        /// Caminho de entrega da notificação.
        /// </summary>
        [Required]
        public string Path { get; set; }
    }

}
