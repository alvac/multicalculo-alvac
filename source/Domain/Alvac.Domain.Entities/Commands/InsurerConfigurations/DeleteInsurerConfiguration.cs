﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.InsurerConfigurations
{
    public class DeleteInsurerConfiguration : ICommand<bool>
    {
        public int InsurerConfigurationId { get; set; }
    }
}
