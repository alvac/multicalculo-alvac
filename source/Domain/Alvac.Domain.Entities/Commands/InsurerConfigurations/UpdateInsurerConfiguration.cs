﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.InsurerConfigurations
{
    public class UpdateInsurerConfiguration : ICommand<InsurerConfiguration>
    {
        public int InsurerConfigurationId { get; set; }

        /// <summary>
        /// Parâmetro obrigatório.
        /// </summary>
        [DisplayName("Obrigatório")]
        [Required]
        public bool Required { get; set; }

        /// <summary>
        /// Nome de exibição do campo.
        /// </summary>
        [DisplayName("Nome de exibição")]
        [Required]
        public string DisplayName { get; set; }

        /// <summary>
        /// Tipo do parametro.
        /// </summary>
        [DisplayName("Tipo do parametro")]
        [Required]
        public string Type { get; set; }

        /// <summary>
        /// Chave
        /// </summary>
        [DisplayName("Chave")]
        [Required]
        public string Key { get; set; }

        /// <summary>
        /// Preenchido em caso de campo 'select'
        /// </summary>
        [DisplayName("Opções")]
        [Required]
        public string Options { get; set; }
    }
}
