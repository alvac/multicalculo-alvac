﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.InsurerValidators
{
    public class UpdateInsurerValidator : ICommand<InsurerValidator>
    {
        [Required]
        public int InsurerValidatorId { get; set; }

        [Required]
        [DisplayName("Seguradora")]

        public Insurer Insurer { get; set; }

        [Required]
        [DisplayName("Tipo Requisição")]
        public RequestType RequestType { get; set; }

        [Required]
        [DisplayName("Descrição")]
        public string Description { get; set; }

        [Required]
        [DisplayName("Nome Assembly Completo")]
        public string FullAssemblyName { get; set; }
    }
}
