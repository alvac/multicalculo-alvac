﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Users
{
    public class CreateUser : ICommand<User>
    {
        [Required]
        public string Name{ get; set; }

        [Required]
        public string Email { get; set; }

        [Required]
        public string Role { get; set; }

        public int AccountId { get; set; }
    }
}
