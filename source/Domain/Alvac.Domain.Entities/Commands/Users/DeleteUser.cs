﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Users
{
    public class DeleteUser : ICommand<bool>
    {
        [Required]
        public string Id { get; set; }
    }
}
