﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.InsurerQuotations
{
    public class UpdateStatusInsurerQuotation : ICommand<InsurerQuotation>
    {
        /// <summary>
        /// Identificador da cotação na seguradora
        /// </summary>
       [Required]
        public int InsurerQuotationId { get; set; }

        /// <summary>
        /// Identificador da conexão
        /// </summary>
        public int ConnectionId { get; set; }

        /// <summary>
        /// Indica o status da operação.
        /// </summary>
        [Required]
        public Status Status { get; set; }

        /// <summary>
        /// Indica o codigo de descrição da operação. 
        /// </summary>
        [Required]
        public DescriptionCode OperationDescriptionCode { get; set; }

        /// <summary>
        /// Descrição sobre o resultado da operação completa.
        /// </summary>
        [Required]
        public string OperationDescription { get; set; }

        /// <summary>
        /// Identificador da transação no gateway.
        /// </summary>      
        public string Gateway { get; set; }
    }
}
