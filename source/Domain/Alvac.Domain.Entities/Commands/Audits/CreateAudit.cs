﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Audits
{
    public class CreateAudit : ICommand<Audit>
    {
        public string UserName { get; set; }
        public string IPAddress { get; set; }
        public string AreaAccessed { get; set; }
        public string Data { get; set; }
        public int HttpStatusCode { get; set; }
    }
}
