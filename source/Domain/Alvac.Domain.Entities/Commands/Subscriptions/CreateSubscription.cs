﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Subscriptions
{
    public class CreateSubscription : ICommand<Subscription>
    {
        /// <summary>
        /// Identificador da conta
        /// </summary>
        [Required]
        public int AccountId { get; set; }

        /// <summary>
        /// Identificador do plano
        /// </summary>
        [Required]
        [DisplayName("Plano")]
        public int PlanId { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        [Required]
        [DisplayName("Tipo de requisição")]
        public RequestType RequestType { get; set; }

        /// <summary>
        /// Tipo de assinatura
        /// </summary>
        [Required]
        [DisplayName("Tipo de assinatura")]
        public SubscriptionType Type { get; set; }

        /// <summary>
        /// Status da assinatura
        /// </summary>
        [Required]
        public SubscriptionStatus Status { get; set; }

        /// <summary>
        /// Data que à assinatura expira
        /// </summary>
        [DisplayName("Data de expiração")]
        public DateTimeOffset? Expiration { get; set; }
    }
}
