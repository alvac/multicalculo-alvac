﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.AccountConfigurations
{
    public class CreateReportConfiguration : ICommand<AccountConfiguration>
    {
        public int AccountId { get; set; }

        public string ReportTitle { get; set; }

        public string ReportSubtitle { get; set; }

        public string ReportLogo { get; set; }

        public string ReportFooter { get; set; }

        public string ReportUserMessage { get; set; }

        public int ReportFontSize { get; set; }

        public string ReportComments { get; set; }
    }
}
