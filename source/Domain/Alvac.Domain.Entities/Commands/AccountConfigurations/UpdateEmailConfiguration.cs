﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.AccountConfigurations
{
    public class UpdateEmailConfiguration : ICommand<AccountConfiguration>
    {
        [Required]
        public int AccountId { get; set; }

        public string EmailReceipt { get; set; }

        public string EmailBody { get; set; }

        public bool EmailSendToInsured { get; set; }

        public string EmailHost { get; set; }

        public string EmailUsername { get; set; }

        public string EmailPassword { get; set; }

        public int EmailPort { get; set; }

        public bool EmailEnableSsl { get; set; }
    }
}
