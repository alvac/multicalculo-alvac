﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Invoices
{
    public class CreateInvoice : ICommand<Invoice>
    {
        public int SubscriptionId { get; set; }

        public DateTimeOffset Closing { get; set; }
    }
}
