﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Invoices
{
    public class CloseInvoice : ICommand<bool>
    {
        [Required]
        public int InvoiceId { get; set; }
    }
}
