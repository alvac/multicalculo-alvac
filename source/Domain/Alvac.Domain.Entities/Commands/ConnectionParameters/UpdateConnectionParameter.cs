﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.ConnectionParameters
{
    public class UpdateConnectionParameter : ICommand<Connection>
    {
        /// <summary>
        /// Identificador da conexão
        /// </summary>
        [Required]
        [DisplayName("Conexão")]
        public int ConnectionId { get; set; }

        /// <summary>
        /// Chave
        /// </summary>
        [Required]
        [DisplayName("Chave")]
        public string Key { get; set; }

        /// <summary>
        /// Valor
        /// </summary>
        [Required]
        [DisplayName("Valor")]
        public string Value { get; set; }
    }
}
