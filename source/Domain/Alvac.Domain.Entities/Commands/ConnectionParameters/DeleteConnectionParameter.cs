﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.ConnectionParameters
{
    public class DeleteConnectionParameter : ICommand<bool>
    {
        [Required]
        public int ConnectionParameterId { get; set; }
    }
}
