﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Connections
{
    public class DeleteConnection : ICommand<bool>
    {
        [Required]
        public int ConnectionId { get; set; }
    }
}
