﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Connections
{
    public class CreateConnection : ICommand<Connection>
    {
        /// <summary>
        /// Identificador do despachante
        /// </summary>
        [Required]
        [DisplayName("Despachante")]
        public int DispatcherId { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        [Required]
        [DisplayName("Nome")]
        public string Name { get; set; }

        /// <summary>
        /// Vazão maxima
        /// </summary>
        [Required]
        [DisplayName("Vazão maxima")]
        public int MaxThroughput { get; set; }

        /// <summary>
        /// Tempo em milisegundos para vazão maxima
        /// </summary>
        [Required]
        [DisplayName("Tempo em milisegundos para vazão maxima")]
        public int LengthTimeUnitThroughput { get; set; }

        /// <summary>
        /// Tamanho da janela
        /// </summary>
        [Required]
        [DisplayName("Tamanho da janela")]
        public int WindowSize { get; set; }

        /// <summary>
        /// Quantidade
        /// </summary>
        [Required]
        [DisplayName("Quantidade de threads")]
        public int Amount { get; set; }

        /// <summary>
        /// Nome da fila de entrada
        /// </summary>
        [Required]
        [DisplayName("Fila de entrada")]
        public string InputQueueName { get; set; }

        /// <summary>
        /// Nome da fila de saída
        /// </summary>
        [Required]
        [DisplayName("Fila de saída")]
        public string OutputQueueName { get; set; }

        /// <summary>
        /// Nome da fila de notificação
        /// </summary>
        [Required]
        [DisplayName("Fila de notificação")]
        public string NotificationQueueName { get; set; }

        /// <summary>
        /// Máximo de retentativa desta conexão
        /// </summary>
        [DisplayName("Máximo de Retentativa")]
        public int? RetryMax { get; set; }

        /// <summary>
        /// Intervalo de retentativa em segundos.
        /// </summary>
        [DisplayName("Intervalo de Retentativa")]
        public int? RetryInterval { get; set; }

        /// <summary>
        /// Determina se a conexão esta ativa
        /// </summary>
        [Required]
        [DisplayName("Ativo")]
        public bool IsActive { get; set; }

        /// <summary>
        /// Determina se a conexão é a padrão para criação automatica 
        /// quando uma nova conta for cadastrada
        /// </summary>
        [Required]
        [DisplayName("Padrão para criação automatica")]
        public bool IsDefault { get; set; }
    }
}
