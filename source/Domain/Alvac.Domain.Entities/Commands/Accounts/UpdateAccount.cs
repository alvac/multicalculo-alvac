﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Accounts
{
    public class UpdateAccount : ICommand<Account>
    {
        [Required]
        public int AccountId { get; set; }

        /// <summary>
        /// Nome do representante legal.
        /// </summary>
        [DisplayName("Representante legal")]
        public string LegalRepresentative { get; set; }

        /// <summary>
        /// SUSEP
        /// </summary>
        [Required]
        [DisplayName("SUSEP")]
        public string Susep { get; set; }

        /// <summary>
        /// Tipo de pessoa
        /// </summary>
        [Required]
        [DisplayName("Tipo de pessoa")]
        public PersonType PersonType { get; set; }

        /// <summary>
        /// Documento RG/CPF
        /// </summary>
        [Required]
        [DisplayName("RG/CPF")]
        public string Document { get; set; }

        /// <summary>
        /// Nome e/ou razao social
        /// </summary>
        [DisplayName("Razão social")]
        public string Name { get; set; }

        /// <summary>
        /// CEP
        /// </summary>
        [DisplayName("CEP")]
        public string ZipCode { get; set; }

        /// <summary>
        /// Rua
        /// </summary>
        [DisplayName("Rua")]
        public string Street { get; set; }

        /// <summary>
        /// Bairro
        /// </summary>
        [DisplayName("Bairro")]
        public string Neighborhood { get; set; }

        /// <summary>
        /// Estado
        /// </summary>
        [DisplayName("Estado")]
        public string State { get; set; }

        /// <summary>
        /// Cidade
        /// </summary>
        [DisplayName("Cidade")]
        public string City { get; set; }

        /// <summary>
        /// Email
        /// </summary>
        public string Email { get; set; }

        /// <summary>
        /// Email de cobrança.
        /// </summary>
        [Required]
        [DisplayName("Email de cobrança")]
        public string ChargeEmail { get; set; }

        /// <summary>
        /// Telefone
        /// </summary>
        [DisplayName("Telefone")]
        public string Phone { get; set; }

        /// <summary>
        /// Celular
        /// </summary>
        [DisplayName("Celular")]
        public string MobilePhone { get; set; }

        /// <summary>
        /// Site da empresa / pessoa.
        /// </summary>
        public string Site { get; set; }
    }
}
