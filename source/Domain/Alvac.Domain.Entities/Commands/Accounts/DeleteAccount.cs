﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Accounts
{
    public class DeleteAccount : ICommand<bool>
    {
        public int AccountId { get; set; }
    }
}
