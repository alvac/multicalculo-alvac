﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Plans
{
    public class UpdatePlan : ICommand<Plan>
    {
        [Required]
        public int PlanId { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        [Required]
        [DisplayName("Nome")]
        public string Name { get; set; }

        /// <summary>
        /// Recorrência do plano
        /// </summary>
        [Required]
        [DisplayName("Recorrência")]
        public int Recurrence { get; set; }

        /// <summary>
        /// Tipo de recorrência do plano.
        /// </summary>
        [Required]
        [DisplayName("Tipo de recorrência")]
        public PlanRecurrenceType RecurrenceType { get; set; }

        /// <summary>
        /// Quantidade cotação
        /// </summary>
        [Required]
        [DisplayName("Quantidade de cotação")]
        public int Amount { get; set; }

        /// <summary>
        /// Valor de cada cotação
        /// </summary>
        [Required]
        [DisplayName("Valor da cotação")]
        public decimal Value { get; set; }

        /// <summary>
        /// Valor adicional de cada cotação
        /// </summary>
        [Required]
        [DisplayName("Valor adicional da cotação")]
        public decimal AdditionalValue { get; set; }

        /// <summary>
        /// Dias de degustação
        /// </summary>
        [Required]
        public int? Trial { get; set; }

        /// <summary>
        /// Determina se o plano esta ativo
        /// </summary>
        [Required]
        [DisplayName("Ativo")]
        public bool IsActive { get; set; }
    }
}
