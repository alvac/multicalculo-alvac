﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Plans
{
    public class DeletePlan : ICommand<bool>
    {
        [Required]
        public int PlanId { get; set; }
    }
}
