﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Dispatchers
{
    public class UpdateDispatcher : ICommand<Dispatcher>
    {
        /// <summary>
        /// Identificador do despachante
        /// </summary>
        [Required]
        [DisplayName("Despachante")]
        public int DispatcherId { get; set; }

        /// <summary>
        /// Seguradora
        /// </summary>
        [Required]
        [DisplayName("Seguradora")]
        public int InsurerId { get; set; }

        /// <summary>
        /// Tecnologia
        /// </summary>
        [Required]
        [DisplayName("Tecnologia")]
        public Technology Technology { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        [Required]
        [DisplayName("Tipo de requisição")]
        public RequestType RequestType { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        [Required]
        [DisplayName("Nome")]
        public string Name { get; set; }

        /// <summary>
        /// Descrição
        /// </summary>
        [Required]
        [DisplayName("Descrição")]
        public string Description { get; set; }

        /// <summary>
        /// Despachante ativo
        /// </summary>
        [Required]
        [DisplayName("Ativo")]
        public bool IsActive { get; set; }
    }
}
