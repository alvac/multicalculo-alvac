﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities.Commands.Dispatchers
{
    public class DeleteDispatcher : ICommand<bool>
    {
        [Required]
        public int DispatcherId { get; set; }
    }
}
