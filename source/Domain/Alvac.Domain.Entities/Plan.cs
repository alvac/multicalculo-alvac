﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa um plano
    /// </summary>
    public class Plan
    {
        /// <summary>
        /// Identificador do plano.
        /// </summary>
        public int PlanId { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Tipo de recorrência do plano.
        /// </summary>
        public PlanRecurrenceType RecurrenceType { get; set; }

        /// <summary>
        /// Recorrência do plano
        /// </summary>
        public int Recurrence { get; set; }

        /// <summary>
        /// Quantidade cotação
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Valor de cada cotação
        /// </summary>
        public decimal Value { get; set; }

        /// <summary>
        /// Valor adicional de cada cotação
        /// </summary>
        public decimal AdditionalValue { get; set; }

        /// <summary>
        /// Dias de degustação
        /// </summary>
        public int? Trial { get; set; }

        /// <summary>
        /// Determina se o plano esta ativo
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Ultima atualização
        /// </summary>
        public DateTimeOffset LastUpdate { get; set; }

        #region Relationship

        public virtual ICollection<Subscription> Subscriptions { get; set; }

        #endregion

    }
}
