﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class DeviceTracker
    {
        public int DeviceTrackerId { get; set; }

        public string Name { get; set; }

        public virtual ICollection<VehicleInfo> VehicleInfos { get; set; }
    }
}
