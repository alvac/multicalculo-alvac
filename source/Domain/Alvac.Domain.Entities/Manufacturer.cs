﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class Manufacturer
    {
        public int ManufacturerId { get; set; }

        public string Name { get; set; }

        public int? InsurerId { get; set; }
        public virtual Insurer Insurer { get; set; }

        public string CodeManufacturerInsurer { get; set; }

        public virtual ICollection<Vehicle> Vehicles { get; set; }

        public virtual ICollection<MatchManufacturer> MatchManufacturerAlvac { get; set; }

        public virtual ICollection<MatchManufacturer> MatchManufacturerInsurer { get; set; }
    }
}
