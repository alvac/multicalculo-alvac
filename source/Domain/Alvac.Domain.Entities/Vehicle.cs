﻿using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class Vehicle
    {
        public int VehicleId { get; set; }
        
        /// <summary>
        /// Código fipe da versão do veículo.
        /// </summary>
        public string FipeCode { get; set; }

        public int ManufacturerId { get; set; }
        public virtual Manufacturer Manufacturer { get; set; }

        public int? InsurerId { get; set; }
        public virtual Insurer Insurer { get; set; }

        /// <summary>
        /// Modelo do veículo.
        /// </summary>
        public string Model { get; set; }

        /// <summary>
        /// Ano inicial.
        /// </summary>
        public int InitialYear { get; set; }

        /// <summary>
        /// Ano final.
        /// </summary>
        public int FinalYear { get; set; }

        /// <summary>
        /// Tipo de combutível do veículo.
        /// </summary>
        public FuelType Fuel { get; set; }

        /// <summary>
        /// Codigo do veiculo na seguradora
        /// </summary>
        public string CodeVehicleInsurer { get; set; }

        /// <summary>
        /// Ano/mes de referencia
        /// </summary>
        public string DateReference { get; set; }

        /// <summary>
        /// Cambio
        /// </summary>
        public int? GearBox { get; set; }

        /// <summary>
        /// Cilindradas
        /// </summary>
        public decimal? CC { get; set; }

        /// <summary>
        /// Portas
        /// </summary>
        public int? Doors { get; set; }

        /// <summary>
        /// Valvulas
        /// </summary>
        public int? Valves { get; set; }

        /// <summary>
        /// Tração
        /// </summary>
        public string Traction { get; set; }

        /// <summary>
        /// Passageiros
        /// </summary>
        public int? Passengers { get; set; }

        /// <summary>
        /// Eixos
        /// </summary>
        public int? Axes { get; set; }

        public virtual ICollection<VehiclePrice> VehiclePrices { get; set; }
    }
}
