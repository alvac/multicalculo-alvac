﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa uma conexão
    /// </summary>
    public class Connection
    {
        /// <summary>
        /// Identificador da conexão
        /// </summary>
        public int ConnectionId { get; set; }

        /// <summary>
        /// Identificador do despachante
        /// </summary>
        public int DispatcherId { get; set; }
        public virtual Dispatcher Dispatcher { get; set; }

        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Vazão maxima
        /// </summary>
        public int MaxThroughput { get; set; }

        /// <summary>
        /// Tempo em milisegundos para vazão maxima
        /// </summary>
        public int LengthTimeUnitThroughput { get; set; }

        /// <summary>
        /// Tamanho da janela
        /// </summary>
        public int WindowSize { get; set; }

        /// <summary>
        /// Quantidade
        /// </summary>
        public int Amount { get; set; }

        /// <summary>
        /// Nome da fila de entrada
        /// </summary>
        public string InputQueueName { get; set; }

        /// <summary>
        /// Nome da fila de saída
        /// </summary>
        public string OutputQueueName { get; set; }

        /// <summary>
        /// Nome da fila de notificação
        /// </summary>
        public string NotificationQueueName { get; set; }

        /// <summary>
        /// Máximo de retentativa desta conexão
        /// </summary>
        public int? RetryMax { get; set; }

        /// <summary>
        /// Intervalo de retentativa em segundos.
        /// </summary>
        public int? RetryInterval { get; set; }

        /// <summary>
        /// Determina se a conexão esta ativa
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Determina se a conexão é a padrão para criação automatica 
        /// quando uma nova conta for cadastrada
        /// </summary>
        public bool IsDefault { get; set; }

        #region Relationship

        public virtual ICollection<InsurerQuotation> InsurerQuotations { get; set; }

        public virtual ICollection<ConnectionParameter> ConnectionParameters { get; set; }

        public virtual ICollection<AccountConnection> AccountConnections { get; set; }
        #endregion
    }
}

