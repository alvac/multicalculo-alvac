﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class IgnitionBlocker
    {
        public int IgnitionBlockerId { get; set; }

        public string Name { get; set; }

        public virtual ICollection<VehicleInfo> VehicleInfos { get; set; }
    }
}
