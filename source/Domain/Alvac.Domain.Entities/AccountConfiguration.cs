﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class AccountConfiguration
    {
        public int AccountId { get; set; }
        public virtual  Account Account { get; set; }

        public string EmailReceipt { get; set; }

        public string EmailBody { get; set; }

        public bool EmailSendToInsured { get; set; }

        public string EmailHost { get; set; }

        public string EmailUsername { get; set; }

        public string EmailPassword { get; set; }

        public int EmailPort { get; set; }

        public bool EmailEnableSsl { get; set; }

        public string ReportTitle { get; set; }

        public string ReportSubtitle { get; set; }

        public string ReportLogo { get; set; }

        public string ReportFooter { get; set; }

        public string ReportUserMessage { get; set; }

        public int ReportFontSize { get; set; }

        public string ReportComments { get; set; }
    }
}
