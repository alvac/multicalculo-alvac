﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class AccountInsurerTemplate
    {
        public int AccountInsurerTemplateId { get; set; }

        /// <summary>
        /// Identificador da conta
        /// </summary>
        public int AccountId { get; set; }
        public virtual Account Account { get; set; }
        
        /// <summary>
        /// Nome
        /// </summary>
        public string Name { get; set; }

        /// <summary>
        /// Descrição
        /// </summary>
        public string Description { get; set; }

        /// <summary>
        /// Ativo
        /// </summary>
        public bool IsActive { get; set; }

        /// <summary>
        /// Data de criãção
        /// </summary>
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Tipo de requisição
        /// </summary>
        public RequestType RequestType { get; set; }

        public ICollection<AccountInsurerConfiguration> AccountInsurerConfigurations { get; set; }
    }
}
