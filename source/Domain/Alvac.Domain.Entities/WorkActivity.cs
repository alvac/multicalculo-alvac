﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    public class WorkActivity
    {
        /// <summary>
        /// Identificador atividade de trabalho
        /// </summary>
        public int WorkActivityId { get; set; }

        /// <summary>
        /// Nome da atividade de trabalho.
        /// </summary>
        public string Name { get; set; }


        #region Relationship

        public virtual ICollection<MainDriver> MainDrivers { get; set; }
        public virtual ICollection<Insured> Insureds { get; set; }

        #endregion
    }
}
