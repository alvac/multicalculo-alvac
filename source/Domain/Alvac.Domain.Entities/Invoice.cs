﻿using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Domain.Entities
{
    /// <summary>
    /// Representa uma fatura
    /// </summary>
    public class Invoice
    {
        /// <summary>
        /// Identificador da fatura
        /// </summary>
        public int InvoiceId { get; set; }

        /// <summary>
        /// Identificador da assinatura
        /// </summary>
        public int SubscriptionId { get; set; }
        public virtual Subscription Subscription { get; set; }

        /// <summary>
        /// Data de fechamento da fatura
        /// </summary>
        public DateTimeOffset Closing { get; set; }

        /// <summary>
        /// Data de criação da fatura
        /// </summary>
        public DateTimeOffset Created { get; set; }

        /// <summary>
        /// Ultima atualização
        /// </summary>
        public DateTimeOffset LastUpdate { get; set; }

        /// <summary>
        /// Status da fatura
        /// </summary>
        public InvoiceStatus Status { get; set; }

        /// <summary>
        /// Valor total da fatura
        /// </summary>
        public decimal Value { get; set; }


        #region Relationship

        public virtual ICollection<Transaction> Transactions { get; set; }

        #endregion
    }
}
