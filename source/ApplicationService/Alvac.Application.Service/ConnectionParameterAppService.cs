﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.ConnectionParameters;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.ConnectionParameters;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class ConnectionParameterAppService : IConnectionParameterAppService
    {
        private readonly ISecurityContext _securityContext;
        private readonly IMapper _mapper;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _commandProcessor;

        public ConnectionParameterAppService(ISecurityContext securityContext,
            IMapper mapper,
            IQueryProcessor queryProcessor,
            ICommandProcessor commandProcessor)
        {
            _securityContext = securityContext;
            _mapper = mapper;
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
        }

        #region IConnectionAppService Members

        public async Task<IQueryable<ConnectionParameter>> GetAllAsync()
        {
            return _mapper.Project<Alvac.Domain.Entities.ConnectionParameter, ConnectionParameter>(await _queryProcessor.ProcessAsync(new GetAll()));
        }

        public async Task CreateConnectionAsync(CreateConnectionParameter command)
        {
            await _commandProcessor.ProcessAsync(command);
        }

        public async Task DeleteConnectionAsync(int id)
        {
            await _commandProcessor.ProcessAsync(new DeleteConnectionParameter
            {
                ConnectionParameterId = id
            });
        }

        public async Task UpdateConnectionAsync(UpdateConnectionParameter command)
        {
            await _commandProcessor.ProcessAsync(command);
        }

        public async Task<ConnectionParameter> GetByIdAsync(int connectionParameterId)
        {
            var findConnectionParameterById = new FindConnectionParametersById
            {
                ConnectionParameterId = connectionParameterId
            };

            return _mapper.Map<ConnectionParameter>(await _queryProcessor.ProcessAsync(findConnectionParameterById));
        }

        public async Task<IQueryable<ConnectionParameter>> GetByConnectionIdAsync(int connectionId)
        {
            var findConnectionParametersByConnectionId = new FindConnectionParametersByConnectionId
            {
                ConnectionId = connectionId
            };

            return _mapper.Project<Alvac.Domain.Entities.ConnectionParameter, ConnectionParameter>(await _queryProcessor.ProcessAsync(findConnectionParametersByConnectionId));
        }

        #endregion
    }
}
