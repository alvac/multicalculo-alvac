﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Dispatchers;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Dispatchers;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class DispatcherAppService : IDispatcherAppService
    {
        private readonly ISecurityContext _securityContext;
        private readonly IMapper _mapper;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _commandProcessor;

        public DispatcherAppService(ISecurityContext securityContext,
            IMapper mapper,
            IQueryProcessor queryProcessor,
            ICommandProcessor commandProcessor)
        {
            _securityContext = securityContext;
            _mapper = mapper;
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
        }

        #region IPlanAppService Members

        public async Task<IEnumerable<Dispatcher>> GetAllAsync()
        {
            var model = await _queryProcessor.ProcessAsync(new GetAll());
            return _mapper.Map<IEnumerable<Dispatcher>>(model);
        }

        public async Task<Dispatcher> GetByIdAsync(int connectionId)
        {
            var findDispatcherById = new FindDispatcherById
            {
                DispatcherId = connectionId
            };

            return _mapper.Map<Dispatcher>(await _queryProcessor.ProcessAsync(findDispatcherById));
        }

        public async Task<Dispatcher> CreateDispatcherAsync(CreateDispatcher command)
        {
            var model = await _commandProcessor.ProcessAsync(command);
            return _mapper.Map<Dispatcher>(model);
        }

        public async Task<Dispatcher> UpdateDispatcherAsync(UpdateDispatcher command)
        {
            var model = await _commandProcessor.ProcessAsync(command);
            return _mapper.Map<Dispatcher>(model);
        }

        public async Task DeleteDispatcherAsync(int id)
        {
            var model = await _commandProcessor.ProcessAsync(new DeleteDispatcher
            {
                DispatcherId = id
            });
        }

        #endregion
    }
}
