﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.AccountConfigurations;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.AccountConfigurations;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class AccountConfigurationAppService : IAccountConfigurationAppService
    {
        private readonly ICommandProcessor _commandProcessor;
        private readonly IMapper _mapper;
        private readonly ISecurityContext _securityContext;
        private readonly IQueryProcessor _queryProcessor;

        public AccountConfigurationAppService(ICommandProcessor commandProcessor,
            IQueryProcessor queryProcessor,
            IMapper mapper,
            ISecurityContext securityContext)
        {
            _commandProcessor = commandProcessor;
            _queryProcessor = queryProcessor;
            _mapper = mapper;
            _securityContext = securityContext;
        }

        public async Task CreateEmailConfigurationAsync(CreateEmailConfiguration createEmailConfiguration)
        {
            var user = await _securityContext.GetUserAsync();
            createEmailConfiguration.AccountId = user.AccountId;
            var response = await _commandProcessor.ProcessAsync(createEmailConfiguration);
        }

        public async Task UpdateEmailConfigurationAsync(UpdateEmailConfiguration updateEmailConfiguration)
        {
            var user = await _securityContext.GetUserAsync();

            if (user.AccountId != updateEmailConfiguration.AccountId)
            {
                throw new AlvacException("Usuario tentando acesso a outra corretora", Domain.Enum.DescriptionCode.InvalidRequest);
            }

            var response = await _commandProcessor.ProcessAsync(updateEmailConfiguration);
        }

        public async Task CreateReportConfigurationAsync(CreateReportConfiguration createReportConfiguration)
        {
            var user = await _securityContext.GetUserAsync();
            createReportConfiguration.AccountId = user.AccountId;
            var response = await _commandProcessor.ProcessAsync(createReportConfiguration);
        }

        public async Task UpdateReportConfigurationAsync(UpdateReportConfiguration updateReportConfiguration)
        {
            var user = await _securityContext.GetUserAsync();

            if (user.AccountId != updateReportConfiguration.AccountId)
            {
                throw new AlvacException("Usuario tentando acesso a outra corretora", Domain.Enum.DescriptionCode.InvalidRequest);
            }

            var response = await _commandProcessor.ProcessAsync(updateReportConfiguration);
        }

        public async Task<Domain.DataTransferObject.AccountConfiguration> GetConfigurationAsync()
        {
            var user = await _securityContext.GetUserAsync();
            var query = new GetAccountConfiguration
            {
                AccountId = user.AccountId
            };

            var response = await _queryProcessor.ProcessAsync(query);
            return _mapper.Map<AccountConfiguration>(response);
        }
    }
}
