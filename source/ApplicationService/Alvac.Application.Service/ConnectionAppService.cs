﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Connections;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Connections;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class ConnectionAppService : IConnectionAppService
    {
        private readonly ISecurityContext _securityContext;
        private readonly IMapper _mapper;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _commandProcessor;

        public ConnectionAppService(ISecurityContext securityContext,
            IMapper mapper,
            IQueryProcessor queryProcessor,
            ICommandProcessor commandProcessor)
        {
            _securityContext = securityContext;
            _mapper = mapper;
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
        }

        #region IConnectionAppService Members

        public async Task<IQueryable<Connection>> GetAllAsync()
        {
            return _mapper.Project<Alvac.Domain.Entities.Connection, Connection>(await _queryProcessor.ProcessAsync(new GetAll()));
        }

        public async Task CreateConnectionAsync(CreateConnection command)
        {
            await _commandProcessor.ProcessAsync(command);
        }

        public async Task DeleteConnectionAsync(int id)
        {
            await _commandProcessor.ProcessAsync(new DeleteConnection
            {
                ConnectionId = id
            });
        }

        public async Task UpdateConnectionAsync(UpdateConnection command)
        {
            await _commandProcessor.ProcessAsync(command);
        }

        public async Task<Connection> GetByIdAsync(int connectionId)
        {
            var findConnectionById = new FindConnectionById
            {
                ConnectionId = connectionId
            };

            return _mapper.Map<Connection>(await _queryProcessor.ProcessAsync(findConnectionById));
        }

        #endregion
    }
}
