﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Subscriptions;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Subscriptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class SubscriptionAppService : ISubscriptionAppService
    {
        private readonly IQueryProcessor _queryProcessor;
        private readonly IMapper _mapper;
        private readonly ICommandProcessor _commandProcessor;

        public SubscriptionAppService(IQueryProcessor queryProcessor,
            ICommandProcessor commandProcessor,
            IMapper mapper)
        {
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
            _mapper = mapper;
        }

        public async Task<IQueryable<Subscription>> GetSubscriptionsByAccountAsync(int accountId)
        {
            var query = new GetSubscriptionsByAccountId
            {
                AccountId = accountId
            };

            var model = await _queryProcessor.ProcessAsync(query);
            return _mapper.Project<Domain.Entities.Subscription, Subscription>(model.OrderByDescending(x => x.Created));
        }


        public async Task UpdateSubscriptionAsync(UpdateSubscription command)
        {
            await _commandProcessor.ProcessAsync(command);
        }

        public async Task CreateSubscriptionAsync(CreateSubscription command)
        {
            await _commandProcessor.ProcessAsync(command);
        }

        public async Task<Subscription> GetSubscription(int subscriptionId)
        {
            var query = new GetSubscriptionById
            {
                SubscriptionId = subscriptionId
            };

            var model = await _queryProcessor.ProcessAsync(query);
            return _mapper.Map<Subscription>(model);
        }
    }
}
