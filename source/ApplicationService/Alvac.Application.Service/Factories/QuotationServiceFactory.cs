﻿using Alvac.Domain.Enum;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Quotation.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Quotation.Factories
{
    public class QuotationServiceFactory : IQuotationServiceFactory
    {
        #region Private Fields

        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Constructor

        public QuotationServiceFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region IQuotationServiceFactory Members

        public IQuotationService Create(RequestType requestType)
        {
            IQuotationService result = null;

            switch (requestType)
            {
                case RequestType.AutoQuotation:

                    result = _serviceProvider.GetService(typeof(AutoQuotationService)) as IQuotationService;

                    break;

                default:
                    throw new NotImplementedException(string.Format("IQuotationService não implementado para o requestType '{0}'", requestType));
            }

            return result;
        }

        #endregion

    }
}
