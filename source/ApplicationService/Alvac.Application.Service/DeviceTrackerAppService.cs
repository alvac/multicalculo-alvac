﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.DeviceTrackers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class DeviceTrackerAppService : IDeviceTrackerAppService
    {
         private readonly IQueryProcessor _queryProcessor;
        private readonly IMapper _mapper;

        public DeviceTrackerAppService(IQueryProcessor queryProcessor,
            IMapper mapper)
        {
            _queryProcessor = queryProcessor;
            _mapper = mapper;
        }

        public async Task<IEnumerable<DeviceTracker>> GetAllAsync()
        {
            var query = new GetAllDeviceTrackers();

            var response = await _queryProcessor.ProcessAsync(query);
            return _mapper.Map<IEnumerable<DeviceTracker>>(response);
        }
    }
}
