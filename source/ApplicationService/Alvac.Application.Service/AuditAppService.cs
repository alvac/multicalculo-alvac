﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Audits;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class AuditAppService : IAuditAppService
    {
        private readonly IMapper _mapper;
        private readonly IQueryProcessor _queryProcessor;

        public AuditAppService(IMapper mapper,
            IQueryProcessor queryProcessor)
        {
            _mapper = mapper;
            _queryProcessor = queryProcessor;
        }

        #region IAuditAppService Members

        public async Task<IQueryable<Audit>> GetAllAsync()
        {
            return _mapper.Project<Domain.Entities.Audit, Audit>(await _queryProcessor.ProcessAsync(new GetAllAudits()));
        }

        #endregion
    }
}
