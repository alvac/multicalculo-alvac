﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Vehicles;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class VehicleAppService : IVehicleAppService
    {
        private readonly IQueryProcessor _queryProcessor;
        private readonly IMapper _mapper;

        public VehicleAppService(IQueryProcessor queryProcessor,
            IMapper mapper)
        {
            _queryProcessor = queryProcessor;
            _mapper = mapper;
        }

        public async Task<IEnumerable<Manufacturer>> GetManufacturersAsync()
        {
            var response = await _queryProcessor.ProcessAsync(new GetManufacturers());
            return _mapper.Map<IEnumerable<Manufacturer>>(response);
        }

        public async Task<IQueryable<Vehicle>> GetVehicleByManufacturerAsync(int manufacturerId, int modelYear)
        {
            var vehicles = await _queryProcessor.ProcessAsync(new GetVehiclesByManufacturer
            {
                ManufacturerId = manufacturerId,
                ModelYear = modelYear
            });

            return _mapper.Project<Domain.Entities.Vehicle, Vehicle>(vehicles);
        }

        public async Task<IEnumerable<Vehicle>> GetVehicleByManufacturerAsync(int manufacturerId, string model)
        {
            var vehicles = await _queryProcessor.ProcessAsync(new GetVehiclesByManufacturerModel
            {
                ManufacturerId = manufacturerId,
                Model = model
            });

            return from v in vehicles
                   select new Vehicle
                   {
                       Model = v.Model,
                       FipeCode = v.FipeCode,
                       Fuel = v.Fuel,
                   };
        }

        public async Task<IEnumerable<Vehicle>> GetVehicleByIdInsurerAsync(int insurer, int vehicleId)
        {
            var vehicle = await _queryProcessor.ProcessAsync(new GetVehicleById
            {
                VehicleId = vehicleId
            });

            var vehicles = await _queryProcessor.ProcessAsync(new GetVehiclesByFipeInsurer
            {
                FipeCode = vehicle.FipeCode,
                Insurer = insurer
            });

            return _mapper.Map<IEnumerable<Vehicle>>(vehicles);
        }

        //private async Task<IEnumerable<Vehicle>> GetMatchingVehicles(Domain.Enum.Insurer insurer, Domain.Entities.Vehicle vehicle)
        //{
        //    var listVehicles = new List<Domain.Entities.Vehicle>();
        //    var newVehicle = new Domain.Entities.Vehicle();
        //    string versao;

        //    vehicle.Model = vehicle.Model.Trim();

        //    if (vehicle.Model.IndexOf(' ') == 1)
        //    {
        //        vehicle.Model.Remove(1, 1);
        //    }

        //    String[] splitVehicle = vehicle.Model.Split(' ', '/', '(', ')', '.');

        //    var cilindrada = Texto.ObtemTextoSemTempoLimite(vehicle.Model, @".*(\d\.\d).*", "$1");
        //    if (cilindrada != null)
        //    {
        //        vehicle.CC = Convert.ToDouble(cilindrada.Replace('.', ','));
        //        vehicle.Model = vehicle.Model.Replace(cilindrada, "").Trim();
        //        splitVehicle = splitVehicle.Concat(new[] { cilindrada }).ToArray();
        //    }

        //    splitVehicle = montaSplit(splitVehicle);
        //    versao = splitVehicle[0];
        //    vehicle.Model = vehicle.Model.Replace(versao, "").Trim();

        //    string strValvulas = Texto.ObtemTextoSemTempoLimite(vehicle.Model, @".*\s(\d?\d)\s*[vV].*", "$1");
        //    if (strValvulas != null)
        //    {
        //        string strValvulasSub = Texto.ObtemTextoSemTempoLimite(vehicle.Model, @".*\s(\d?\d\s*[vV]).*", "$1");
        //        vehicle.Valves = Convert.ToInt32(strValvulas.Trim());
        //        vehicle.Model = vehicle.Model.Replace(strValvulasSub, "").Trim();
        //        splitVehicle = splitVehicle.Concat(new[] { strValvulas }).ToArray();
        //    }

        //    string strPortas = Texto.ObtemTextoSemTempoLimite(vehicle.Model, @".*(\d)\s?[pP].*", "$1");

        //    if (strPortas != null)
        //    {
        //        string strPortasSub = Texto.ObtemTextoSemTempoLimite(vehicle.Model, @".*(\d\s?[pP]).*", "$1");
        //        vehicle.Doors = Convert.ToInt32(strPortas.Trim());
        //        vehicle.Model = vehicle.Model.Replace(strPortasSub, "").Trim();
        //    }

        //    var regex = new Regex(@"([aA][uU][tT]).+\s");
        //    if (regex.Matches(vehicle.Model).Count != 0)
        //    {
        //        vehicle.GearBox = 2; // 1 manual 2 automatico
        //        vehicle.Model = vehicle.Model.Replace(vehicle.GearBox.ToString(), "").Trim();
        //    }
        //    else
        //    {
        //        regex = new Regex(@"([mM][eE][cC]).+\s");
        //        if (regex.Matches(vehicle.Model).Count != 0)
        //        {
        //            vehicle.GearBox = 1;
        //            vehicle.Model = vehicle.Model.Replace(vehicle.GearBox.ToString(), "").Trim();
        //        }
        //        else
        //        {
        //            vehicle.GearBox = 1;
        //        }
        //    }


        //    splitVehicle = splitVehicle.Distinct().ToArray();

        //    foreach (var seguradora in seguradoras.ObtemIds())
        //    {
        //        listVehicles.AddRange(pegaCorrespondencia(splitVehicle, seguradora, vehicle.Fabricante, vehicle, vehicle.AnoModelo, vehicle.Fipe));
        //    }

        //    return listVehicles;
        //}
    }
}
