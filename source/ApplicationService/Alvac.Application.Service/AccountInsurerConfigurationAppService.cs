﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.AccountInsurerConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Alvac.Domain.Entities.Commands.AccountInsurerConfigurations;
using Alvac.Domain.Entities.Commands;

namespace Alvac.Application.Services
{
    public class AccountInsurerConfigurationAppService : IAccountInsurerConfigurationAppService
    {
        #region Private Fields

        private readonly IQueryProcessor _queryProcessor;
        private readonly IMapper _mapper;
        private readonly ICommandProcessor _commandProcessor;

        #endregion

        #region Constructor

        public AccountInsurerConfigurationAppService(IQueryProcessor queryProcessor, IMapper mapper, ICommandProcessor commandProcessor)
        {
            _queryProcessor = queryProcessor;
            _mapper = mapper;
            _commandProcessor = commandProcessor;
        }

        #endregion

        #region IAccountInsurerConfigurationAppService Members

        public async Task<IEnumerable<AccountInsurerConfigurationParameters>> GetAccountInsurerConfigurationParameters(int insurer, int accountInsurerTemplateId)
        {
            var findAccountInsurerConfigurationByInsurerTemplateId = new FindAccountInsurerConfigurationByInsurerTemplateId
            {
                AccountInsurerTemplateId = accountInsurerTemplateId,
                Insurer = insurer
            };

            var accountInsurerTemplates = await _queryProcessor.ProcessAsync(findAccountInsurerConfigurationByInsurerTemplateId);

            return _mapper.Map<List<AccountInsurerConfigurationParameters>>(accountInsurerTemplates);
        }

        public async Task CreateAccountInsurerConfigurationParameters(List<CreateAccountInsurerConfiguration> createAccountInsurerConfigurationCommand)
        {
            foreach (var item in createAccountInsurerConfigurationCommand)
            {
                await _commandProcessor.ProcessAsync(item);
            }
        }

        #endregion
    }
}
