﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.AccountInsurerTemplates;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.AccountInsurerTemplates;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class AccountInsurerTemplateAppService : IAccountInsurerTemplateAppService
    {
        #region Private Fields

        private readonly IQueryProcessor _queryProcessor;
        private readonly IMapper _mapper;
        private readonly ICommandProcessor _commandProcessor;
        private readonly ISecurityContext _securityContext;

        #endregion

        #region Constructor

        public AccountInsurerTemplateAppService(IQueryProcessor queryProcessor,
            IMapper mapper,
            ICommandProcessor commandProcessor,
            ISecurityContext securityContext)
        {
            _queryProcessor = queryProcessor;
            _mapper = mapper;
            _commandProcessor = commandProcessor;
            _securityContext = securityContext;
        }

        #endregion

        #region IAccountInsurerTemplateAppService Members

        public async Task<IQueryable<AccountInsurerTemplate>> GetAccountInsurerTemplate()
        {
            var user = await _securityContext.GetUserAsync();
            var findAccountInsurerTemplateByAccountId = new FindAccountInsurerTemplateByAccountId
            {
                AccountId = user.AccountId,
                IsActive = true
            };

            var accountInsurerTemplates = await _queryProcessor.ProcessAsync(findAccountInsurerTemplateByAccountId);

            return _mapper.Project<Domain.Entities.AccountInsurerTemplate, AccountInsurerTemplate>(accountInsurerTemplates);
        }

        public async Task DeleteTemplate(int accountInsurerTemplateId)
        {
            var command = new DeleteAccountInsurerTemplate
            {
                AccountInsurerTemplateId = accountInsurerTemplateId
            };

            await _commandProcessor.ProcessAsync(command);
        }

        public async Task<AccountInsurerTemplate> CreateTemplate(CreateAccountInsurerTemplate command)
        {
            command.AccountId = (await _securityContext.GetUserAsync()).AccountId;
            var entity = _mapper.Map<AccountInsurerTemplate>(await _commandProcessor.ProcessAsync(command));
            return entity;
        }

        public async Task<bool> UpdateStatusAccountInsurerTemplate(UpdateStatusAccountInsurerTemplate command)
        {
            return await _commandProcessor.ProcessAsync(command);
        }
        #endregion
    }
}
