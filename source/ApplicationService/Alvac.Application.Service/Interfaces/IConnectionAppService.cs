﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Connections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IConnectionAppService
    {
        Task<Connection> GetByIdAsync(int id);

        Task<IQueryable<Connection>> GetAllAsync();

        Task CreateConnectionAsync(CreateConnection command);

        Task DeleteConnectionAsync(int id);

        Task UpdateConnectionAsync(UpdateConnection command);
    }
}
