﻿using Alvac.Domain.Services.Reports;
using Alvac.Domain.Services.Reports.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IReportAppService
    {
        Task<ReportGeneral> GetGeneralAsync(ReportGeneralQuery query);
        Task<List<ReportQuotationGroupByUser>> GetGroupByUserAsync(ReportGroupByUserQuery query);
        
    }
}
