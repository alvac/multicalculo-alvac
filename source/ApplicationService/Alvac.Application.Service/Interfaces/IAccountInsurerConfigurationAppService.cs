﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.AccountInsurerConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IAccountInsurerConfigurationAppService
    {
        Task<IEnumerable<AccountInsurerConfigurationParameters>> GetAccountInsurerConfigurationParameters(int insurer, int accountInsurerTemplateId);

        Task CreateAccountInsurerConfigurationParameters(List<CreateAccountInsurerConfiguration> createAccountInsurerConfigurationCommand);
    }
}
