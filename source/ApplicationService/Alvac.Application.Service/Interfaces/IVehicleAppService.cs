﻿using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IVehicleAppService
    {
        /// <summary>
        /// Obtem todos os fabricantes
        /// </summary>
        /// <returns></returns>
        Task<IEnumerable<Manufacturer>> GetManufacturersAsync();

        /// <summary>
        /// Obtem todos os veiculos de uma fabricante
        /// </summary>
        /// <param name="manufacturerId">Fabricante veiculo.</param>
        /// <param name="modelYear">Ano modelo.</param>
        /// <returns></returns>
        Task<IQueryable<Vehicle>> GetVehicleByManufacturerAsync(int manufacturerId, int modelYear);

        /// <summary>
        /// Obtem todos os modelos de um veiculos de uma fabricante
        /// </summary>
        /// <param name="manufacturerId">Fabricante veiculo.</param>
        /// <param name="model">Modelo veiculo.</param>
        /// <returns></returns>
        Task<IEnumerable<Vehicle>> GetVehicleByManufacturerAsync(int manufacturerId, string model);

        /// <summary>
        /// Obtem correspondencia do veiculo em uma seguradora
        /// </summary>
        /// <param name="insurer">Seguradora</param>
        /// <param name="vehicleId">Identificador do veiculo</param>
        /// <returns></returns>
        Task<IEnumerable<Vehicle>> GetVehicleByIdInsurerAsync(int insurer, int vehicleId);
    }
}
