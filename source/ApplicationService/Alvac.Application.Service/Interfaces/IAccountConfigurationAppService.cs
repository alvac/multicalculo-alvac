﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.AccountConfigurations;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IAccountConfigurationAppService
    {
        Task CreateEmailConfigurationAsync(CreateEmailConfiguration createEmailConfiguration);
        Task UpdateEmailConfigurationAsync(UpdateEmailConfiguration updateEmailConfiguration);

        Task CreateReportConfigurationAsync(CreateReportConfiguration createReportConfiguration);
        Task UpdateReportConfigurationAsync(UpdateReportConfiguration updateReportConfiguration);

        Task<AccountConfiguration> GetConfigurationAsync();
    }
}
