﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.InsurerValidators;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IInsurerValidatorAppService
    {
        Task<InsurerValidator> GetByIdAsync(int id);

        Task<IQueryable<InsurerValidator>> GetAllAsync();

        Task CreateConnectionAsync(CreateInsurerValidator command);

        Task DeleteConnectionAsync(int id);

        Task UpdateConnectionAsync(UpdateInsurerValidator command);

        Task<InsurerValidatorResponse> Validate(InsurerQuotationRequest insurerQuotationRequest);
    }
}
