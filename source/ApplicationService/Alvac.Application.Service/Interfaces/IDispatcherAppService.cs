﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Dispatchers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IDispatcherAppService
    {
        Task<Dispatcher> GetByIdAsync(int id);

        Task<IEnumerable<Dispatcher>> GetAllAsync();

        Task<Dispatcher> CreateDispatcherAsync(CreateDispatcher command);

        Task<Dispatcher> UpdateDispatcherAsync(UpdateDispatcher command);

        Task DeleteDispatcherAsync(int id);
    }
}
