﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.ConnectionParameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IConnectionParameterAppService
    {
        Task<ConnectionParameter> GetByIdAsync(int id);

        Task<IQueryable<ConnectionParameter>> GetByConnectionIdAsync(int connectionId);

        Task<IQueryable<ConnectionParameter>> GetAllAsync();

        Task CreateConnectionAsync(CreateConnectionParameter command);

        Task DeleteConnectionAsync(int id);

        Task UpdateConnectionAsync(UpdateConnectionParameter command);
    }
}
