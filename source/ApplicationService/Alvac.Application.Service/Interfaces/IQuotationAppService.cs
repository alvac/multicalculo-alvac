﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.DataTransferObject.Commands.Emails;
using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IQuotationAppService
    {
        Task<QuotationApiResponse> ProcessAsync(QuotationApiRequest quotationApiRequest, string ip);
        Task<IEnumerable<InsurerQuotation>> GetQuotationAsync(int quotationId);
        Task<IQueryable<SimpleAutoQuotation>> GetAutoQuotationsAsync();
        Task SendQuotationResponseByEmailAsync(SendQuotationByEmail command);
    }
}
