﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.AccountInsurerTemplates;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IAccountInsurerTemplateAppService
    {
        Task<IQueryable<AccountInsurerTemplate>> GetAccountInsurerTemplate();

        Task DeleteTemplate(int accountInsurerTemplateId);

        Task<AccountInsurerTemplate> CreateTemplate(CreateAccountInsurerTemplate command);

        Task<bool> UpdateStatusAccountInsurerTemplate(UpdateStatusAccountInsurerTemplate command);
    }
}
