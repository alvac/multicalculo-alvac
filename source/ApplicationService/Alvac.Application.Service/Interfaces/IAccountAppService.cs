﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Accounts;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IAccountAppService
    {
        Task<Account> GetCurrentAccountAsync();

        Task CreateAsync(CreateAccount command);

        Task UpdateAsync(UpdateAccount command);

        Task DeleteAsync(int id);

        Task<IQueryable<Account>> GetAccountsAsync();

        Task<Account> GetAccountByIdAsync(int id);
    }
}
