﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Plans;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IPlanAppService
    {
        Task<IEnumerable<Plan>> GetAllAsync();

        Task<Plan> GetByIdAsync(int planId);

        Task CreatePlanAsync(CreatePlan command);

        Task UpdatePlanAsync(UpdatePlan command);

        Task DeletePlanAsync(int id);
    }
}
