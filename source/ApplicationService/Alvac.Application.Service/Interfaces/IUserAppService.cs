﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Users;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IUserAppService
    {
        Task ConfirmEmailAsync(ConfirmEmail confirmEmailCmd);
        Task<User> CreateUserAsync(CreateUser userCmd);
        Task<IQueryable<User>> GetUsersAsync();
        Task<IQueryable<Role>> GetRolesAsync();
        Task<bool> IsUniqueEmailAsync(string email, string id);
        Task DeleteUserAsync(string id);
        Task<User> UpdateUserAsync(UpdateUser user);
        Task<User> GetUserAsync(string id);
        Task<User> GetUserSelfAsync();
    }
}
