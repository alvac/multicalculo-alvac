﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.InsurerConfigurations;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IInsurerConfigurationAppService
    {
        Task<InsurerConfiguration> GetInsurerConfigurationById(int insurerConfigurationId);

        Task<InsurerConfiguration> GetInsurerConfigurationByInsurerKey(int insurer, string key, RequestType requestType);

        Task<IEnumerable<InsurerConfiguration>> GetInsurerConfigurationByInsurer(int insurer, RequestType requestType);

        Task<bool> DeleteInsurerConfiguration(DeleteInsurerConfiguration command);

        Task<InsurerConfiguration> UpdateInsurerConfiguration(UpdateInsurerConfiguration command);

        Task<InsurerConfiguration> CreateInsurerConfiguration(CreateInsurerConfiguration command);
    }
}
