﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Subscriptions;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface ISubscriptionAppService
    {
        Task<IQueryable<Subscription>> GetSubscriptionsByAccountAsync(int accountId);
        Task UpdateSubscriptionAsync(UpdateSubscription command);
        Task CreateSubscriptionAsync(CreateSubscription command);
        Task<Subscription> GetSubscription(int subscriptionId);
    }
}
