﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.AccountConnections;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IAccountConnectionAppService
    {
        Task<AccountConnection> CreateAccountConnection(CreateAccountConnection command);
        Task<bool> DeleteAccountConnection(DeleteAccountConnection command);
        Task<IEnumerable<AccountConnection>> GetInsurersAsync(Alvac.Domain.Enum.RequestType requestType);
        Task<IEnumerable<AccountConnection>> GetInsurersByAccountAsync(int accountId);
    }
}
