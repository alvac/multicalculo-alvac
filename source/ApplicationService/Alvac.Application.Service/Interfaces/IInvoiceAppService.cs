﻿using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services.Interfaces
{
    public interface IInvoiceAppService
    {
        Task<IQueryable<Invoice>> GetInvoicesBySubscriptionAsync(int subscriptionId);
        Task<bool> CloseInvoiceAsync(int invoiceId);
    }
}
