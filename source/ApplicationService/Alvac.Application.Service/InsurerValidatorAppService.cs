﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.InsurerValidators;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.InsurerValidators;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class InsurerValidatorAppService : IInsurerValidatorAppService
    {
        #region Private Fields

        private readonly IContainerProvider _containerProvider;
        protected readonly ICommandProcessor _commandProcessor;
        protected readonly ILogger _logger;
        protected readonly IQueryProcessor _queryProcessor;

        #endregion Private Fields

        #region Constructors

        public InsurerValidatorAppService(IContainerProvider containerProvider, IQueryProcessor queryProcessor, ICommandProcessor commandProcessor, ILogger logger)
        {
            _containerProvider = containerProvider;
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
            _logger = logger;
        }

        #endregion Constructors

        #region IInsurerValidatorAppService Members

        public Task<InsurerValidator> GetByIdAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task<IQueryable<InsurerValidator>> GetAllAsync()
        {
            throw new NotImplementedException();
        }

        public Task CreateConnectionAsync(CreateInsurerValidator command)
        {
            throw new NotImplementedException();
        }

        public Task DeleteConnectionAsync(int id)
        {
            throw new NotImplementedException();
        }

        public Task UpdateConnectionAsync(UpdateInsurerValidator command)
        {
            throw new NotImplementedException();
        }

        public async Task<InsurerValidatorResponse> Validate(InsurerQuotationRequest insurerQuotationRequest)
        {
            var result = new InsurerValidatorResponse
            {
                Code = Alvac.Domain.Enum.Code.Success,
                Descriptions = new List<InsurerValidatorResponseDescription>()
            };

            try
            {
                var findInsurerValidatorByInsurerRequestType = new FindInsurerValidatorByInsurerRequestType
                {
                    Insurer = insurerQuotationRequest.Insurer,
                    RequestType = insurerQuotationRequest.Request.RequestType
                };

                var insurerValidators = await _queryProcessor.ProcessAsync(findInsurerValidatorByInsurerRequestType);

                if (insurerValidators != null)
                {
                    foreach (var insurerValidator in insurerValidators)
                    {
                        var insurerValidatorProcessor = GetValidatorProcessorByFullAssemblyName(insurerValidator.FullAssemblyName);

                        var insurerValidatorResponse = await insurerValidatorProcessor.Validate(insurerQuotationRequest);

                        if (insurerValidatorResponse != null && insurerValidatorResponse.Code == Alvac.Domain.Enum.Code.Failure)
                        {
                            result.Code = Alvac.Domain.Enum.Code.Failure;

                            if (insurerValidatorResponse.Descriptions != null && insurerValidatorResponse.Descriptions.Count > 0)
                            {
                                foreach (var insurerValidatorResponseDescription in insurerValidatorResponse.Descriptions)
                                {
                                    result.Descriptions.Add(insurerValidatorResponseDescription);
                                }
                            }
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                result.Code = Alvac.Domain.Enum.Code.Failure;
                result.Descriptions.Add(new InsurerValidatorResponseDescription { Description = ex.Message, Message = "Erro inesperado ao validar a requisição." });
            }

            return result;
        }

        #endregion

        #region Private Methods

        private IInsurerValidatorProcessor GetValidatorProcessorByFullAssemblyName(string fullAssemblyName)
        {
            Type type = Type.GetType(fullAssemblyName);

            return _containerProvider.GetService(type) as IInsurerValidatorProcessor;
        }

        #endregion
    }
}
