﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.IgnitionBlockers;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class IgnitionBlockerAppService : IIgnitionBlockerAppService
    {
        private readonly IQueryProcessor _queryProcessor;
        private readonly IMapper _mapper;

        public IgnitionBlockerAppService(IQueryProcessor queryProcessor,
            IMapper mapper)
        {
            _queryProcessor = queryProcessor;
            _mapper = mapper;
        }

        public async Task<IEnumerable<IgnitionBlocker>> GetAllAsync()
        {
            var query = new GetAllIgnitionBlockers();

            var response = await _queryProcessor.ProcessAsync(query);
            return _mapper.Map<IEnumerable<IgnitionBlocker>>(response);
        }
    }
}
