﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Reports;
using Alvac.Domain.Services.Reports.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class ReportAppService : IReportAppService
    {
        private readonly IQueryProcessor _queryProcessor;
        private readonly ISecurityContext _securityContext;

        public ReportAppService(IQueryProcessor queryProcessor,
            ISecurityContext securityContext)
        {
            _queryProcessor = queryProcessor;
            _securityContext = securityContext;
        }

        public async Task<ReportGeneral> GetGeneralAsync(ReportGeneralQuery query)
        {
            var user = await _securityContext.GetUserAsync();
            query.AccountId = user.AccountId;
            var response = await _queryProcessor.ProcessAsync(query);
            return response;
        }

        public async Task<List<ReportQuotationGroupByUser>> GetGroupByUserAsync(ReportGroupByUserQuery query)
        {
            var user = await _securityContext.GetUserAsync();
            query.AccountId = user.AccountId;
            var response = await _queryProcessor.ProcessAsync(query);
            return response;
        }
    }
}
