﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Enum;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Insurers;
using Alvac.Domain.Contract;

namespace Alvac.Application.Services
{
    public class InsurerAppService : IInsurerAppService
    {
        private readonly IMapper _mapper;
        private readonly IQueryProcessor _queryProcessor;

        public InsurerAppService(IQueryProcessor queryProcessor,
            IMapper mapper)
        {
            _mapper = mapper;
            _queryProcessor = queryProcessor;
        }

        public async Task<IEnumerable<Insurer>> GetInsurersAsync()
        {
            var query = new GetAll();
            var model = await _queryProcessor.ProcessAsync(query);
            return _mapper.Map<IEnumerable<Insurer>>(model);
        }
    }
}
