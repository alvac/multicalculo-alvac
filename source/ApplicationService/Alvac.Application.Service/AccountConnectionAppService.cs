﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.AccountConnections;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.AccountConnections;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class AccountConnectionAppService : IAccountConnectionAppService
    {
        private readonly IMapper _mapper;
        private readonly ISecurityContext _securityContext;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _commandProcessor;

        public AccountConnectionAppService(IMapper mapper,
            ISecurityContext securityContext,
            IQueryProcessor queryProcessor,
            ICommandProcessor commandProcessor)
        {
            _mapper = mapper;
            _securityContext = securityContext;
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
        }

        public async Task<AccountConnection> CreateAccountConnection(CreateAccountConnection command)
        {
            return _mapper.Map<AccountConnection>(await _commandProcessor.ProcessAsync(command));
        }

        public async Task<bool> DeleteAccountConnection(DeleteAccountConnection command)
        {
            return await _commandProcessor.ProcessAsync(command);
        }

        public async Task<IEnumerable<AccountConnection>> GetInsurersAsync(Domain.Enum.RequestType requestType)
        {
            var user = await _securityContext.GetUserAsync();

            var query = new FindAccountConnectionsByAccountRequestType
            {
                AccountId = user.AccountId,
                RequestType = requestType
            };

            var model = await _queryProcessor.ProcessAsync(query);
            var response = _mapper.Map<IEnumerable<AccountConnection>>(model);
            return response;
        }


        public async Task<IEnumerable<AccountConnection>> GetInsurersByAccountAsync(int accountId)
        {
            var user = await _securityContext.GetUserAsync();

            var query = new FindAccountConnectionsByAccount
            {
                AccountId = accountId,
            };

            var model = await _queryProcessor.ProcessAsync(query);
            var response = _mapper.Map<IEnumerable<AccountConnection>>(model);
            return response;
        }
    }
}
