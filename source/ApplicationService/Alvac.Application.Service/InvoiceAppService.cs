﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Invoices;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Invoices;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class InvoiceAppService : IInvoiceAppService
    {
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _commandProcessor;
        private readonly IMapper _mapper;

        public InvoiceAppService(IQueryProcessor queryProcessor,
            ICommandProcessor commandProcessor,
            IMapper mapper)
        {
            _queryProcessor = queryProcessor;
            _mapper = mapper;
            _commandProcessor = commandProcessor;
        }

        public async Task<IQueryable<Invoice>> GetInvoicesBySubscriptionAsync(int subscriptionId)
        {
            var query = new GetInvoiceBySubscriptionId
            {
                SubscriptionId = subscriptionId
            };

            var response = _mapper.Project<Domain.Entities.Invoice, Invoice>(await _queryProcessor.ProcessAsync(query));
            return response;
        }

        public async Task<bool> CloseInvoiceAsync(int invoiceId)
        {
            var command = new CloseInvoice
            {
                InvoiceId = invoiceId
            };

            await _commandProcessor.ProcessAsync(command);
            return true;
        }
    }
}
