﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.WorkActivities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class WorkActivityAppService : IWorkActivityAppService
    {
        private readonly IQueryProcessor _queryProcessor;
        private readonly IMapper _mapper;

        public WorkActivityAppService(IQueryProcessor queryProcessor, IMapper mapper)
        {
            _queryProcessor = queryProcessor;
            _mapper = mapper;
        }

        public async Task<IEnumerable<WorkActivity>> GetWorkActivitiesAsync()
        {
            var works = await _queryProcessor.ProcessAsync(new GetWorkActivities());
            return _mapper.Map<IEnumerable<WorkActivity>>(works);
        }
    }
}
