﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Users;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Users;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web;

namespace Alvac.Application.Services
{
    public class UserAppService : IUserAppService
    {
        private readonly ISecurityContext _securityContext;
        private readonly IMapper _mapper;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _comandProcessor;
        private readonly UserManager<Domain.Entities.User> _manager;

        public UserAppService(ISecurityContext securityContext,
            IMapper mapper,
            IQueryProcessor queryProcessor,
            ICommandProcessor comandProcessor,
            UserManager<Domain.Entities.User> manager)
        {
            _manager = manager;
            _securityContext = securityContext;
            _mapper = mapper;
            _queryProcessor = queryProcessor;
            _comandProcessor = comandProcessor;
        }

        public async Task<User> CreateUserAsync(CreateUser userCmd)
        {
            var currentUser = await _securityContext.GetUserAsync();
            userCmd.AccountId = currentUser.AccountId;

            var user = await _comandProcessor.ProcessAsync(userCmd);

            var token = await _manager.GenerateEmailConfirmationTokenAsync(user.Id);
            var callbackUrl = string.Format("http://{0}/auth/{1}/token/{2}", ConfigurationManager.AppSettings.Get("AppUri"), HttpUtility.UrlEncode(user.UserName), HttpUtility.UrlEncode(token));
            await _manager.SendEmailAsync(user.Id, "Confirme a sua conta", "Por favor confirme a sua conta clicando nesse <a href=\"" + callbackUrl + "\">link</a>");
            return _mapper.Map<User>(user);
        }

        public async Task ConfirmEmailAsync(ConfirmEmail confirmEmailCmd)
        {
            await _comandProcessor.ProcessAsync(confirmEmailCmd);
        }

        public async Task<IQueryable<User>> GetUsersAsync()
        {
            var user = await _securityContext.GetUserAsync();
            var query = new GetUsersByAccountId
            {
                AccountId = user.AccountId
            };

            var response = await _queryProcessor.ProcessAsync(query);
            return _mapper.Map<List<User>>(response).AsQueryable();
        }

        public async Task<IQueryable<Role>> GetRolesAsync()
        {
            var query = new GetRoles();

            var response = await _queryProcessor.ProcessAsync(query);
            return _mapper.Project<IdentityRole, Role>(response);
        }

        public async Task<bool> IsUniqueEmailAsync(string email, string id)
        {
            var query = new GetUserByEmail
            {
                Email = email
            };

            var response = await _queryProcessor.ProcessAsync(query);
            if (response != null)
            {
                return string.IsNullOrEmpty(id) ? false : response.Id == id;
            }

            return true;
        }

        public async Task DeleteUserAsync(string id)
        {
            var cmd = new DeleteUser
            {
                Id = id
            };

            await _comandProcessor.ProcessAsync(cmd);
        }

        public async Task<User> UpdateUserAsync(UpdateUser user)
        {
            var response = await _comandProcessor.ProcessAsync(user);
            return _mapper.Map<User>(response);
        }

        public async Task<User> GetUserAsync(string id)
        {
            var query = new GetUserById
            {
                Id = id
            };

            var response = await _queryProcessor.ProcessAsync(query);
            return _mapper.Map<User>(response);
        }

        public async Task<User> GetUserSelfAsync()
        {
            var user = await _securityContext.GetUserAsync();
            return _mapper.Map<User>(user);
        }
    }
}
