﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.InsurerConfigurations;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.InsurerConfigurations;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class InsurerConfigurationAppService : IInsurerConfigurationAppService
    {
        #region Private Fields

        private readonly IQueryProcessor _queryProcessor;
        private readonly IMapper _mapper;
        private readonly ICommandProcessor _commandProcessor;

        #endregion

        #region Constructor

        public InsurerConfigurationAppService(IQueryProcessor queryProcessor, 
            IMapper mapper,
            ICommandProcessor commandProcessor)
        {
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
            _mapper = mapper;
        }

        #endregion

        #region IInsurerConfigurationAppService Members

        public async Task<InsurerConfiguration> GetInsurerConfigurationById(int insurerConfigurationId)
        {
            var findInsurerConfigurationById = new FindInsurerConfigurationById
            {
                InsurerConfigurationId = insurerConfigurationId
            };

            var insurerConfiguration = await _queryProcessor.ProcessAsync<Alvac.Domain.Entities.InsurerConfiguration>(findInsurerConfigurationById);

            return _mapper.Map<InsurerConfiguration>(insurerConfiguration);
        }

        public async Task<InsurerConfiguration> GetInsurerConfigurationByInsurerKey(int insurer, string key, RequestType requestType)
        {
            var findInsurerConfigurationByInsurerKey = new FindInsurerConfigurationByInsurerKey
            {
                Insurer = insurer,
                Key = key
            };

            var insurerConfiguration = await _queryProcessor.ProcessAsync<Alvac.Domain.Entities.InsurerConfiguration>(findInsurerConfigurationByInsurerKey);

            return _mapper.Map<InsurerConfiguration>(insurerConfiguration);
        }

        public async Task<IEnumerable<InsurerConfiguration>> GetInsurerConfigurationByInsurer(int insurer, RequestType requestType)
        {
            var findInsurerConfigurationByInsurer = new FindInsurerConfigurationByInsurer
            {
                Insurer = insurer,
                RequestType = requestType
            };

            var insurerConfigurations = await _queryProcessor.ProcessAsync<IEnumerable<Alvac.Domain.Entities.InsurerConfiguration>>(findInsurerConfigurationByInsurer);

            return _mapper.Map<IEnumerable<InsurerConfiguration>>(insurerConfigurations);
        }

        public async Task<bool> DeleteInsurerConfiguration(DeleteInsurerConfiguration command)
        {
            return await _commandProcessor.ProcessAsync(command);
        }

        public async Task<InsurerConfiguration> UpdateInsurerConfiguration(UpdateInsurerConfiguration command)
        {
            var response = await _commandProcessor.ProcessAsync(command);
            return _mapper.Map<InsurerConfiguration>(response);
        }

        public async Task<InsurerConfiguration> CreateInsurerConfiguration(CreateInsurerConfiguration command)
        {
            var response = await _commandProcessor.ProcessAsync(command);
            return _mapper.Map<InsurerConfiguration>(response);
        }

        #endregion
    }
}
