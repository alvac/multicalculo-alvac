﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Plans;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Plans;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class PlanAppService : IPlanAppService
    {
        private readonly ISecurityContext _securityContext;
        private readonly IMapper _mapper;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _commandProcessor;

        public PlanAppService(ISecurityContext securityContext,
            IMapper mapper,
            IQueryProcessor queryProcessor,
            ICommandProcessor commandProcessor)
        {
            _securityContext = securityContext;
            _mapper = mapper;
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
        }

        #region IPlanAppService Members

        public async Task<IEnumerable<Plan>> GetAllAsync()
        {
            return _mapper.Map<IEnumerable<Plan>>(await _queryProcessor.ProcessAsync(new GetAll()));
        }

        public async Task CreatePlanAsync(CreatePlan command)
        {
            await _commandProcessor.ProcessAsync(command);
        }

        public async Task DeletePlanAsync(int id)
        {
            await _commandProcessor.ProcessAsync(new DeletePlan
            {
                PlanId = id
            });
        }

        public async Task UpdatePlanAsync(UpdatePlan command)
        {
            await _commandProcessor.ProcessAsync(command);
        }

        public async Task<Plan> GetByIdAsync(int planId)
        {
            var findPlanById = new GetPlanById
            {
                PlanId = planId
            };

            return _mapper.Map<Plan>(await _queryProcessor.ProcessAsync(findPlanById));
        }

        #endregion



    }
}
