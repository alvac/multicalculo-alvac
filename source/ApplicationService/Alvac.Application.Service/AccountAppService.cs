﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Accounts;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Accounts;
using Alvac.Domain.Entities.Queries.Users;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Application.Services
{
    public class AccountAppService : IAccountAppService
    {
        private readonly ISecurityContext _securityContext;
        private readonly IMapper _mapper;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ICommandProcessor _commandProcessor;

        public AccountAppService(ISecurityContext securityContext,
            IMapper mapper,
            IQueryProcessor queryProcessor,
            ICommandProcessor commandProcessor)
        {
            _securityContext = securityContext;
            _mapper = mapper;
            _queryProcessor = queryProcessor;
            _commandProcessor = commandProcessor;
        }

        public async Task<Account> GetCurrentAccountAsync()
        {
            var user = await _securityContext.GetUserAsync();

            var findAccountById = new FindAccountById
            {
                AccountId = user.AccountId
            };

            var account = await _queryProcessor.ProcessAsync(findAccountById);

            return _mapper.Map<Account>(account);
        }

        public async Task CreateAsync(CreateAccount command)
        {
            await _commandProcessor.ProcessAsync(command);
        }

        public async Task UpdateAsync(UpdateAccount command)
        {
            if (!_securityContext.IsInRole(Domain.Enum.Role.AlvacString))
            {
                var user = await _securityContext.GetUserAsync();
                if (command.AccountId != user.AccountId)
                {
                    throw new AlvacException("Você não tem permissão para efetuar esse ação.", Domain.Enum.DescriptionCode.Unauthorized);
                }
            }

            await _commandProcessor.ProcessAsync(command);
        }

        public async Task<IQueryable<Account>> GetAccountsAsync()
        {
            var query = new GetActiveAccounts();

            var accounts = await _queryProcessor.ProcessAsync(query);
            return _mapper.Project<Domain.Entities.Account, Account>(accounts).OrderBy(x => x.Name);
        }

        public async Task<Account> GetAccountByIdAsync(int id)
        {
            var findAccountById = new FindAccountById
            {
                AccountId = id
            };

            var account = await _queryProcessor.ProcessAsync(findAccountById);

            return _mapper.Map<Account>(account);
        }


        public async Task DeleteAsync(int id)
        {
            await _commandProcessor.ProcessAsync(new DeleteAccount
            {
                AccountId = id
            });
        }
    }
}
