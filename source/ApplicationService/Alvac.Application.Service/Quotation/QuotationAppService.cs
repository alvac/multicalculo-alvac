﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Dispatchers;
using Alvac.Domain.Entities.Queries.Quotations;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using Alvac.Domain.DataTransferObject.Commands.Emails;
using Alvac.Domain.Entities.Queries.InsurerQuotations;
using Alvac.Domain.Entities.Queries.AccountConfigurations;
using Alvac.Domain.Services;

namespace Alvac.Application.Services.Quotation
{
    public class QuotationAppService : IQuotationAppService
    {
        #region Private Fields

        private readonly ISecurityContext _securityContext;
        private readonly IQuotationServiceFactory _quotationServiceFactory;
        private readonly IMapper _mapper;
        private readonly ILogger _logger;
        private readonly IQueryProcessor _queryProcessor;
        private readonly IEmailSender _emailSender;

        #endregion Private Fields

        #region Constructor

        public QuotationAppService(IQuotationServiceFactory quotationServiceFactory,
            ISecurityContext securityContext,
            IMapper mapper,
            ILogger logger,
            IQueryProcessor queryProcessor,
            IEmailSender emailSender)
        {
            _quotationServiceFactory = quotationServiceFactory;
            _securityContext = securityContext;
            _mapper = mapper;
            _logger = logger;
            _queryProcessor = queryProcessor;
            _emailSender = emailSender;
        }

        #endregion Constructor

        #region IQuotationAppService Members

        public async Task<QuotationApiResponse> ProcessAsync(QuotationApiRequest quotationApiRequest, string ip)
        {
            QuotationApiResponse response = null;

            await _logger.WriteVerboseAsync("QuotationAppService", "ProcessAsync", "Começo do processamento da cotação.");

            ExceptionDispatchInfo exception = null;

            try
            {
                if (quotationApiRequest != null)
                {
                    if (quotationApiRequest.Request != null)
                    {
                        await SetCredentials(quotationApiRequest.Request, ip);

                        var insurerQuotationRequestList = quotationApiRequest.ToInsurerQuotations(_mapper);

                        if (insurerQuotationRequestList != null && insurerQuotationRequestList.Count() > 0)
                        {
                            var quotation = await RegisterAndEnqueueAsync(insurerQuotationRequestList, quotationApiRequest.Request.RequestType);

                            if (quotation != null)
                            {
                                response = new QuotationApiResponse
                                {
                                    QuotationId = quotation.QuotationId,
                                    Result = new OperationResult
                                    {
                                        Code = Code.Success,
                                        DescriptionCode = DescriptionCode.Successfully,
                                        Description = "Request enqueued"
                                    }
                                };
                            }
                            else
                            {
                                throw new Exception("Erro inesperado. Quotation <null>.");
                            }
                        }
                        else
                        {
                            throw new AlvacException("InsurerQuotation é vazio ou null.", DescriptionCode.InvalidRequest);
                        }
                    }
                    else
                    {
                        throw new AlvacException("Base request é vazio ou null.", DescriptionCode.InvalidRequest);
                    }
                }
                else
                {
                    throw new AlvacException("QuotationApiRequest é vazio ou null.", DescriptionCode.InvalidRequest);
                }
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                response = new QuotationApiResponse
                {
                    Result = new OperationResult
                    {
                        Code = Code.Failure,
                        DescriptionCode = DescriptionCode.InternalServerError,
                        Description = "Unexpected error enqueue request."
                    }
                };

                await _logger.WriteErrorAsync("QuotationAppService", "ProcessAsync", "Erro inesperado", exception.SourceException);
            }

            return response;
        }

        public async Task<IEnumerable<Domain.DataTransferObject.InsurerQuotation>> GetQuotationAsync(int quotationId)
        {
            var query = new GetInsurerQuotationByQuotationId
            {
                QuotationId = quotationId
            };

            var quotations = await _queryProcessor.ProcessAsync(query);
            return _mapper.Map<IEnumerable<InsurerQuotation>>(quotations);
        }

        public async Task<IQueryable<SimpleAutoQuotation>> GetAutoQuotationsAsync()
        {
            var user = await _securityContext.GetUserAsync();

            IQuery<IQueryable<AutoQuotation>> query;

            if (user.Roles.Any(x => x.RoleId == Domain.Enum.Role.Master))
            {
                query = new GetAutoQuotationsByAccountId
                {
                    AccountId = user.AccountId
                };
            }
            else
            {
                query = new GetAutoQuotationsByUserId
                {
                    UserId = user.Id
                };
            }

            var quotations = await _queryProcessor.ProcessAsync(query);
            return _mapper.Project<AutoQuotation, SimpleAutoQuotation>(quotations);
        }

        public async Task SendQuotationResponseByEmailAsync(SendQuotationByEmail command)
        {
            var user = await _securityContext.GetUserAsync();

            var queryQuotations = new GetInsurerQuotationsById
            {
                InsurerQuotationId = command.InsurerQuotations
            };

            var quotations = await _queryProcessor.ProcessAsync(queryQuotations);

            var queryAccountConfiguration = new GetAccountConfiguration
            {
                AccountId = user.AccountId
            };

            var accountConfiguration = await _queryProcessor.ProcessAsync(queryAccountConfiguration);

            await _emailSender.SendAsync(accountConfiguration.EmailHost, accountConfiguration.EmailPort, accountConfiguration.EmailUsername,
                accountConfiguration.EmailPassword, accountConfiguration.EmailEnableSsl, accountConfiguration.EmailUsername, command.Email,
                "Resultado cotação", "BODY HTML");
        }

        #endregion IQuotationAppService Members

        #region Private Methods

        /// <summary>
        /// Registra a cotação de cada seguradora e enfileira as requisições.
        /// </summary>
        /// <param name="insurerQuotationRequestList"></param>
        /// <param name="requestType"></param>
        /// <returns></returns>
        private async Task<Domain.Entities.Quotation> RegisterAndEnqueueAsync(IEnumerable<InsurerQuotationRequest> insurerQuotationRequestList, RequestType requestType)
        {
            var quotationService = _quotationServiceFactory.Create(requestType);

            int enqueueCount = 0;

            var quotation = await quotationService.RegisterQuotationAsync(insurerQuotationRequestList.First().Request);

            foreach (var insurerQuotationRequest in insurerQuotationRequestList)
            {
                if (insurerQuotationRequest.Request != null)
                {
                    ExceptionDispatchInfo registerException = null;

                    try
                    {
                        await _logger.WriteVerboseAsync("QuotationAppService", "ProcessAsync",
                            string.Format("Começo do processo para colocar na fila o pedido insurerQuotationRequest '{0}'.",
                        insurerQuotationRequest.Request.Identifier));

                        var connection = await quotationService.GetConnectionAsync(insurerQuotationRequest.Request.AccountId, insurerQuotationRequest.Insurer,
                            insurerQuotationRequest.Request.RequestType);

                        if (connection != null)
                        {
                            if (connection.RetryMax.HasValue && connection.RetryMax.Value > 0)
                            {
                                insurerQuotationRequest.Request.Retry = new RetryRequest
                                {
                                    Max = connection.RetryMax.Value,
                                    Interval = connection.RetryInterval.HasValue ? connection.RetryInterval.Value : 0
                                };
                            }


                            await quotationService.RegisterInsurerQuotationAsync(insurerQuotationRequest, quotation.QuotationId, connection.ConnectionId);

                            ExceptionDispatchInfo enqueueException = null;

                            try
                            {
                                await quotationService.EnqueueRequestAsync(insurerQuotationRequest, connection.InputQueueName);

                                await quotationService.UpdateInsurerQuotationStatusAsync(insurerQuotationRequest.Request.Identifier,
                                    connection.ConnectionId, Status.Enqueued, DescriptionCode.Successfully,
                                    "Pedido enfileirada com sucesso.");

                                enqueueCount++;
                            }
                            catch (Exception ex)
                            {
                                enqueueException = ExceptionDispatchInfo.Capture(ex);
                            }

                            if (enqueueException != null)
                            {
                                await quotationService.UpdateInsurerQuotationStatusAsync(insurerQuotationRequest.Request.Identifier, connection.ConnectionId,
                                    Status.Failure, DescriptionCode.InternalServerError,
                                    enqueueException.SourceException.Message);

                                await _logger.WriteErrorAsync("QuotationAppService", "ProcessAsync",
                                    string.Format("Falha para enfileirar o pedido InsurerQuotationId: {0}",
                                    insurerQuotationRequest.Request.Identifier), enqueueException.SourceException);
                            }
                        }
                        else
                        {
                            throw new AlvacException(string.Format("Coneção nao encontrada. Parametros - AccountId: {0} - Insurer: {1} - RequestType: {2}",
                                insurerQuotationRequest.Request.AccountId, insurerQuotationRequest.Insurer,
                                insurerQuotationRequest.Request.RequestType), DescriptionCode.NotFound);
                        }
                    }
                    catch (Exception ex)
                    {
                        registerException = ExceptionDispatchInfo.Capture(ex);
                    }

                    if (registerException != null)
                    {
                        await _logger.WriteErrorAsync("QuotationAppService", "ProcessAsync", string.Format("Erro ao registrar pedido - InsurerQuotationId: {0}",
                            insurerQuotationRequest.Request.Identifier), registerException.SourceException);
                    }
                }
                else
                {
                    await _logger.WriteErrorAsync("QuotationAppService", "ProcessAsync", "InsurerQuotation e vazio ou null.");
                }
            }

            if (enqueueCount == 0)
            {
                throw new Exception("Nenhum pedido foi enfileirado.");
            }

            return quotation;
        }

        private async Task SetCredentials(BaseRequest request, string ip)
        {
            var currentUser = await _securityContext.GetUserAsync();

            request.AccountId = currentUser.AccountId;
            request.UserId = currentUser.Id;
            request.Ip = ip;
        }

        #endregion Private Methods
    }
}
