﻿using Alvac.Domain.Services;
using Mandrill;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.Util.Email
{
    public class MandrillEmailSender : IEmailSender
    {
        private readonly MandrillApi _api;

        public MandrillEmailSender()
        {
            _api = new MandrillApi("AaSgvtobVBHaQRSzvgJmWQ");
        }

        public async Task SendAsync(string from, string to, string subject, string html)
        {
            var response = await _api.SendMessageAsync(new EmailMessage
            {
                from_email = from,
                from_name = from,
                to = new List<EmailAddress>
                    {
                        new EmailAddress(to)
                    },
                subject = subject,
                html = html
            });
        }

        public async Task SendAsync(string host, int port, string user, string password, bool enableSsl, 
            string from, string to, string subject, string html)
        {
            var mail = new MailMessage(from, to);
            using (var client = new SmtpClient())
            {
                client.Credentials = new System.Net.NetworkCredential(user, password);
                client.Port = port;
                client.Host = host;
                client.EnableSsl = enableSsl;

                mail.Subject = subject;
                mail.Body = html;
                await client.SendMailAsync(mail);
            }
        }
    }
}
