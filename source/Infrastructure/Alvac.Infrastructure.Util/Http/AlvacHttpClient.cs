﻿using Alvac.Domain.Services.Interfaces;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.Util.Http
{
    public class AlvacHttpClient : IHttpClient
    {
        private readonly HttpClient _httpClient;
        private readonly string _baseUri;

        public AlvacHttpClient()
        {
            _baseUri = string.Empty;
            _httpClient = new HttpClient();
        }

        public AlvacHttpClient(string baseUri, string token)
        {
            _baseUri = baseUri;
            _httpClient = new HttpClient();
            _httpClient.DefaultRequestHeaders.Authorization = new System.Net.Http.Headers.AuthenticationHeaderValue("Bearer", token);
        }

        #region IHttpClient Members

        public TimeSpan Timeout
        {
            get
            {
                return _httpClient.Timeout;
            }
            set
            {
                _httpClient.Timeout = value;
            }
        }

        public Task<HttpResponseMessage> GetAsync(Uri uri)
        {
            return _httpClient.GetAsync(uri);
        }

        public Task<HttpResponseMessage> PostAsync(HttpRequestMessage requestMessage)
        {
            return _httpClient.SendAsync(requestMessage);
        }

        public async Task<TResult> GetAsync<TResult>(string uri)
        {
            var message = await _httpClient.GetAsync(_baseUri + uri);

            if(!message.IsSuccessStatusCode)
            {
                throw new Exception(await message.Content.ReadAsStringAsync());
            }

            return await message.Content.ReadAsObjAsync<TResult>();
        }

        public async Task<HttpResponseMessage> PostAsync(string uri, object content)
        {
            return await _httpClient.PostAsync(_baseUri + uri, new StringContent(JsonConvert.SerializeObject(content), UTF8Encoding.UTF8, "application/json"));
        }

        public async Task<TResult> PostAsync<TResult>(string uri, object content)
        {
            var message = await _httpClient.PostAsync(_baseUri + uri, new StringContent(JsonConvert.SerializeObject(content), UTF8Encoding.UTF8, "application/json"));
            return await message.Content.ReadAsObjAsync<TResult>();
        }

        public async Task<HttpResponseMessage> PutAsync(string uri, object content)
        {
            return await _httpClient.PutAsync(_baseUri + uri, new StringContent(JsonConvert.SerializeObject(content), UTF8Encoding.UTF8, "application/json"));
        }

        public async Task<TResult> PutAsync<TResult>(string uri, object content)
        {
            var message = await _httpClient.PutAsync(_baseUri + uri, new StringContent(JsonConvert.SerializeObject(content), UTF8Encoding.UTF8, "application/json"));
            return await message.Content.ReadAsObjAsync<TResult>();
        }

        public async Task<HttpResponseMessage> DeleteAsync(string uri)
        {
            return await _httpClient.DeleteAsync(_baseUri + uri);
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                _httpClient.Dispose();
            }
        }

        #endregion
    }

    public static class ContentExtensions
    {
        public static async Task<T> ReadAsObjAsync<T>(this HttpContent content)
        {
            var stringContent = await content.ReadAsStringAsync();
            return JsonConvert.DeserializeObject<T>(stringContent);
        }
    }
}
