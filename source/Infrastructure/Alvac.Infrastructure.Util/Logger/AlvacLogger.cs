﻿using Alvac.Domain.Services.Logger;
using NLog;
using System;
using System.Diagnostics;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.Util.Logger
{
    public class AlvacLogger : ILogger
    {
        private static NLog.Logger _logger = LogManager.GetCurrentClassLogger();

        public AlvacLogger()
        {
            //Para as dlls serem copiadas para o projeto que referencia elas precisam estar sendo utilizadas
            //como essas duas assembly são carregar via web.config pelo logger precisamos cadastrar explicitamente o uso delas
            //para que elas sejam jogadas na bin dos projetos que utilizarem o log
            var loadLogEntriesCoreAssembly = new LogentriesCore.Net.AsyncLogger();
            var loadLogEntriesNlogAssembly = new NLog.Targets.LogentriesTarget();
        }

        #region ILogger Members

        public async Task WriteVerboseAsync(string source, string eventName, string message)
        {
            await WriteAsync(null, source, TraceEventType.Verbose, message);
        }

        public async Task WriteVerboseAsync(string correlationId, string source, string eventName, string message)
        {
            await WriteAsync(correlationId, source, TraceEventType.Verbose, message);
        }

        public async Task WriteInformationAsync(string source, string eventName, string message)
        {
            await WriteAsync(null, source, TraceEventType.Information, message);
        }

        public async Task WriteInformationAsync(string correlationId, string source, string eventName, string message)
        {
            await WriteAsync(correlationId, source, TraceEventType.Information, message);
        }

        public async Task WriteWarningAsync(string source, string eventName, string message, Exception ex = null)
        {
            await WriteAsync(string.Empty, source, TraceEventType.Warning, message, ex);
        }
        public async Task WriteWarningAsync(string source, string eventName, string message, string correlationId, Exception ex = null)
        {
            await WriteAsync(correlationId, source, TraceEventType.Warning, message, ex);
        }

        public async Task WriteErrorAsync(string source, string eventName, string message, Exception ex = null)
        {
            await WriteAsync(string.Empty, source, TraceEventType.Error, message);
        }
        public async Task WriteErrorAsync(string source, string eventName, string message, string correlationId, Exception ex = null)
        {
            await WriteAsync(correlationId, source, TraceEventType.Error, message, ex);
        }

        public async Task WriteCriticalAsync(string source, string eventName, string message, Exception ex = null)
        {
            await WriteAsync(string.Empty, source, TraceEventType.Critical, message, ex);
        }
        public async Task WriteCriticalAsync(string source, string eventName, string message, string correlationId, Exception ex = null)
        {
            await WriteAsync(correlationId, source, TraceEventType.Critical, message, ex);
        }

        public async Task WriteWarningAsync(string source, string eventName, Exception ex)
        {
            await WriteAsync(string.Empty, source, TraceEventType.Warning, ex.Message, ex);
        }

        public async Task WriteErrorAsync(string source, string eventName, Exception ex)
        {
            await WriteAsync(string.Empty, source, TraceEventType.Error, ex.Message, ex);
        }

        public async Task WriteCriticalAsync(string source, string eventName, Exception ex)
        {
            await WriteAsync(string.Empty, source, TraceEventType.Critical, ex.Message, ex);
        }

        #endregion

        #region Private Methods

        private Task WriteAsync(string correlationId, string source, TraceEventType level, string message, Exception ex = null)
        {
            try
            {
                _logger.Log(new LogEventInfo
                    {
                        Level = GetLevel(level),
                        Exception = ex,
                        Message = message,
                        LoggerName = source,
                        Parameters = new[] { new { correlationId } }
                    });
            }
            catch
            {

            }

            return Task.FromResult<object>(null);
        }

        private LogLevel GetLevel(TraceEventType level)
        {
            switch (level)
            {
                case TraceEventType.Critical:
                    return LogLevel.Fatal;
                case TraceEventType.Error:
                    return LogLevel.Error;
                case TraceEventType.Information:
                    return LogLevel.Info;
                case TraceEventType.Verbose:
                    return LogLevel.Debug;
                case TraceEventType.Warning:
                    return LogLevel.Warn;
                default:
                    return LogLevel.Trace;
            }
        }
        #endregion

        
    }
}
