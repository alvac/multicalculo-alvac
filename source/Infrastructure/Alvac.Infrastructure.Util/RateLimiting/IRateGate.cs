﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.Util.RateLimiting
{
    public interface IRateGate
    {
        int Occurrences { get; }
        int TimeUnitMilliseconds { get; }


        Task WaitToProceedAsync();
        Task<bool> WaitToProceedAsync(int millisecondsTimeout);
        Task<bool> WaitToProceedAsync(TimeSpan timeout);
    }
}
