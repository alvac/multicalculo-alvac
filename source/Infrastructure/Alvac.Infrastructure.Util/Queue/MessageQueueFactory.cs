﻿using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Queue.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.Util.Queue
{
    public class MessageQueueFactory : IMessageQueueFactory
    {
        #region Private Fields

        private readonly IServiceProvider _serviceProvider;

        #endregion

        #region Constructor

        public MessageQueueFactory(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion

        #region IMessageQueueFactory Members

        public IMessageQueue<T> Create<T>(string queueName) where T : class
        {
            var messageQueue = _serviceProvider.GetService(typeof(ServiceBusQueue<T>)) as IMessageQueue<T>;

            if (messageQueue != null)
            {
                messageQueue.Initializer(queueName);
            }
            else
            {
                throw new Exception("ServiceProvider get 'IMessageQueue<T>' service error.");
            }

            return messageQueue;
        }

        #endregion
    }
}
