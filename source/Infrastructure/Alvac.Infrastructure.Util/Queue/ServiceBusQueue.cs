﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Logger;
using Alvac.Domain.Services.Queue;
using Alvac.Domain.Services.Queue.Interfaces;
using Microsoft.Azure;
using Microsoft.ServiceBus;
using Microsoft.ServiceBus.Messaging;
using Microsoft.WindowsAzure;
using System;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.Util.Queue
{
    public class ServiceBusQueue<T> : IMessageQueue<T> where T : class
    {
        #region Private Fields

        private readonly ILogger _logger;

        private QueueClient _queueClient;

        private string _queueName;

        private string _serviceBusConnectionString;

        private static object _syncRoot = new object();

        #endregion

        #region Constructor

        public ServiceBusQueue(ILogger logger)
        {
            _logger = logger;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Create queue if not exists.
        /// </summary>
        private void CheckCreateQueue(string queueName)
        {
            var namespaceManager = NamespaceManager.CreateFromConnectionString(_serviceBusConnectionString);

            if (!namespaceManager.QueueExists(queueName))
            {
                lock (_syncRoot)
                {
                    if (!namespaceManager.QueueExists(queueName))
                    {
                        namespaceManager.CreateQueue(queueName);
                    }
                }

                _logger.WriteVerboseAsync(string.Empty, "SbQueue", "CheckCreateQueue", string.Format("Create queue - QueueName: {0}", queueName)).Wait();
            }
        }

        #endregion

        #region IMessageQueue Members

        public async Task<string> PushAsync(string correlationId, string body)
        {
            return await PushAsync(correlationId, string.Empty, body);
        }

        public async Task<string> PushAsync(string correlationId, string label, string body)
        {
            using (BrokeredMessage message = new BrokeredMessage(body))
            {
                message.CorrelationId = correlationId;
                message.Label = label;

                await _queueClient.SendAsync(message);

                await _logger.WriteVerboseAsync(string.Empty, "ServiceBusQueue", "PushAsync", string.Format("Message send: MessageId = {0}, Body = {1}", message.MessageId, body));

                return message.MessageId;
            }
        }

        public async Task PushAsync(QueueMessage<T> message)
        {
            var body = AlvacJson<T>.Serialize(message.Item);

            using (var brokeredMessage = new BrokeredMessage(body))
            {
                brokeredMessage.CorrelationId = message.CorrelationId;

                await _queueClient.SendAsync(brokeredMessage);
                message.MessageId = brokeredMessage.MessageId;

                await _logger.WriteVerboseAsync(string.Empty, "ServiceBusQueue", "PushAsync", string.Format("Message send: MessageId = {0}, Body = {1}", message.MessageId, body));
            }


        }

        public async Task<QueueMessage<T>> ReceiveAsync()
        {
            try
            {
                var message = await _queueClient.ReceiveAsync();

                if (message == null)
                {
                    return null;
                }

                var body = message.GetBody<string>();

                var item = AlvacJson<T>.Deserialize(body);

                var result = new QueueMessage<T>
                 {
                     MessageId = message.MessageId,
                     Item = AlvacJson<T>.Deserialize(body),
                     CorrelationId = message.CorrelationId
                 };

                await _logger.WriteInformationAsync(string.Empty, "ServiceBusQueue", "ReceiveAsync", string.Format("Message received: MessageId = {0}, Body = {1}", message.MessageId, body));

                return result;
            }
            catch (Exception ex)
            {
                string error = ex.ToString();

                throw;
            }

        }

        public async Task<QueueMessage<T>> ReceiveAsync(TimeSpan timeout)
        {
            if (timeout != TimeSpan.Zero)
            {
                var message = await _queueClient.ReceiveAsync(timeout);

                if (message == null)
                {
                    return null;
                }

                var body = message.GetBody<string>();

                var queueMessage = new QueueMessage<T>
                {
                    MessageId = message.MessageId,
                    Item = AlvacJson<T>.Deserialize(body),
                    CorrelationId = message.CorrelationId
                };

                await _logger.WriteInformationAsync(string.Empty, "ServiceBusQueue", "ReceiveAsync", string.Format("Message received: MessageId = {0}, Body = {1}", message.MessageId, body));

                return queueMessage;
            }
            else
            {
                return await ReceiveAsync();
            }
        }

        public async Task<QueueMessage<T>> PeekAsync()
        {
            BrokeredMessage message = await _queueClient.PeekAsync();

            QueueMessage<T> queueMessage = null;

            if (message != null)
            {
                var body = message.GetBody<string>();

                queueMessage = new QueueMessage<T>
                {
                    MessageId = message.MessageId,
                    Item = AlvacJson<T>.Deserialize(body),
                    CorrelationId = message.CorrelationId
                };

                await _logger.WriteVerboseAsync(string.Empty, "ServiceBusQueue", "ReceiveAsync", string.Format("Message received: MessageId = {0}, Body = {1}", message.MessageId, body));
            }

            return queueMessage;
        }

        public async Task<QueueMessage<T>> PeekAsync(long sequenceNumber)
        {
            BrokeredMessage message = await _queueClient.PeekAsync(sequenceNumber);

            QueueMessage<T> queueMessage = null;

            if (message != null)
            {
                var body = message.GetBody<string>();

                queueMessage = new QueueMessage<T>
                {
                    MessageId = message.MessageId,
                    Item = AlvacJson<T>.Deserialize(body),
                    CorrelationId = message.CorrelationId
                };

                await _logger.WriteVerboseAsync(string.Empty, "ServiceBusQueue", "ReceiveAsync", string.Format("Message received: MessageId = {0}, Body = {1}", message.MessageId, body));
            }

            return queueMessage;
        }

        public void Initializer(string queueName)
        {
            _queueName = queueName;

            _serviceBusConnectionString = CloudConfigurationManager.GetSetting("ServiceBusConnectionString");

            CheckCreateQueue(queueName);

            _queueClient = QueueClient.CreateFromConnectionString(_serviceBusConnectionString, _queueName, ReceiveMode.ReceiveAndDelete);
        }

        public void Close()
        {
            if (_queueClient != null)
            {
                _queueClient.CloseAsync();
            }
        }

        public string QueueName
        {
            get { return _queueName; }
        }

        public void Dispose()
        {
            Dispose(true);
            GC.SuppressFinalize(this);
        }

        protected virtual void Dispose(bool disposing)
        {
            if (disposing)
            {
                // free managed resources
                if (_queueClient != null)
                {
                    _queueClient.Close();
                    _queueClient = null;
                }
            }
        }

        #endregion
    }
}
