﻿using Alvac.Domain.Entities;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security.DataProtection;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Security;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework
{
    public class ManagerUserTokenProvider : IUserTokenProvider<User, string>
    {
        private readonly DataProtectorTokenProvider<User, string> tokenProvider;

        public ManagerUserTokenProvider()
        {
            tokenProvider = new DataProtectorTokenProvider<User, string>(new CustomProtector("AlvacApp 2.0"));
        }

        public Task<string> GenerateAsync(string purpose, UserManager<User, string> manager, User user)
        {
            return tokenProvider.GenerateAsync(purpose, manager, user);
        }

        public Task<bool> IsValidProviderForUserAsync(UserManager<User, string> manager, User user)
        {
            return tokenProvider.IsValidProviderForUserAsync(manager, user);
        }

        public Task NotifyAsync(string token, UserManager<User, string> manager, User user)
        {
            return tokenProvider.NotifyAsync(token, manager, user);
        }

        public Task<bool> ValidateAsync(string purpose, string token, UserManager<User, string> manager, User user)
        {
            return tokenProvider.ValidateAsync(purpose, token, manager, user);
        }
    }

    public class CustomProtector : IDataProtector
    {
        private byte[] key;

        public CustomProtector(string key)
        {
            using (var sha1 = new SHA256Managed())
            {
                this.key = sha1.ComputeHash(Encoding.UTF8.GetBytes(key));
            }
        }

        public byte[] Protect(byte[] userData)
        {
            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = this.key;
                aesAlg.GenerateIV();

                using (var encryptor = aesAlg.CreateEncryptor(aesAlg.Key, aesAlg.IV))
                using (var msEncrypt = new MemoryStream())
                {
                    msEncrypt.Write(aesAlg.IV, 0, 16);

                    using (var csEncrypt = new CryptoStream(msEncrypt, encryptor, CryptoStreamMode.Write))
                    using (var bwEncrypt = new BinaryWriter(csEncrypt))
                    {
                        bwEncrypt.Write(userData.Length);
                        bwEncrypt.Write(userData);
                    }
                    var protectedData = msEncrypt.ToArray();

                    var base64 = Convert.ToBase64String(protectedData, 0, protectedData.Length);
                    return Encoding.UTF8.GetBytes(base64);
                }
            }
        }

        public byte[] Unprotect(byte[] protectedData)
        {
            var base64 = Encoding.UTF8.GetString(protectedData);
            protectedData = Convert.FromBase64String(base64);

            using (AesManaged aesAlg = new AesManaged())
            {
                aesAlg.Key = this.key;

                using (var msDecrypt = new MemoryStream(protectedData))
                {
                    byte[] iv = new byte[16];
                    msDecrypt.Read(iv, 0, 16);

                    aesAlg.IV = iv;

                    using (var decryptor = aesAlg.CreateDecryptor(aesAlg.Key, aesAlg.IV))
                    using (var csDecrypt = new CryptoStream(msDecrypt, decryptor, CryptoStreamMode.Read))
                    using (var brDecrypt = new BinaryReader(csDecrypt))
                    {
                        var len = brDecrypt.ReadInt32();
                        var data = brDecrypt.ReadBytes(len);
                        return data;
                    }
                }
            }
        }
    }
}
