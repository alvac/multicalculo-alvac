﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class InsurerConfiguration : EntityTypeConfiguration<Insurer>
    {
        public InsurerConfiguration()
        {
            ToTable("Insurers")
                .HasKey(x => x.InsurerId);

            HasMany(x => x.Manufacturers)
                .WithOptional(x => x.Insurer)
                .HasForeignKey(x => x.InsurerId);

            HasMany(x => x.AccountConnections)
                .WithRequired(x => x.Insurer)
                .HasForeignKey(x => x.InsurerId);

            HasMany(x => x.Dispatcher)
                .WithRequired(x => x.Insurer)
                .HasForeignKey(x => x.InsurerId);

            HasMany(x => x.InsurerConfigurations)
                .WithRequired(x => x.Insurer)
                .HasForeignKey(x => x.InsurerId);

            HasMany(x => x.InsurerQuotations)
                .WithRequired(x => x.Insurer)
                .HasForeignKey(x => x.InsurerId);

            HasMany(x => x.InsurerValidators)
                .WithRequired(x => x.Insurer)
                .HasForeignKey(x => x.InsurerId);
        }
    }
}
