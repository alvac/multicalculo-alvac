﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class InsurerQuotationConfiguration : EntityTypeConfiguration<InsurerQuotation>
    {
        public InsurerQuotationConfiguration()
        {
            ToTable("InsurerQuotations")
                .HasKey(x => x.InsurerQuotationId);

            HasRequired(x => x.Quotation)
                .WithMany(x => x.InsurerQuotations)
                .HasForeignKey(x => x.QuotationId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Connection)
                .WithMany(x => x.InsurerQuotations)
                .HasForeignKey(x => x.ConnectionId)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Transaction)
                .WithRequired(x => x.InsurerQuotation)
                .WillCascadeOnDelete(false);

            HasOptional(x => x.Notification)
               .WithRequired(x => x.InsurerQuotation)
               .WillCascadeOnDelete(false);
        }
    }
}
