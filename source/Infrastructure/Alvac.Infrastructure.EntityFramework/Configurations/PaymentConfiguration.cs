﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class PaymentConfiguration : EntityTypeConfiguration<Payment>
    {
        public PaymentConfiguration()
        {
            ToTable("Payments")
                .HasKey(x => x.PaymentId);

            HasRequired(x => x.InsurerQuotation)
                .WithMany(x => x.Payments)
                .HasForeignKey(x => x.InsurerQuotationId)
                .WillCascadeOnDelete(false);
        }
    }
}
