﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class DeviceTrackerConfiguration : EntityTypeConfiguration<DeviceTracker>
    {
        public DeviceTrackerConfiguration()
        {
            ToTable("DeviceTrackers")
                .HasKey(x => x.DeviceTrackerId);

            Property(x => x.Name)
                .IsRequired();
        }
    }
}
