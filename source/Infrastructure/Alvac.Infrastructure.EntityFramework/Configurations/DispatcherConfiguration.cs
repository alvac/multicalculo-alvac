﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class DispatcherConfiguration : EntityTypeConfiguration<Dispatcher>
    {
        public DispatcherConfiguration()
        {
            ToTable("Dispatchers")
                .HasKey(x => x.DispatcherId);

            Property(x => x.Name)
                .HasMaxLength(255)
                .IsRequired();

            Property(x => x.Description)
                .HasMaxLength(255)
                .IsRequired();
        }
    }
}
