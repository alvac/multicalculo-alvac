﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations.Auto
{
    public class VehicleInfoConfiguration : EntityTypeConfiguration<VehicleInfo>
    {
        public VehicleInfoConfiguration()
        {
            ToTable("VehicleInfos")
                .HasKey(x => x.QuotationId);

            HasMany(x => x.DevicesIgnitionBlocker)
                .WithMany(x => x.VehicleInfos)
                .Map(x =>
                    x.ToTable("QuotationIgnitionBlockers")
                    .MapLeftKey("QuotationId")
                    .MapRightKey("IgnitionBlockerId"));

            HasMany(x => x.DevicesTracker)
                .WithMany(x => x.VehicleInfos)
                .Map(x => 
                    x.ToTable("QuotationDeviceTrackers")
                    .MapLeftKey("QuotationId")
                    .MapRightKey("DeviceTrackerId"));
        }
    }
}
