﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations.Auto
{
    public class AdditionalDriverConfiguration : EntityTypeConfiguration<AdditionalDriver>
    {
        public AdditionalDriverConfiguration()
        {
            ToTable("AdditionalDrivers")
                .HasKey(x => x.AdditionalDriverId);

            HasRequired(x => x.AutoQuotation)
                .WithMany(x => x.AdditionalDrivers)
                .HasForeignKey(x => x.QuotationId)
                .WillCascadeOnDelete(false);


        }
    }
}
