﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations.Auto
{
    public class InsurerAutoQuotationConfiguration : EntityTypeConfiguration<InsurerAutoQuotation>
    {
        public InsurerAutoQuotationConfiguration()
        {
            ToTable("InsurerAutoQuotations")
                .HasKey(x => x.InsurerQuotationId);

            HasRequired(x => x.InsurerAutoQuotationParameter)
               .WithRequiredPrincipal(x => x.InsurerAutoQuotation);
        }
    }
}
