﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations.Auto
{
    public class MainDriverConfiguration : EntityTypeConfiguration<MainDriver>
    {
        public MainDriverConfiguration()
        {
            ToTable("MainDrivers")
                .HasKey(x => x.QuotationId);

            HasRequired(x => x.WorkActivity)
                .WithMany(x => x.MainDrivers)
                .HasForeignKey(x => x.WorkActivityId)
                .WillCascadeOnDelete(false);

        }
    }
}
