﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations.Auto
{
    public class InsurerAutoQuotationParameterConfiguration : EntityTypeConfiguration<InsurerAutoQuotationParameter>
    {
        public InsurerAutoQuotationParameterConfiguration()
        {
            ToTable("InsurerAutoQuotationParameters")
                .HasKey(x => x.InsurerAutoQuotationId);

        }
    }
}
