﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations.Auto
{
    public class VehicleOwnerConfiguration : EntityTypeConfiguration<VehicleOwner>
    {
        public VehicleOwnerConfiguration()
        {
            ToTable("VehicleOwners")
                .HasKey(x => x.QuotationId);
        }
    }
}
