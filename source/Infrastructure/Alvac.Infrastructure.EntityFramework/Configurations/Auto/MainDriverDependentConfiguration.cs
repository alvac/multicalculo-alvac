﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations.Auto
{
    public class MainDriverDependentConfiguration : EntityTypeConfiguration<MainDriverDependent>
    {
        public MainDriverDependentConfiguration()
        {
            ToTable("MainDriverDependents")
                .HasKey(x => x.MainDriverDependentId);

            HasRequired(x => x.MainDriver)
                .WithMany(x => x.MainDriverDependents)
                .HasForeignKey(x => x.MainDriverId)
                .WillCascadeOnDelete(false);
        }
    }
}
