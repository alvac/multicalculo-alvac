﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations.Auto
{
    public class InsuranceConfiguration : EntityTypeConfiguration<Insurance>
    {
        public InsuranceConfiguration()
        {
            ToTable("Insurances")
                .HasKey(x => x.QuotationId);
        }
    }
}
