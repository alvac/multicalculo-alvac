﻿using Alvac.Domain.Entities.Auto;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations.Auto
{
    public class AutoQuotationConfiguration : EntityTypeConfiguration<AutoQuotation>
    {
        public AutoQuotationConfiguration()
        {
            ToTable("AutoQuotations");

            HasRequired(x => x.VehicleInfo)
                .WithRequiredPrincipal(x => x.AutoQuotation);

            HasRequired(x => x.VehicleUse)
                .WithRequiredPrincipal(x => x.AutoQuotation);

            HasRequired(x => x.MainDriver)
               .WithRequiredPrincipal(x => x.AutoQuotation);

            HasRequired(x => x.VehicleOwner)
               .WithRequiredPrincipal(x => x.AutoQuotation);

            HasRequired(x => x.Insurance)
               .WithRequiredPrincipal(x => x.AutoQuotation);
        }
    }
}
