﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class AccountConnectionConfiguration : EntityTypeConfiguration<AccountConnection>
    {
        public AccountConnectionConfiguration()
        {
            ToTable("AccountConnections")
                .HasKey(x => new { x.AccountId, x.InsurerId, x.RequestType });

            HasRequired(x => x.Connection)
                .WithMany(x => x.AccountConnections)
                .HasForeignKey(x => x.ConnectionId)
                .WillCascadeOnDelete(false);
        }
    }
}
