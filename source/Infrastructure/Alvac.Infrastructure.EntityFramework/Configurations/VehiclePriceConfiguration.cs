﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class VehiclePriceConfiguration : EntityTypeConfiguration<VehiclePrice>
    {
        public VehiclePriceConfiguration()
        {
            ToTable("VehiclePrices")
                .HasKey(x => x.VehiclePriceId);

            HasRequired(x => x.Vehicle)
                .WithMany(x => x.VehiclePrices)
                .HasForeignKey(x => x.VehicleId)
                .WillCascadeOnDelete(true);
        }
    }
}
