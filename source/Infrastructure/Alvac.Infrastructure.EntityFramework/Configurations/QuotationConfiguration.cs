﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class QuotationConfiguration : EntityTypeConfiguration<Quotation>
    {
        public QuotationConfiguration()
        {
            ToTable("Quotations")
                .HasKey(x => x.QuotationId);

            HasRequired(x => x.Account)
                .WithMany(x => x.Quotations)
                .HasForeignKey(x => x.AccountId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.User)
                .WithMany(x => x.Quotations)
                .HasForeignKey(x => x.UserId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Insured)
               .WithRequiredPrincipal(x => x.Quotation);
        }
    }
}
