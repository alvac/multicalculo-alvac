﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class InsurerConfigurationConfiguration : EntityTypeConfiguration<Domain.Entities.InsurerConfiguration>
    {
        public InsurerConfigurationConfiguration()
        {
            ToTable("InsurerConfigurations")
                .HasKey(x => x.InsurerConfigurationId);
            
             Property(x => x.Required)
               .IsRequired();

             Property(x => x.DisplayName)
               .IsRequired();

            Property(x => x.Type)
               .IsRequired();

            Property(x => x.Key)
               .IsRequired();
        }
    }
}
