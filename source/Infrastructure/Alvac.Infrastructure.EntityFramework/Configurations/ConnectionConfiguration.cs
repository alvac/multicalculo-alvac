﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class ConnectionConfiguration : EntityTypeConfiguration<Connection>
    {
        public ConnectionConfiguration()
        {
            ToTable("Connections")
                .HasKey(x => x.ConnectionId);

            HasRequired(x => x.Dispatcher)
                .WithMany(x => x.Connections)
                .HasForeignKey(x => x.DispatcherId)
                .WillCascadeOnDelete(false);

            Property(x => x.Name)
               .HasMaxLength(200)
               .IsRequired();

            Property(x => x.InputQueueName)
               .IsRequired();

            Property(x => x.OutputQueueName)
               .IsRequired();

            Property(x => x.NotificationQueueName)
               .IsRequired();
        }
    }
}
