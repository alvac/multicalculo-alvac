﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class VehicleConfiguration : EntityTypeConfiguration<Vehicle>
    {
        public VehicleConfiguration()
        {
            ToTable("Vehicles")
                .HasKey(x => x.VehicleId);

            HasRequired(x => x.Manufacturer)
                .WithMany(x => x.Vehicles)
                .HasForeignKey(x => x.ManufacturerId)
                .WillCascadeOnDelete(true);
        }
    }
}
