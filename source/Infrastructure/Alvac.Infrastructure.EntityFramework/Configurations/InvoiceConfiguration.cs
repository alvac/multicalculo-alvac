﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class InvoiceConfiguration : EntityTypeConfiguration<Invoice>
    {
        public InvoiceConfiguration()
        {
            ToTable("Invoices")
                .HasKey(x => x.InvoiceId);

            HasRequired(x => x.Subscription)
                .WithMany(x => x.Invoices)
                .HasForeignKey(x => x.SubscriptionId)
                .WillCascadeOnDelete(false);
        }
    }
}
