﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class IgnitionBlockerConfiguration : EntityTypeConfiguration<IgnitionBlocker>
    {
        public IgnitionBlockerConfiguration()
        {
            ToTable("IgnitionBlockers")
                .HasKey(x => x.IgnitionBlockerId);

            Property(x => x.Name)
                .IsRequired();
        }
    }
}
