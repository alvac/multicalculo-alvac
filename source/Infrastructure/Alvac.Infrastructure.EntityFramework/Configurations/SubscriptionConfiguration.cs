﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class SubscriptionConfiguration : EntityTypeConfiguration<Subscription>
    {
        public SubscriptionConfiguration()
        {
            ToTable("Subscriptions")
                .HasKey(x => x.SubscriptionId);

            HasRequired(x => x.Account)
                .WithMany(x => x.Subscriptions)
                .HasForeignKey(x => x.AccountId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.Plan)
                .WithMany(x => x.Subscriptions)
                .HasForeignKey(x => x.PlanId)
                .WillCascadeOnDelete(false);
        }
    }
}
