﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class InsuredConfiguration : EntityTypeConfiguration<Insured>
    {
        public InsuredConfiguration()
        {
            ToTable("Insureds")
                .HasKey(x => x.QuotationId);
        }
    }
}
