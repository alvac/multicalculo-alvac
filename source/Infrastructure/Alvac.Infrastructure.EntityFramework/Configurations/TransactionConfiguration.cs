﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class TransactionConfiguration : EntityTypeConfiguration<Transaction>
    {
        public TransactionConfiguration()
        {
            ToTable("Transactions")
                .HasKey(x => x.InsurerQuotationId);

            HasRequired(x => x.Invoice)
                .WithMany(x => x.Transactions)
                .HasForeignKey(x => x.InvoiceId)
                .WillCascadeOnDelete(false);

        }
    }
}
