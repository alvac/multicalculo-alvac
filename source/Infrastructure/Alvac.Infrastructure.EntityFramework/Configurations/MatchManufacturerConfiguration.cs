﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class MatchManufacturerConfiguration : EntityTypeConfiguration<MatchManufacturer>
    {
        public MatchManufacturerConfiguration()
        {
            ToTable("MatchManufacturers")
                .HasKey(x => x.MatchManufacturerId);

            HasRequired(x => x.ManufacturerAlvac)
                .WithMany(x => x.MatchManufacturerAlvac)
                .HasForeignKey(x => x.ManufacturerAlvacId)
                .WillCascadeOnDelete(false);

            HasRequired(x => x.ManufacturerInsurer)
                .WithMany(x => x.MatchManufacturerInsurer)
                .HasForeignKey(x => x.ManufacturerInsurerId)
                .WillCascadeOnDelete(false);
        }
    }
}
