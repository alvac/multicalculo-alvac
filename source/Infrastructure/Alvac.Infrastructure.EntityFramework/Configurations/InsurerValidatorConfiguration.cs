﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class InsurerValidatorConfiguration : EntityTypeConfiguration<InsurerValidator>
    {
        public InsurerValidatorConfiguration()
        {
            ToTable("InsurerValidators")
                .HasKey(x => x.InsurerValidatorId);

            Property(x => x.Description)
                .IsRequired();

            Property(x => x.FullAssemblyName)
                .IsRequired();
        }
    }
}
