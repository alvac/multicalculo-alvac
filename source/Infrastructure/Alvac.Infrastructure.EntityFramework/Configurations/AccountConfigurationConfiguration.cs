﻿using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class AccountConfigurationConfiguration : EntityTypeConfiguration<Domain.Entities.AccountConfiguration>
    {
        public AccountConfigurationConfiguration()
        {
            ToTable("AccountConfigurations")
                .HasKey(x => x.AccountId);

            HasRequired(x => x.Account)
                .WithOptional(x => x.AccountConfiguration);
        }
    }
}
