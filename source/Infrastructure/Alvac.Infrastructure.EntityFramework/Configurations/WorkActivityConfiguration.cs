﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class WorkActivityConfiguration : EntityTypeConfiguration<WorkActivity>
    {
        public WorkActivityConfiguration()
        {
            ToTable("WorkActivities")
                .HasKey(x => x.WorkActivityId);
        }
    }
}
