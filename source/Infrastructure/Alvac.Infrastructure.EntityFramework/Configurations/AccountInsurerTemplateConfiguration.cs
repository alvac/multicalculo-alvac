﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    class AccountInsurerTemplateConfiguration: EntityTypeConfiguration<AccountInsurerTemplate>
    {
        public AccountInsurerTemplateConfiguration()
        {
            ToTable("AccountInsurerTemplates")
                .HasKey(x => x.AccountInsurerTemplateId);

            HasRequired(x => x.Account)
              .WithMany(x => x.AccountInsurerTemplates)
              .HasForeignKey(x => x.AccountId)
              .WillCascadeOnDelete(false);
        }
    }
}
