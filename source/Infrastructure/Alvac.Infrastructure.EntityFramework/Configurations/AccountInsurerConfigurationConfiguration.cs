﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class AccountInsurerConfigurationConfiguration: EntityTypeConfiguration<AccountInsurerConfiguration>
    {
        public AccountInsurerConfigurationConfiguration()
        {
            ToTable("AccountInsurerConfigurations")
                .HasKey(x => x.AccountInsurerConfigurationId);

            HasRequired(x => x.AccountInsurerTemplate)
               .WithMany(x => x.AccountInsurerConfigurations)
               .HasForeignKey(x => x.AccountInsurerTemplateId)
               .WillCascadeOnDelete(true);

            HasRequired(x => x.InsurerConfiguration)
              .WithMany(x => x.AccountInsurerConfigurations)
              .HasForeignKey(x => x.InsurerConfigurationId)
              .WillCascadeOnDelete(true);
            
        }
    }
}
