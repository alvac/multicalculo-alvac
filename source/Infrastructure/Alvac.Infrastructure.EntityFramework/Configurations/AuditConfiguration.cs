﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class AuditConfiguration : EntityTypeConfiguration<Audit>
    {
        public AuditConfiguration()
        {
            ToTable("Audits")
                .HasKey(x => x.AuditId);

            Property(x => x.AreaAccessed)
                .IsRequired();

            Property(x => x.IPAddress)
                .IsRequired();

            Property(x => x.UserName)
                .IsRequired();
        }
    }
}
