﻿using Alvac.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity.ModelConfiguration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Configurations
{
    public class ConnectionParameterConfiguration : EntityTypeConfiguration<ConnectionParameter>
    {
        public ConnectionParameterConfiguration()
        {
            ToTable("ConnectionParameters")
                .HasKey(x => x.ConnectionParameterId);

            HasRequired(x => x.Connection)
                .WithMany(x => x.ConnectionParameters)
                .HasForeignKey(x => x.ConnectionId)
                .WillCascadeOnDelete(false);

            Property(x => x.Key)
                .IsRequired();

            Property(x => x.Value)
                .IsRequired();
        }
    }
}
