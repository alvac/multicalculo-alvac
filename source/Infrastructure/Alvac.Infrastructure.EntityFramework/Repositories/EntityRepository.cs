﻿using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.Infrastructure;
using System.Linq;
using System.Linq.Expressions;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Repositories
{
    public class EntityRepository<TEntity> : IRepository<TEntity> where TEntity : class
    {
        #region Private Fields

        protected internal readonly DbContext _context;

        #endregion

        #region Constructor

        /// <summary>
        /// Instantiate a new repository
        /// </summary>
        /// <param name="context"></param>
        public EntityRepository(DbContext context)
        {
            _context = context;

            var objectContext = (this._context as IObjectContextAdapter).ObjectContext;
            objectContext.CommandTimeout = this._context.Database.Connection.ConnectionTimeout;
        }

        #endregion

        #region IEntityRepository<TEntity,TId> Members

        public virtual Task AddRangeAsync(IEnumerable<TEntity> entities)
        {
            _context.Set<TEntity>().AddRange(entities);

            return Task.FromResult<IEnumerable<TEntity>>(entities);
        }

        public virtual Task AddAsync(TEntity entity)
        {
            _context.Set<TEntity>().Add(entity);

            return Task.FromResult<TEntity>(entity);
        }

        public virtual Task UpdateAsync(TEntity entity)
        {
            if (_context.Entry<TEntity>(entity).State == EntityState.Detached)
            {
                _context.Set<TEntity>().Attach(entity);
            }

            _context.Entry<TEntity>(entity).State = EntityState.Modified;

            return Task.FromResult<TEntity>(entity);
        }

        public virtual Task RemoveAsync(TEntity entity)
        {
            if (_context.Entry<TEntity>(entity).State == EntityState.Detached)
            {
                _context.Set<TEntity>().Attach(entity);
            }

            _context.Set<TEntity>().Remove(entity);

            return Task.FromResult<TEntity>(entity);
        }

        public async virtual Task<IEnumerable<TEntity>> GetAllAsync()
        {
            return await _context.Set<TEntity>().ToListAsync().ConfigureAwait(false);
        }

        public async virtual Task<TEntity> GetByIdAsync(object id)
        {
            return await _context.Set<TEntity>().FindAsync(id).ConfigureAwait(false);
        }

        public virtual IQueryable<TEntity> AsQueryable()
        {
            return _context.Set<TEntity>();
        }

        public async Task<TEntity> SingleOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().AsNoTracking().SingleOrDefaultAsync(predicate).ConfigureAwait(false);
        }

        public async Task<TEntity> FirstOrDefaultAsync(Expression<Func<TEntity, bool>> predicate)
        {
            return await _context.Set<TEntity>().AsNoTracking().FirstOrDefaultAsync(predicate).ConfigureAwait(false);
        }
        #endregion
    }
}
