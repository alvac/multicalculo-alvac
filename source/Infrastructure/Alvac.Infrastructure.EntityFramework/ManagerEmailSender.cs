﻿using Alvac.Domain.Services;
using Microsoft.AspNet.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework
{
    public class ManagerEmailSender : IIdentityMessageService
    {
        private readonly IEmailSender _emailSender;

        public ManagerEmailSender(IEmailSender emailSender)
        {
            _emailSender = emailSender;
        }

        public async Task SendAsync(IdentityMessage message)
        {
            await _emailSender.SendAsync("contato@Alvac.com.br", message.Destination, message.Subject, message.Body);
        }
    }
}
