﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Repositories;
using Alvac.Infrastructure.EntityFramework.Configurations;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Data.Entity.ModelConfiguration.Conventions;
using System.Diagnostics;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework
{
    /// <summary>
    /// DbContext que utiliza uma entidade de usuário personalizada
    /// </summary>
    [System.Diagnostics.CodeAnalysis.SuppressMessage("Microsoft.Design", "CA1063:ImplementIDisposableCorrectly")]
    public class AlvacContext : IdentityDbContext<User>, IUnitOfWork
    {
        public AlvacContext()
            : base("DefaultConnection")
        {
            var loadEFAssembly = System.Data.Entity.SqlServer.SqlProviderServices.SqlServerTypesAssemblyName;

            InstanceId = Guid.NewGuid();
            Trace.TraceInformation("Context id:" + InstanceId);
            this.Database.Log = (sql) => Trace.TraceInformation(sql);
            this.Configuration.ValidateOnSaveEnabled = false;
        }

        public Guid InstanceId { get; set; }
        public IDbSet<Account> Accounts { get; set; }
        public IDbSet<AccountConnection> AccountConnections { get; set; }
        public IDbSet<AccountInsurerConfiguration> AccountInsurerConfigurations { get; set; }
        public IDbSet<AccountInsurerTemplate> AccountInsurerTemplates { get; set; }
        public IDbSet<Connection> Connections { get; set; }
        public IDbSet<ConnectionParameter> ConnectionParameters { get; set; }
        public IDbSet<Dispatcher> Dispatchers { get; set; }
        public IDbSet<Insured> Insureds { get; set; }
        public IDbSet<Domain.Entities.InsurerConfiguration> InsurerConfigurations { get; set; }
        public IDbSet<InsurerQuotation> InsurerQuotations { get; set; }
        public IDbSet<Invoice> Invoices { get; set; }
        public IDbSet<Notification> Notifications { get; set; }
        public IDbSet<Payment> Payments { get; set; }
        public IDbSet<Plan> Plans { get; set; }
        public IDbSet<Quotation> Quotations { get; set; }
        public IDbSet<Subscription> Subscriptions { get; set; }
        public IDbSet<Transaction> Transactions { get; set; }
        public IDbSet<WorkActivity> WorkActivities { get; set; }

        public IDbSet<IgnitionBlocker> IgnitionBlockers { get; set; }
        public IDbSet<DeviceTracker> DeviceTrackers { get; set; }

        #region Auto

        public IDbSet<AutoQuotation> AutoQuotations { get; set; }

        public IDbSet<InsurerAutoQuotation> InsurerAutoQuotations { get; set; }

        #endregion

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            modelBuilder.Conventions.Remove<PluralizingTableNameConvention>();
            modelBuilder.Configurations.AddFromAssembly(Assembly.GetAssembly(typeof(UserConfiguration)));
            base.OnModelCreating(modelBuilder);
        }

        public async Task SaveAsync()
        {
            try
            {
                await SaveChangesAsync();
            }
            catch (Exception ex)
            {
                throw;
            }
        }
    }
}

