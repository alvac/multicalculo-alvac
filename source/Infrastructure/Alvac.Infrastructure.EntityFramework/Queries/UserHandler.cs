﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Users;
using Alvac.Domain.Entities.Repositories;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class UserHandler :
        IQueryHandler<GetUsersByAccountId, IQueryable<User>>,
        IQueryHandler<GetUserByEmail, User>,
        IQueryHandler<GetRoles, IQueryable<IdentityRole>>,
        IQueryHandler<GetUserById, User>
    {
        private readonly IRepository<User> _userRepository;
        private readonly IRepository<IdentityRole> _identityRoleRepository;

        public UserHandler(IRepository<User> userRepository, IRepository<IdentityRole> identityRoleRepository)
        {
            _identityRoleRepository = identityRoleRepository;
            _userRepository = userRepository;
        }

        public Task<IQueryable<User>> HandleAsync(GetUsersByAccountId query)
        {
            return Task.FromResult(_userRepository.AsQueryable().Where(x => x.AccountId == query.AccountId));
        }

        public async Task<User> HandleAsync(GetUserByEmail query)
        {
            return await _userRepository.FirstOrDefaultAsync(x => x.Email == query.Email);
        }

        public Task<IQueryable<IdentityRole>> HandleAsync(GetRoles query)
        {
            return Task.FromResult(_identityRoleRepository.AsQueryable());
        }

        public async Task<User> HandleAsync(GetUserById query)
        {
            return await _userRepository.GetByIdAsync(query.Id);
        }
    }
}
