﻿using Alvac.Domain.Entities.Queries;
using System;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public sealed class QueryProcessor : IQueryProcessor
    {
        #region Private Fields

        private readonly IServiceProvider _serviceProvider;

        #endregion Private Fields

        #region Constructors

        public QueryProcessor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion Constructors

        #region IQueryProcessor Members

        public async Task<TResult> ProcessAsync<TResult>(IQuery<TResult> query)
        {
            var handlerType = typeof(IQueryHandler<,>).MakeGenericType(query.GetType(), typeof(TResult));

            dynamic handler = _serviceProvider.GetService(handlerType);

            if (handler == null)
            {
                throw new Exception("Processador para query não encontrado.");
            }

            return await handler.HandleAsync((dynamic)query);
        }

        #endregion IQueryProcessor Members
    }
}