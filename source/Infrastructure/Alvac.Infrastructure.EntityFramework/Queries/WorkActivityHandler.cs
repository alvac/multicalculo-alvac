﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.WorkActivities;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class WorkActivityHandler :
         IQueryHandler<GetWorkActivities, IEnumerable<WorkActivity>>
    {
        #region Private Fields

        private readonly IRepository<WorkActivity> _repo;

        #endregion Private Fields

        #region Constructors

        public WorkActivityHandler(IRepository<WorkActivity> repo)
        {
            _repo = repo;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public Task<IEnumerable<WorkActivity>> HandleAsync(GetWorkActivities query)
        {
            return _repo.GetAllAsync();
        }

        #endregion IQueryHandler Members
    }
}