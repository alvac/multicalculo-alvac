﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Dispatchers;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class DispatcherHandler :
        IQueryHandler<FindDispatcherById, Dispatcher>,
        IQueryHandler<GetAll, IEnumerable<Dispatcher>>
    {
        #region Private Fields

        private readonly IRepository<Dispatcher> _repository;

        #endregion Private Fields

        #region Constructors

        public DispatcherHandler(IRepository<Dispatcher> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<Dispatcher> HandleAsync(FindDispatcherById query)
        {
            return await _repository.GetByIdAsync(query.DispatcherId);
        }

        public async Task<IEnumerable<Dispatcher>> HandleAsync(GetAll query)
        {
            return await _repository.GetAllAsync();
        }

        #endregion IQueryHandler Members
    }
}