﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.AccountConfigurations;
using Alvac.Domain.Entities.Queries.Accounts;
using Alvac.Domain.Entities.Repositories;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class AccountConfigurationHandler :
         IQueryHandler<GetAccountConfiguration, AccountConfiguration>
    {
        #region Private Fields

        private readonly IRepository<AccountConfiguration> _repository;

        #endregion Private Fields

        #region Constructors

        public AccountConfigurationHandler(IRepository<AccountConfiguration> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<AccountConfiguration> HandleAsync(GetAccountConfiguration query)
        {
            return await _repository.GetByIdAsync(query.AccountId);
        }

        #endregion IQueryHandler Members
    }
}