﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Accounts;
using Alvac.Domain.Entities.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class AccountHandler :
        IQueryHandler<FindAccountById, Account>,
        IQueryHandler<GetActiveAccounts, IQueryable<Account>>
    {
        #region Private Fields

        private readonly IRepository<Account> _repository;

        #endregion Private Fields

        #region Constructors

        public AccountHandler(IRepository<Account> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<Account> HandleAsync(FindAccountById query)
        {
            return await _repository.GetByIdAsync(query.AccountId);
        }

        public Task<IQueryable<Account>> HandleAsync(GetActiveAccounts query)
        {
            return Task.FromResult(_repository.AsQueryable().Where(x => x.IsActive));
        }

        #endregion IQueryHandler Members


    }
}