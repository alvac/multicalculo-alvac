﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Connections;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class ConnectionHandler :
        IQueryHandler<FindConnectionById, Connection>,
        IQueryHandler<GetAll, IQueryable<Connection>>,
        IQueryHandler<FindConnectionsByActive, IEnumerable<Connection>>
    {
        #region Private Fields

        private readonly IRepository<Connection> _repository;

        #endregion Private Fields

        #region Constructors

        public ConnectionHandler(IRepository<Connection> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<Connection> HandleAsync(FindConnectionById query)
        {
            return await _repository.GetByIdAsync(query.ConnectionId);
        }

        public async Task<IEnumerable<Connection>> HandleAsync(FindConnectionsByActive query)
        {
            return await _repository.AsQueryable().Where(x => x.IsActive == query.IsActive).ToListAsync();
        }

        public Task<IQueryable<Connection>> HandleAsync(GetAll query)
        {
            return Task.FromResult(_repository.AsQueryable());
        }

        #endregion IQueryHandler Members

        
    }
}