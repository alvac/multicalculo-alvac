﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Quotations;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class QuotationHandler :
        IQueryHandler<GetAutoQuotationsByUserId, IQueryable<AutoQuotation>>,
        IQueryHandler<GetAutoQuotationsByAccountId, IQueryable<AutoQuotation>>,
        IQueryHandler<GetQuotationById, Quotation>
    {
        #region Private Fields

        private readonly IRepository<AutoQuotation> _autoQuotationrepository;
        private readonly IRepository<Quotation> _quotationRepository;

        #endregion Private Fields

        #region Constructors

        public QuotationHandler(IRepository<AutoQuotation> autoQuotationrepository,
            IRepository<Quotation> quotationRepository,
            IRepository<InsurerQuotation> insurerQuotationRepository)
        {
            _autoQuotationrepository = autoQuotationrepository;
            _quotationRepository = quotationRepository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public Task<IQueryable<AutoQuotation>> HandleAsync(GetAutoQuotationsByUserId query)
        {
            return Task.FromResult(_autoQuotationrepository.AsQueryable().OrderByDescending(x => x.QuotationId).Where(x => x.UserId == query.UserId));
        }

        public async Task<Quotation> HandleAsync(GetQuotationById query)
        {
            return await _quotationRepository.GetByIdAsync(query.QuotationId);
        }

        public Task<IQueryable<AutoQuotation>> HandleAsync(GetAutoQuotationsByAccountId query)
        {
            return Task.FromResult(_autoQuotationrepository.AsQueryable().OrderByDescending(x => x.QuotationId).Where(x => x.AccountId == query.AccountId));
        }

        #endregion IQueryHandler Members
    }
}