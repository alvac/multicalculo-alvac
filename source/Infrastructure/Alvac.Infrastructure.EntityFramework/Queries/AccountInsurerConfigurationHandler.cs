﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.AccountInsurerConfigurations;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class AccountInsurerConfigurationHandler :
        IQueryHandler<FindAccountInsurerConfigurationByAccountInsurerTemplateId, IEnumerable<AccountInsurerConfiguration>>,
        IQueryHandler<FindAccountInsurerConfigurationByInsurerTemplateId, IEnumerable<AccountInsurerConfiguration>>
    {
        #region Private Fields

        private readonly IRepository<AccountInsurerConfiguration> _accountInsurerConfigurationRepository;
        private readonly IRepository<InsurerConfiguration> _insurerConfigurationRepository;
        private readonly IRepository<AccountInsurerTemplate> _accountInsurerTemplateRepository;

        #endregion Private Fields

        #region Constructors

        public AccountInsurerConfigurationHandler(IRepository<InsurerConfiguration> insurerConfigurationRepository,
            IRepository<AccountInsurerConfiguration> accountInsurerConfigurationRepository,
            IRepository<AccountInsurerTemplate> accountInsurerTemplateRepository)
        {
            _insurerConfigurationRepository = insurerConfigurationRepository;
            _accountInsurerConfigurationRepository = accountInsurerConfigurationRepository;
            _accountInsurerTemplateRepository = accountInsurerTemplateRepository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<IEnumerable<AccountInsurerConfiguration>> HandleAsync(FindAccountInsurerConfigurationByAccountInsurerTemplateId query)
        {
            return await _accountInsurerConfigurationRepository.AsQueryable().Where(x => x.AccountInsurerTemplateId == query.AccountInsurerTemplateId).ToListAsync();
        }

        public async Task<IEnumerable<AccountInsurerConfiguration>> HandleAsync(FindAccountInsurerConfigurationByInsurerTemplateId query)
        {
            var template = await _accountInsurerTemplateRepository.GetByIdAsync(query.AccountInsurerTemplateId);

            var model = await (from c in _insurerConfigurationRepository.AsQueryable().Where(x => x.RequestType == template.RequestType)
                               join t in _accountInsurerConfigurationRepository.AsQueryable().Where(x => x.AccountInsurerTemplateId == query.AccountInsurerTemplateId)
                               on c.InsurerConfigurationId equals t.InsurerConfigurationId into _j
                               from t in _j.DefaultIfEmpty()
                               where c.InsurerId == query.Insurer
                               select new
                               {
                                   AccountInsurerConfigurationId = t != null ? t.AccountInsurerConfigurationId : 0,
                                   AccountInsurerTemplateId = query.AccountInsurerTemplateId,
                                   InsurerConfigurationId = c.InsurerConfigurationId,
                                   InsurerConfiguration = c,
                                   Value = t != null ? t.Value : null
                               }).ToListAsync();

            return model.Select(x => new AccountInsurerConfiguration
            {
                AccountInsurerConfigurationId = x.AccountInsurerConfigurationId,
                AccountInsurerTemplateId = x.AccountInsurerTemplateId,
                InsurerConfigurationId = x.InsurerConfigurationId,
                InsurerConfiguration = x.InsurerConfiguration,
                Value = x.Value
            });
        }

        #endregion IQueryHandler Members
    }
}