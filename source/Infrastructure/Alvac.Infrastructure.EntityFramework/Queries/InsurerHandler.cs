﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Insurers;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class InsurerHandler :
        IQueryHandler<GetAll, IEnumerable<Insurer>>
    {
        private readonly IRepository<Insurer> _insurerRepository;

        public InsurerHandler(IRepository<Insurer> insurerRepository)
        {
            _insurerRepository = insurerRepository;
        }

        public async Task<IEnumerable<Insurer>> HandleAsync(GetAll query)
        {
            return await _insurerRepository.GetAllAsync();
        }
    }
}
