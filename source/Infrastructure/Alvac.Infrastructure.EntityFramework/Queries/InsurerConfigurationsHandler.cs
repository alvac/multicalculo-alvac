﻿using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.InsurerConfigurations;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class InsurerConfigurationsHandler :
        IQueryHandler<FindInsurerConfigurationById, Domain.Entities.InsurerConfiguration>,
        IQueryHandler<FindInsurerConfigurationByInsurer, IEnumerable<Domain.Entities.InsurerConfiguration>>,
        IQueryHandler<FindInsurerConfigurationByInsurerKey, Domain.Entities.InsurerConfiguration>
    {
        #region Private Fields

        private readonly IRepository<Domain.Entities.InsurerConfiguration> _repository;

        #endregion Private Fields

        #region Constructors

        public InsurerConfigurationsHandler(IRepository<Domain.Entities.InsurerConfiguration> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<Domain.Entities.InsurerConfiguration> HandleAsync(FindInsurerConfigurationById query)
        {
            return await _repository.SingleOrDefaultAsync(x => x.InsurerConfigurationId == query.InsurerConfigurationId);
        }

        public async Task<IEnumerable<Domain.Entities.InsurerConfiguration>> HandleAsync(FindInsurerConfigurationByInsurer query)
        {
            return await _repository.AsQueryable().Where(x => x.InsurerId == query.Insurer
                && x.RequestType == query.RequestType).ToListAsync();
        }

        public async Task<Domain.Entities.InsurerConfiguration> HandleAsync(FindInsurerConfigurationByInsurerKey query)
        {
            return await _repository.SingleOrDefaultAsync(x => x.InsurerId == query.Insurer
                && x.Key == query.Key
                && x.RequestType == query.RequestType);
        }

        #endregion IQueryHandler Members
    }
}