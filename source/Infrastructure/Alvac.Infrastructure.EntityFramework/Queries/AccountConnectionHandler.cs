﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.AccountConnections;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class AccountConnectionHandler :
        IQueryHandler<FindAccountConnectionByAccountIdInsurerRequestType, AccountConnection>,
        IQueryHandler<FindAccountConnectionsByAccountRequestType, IEnumerable<AccountConnection>>,
        IQueryHandler<FindAccountConnectionsByAccount, IEnumerable<AccountConnection>>
    {
        #region Private Fields

        private readonly IRepository<AccountConnection> _repository;

        #endregion Private Fields

        #region Constructors

        public AccountConnectionHandler(IRepository<AccountConnection> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<AccountConnection> HandleAsync(FindAccountConnectionByAccountIdInsurerRequestType query)
        {
            return await _repository.SingleOrDefaultAsync(x => x.AccountId == query.AccountId
                && x.InsurerId == query.Insurer
                && x.RequestType == query.RequestType);
        }

        public async Task<IEnumerable<AccountConnection>> HandleAsync(FindAccountConnectionsByAccountRequestType query)
        {
            return await _repository.AsQueryable()
                .Where(x => x.AccountId == query.AccountId && x.RequestType == query.RequestType)
                .ToListAsync();
        }

        public async Task<IEnumerable<AccountConnection>> HandleAsync(FindAccountConnectionsByAccount query)
        {
            return await _repository.AsQueryable()
                .Where(x => x.AccountId == query.AccountId)
                .ToListAsync();
        }

        #endregion IQueryHandler Members
    }
}