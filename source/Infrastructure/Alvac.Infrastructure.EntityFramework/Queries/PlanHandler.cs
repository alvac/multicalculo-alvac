﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Plans;
using Alvac.Domain.Entities.Queries.Transactions;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class PlanHandler :
        IQueryHandler<GetPlanById, Plan>,
        IQueryHandler<GetAll, IEnumerable<Plan>>,
        IQueryHandler<GetTransactionCountByInvoiceId, int>
    {
        #region Private Fields

        private readonly IRepository<Plan> _repository;
        private readonly IRepository<Transaction> _transactionRepository;

        #endregion Private Fields

        #region Constructors

        public PlanHandler(IRepository<Plan> repository, IRepository<Transaction> transactionRepository)
        {
            _repository = repository;
            _transactionRepository = transactionRepository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<Plan> HandleAsync(GetPlanById query)
        {
            return await _repository.SingleOrDefaultAsync(x => x.PlanId == query.PlanId);
        }

        public Task<int> HandleAsync(GetTransactionCountByInvoiceId query)
        {
            return Task.FromResult<int>(_transactionRepository.AsQueryable().Where(x => x.InvoiceId == query.InvoiceId).Count());
        }

        public async Task<IEnumerable<Plan>> HandleAsync(GetAll query)
        {
            return await _repository.GetAllAsync();
        }

        #endregion IQueryHandler Members


    }
}