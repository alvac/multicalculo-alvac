﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.DeviceTrackers;
using Alvac.Domain.Entities.Queries.IgnitionBlockers;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class DeviceTrackerHandler : IQueryHandler<GetAllDeviceTrackers, IEnumerable<DeviceTracker>>
    {
        private readonly IRepository<DeviceTracker> _repository;

        public DeviceTrackerHandler(IRepository<DeviceTracker> repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<DeviceTracker>> HandleAsync(GetAllDeviceTrackers query)
        {
            return await _repository.GetAllAsync();
        }
    }
}
