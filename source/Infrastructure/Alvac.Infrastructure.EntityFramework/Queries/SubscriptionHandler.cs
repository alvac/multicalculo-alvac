﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Subscriptions;
using Alvac.Domain.Entities.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class SubscriptionHandler :
        IQueryHandler<GetSubscriptionByAccountIdRequestType, Subscription>,
        IQueryHandler<GetSubscriptionsByAccountId, IQueryable<Subscription>>,
        IQueryHandler<GetSubscriptionById, Subscription>
    {
        #region Private Fields

        private readonly IRepository<Subscription> _repository;

        #endregion Private Fields

        #region Constructors

        public SubscriptionHandler(IRepository<Subscription> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<Subscription> HandleAsync(GetSubscriptionByAccountIdRequestType query)
        {
            return await _repository.SingleOrDefaultAsync(x => x.AccountId == query.AccountId
                && x.RequestType == query.RequestType
                && x.Status == query.Status);
        }

        public Task<IQueryable<Subscription>> HandleAsync(GetSubscriptionsByAccountId query)
        {
            return Task.FromResult(_repository.AsQueryable().Where(x => x.AccountId == query.AccountId));
        }

        public async Task<Subscription> HandleAsync(GetSubscriptionById query)
        {
            return await _repository.GetByIdAsync(query.SubscriptionId);
        }

        #endregion IQueryHandler Members
    }
}