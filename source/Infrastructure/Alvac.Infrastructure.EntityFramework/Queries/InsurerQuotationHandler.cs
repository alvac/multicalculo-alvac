﻿using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.InsurerQuotations;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;
using Alvac.Domain.Entities;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class InsurerQuotationHandler :
        IQueryHandler<GetInsurerQuotationById, InsurerQuotation>,
        IQueryHandler<GetInsurerQuotationByQuotationId, IEnumerable<InsurerQuotation>>,
        IQueryHandler<GetInsurerQuotationsById, IEnumerable<InsurerQuotation>>
    {
        #region Private Fields

        private readonly IRepository<InsurerQuotation> _repository;

        #endregion Private Fields

        #region Constructors

        public InsurerQuotationHandler(IRepository<InsurerQuotation> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<IEnumerable<InsurerQuotation>> HandleAsync(GetInsurerQuotationsById query)
        {
            return await _repository.AsQueryable()
                .Where(x => query.InsurerQuotationId.Contains(x.InsurerQuotationId))
                .ToListAsync();

        }
        public async Task<InsurerQuotation> HandleAsync(GetInsurerQuotationById query)
        {
            return await _repository.GetByIdAsync(query.InsurerQuotationId);
        }

        public async Task<IEnumerable<InsurerQuotation>> HandleAsync(GetInsurerQuotationByQuotationId query)
        {
            return await _repository.AsQueryable()
                .Where(x => x.QuotationId == query.QuotationId)
                .ToListAsync();
        }

        #endregion IQueryHandler Members
    }
}