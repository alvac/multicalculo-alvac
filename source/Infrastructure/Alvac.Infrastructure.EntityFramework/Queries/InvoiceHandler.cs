﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Invoices;
using Alvac.Domain.Entities.Repositories;
using System.Linq;
using System.Threading.Tasks;
using System;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class InvoiceHandler :
        IQueryHandler<GetInvoiceBySubscriptionIdStatus, Invoice>,
        IQueryHandler<GetInvoiceBySubscriptionId, IQueryable<Invoice>>,
        IQueryHandler<GetInvoicesByStatusClosing, IQueryable<Invoice>>
    {
        #region Private Fields

        private readonly IRepository<Invoice> _repository;

        #endregion Private Fields

        #region Constructors

        public InvoiceHandler(IRepository<Invoice> repository)
        {
            _repository = repository;
        }



        #endregion Constructors

        #region IQueryHandler Members

        public Task<IQueryable<Invoice>> HandleAsync(GetInvoicesByStatusClosing query)
        {
            return Task.FromResult(_repository.AsQueryable().Where(x => x.Closing <= query.Closing && x.Status == query.Status));
        }

        public Task<IQueryable<Invoice>> HandleAsync(GetInvoiceBySubscriptionId query)
        {
            return Task.FromResult(_repository.AsQueryable().Where(x => x.SubscriptionId == query.SubscriptionId));
        }

        public async Task<Invoice> HandleAsync(GetInvoiceBySubscriptionIdStatus query)
        {
            return await _repository.SingleOrDefaultAsync(x => x.SubscriptionId == query.SubscriptionId && x.Status == query.Status);
        }

        #endregion IQueryHandler Members
    }
}