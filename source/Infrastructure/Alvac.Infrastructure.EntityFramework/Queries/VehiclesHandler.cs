﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Vehicles;
using Alvac.Domain.Entities.Repositories;
using Alvac.Domain.Enum.Auto;
using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Threading.Tasks;
using System.Data.Entity;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class VehiclesHandler :
        IQueryHandler<GetManufacturers, IEnumerable<Manufacturer>>,
        IQueryHandler<GetVehiclesByManufacturer, IQueryable<Vehicle>>,
        IQueryHandler<GetVehiclesByManufacturerModel, IEnumerable<Vehicle>>,
        IQueryHandler<GetVehicleById, Vehicle>,
        IQueryHandler<GetVehiclesByFipeInsurer, IEnumerable<Vehicle>>
    {
        #region Private Fields

        private readonly IRepository<Vehicle> _vehicleRepository;
        private readonly IRepository<Manufacturer> _manufacturerRepository;

        #endregion Private Fields

        #region Constructors

        public VehiclesHandler(IRepository<Vehicle> vehicleRepository, IRepository<Manufacturer> manufacturerRepository)
        {
            _vehicleRepository = vehicleRepository;
            _manufacturerRepository = manufacturerRepository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<IEnumerable<Manufacturer>> HandleAsync(GetManufacturers query)
        {
            var response = await _manufacturerRepository.AsQueryable().Where(x => !x.InsurerId.HasValue).ToListAsync();
            return response;
        }

        public Task<IQueryable<Vehicle>> HandleAsync(GetVehiclesByManufacturer query)
        {
            var vehicles = _vehicleRepository.AsQueryable()
                .Where(x => x.ManufacturerId == query.ManufacturerId
                    && !x.InsurerId.HasValue
                    && query.ModelYear >= x.InitialYear
                    && query.ModelYear <= x.FinalYear);

            return Task.FromResult(vehicles);
        }

        public async Task<IEnumerable<Vehicle>> HandleAsync(GetVehiclesByManufacturerModel query)
        {
            var vehicles = await _vehicleRepository.AsQueryable()
                .Where(x => x.ManufacturerId == query.ManufacturerId
                    && !x.InsurerId.HasValue
                    && x.Model == query.Model).ToListAsync();

            return vehicles;
        }

        public async Task<Vehicle> HandleAsync(GetVehicleById query)
        {
            return await _vehicleRepository.GetByIdAsync(query.VehicleId);
        }

        public async Task<IEnumerable<Vehicle>> HandleAsync(GetVehiclesByFipeInsurer query)
        {
            var fipeCode = query.FipeCode.Replace("-", "");
            return await _vehicleRepository.AsQueryable().Where(x => (x.FipeCode == query.FipeCode || x.FipeCode == fipeCode) 
                && x.InsurerId == query.Insurer).ToListAsync();
        }

        #endregion IQueryHandler Members


    }
}