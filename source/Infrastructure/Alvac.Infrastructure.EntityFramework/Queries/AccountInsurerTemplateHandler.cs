﻿using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.AccountInsurerTemplates;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class AccountInsurerTemplateHandler :
        IQueryHandler<FindAccountInsurerTemplateByAccountId, IQueryable<Alvac.Domain.Entities.AccountInsurerTemplate>>
    {
        #region Private Fields

        private readonly IRepository<Alvac.Domain.Entities.AccountInsurerTemplate> _repository;

        #endregion Private Fields

        #region Constructors

        public AccountInsurerTemplateHandler(IRepository<Alvac.Domain.Entities.AccountInsurerTemplate> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public Task<IQueryable<Alvac.Domain.Entities.AccountInsurerTemplate>> HandleAsync(FindAccountInsurerTemplateByAccountId query)
        {
            return Task.FromResult(_repository.AsQueryable().OrderByDescending(x => x.Created).Where(x => x.AccountId == query.AccountId));
        }

        #endregion IQueryHandler Members
    }
}