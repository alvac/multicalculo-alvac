﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.ConnectionParameters;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class ConnectionParameterHandler :
         IQueryHandler<FindConnectionParametersByConnectionId, IQueryable<ConnectionParameter>>
    {
        #region Private Fields

        private readonly IRepository<ConnectionParameter> _repository;

        #endregion Private Fields

        #region Constructors

        public ConnectionParameterHandler(IRepository<ConnectionParameter> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public Task<IQueryable<ConnectionParameter>> HandleAsync(FindConnectionParametersByConnectionId query)
        {
            return Task.FromResult(_repository.AsQueryable().Where(x => x.ConnectionId == query.ConnectionId));
        }

        #endregion IQueryHandler Members
    }
}