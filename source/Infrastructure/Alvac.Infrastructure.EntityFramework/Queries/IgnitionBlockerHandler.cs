﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.IgnitionBlockers;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class IgnitionBlockerHandler : IQueryHandler<GetAllIgnitionBlockers, IEnumerable<IgnitionBlocker>>
    {
        private readonly IRepository<IgnitionBlocker> _repository;

        public IgnitionBlockerHandler(IRepository<IgnitionBlocker> repository)
        {
            _repository = repository;
        }

        public async Task<IEnumerable<IgnitionBlocker>> HandleAsync(GetAllIgnitionBlockers query)
        {
            return await _repository.GetAllAsync();
        }
    }
}
