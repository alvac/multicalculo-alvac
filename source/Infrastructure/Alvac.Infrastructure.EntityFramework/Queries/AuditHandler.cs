﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Accounts;
using Alvac.Domain.Entities.Queries.Audits;
using Alvac.Domain.Entities.Repositories;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class AuditHandler :
        IQueryHandler<GetAllAudits, IQueryable<Audit>>
    {
        #region Private Fields

        private readonly IRepository<Audit> _repository;

        #endregion Private Fields

        #region Constructors

        public AuditHandler(IRepository<Audit> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public Task<IQueryable<Audit>> HandleAsync(GetAllAudits query)
        {
            return Task.FromResult(_repository.AsQueryable());
        }

        #endregion IQueryHandler Members




    }
}