﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Notifications;
using Alvac.Domain.Entities.Repositories;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class NotificationHandler :
         IQueryHandler<GetNotificationById, Notification>
    {
        #region Private Fields

        private readonly IRepository<Notification> _repository;

        #endregion Private Fields

        #region Constructors

        public NotificationHandler(IRepository<Notification> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<Notification> HandleAsync(GetNotificationById query)
        {
            return await _repository.SingleOrDefaultAsync(x => x.InsurerQuotationId == query.InsurerQuotationId);
        }

        #endregion IQueryHandler Members
    }
}