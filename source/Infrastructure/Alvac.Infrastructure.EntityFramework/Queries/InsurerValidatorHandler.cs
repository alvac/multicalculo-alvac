﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.InsurerValidators;
using Alvac.Domain.Entities.Repositories;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class InsurerValidatorHandler :
         IQueryHandler<FindInsurerValidatorById, InsurerValidator>,
         IQueryHandler<FindInsurerValidatorByInsurerRequestType, IEnumerable<InsurerValidator>>,
         IQueryHandler<GetAll, IQueryable<InsurerValidator>>
    {
        #region Private Fields

        private readonly IRepository<InsurerValidator> _repository;

        #endregion Private Fields

        #region Constructors

        public InsurerValidatorHandler(IRepository<InsurerValidator> repository)
        {
            _repository = repository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<InsurerValidator> HandleAsync(FindInsurerValidatorById query)
        {
            return await _repository.GetByIdAsync(query.InsurerValidatorId);
        }

        public Task<IQueryable<InsurerValidator>> HandleAsync(GetAll query)
        {
            return Task.FromResult(_repository.AsQueryable());
        }

        public async Task<IEnumerable<InsurerValidator>> HandleAsync(FindInsurerValidatorByInsurerRequestType query)
        {
            return await _repository.AsQueryable().Where(x => x.InsurerId == query.Insurer 
                && x.RequestType == query.RequestType).ToListAsync();
        }

        #endregion




    }
}