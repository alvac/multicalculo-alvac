﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.WorkActivities;
using Alvac.Domain.Entities.Repositories;
using Alvac.Domain.Services.Reports;
using Alvac.Domain.Services.Reports.Queries;
using System.Collections.Generic;
using System.Linq;
using System.Data.Entity;
using System.Threading.Tasks;
using Alvac.Domain.Enum;
using System;

namespace Alvac.Infrastructure.EntityFramework.Queries
{
    public class ReportHandler :
        IQueryHandler<ReportGroupByUserQuery, List<ReportQuotationGroupByUser>>,
        IQueryHandler<ReportGeneralQuery, ReportGeneral>
    {

        #region Private Fields

        private readonly IRepository<Insurer> _insurerRepository;
        private readonly IRepository<InsurerAutoQuotation> _repo;

        #endregion Private Fields

        #region Constructors

        public ReportHandler(IRepository<InsurerAutoQuotation> repo,
            IRepository<Insurer> insurerRepository)
        {
            _repo = repo;
            _insurerRepository = insurerRepository;
        }

        #endregion Constructors

        #region IQueryHandler Members

        public async Task<List<ReportQuotationGroupByUser>> HandleAsync(ReportGroupByUserQuery query)
        {
            var dataQuery = _repo.AsQueryable().Where(x => x.Quotation.AccountId == query.AccountId
                && x.Quotation.RequestDateTime >= query.StartDate
                && x.Quotation.RequestDateTime <= query.EndDate);

            if (query.Status.HasValue)
            {
                dataQuery = dataQuery.Where(x => x.Status == query.Status.Value);
            }

            var report = await dataQuery.GroupBy(x => new { x.Quotation.User.UserName }, (key, quotations) => new ReportQuotationGroupByUser
            {
                UserName = key.UserName,
                Quotations = quotations.GroupBy(q => new { q.InsurerId, q.Insurer.Name }, (insurerKey, insurerQuotations) => new ReportQuotationInsurer
                {
                    Insurer = insurerKey.InsurerId,
                    InsurerDisplay = insurerKey.Name,
                    Count = insurerQuotations.Count()
                })
            }).ToListAsync();

            var insurers = await GetInsurersAsync();

            report.ForEach(x =>
            {
                var quotationList = x.Quotations.ToList();
                var insurerUser = x.Quotations.Select(s => s.Insurer);

                foreach (var item in insurers.Select(s => s.InsurerId).Except(insurerUser))
                {
                    quotationList.Add(new ReportQuotationInsurer
                    {
                        Insurer = item,
                        InsurerDisplay = insurers.FirstOrDefault(r => r.InsurerId == item).Name,
                        Count = 0
                    });
                }

                x.Quotations = quotationList.OrderBy(order => order.Insurer);
            });

            return report;
        }

        public async Task<ReportGeneral> HandleAsync(ReportGeneralQuery query)
        {
            var report = new ReportGeneral();

            var dataQuery = _repo.AsQueryable().Where(x => x.Quotation.AccountId == query.AccountId
                && x.Quotation.RequestDateTime >= query.StartDate
                && x.Quotation.RequestDateTime <= query.EndDate);

            report.InsurerQuotations = await dataQuery.GroupBy(x => new { x.InsurerId, x.Insurer.Name }, (key, quotations) => new ReportQuotationInsurer
            {
                Insurer = key.InsurerId,
                InsurerDisplay = key.Name,
                Count = quotations.Count()
            }).ToListAsync();

            foreach (var item in report.InsurerQuotations)
            {
                item.Status = new List<ReportQuotationStatus>();
                item.Status.Add(new ReportQuotationStatus
                {
                    DescriptionCode = DescriptionCode.Successfully,
                    Count = dataQuery.Count(x => x.OperationDescriptionCode == DescriptionCode.Successfully
                        && x.InsurerId == item.Insurer)
                });

                item.Status.Add(new ReportQuotationStatus
                {
                    DescriptionCode = DescriptionCode.UserError,
                    Count = dataQuery.Count(x => x.OperationDescriptionCode == DescriptionCode.UserError
                        && x.InsurerId == item.Insurer)
                });

                item.Status.Add(new ReportQuotationStatus
                {
                    DescriptionCode = DescriptionCode.InternalServerError,
                    Count = dataQuery.Count(x => x.OperationDescriptionCode != DescriptionCode.Successfully
                        && x.OperationDescriptionCode != DescriptionCode.UserError 
                        && x.InsurerId == item.Insurer)
                });
            }

            report.InsurerQuotationsStatus = new List<ReportQuotationStatus>();
            report.InsurerQuotationsStatus.Add(new ReportQuotationStatus
            {
                DescriptionCode = DescriptionCode.Successfully,
                Count = dataQuery.Count(x => x.OperationDescriptionCode == DescriptionCode.Successfully)
            });

            report.InsurerQuotationsStatus.Add(new ReportQuotationStatus
            {
                DescriptionCode = DescriptionCode.UserError,
                Count = dataQuery.Count(x => x.OperationDescriptionCode == DescriptionCode.UserError)
            });

            report.InsurerQuotationsStatus.Add(new ReportQuotationStatus
            {
                DescriptionCode = DescriptionCode.InternalServerError,
                Count = dataQuery.Count(x => x.OperationDescriptionCode != DescriptionCode.Successfully && x.OperationDescriptionCode != DescriptionCode.UserError)
            });

            return report;
        }

        #endregion IQueryHandler Members

        private async Task<IEnumerable<Insurer>> GetInsurersAsync()
        {
            return await _insurerRepository.GetAllAsync();
        }
    }
}