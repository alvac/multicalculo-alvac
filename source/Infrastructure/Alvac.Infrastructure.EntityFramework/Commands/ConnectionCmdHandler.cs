﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Audits;
using Alvac.Domain.Entities.Commands.Connections;
using Alvac.Domain.Entities.Commands.Plans;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class ConnectionCmdHandler :
         ICommandHandler<CreateConnection, Connection>,
         ICommandHandler<UpdateConnection, Connection>,
         ICommandHandler<DeleteConnection, bool>
    {
        private readonly IRepository<Connection> _connectionRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public ConnectionCmdHandler(IRepository<Connection> connectionRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _connectionRepository = connectionRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Connection> HandleAsync(CreateConnection command)
        {
            var connection = _mapper.Map<Connection>(command);
            await _connectionRepository.AddAsync(connection);
            await _unitOfWork.SaveAsync();
            return connection;
        }

        public async Task<Connection> HandleAsync(UpdateConnection command)
        {
            var connection = await _connectionRepository.GetByIdAsync(command.ConnectionId);
            _mapper.Map(command, connection);

            await _connectionRepository.UpdateAsync(connection);
            await _unitOfWork.SaveAsync();
            return connection;
        }

        public async Task<bool> HandleAsync(DeleteConnection command)
        {
            var connection = await _connectionRepository.GetByIdAsync(command.ConnectionId);
            await _connectionRepository.RemoveAsync(connection);
            await _unitOfWork.SaveAsync();
            return true;
        }
    }
}
