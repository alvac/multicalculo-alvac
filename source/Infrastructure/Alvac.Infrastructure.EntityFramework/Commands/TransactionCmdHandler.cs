﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Transactions;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands.Transactions
{
    public class TransactionCmdHandler :
        ICommandHandler<CreateTransaction, Transaction>
    {
        #region Private Fields

        private readonly IMapper _mapper;
        private readonly IRepository<Transaction> _repository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        #region Constructors

        public TransactionCmdHandler(IUnitOfWork unitOfWork, IRepository<Transaction> repository, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
            _mapper = mapper;
        }

        #endregion Constructors

        #region ICommandHandler Members

        public async Task<Transaction> HandleAsync(CreateTransaction command)
        {
            var model = _mapper.Map<Transaction>(command);
            model.Created = DateTimeOffset.Now;

            await _repository.AddAsync(model);
            await _unitOfWork.SaveAsync();

            return model;
        }

        #endregion ICommandHandler Members
    }
}