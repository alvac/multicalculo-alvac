﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Users;
using Alvac.Domain.Entities.Repositories;
using Alvac.Domain.Enum;
using Alvac.Domain.Services;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class UserCmdHandler :
        ICommandHandler<CreateUser, User>,
        ICommandHandler<ConfirmEmail, bool>,
        ICommandHandler<DeleteUser, bool>,
        ICommandHandler<UpdateUser, User>
    {
        private readonly UserManager<User> _manager;
        private readonly IMapper _mapper;
        private readonly IRepository<IdentityRole> _roleRepository;
        private readonly ITimeProvider _timeProvider;

        public UserCmdHandler(UserManager<User> manager,
            IRepository<IdentityRole> roleRepository,
            IMapper mapper,
            ITimeProvider timeProvider)
        {
            _manager = manager;
            _timeProvider = timeProvider;
            _roleRepository = roleRepository;
            _mapper = mapper;
        }

        public async Task<User> HandleAsync(CreateUser command)
        {
            var user = _mapper.Map<User>(command);
            user.Created = _timeProvider.Now;
            user.IsActive = true;
            var response = await _manager.CreateAsync(user);

            if (response.Succeeded)
            {
                var role = await _roleRepository.GetByIdAsync(command.Role);
                var roleResponse = await _manager.AddToRoleAsync(user.Id, role.Name);
                if (roleResponse.Succeeded)
                {
                    return user;
                }
            }

            throw new AlvacException(string.Join(" - ", response.Errors), DescriptionCode.InvalidRequest);
        }

        public async Task<bool> HandleAsync(ConfirmEmail command)
        {
            var user = await _manager.FindByEmailAsync(command.Email);
            var response = await _manager.ConfirmEmailAsync(user.Id, command.Token);

            if (response.Succeeded)
            {
                var pwResponse = await _manager.AddPasswordAsync(user.Id, command.Password);
                if (pwResponse.Succeeded)
                {
                    return true;
                }
            }

            throw new AlvacException(string.Join(" - ", response.Errors), DescriptionCode.InvalidRequest);
        }

        public async Task<bool> HandleAsync(DeleteUser command)
        {
            var user = await _manager.FindByIdAsync(command.Id);

            var guid = user.Id.Replace("-", string.Empty);
            user.IsActive = false;
            user.UserName = string.Format("{0}_{1}", guid, user.UserName);
            user.Email = string.Format("{0}_{1}", guid, user.Email);

            var response = await _manager.UpdateAsync(user);

            if (response.Succeeded)
            {
                return true;
            }

            throw new AlvacException(string.Join(" - ", response.Errors), DescriptionCode.InvalidRequest);

        }

        public async Task<User> HandleAsync(UpdateUser command)
        {
            var user = await _manager.FindByIdAsync(command.Id);
            user.Name = command.Name;
            var roleRemove = await _roleRepository.GetByIdAsync(user.Roles.First().RoleId);
            var response = await _manager.UpdateAsync(user);

            if (response.Succeeded)
            {
                await _manager.RemoveFromRoleAsync(user.Id, roleRemove.Name);

                var role = await _roleRepository.GetByIdAsync(command.Role);
                var roleResponse = await _manager.AddToRoleAsync(user.Id, role.Name);
                return user;
            }

            throw new AlvacException(string.Join(" - ", response.Errors), DescriptionCode.InvalidRequest);
        }
    }
}
