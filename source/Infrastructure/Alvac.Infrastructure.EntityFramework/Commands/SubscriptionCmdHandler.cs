﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Subscriptions;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class SubscriptionCmdHandler :
        ICommandHandler<CreateSubscription, Subscription>,
        ICommandHandler<UpdateSubscription, Subscription>
    {
        private readonly IRepository<Subscription> _subscriptionRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        public SubscriptionCmdHandler(IRepository<Subscription> subscriptionRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _subscriptionRepository = subscriptionRepository;
            _mapper = mapper;
        }

        public async Task<Subscription> HandleAsync(UpdateSubscription command)
        {
            var model = await _subscriptionRepository.GetByIdAsync(command.SubscriptionId);
            _mapper.Map(command, model);
            model.LastUpdate = DateTimeOffset.Now;
            await _subscriptionRepository.UpdateAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }

        public async Task<Subscription> HandleAsync(CreateSubscription command)
        {
            var model = new Subscription();
            _mapper.Map(command, model);
            model.Created = DateTimeOffset.Now;
            model.LastUpdate = DateTimeOffset.Now;
            await _subscriptionRepository.AddAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }
    }
}
