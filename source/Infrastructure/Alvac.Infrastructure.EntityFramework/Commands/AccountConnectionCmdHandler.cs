﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.AccountConnections;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class AccountConnectionCmdHandler :
        ICommandHandler<CreateAccountConnection, AccountConnection>,
        ICommandHandler<DeleteAccountConnection, bool>
    {

        #region Private Fields

        private readonly IRepository<AccountConnection> _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        #endregion Private Fields

        #region Constructors

        public AccountConnectionCmdHandler(IRepository<AccountConnection> repository,
            IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
            _mapper = mapper;
        }

        #endregion Private Fields

        public async Task<AccountConnection> HandleAsync(CreateAccountConnection command)
        {
            var accountConnection = new AccountConnection
            {
                AccountId = command.AccountId,
                ConnectionId = command.ConnectionId,
                Insurer = command.Insurer,
                RequestType = command.RequestType
            };

            await _repository.AddAsync(accountConnection);
            await _unitOfWork.SaveAsync();
            return accountConnection;
        }

        public async Task<bool> HandleAsync(DeleteAccountConnection command)
        {
            var model = await _repository.FirstOrDefaultAsync(x => x.ConnectionId == command.ConnectionId
                && x.AccountId == command.AccountId);

            await _repository.RemoveAsync(model);
            await _unitOfWork.SaveAsync();
            return true;
        }
    }
}