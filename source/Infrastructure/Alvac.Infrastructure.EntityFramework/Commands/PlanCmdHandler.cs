﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Audits;
using Alvac.Domain.Entities.Commands.Plans;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class PlanCmdHandler :
         ICommandHandler<CreatePlan, Plan>,
         ICommandHandler<UpdatePlan, Plan>,
        ICommandHandler<DeletePlan, bool>
    {
        private readonly IRepository<Plan> _planRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public PlanCmdHandler(IRepository<Plan> planRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _planRepository = planRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Plan> HandleAsync(CreatePlan command)
        {
            var plan = _mapper.Map<Plan>(command);
            await _planRepository.AddAsync(plan);
            await _unitOfWork.SaveAsync();
            return plan;
        }

        public async Task<Plan> HandleAsync(UpdatePlan command)
        {
            var plan = await _planRepository.GetByIdAsync(command.PlanId);
            _mapper.Map(command, plan);

            await _planRepository.UpdateAsync(plan);
            await _unitOfWork.SaveAsync();
            return plan;
        }

        public async Task<bool> HandleAsync(DeletePlan command)
        {
            var plan = await _planRepository.GetByIdAsync(command.PlanId);
            await _planRepository.RemoveAsync(plan);
            await _unitOfWork.SaveAsync();
            return true;
        }
    }
}
