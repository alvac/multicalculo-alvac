﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Auto.InsurerAutoQuotationParameters;
using Alvac.Domain.Entities.Repositories;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands.Auto
{
    public class InsurerAutoQuotationParameterCmdHandler :
        ICommandHandler<CreateInsurerAutoQuotationParameter, InsurerAutoQuotationParameter>
    {
        #region Private Fields

        private readonly IMapper _mapper;
        private readonly IRepository<InsurerAutoQuotationParameter> _repository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        #region Constructors

        public InsurerAutoQuotationParameterCmdHandler(IUnitOfWork unitOfWork, IRepository<InsurerAutoQuotationParameter> repository, IMapper mapper)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        #endregion Constructors

        #region ICommandHandler Members

        public async Task<InsurerAutoQuotationParameter> HandleAsync(CreateInsurerAutoQuotationParameter command)
        {
            var model = _mapper.Map<InsurerAutoQuotationParameter>(command);

            await _repository.AddAsync(model);
            await _unitOfWork.SaveAsync();

            return model;
        }

        #endregion ICommandHandler Members
    }
}