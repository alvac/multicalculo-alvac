﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Response.Auto;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Auto.InsurerAutoQuotations;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands.Auto
{
    public class InsurerAutoQuotationCmdHandler :
        ICommandHandler<CreateInsurerAutoQuotation, InsurerAutoQuotation>,
        ICommandHandler<AutoQuotationResponse, Domain.Entities.Auto.InsurerAutoQuotation>
    {
        #region Private Fields

        private readonly IMapper _mapper;
        private readonly IRepository<InsurerAutoQuotation> _repository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        #region Constructor

        public InsurerAutoQuotationCmdHandler(IUnitOfWork unitOfWork, IRepository<InsurerAutoQuotation> repository, IMapper mapper)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        #endregion Constructor

        #region ICommandHandler Members

        public async Task<InsurerAutoQuotation> HandleAsync(CreateInsurerAutoQuotation command)
        {
            var model = _mapper.Map<InsurerAutoQuotation>(command);
            model.LastUpdate = DateTimeOffset.Now;

            await _repository.AddAsync(model);
            await _unitOfWork.SaveAsync();

            return model;
        }

        public async Task<Domain.Entities.Auto.InsurerAutoQuotation> HandleAsync(AutoQuotationResponse command)
        {

            var insurerAutoQuotation = await _repository.GetByIdAsync(command.Identifier) as Domain.Entities.Auto.InsurerAutoQuotation;
            var model = _mapper.Map(command, insurerAutoQuotation);
            
            if (command.Payment != null && command.Payment.Count > 0)
            {
                insurerAutoQuotation.Payments = new List<Domain.Entities.Payment>();

                foreach (var item in command.Payment)
                {
                    var payment = new Domain.Entities.Payment
                    {
                        InsurerQuotationId = insurerAutoQuotation.InsurerQuotationId,
                        Code = item.Code,
                        Description = item.Description,
                        FirstInstallmentValue = item.FirstInstallmentValue,
                        OtherInstallmentsValue = item.OtherInstallmentsValue,
                        TotalInstallmentsValue = item.TotalInstallmentsValue
                    };

                    insurerAutoQuotation.Payments.Add(payment);
                }
            }

            await _repository.UpdateAsync(model);
            await _unitOfWork.SaveAsync();

            return model;
        }

        #endregion ICommandHandler Members
    }
}