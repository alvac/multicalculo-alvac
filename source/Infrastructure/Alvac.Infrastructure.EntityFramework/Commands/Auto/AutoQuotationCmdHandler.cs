﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Auto;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands.Auto
{
    internal class AutoQuotationCmdHandler :
        ICommandHandler<AutoQuotationRequest, AutoQuotation>
    {
        #region Private Fields

        private readonly IMapper _mapper;
        private readonly IRepository<AutoQuotation> _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<IgnitionBlocker> _ignitionBlockerRepository;
        private readonly IRepository<DeviceTracker> _deviceTrackerRepository;

        #endregion Private Fields

        #region Constructors

        public AutoQuotationCmdHandler(IUnitOfWork unitOfWork, 
            IRepository<AutoQuotation> repository,
            IMapper mapper,
            IRepository<DeviceTracker> deviceTrackerRepository,
            IRepository<IgnitionBlocker> ignitionBlockerRepository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
            _deviceTrackerRepository = deviceTrackerRepository;
            _ignitionBlockerRepository = ignitionBlockerRepository;
        }

        #endregion Constructors

        #region ICommandHandler Members

        public async Task<AutoQuotation> HandleAsync(AutoQuotationRequest command)
        {
            var model = _mapper.Map<AutoQuotation>(command);

            model.VehicleInfo.DevicesTracker = new List<DeviceTracker>();
            model.VehicleInfo.DevicesIgnitionBlocker = new List<IgnitionBlocker>();

            _deviceTrackerRepository.AsQueryable().Where(x => command.VehicleInfo.DevicesTracker.Contains(x.DeviceTrackerId)).ToList().ForEach(x => {
                model.VehicleInfo.DevicesTracker.Add(x);
            });

            _ignitionBlockerRepository.AsQueryable().Where(x => command.VehicleInfo.DevicesIgnitionBlocker.Contains(x.IgnitionBlockerId)).ToList().ForEach(x =>
            {
                model.VehicleInfo.DevicesIgnitionBlocker.Add(x);
            }); ;

            model.LastUpdate = DateTimeOffset.Now;

            await _repository.AddAsync(model);
            await _unitOfWork.SaveAsync();

            return model;
        }

        #endregion ICommandHandler Members
    }
}