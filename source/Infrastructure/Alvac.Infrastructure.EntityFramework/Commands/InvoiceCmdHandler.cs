﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Invoices;
using Alvac.Domain.Entities.Repositories;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands.Invoices
{
    public class InvoiceCmdHandler :
        ICommandHandler<CloseInvoice, bool>,
        ICommandHandler<CreateInvoice, Invoice>
    {
        #region Private Fields

        private readonly IRepository<Invoice> _invoiceRepository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        #region Constructor

        public InvoiceCmdHandler(IUnitOfWork unitOfWork, IRepository<Invoice> invoiceRepository)
        {
            _unitOfWork = unitOfWork;
            _invoiceRepository = invoiceRepository;
        }



        #endregion Constructor

        #region ICommandHandler Members

        public async Task<Invoice> HandleAsync(CreateInvoice command)
        {
            var invoice = new Invoice
            {
                SubscriptionId = command.SubscriptionId,
                Status = InvoiceStatus.Open,
                Closing = command.Closing,
                Created = DateTimeOffset.Now,
                LastUpdate = DateTimeOffset.Now,
            };

            await _invoiceRepository.AddAsync(invoice);
            await _unitOfWork.SaveAsync();

            return invoice;
        }

        public async Task<bool> HandleAsync(CloseInvoice command)
        {
            var invoice = await _invoiceRepository.GetByIdAsync(command.InvoiceId);

            invoice.Status = InvoiceStatus.Closed;
            invoice.Closing = DateTimeOffset.Now;

            await _invoiceRepository.UpdateAsync(invoice);
            await _unitOfWork.SaveAsync();
            return true;
        }

        #endregion ICommandHandler Members
    }
}