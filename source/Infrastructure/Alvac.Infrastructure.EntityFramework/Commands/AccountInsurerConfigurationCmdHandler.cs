﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.AccountInsurerConfigurations;
using Alvac.Domain.Entities.Repositories;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class AccountInsurerConfigurationCmdHandler :
        ICommandHandler<CreateAccountInsurerConfiguration, AccountInsurerConfiguration>
    {
        #region Private Fields

        private readonly IRepository<AccountInsurerConfiguration> _repository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        #region Constructors

        public AccountInsurerConfigurationCmdHandler(IRepository<AccountInsurerConfiguration> repository,
            IUnitOfWork unitOfWork)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
        }

        #endregion Constructors

        #region ICommandHandler Members

        public async Task<AccountInsurerConfiguration> HandleAsync(CreateAccountInsurerConfiguration command)
        {
            AccountInsurerConfiguration entity = null;

            entity = await _repository.SingleOrDefaultAsync(x => x.AccountInsurerTemplateId == command.AccountInsurerTemplateId
                && x.InsurerConfigurationId == command.InsurerConfigurationId);

            if (entity == null)
            {
                entity = new AccountInsurerConfiguration
                {
                    InsurerConfigurationId = command.InsurerConfigurationId,
                    AccountInsurerTemplateId = command.AccountInsurerTemplateId,
                    Value = command.Value
                };

                await _repository.AddAsync(entity);
            }
            else
            {
                entity.Value = command.Value;
                await _repository.UpdateAsync(entity);
            }

            await _unitOfWork.SaveAsync();
            return entity;
        }

        #endregion ICommandHandler Members
    }
}