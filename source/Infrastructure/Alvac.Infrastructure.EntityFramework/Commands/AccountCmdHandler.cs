﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Accounts;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{

    public class AccountCmdHandler :
        ICommandHandler<UpdateAccount, Account>,
        ICommandHandler<CreateAccount, Account>,
        ICommandHandler<DeleteAccount, bool>
    {
        #region Private Fields

        private readonly IRepository<Account> _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        #endregion Private Fields

        #region Constructors

        public AccountCmdHandler(IRepository<Account> repository,
            IUnitOfWork unitOfWork, IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
            _mapper = mapper;
        }

        #endregion

        #region ICommandHandler Members

        public async Task<Account> HandleAsync(UpdateAccount command)
        {
            var model = await _repository.GetByIdAsync(command.AccountId);
            _mapper.Map(command, model);
            await _repository.UpdateAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }

        public async Task<Account> HandleAsync(CreateAccount command)
        {
            var model = _mapper.Map<Account>(command);
            model.IsActive = true;
            await _repository.AddAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }

        public async Task<bool> HandleAsync(DeleteAccount command)
        {
            var model = await _repository.GetByIdAsync(command.AccountId);
            model.IsActive = false;

            await _repository.UpdateAsync(model);
            await _unitOfWork.SaveAsync();
            return true;
        }

        #endregion
    }
}
