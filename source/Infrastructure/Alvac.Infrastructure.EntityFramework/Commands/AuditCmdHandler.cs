﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Audits;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class AuditCmdHandler :
         ICommandHandler<CreateAudit, Audit>
    {
        private readonly IRepository<Audit> _auditRepository;
        private readonly IUnitOfWork _unitOfWork;

        public AuditCmdHandler(IRepository<Audit> auditRepository, IUnitOfWork unitOfWork)
        {
            _auditRepository = auditRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<Audit> HandleAsync(CreateAudit command)
        {
            var audit = new Audit
                {
                    Timestamp = DateTimeOffset.Now,
                    UserName = command.UserName,
                    IPAddress = command.IPAddress,
                    HttpStatusCode = command.HttpStatusCode,
                    Data = command.Data,
                    AreaAccessed = command.AreaAccessed
                };

            await _auditRepository.AddAsync(audit);
            await _unitOfWork.SaveAsync();
            return audit;
        }
    }
}
