﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.AccountInsurerTemplates;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class AccountInsurerTemplateCmdHandler :
        ICommandHandler<CreateAccountInsurerTemplate, AccountInsurerTemplate>,
        ICommandHandler<DeleteAccountInsurerTemplate, bool>,
        ICommandHandler<UpdateStatusAccountInsurerTemplate, bool>
    {
        #region Private Fields

        private readonly IRepository<AccountInsurerTemplate> _accountInsurerTemplateRepository;
        private readonly IMapper _mapper;
        private readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        #region Constructors

        public AccountInsurerTemplateCmdHandler(IRepository<AccountInsurerTemplate> accountInsurerTemplateRepository,
            IMapper mapper,
            IUnitOfWork unitOfWork)
        {
            _accountInsurerTemplateRepository = accountInsurerTemplateRepository;
            _mapper = mapper;
            _unitOfWork = unitOfWork;
        }

        #endregion Constructors

        #region ICommandHandler Members

        public async Task<AccountInsurerTemplate> HandleAsync(CreateAccountInsurerTemplate command)
        {
            var entity = _mapper.Map<AccountInsurerTemplate>(command);
            entity.Created = DateTimeOffset.Now;
            await _accountInsurerTemplateRepository.AddAsync(entity);
            await _unitOfWork.SaveAsync();
            return entity;
        }

        public async Task<bool> HandleAsync(DeleteAccountInsurerTemplate command)
        {
            var entity = await _accountInsurerTemplateRepository.GetByIdAsync(command.AccountInsurerTemplateId);
            await _accountInsurerTemplateRepository.RemoveAsync(entity);
            await _unitOfWork.SaveAsync();
            return true;
        }

        public async Task<bool> HandleAsync(UpdateStatusAccountInsurerTemplate command)
        {
            var entity = await _accountInsurerTemplateRepository.GetByIdAsync(command.AccountInsurerTemplateId);
            entity.IsActive = command.IsActive;
            await _accountInsurerTemplateRepository.UpdateAsync(entity);
            await _unitOfWork.SaveAsync();
            return true;
        }

        #endregion ICommandHandler Members


    }
}