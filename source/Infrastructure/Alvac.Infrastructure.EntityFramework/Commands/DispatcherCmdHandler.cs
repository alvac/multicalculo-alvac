﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Dispatchers;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class DispatcherCmdHandler :
        ICommandHandler<CreateDispatcher, Dispatcher>,
        ICommandHandler<UpdateDispatcher, Dispatcher>,
        ICommandHandler<DeleteDispatcher, bool>
    {
        private readonly IUnitOfWork _unitOfWork;
        private readonly IRepository<Dispatcher> _dispatcherRepository;
        private readonly IMapper _mapper;

        public DispatcherCmdHandler(IRepository<Dispatcher> dispatcherRepository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _dispatcherRepository = dispatcherRepository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<Dispatcher> HandleAsync(CreateDispatcher command)
        {
            var model = _mapper.Map<Dispatcher>(command);
            await _dispatcherRepository.AddAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }

        public async Task<Dispatcher> HandleAsync(UpdateDispatcher command)
        {
            var model = await _dispatcherRepository.GetByIdAsync(command.DispatcherId);
            _mapper.Map(command, model);

            await _dispatcherRepository.UpdateAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }

        public async Task<bool> HandleAsync(DeleteDispatcher command)
        {
            var model = await _dispatcherRepository.GetByIdAsync(command.DispatcherId);
            await _dispatcherRepository.RemoveAsync(model);
            await _unitOfWork.SaveAsync();
            return true;
        }
    }
}
