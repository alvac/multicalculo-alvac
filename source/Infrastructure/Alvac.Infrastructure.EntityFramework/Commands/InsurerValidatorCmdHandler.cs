﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.InsurerValidators;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class InsurerValidatorCmdHandler :
      ICommandHandler<CreateInsurerValidator, InsurerValidator>,
      ICommandHandler<UpdateInsurerValidator, InsurerValidator>,
      ICommandHandler<DeleteInsurerValidator, bool>
    {
        private readonly IRepository<InsurerValidator> _validatorInsurerRepository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public InsurerValidatorCmdHandler(IRepository<InsurerValidator> validatorRepository, IUnitOfWork unitOfWork, IMapper mapper)
        {
            _mapper = mapper;
            _validatorInsurerRepository = validatorRepository;
            _unitOfWork = unitOfWork;
        }

        public async Task<InsurerValidator> HandleAsync(CreateInsurerValidator command)
        {
            var validator = _mapper.Map<InsurerValidator>(command);
            await _validatorInsurerRepository.AddAsync(validator);
            await _unitOfWork.SaveAsync();
            return validator;
        }

        public async Task<InsurerValidator> HandleAsync(UpdateInsurerValidator command)
        {
            var validator = await _validatorInsurerRepository.GetByIdAsync(command.InsurerValidatorId);
            _mapper.Map(command, validator);

            await _validatorInsurerRepository.UpdateAsync(validator);
            await _unitOfWork.SaveAsync();
            return validator;
        }

        public async Task<bool> HandleAsync(DeleteInsurerValidator command)
        {
            var validator = await _validatorInsurerRepository.GetByIdAsync(command.InsurerValidatorId);
            await _validatorInsurerRepository.RemoveAsync(validator);
            await _unitOfWork.SaveAsync();
            return true;
        }
    }
}
