﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Notifications;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Command.Notifications
{
    public class NotificationCmdHandler : ICommandHandler<CreateNotification, Notification>
    {
        #region Private Fields

        private readonly IMapper _mapper;
        private readonly IRepository<Notification> _repository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        #region Constructors

        public NotificationCmdHandler(IUnitOfWork unitOfWork, IRepository<Notification> repository, IMapper mapper)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        #endregion Constructors

        #region ICommandHandler Members

        public async Task<Notification> HandleAsync(CreateNotification command)
        {
            var model = _mapper.Map<Notification>(command);
            model.Created = DateTimeOffset.Now;

            await _repository.AddAsync(model);
            await _unitOfWork.SaveAsync();

            return model;
        }

        #endregion ICommandHandler Members
    }
}