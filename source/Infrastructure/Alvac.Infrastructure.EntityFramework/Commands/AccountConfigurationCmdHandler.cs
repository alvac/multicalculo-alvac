﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.AccountConfigurations;
using Alvac.Domain.Entities.Commands.Accounts;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{

    public class AccountConfigurationCmdHandler :
        ICommandHandler<CreateEmailConfiguration, AccountConfiguration>,
        ICommandHandler<UpdateEmailConfiguration, AccountConfiguration>,
        ICommandHandler<CreateReportConfiguration, AccountConfiguration>,
        ICommandHandler<UpdateReportConfiguration, AccountConfiguration>
    {
        #region Private Fields

        private readonly IRepository<AccountConfiguration> _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        #endregion Private Fields

        #region Constructors

        public AccountConfigurationCmdHandler(IRepository<AccountConfiguration> repository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _unitOfWork = unitOfWork;
            _repository = repository;
            _mapper = mapper;
        }

        #endregion

        #region ICommandHandler Members

        public async Task<AccountConfiguration> HandleAsync(CreateEmailConfiguration command)
        {
            var configuration = new AccountConfiguration();
            var model = _mapper.Map(command, configuration);
            await _repository.AddAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }

        public async Task<AccountConfiguration> HandleAsync(UpdateEmailConfiguration command)
        {
            var configuration = await _repository.GetByIdAsync(command.AccountId);
            var model = _mapper.Map(command, configuration);
            await _repository.UpdateAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }

        public async Task<AccountConfiguration> HandleAsync(UpdateReportConfiguration command)
        {
            var configuration = await _repository.GetByIdAsync(command.AccountId);
            var model = _mapper.Map(command, configuration);
            await _repository.UpdateAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }

        public async Task<AccountConfiguration> HandleAsync(CreateReportConfiguration command)
        {
            var configuration = new AccountConfiguration();
            var model = _mapper.Map(command, configuration);
            await _repository.AddAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }
        #endregion


     
    }
}
