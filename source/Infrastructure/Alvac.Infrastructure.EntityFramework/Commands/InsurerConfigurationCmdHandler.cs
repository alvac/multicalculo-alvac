﻿using Alvac.Domain.Contract;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.InsurerConfigurations;
using Alvac.Domain.Entities.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class InsurerConfigurationCmdHandler :
        ICommandHandler<DeleteInsurerConfiguration, bool>,
        ICommandHandler<CreateInsurerConfiguration, InsurerConfiguration>,
        ICommandHandler<UpdateInsurerConfiguration, InsurerConfiguration>
    {
        private readonly IRepository<InsurerConfiguration> _repository;
        private readonly IUnitOfWork _unitOfWork;
        private readonly IMapper _mapper;

        public InsurerConfigurationCmdHandler(IRepository<InsurerConfiguration> repository,
            IUnitOfWork unitOfWork,
            IMapper mapper)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
            _mapper = mapper;
        }

        public async Task<bool> HandleAsync(DeleteInsurerConfiguration command)
        {
            var model = await _repository.GetByIdAsync(command.InsurerConfigurationId);
            await _repository.RemoveAsync(model);
            await _unitOfWork.SaveAsync();
            return true;
        }

        public async Task<InsurerConfiguration> HandleAsync(CreateInsurerConfiguration command)
        {
            var model = _mapper.Map<InsurerConfiguration>(command);
            await _repository.AddAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }

        public async Task<InsurerConfiguration> HandleAsync(UpdateInsurerConfiguration command)
        {
            var model = await _repository.GetByIdAsync(command.InsurerConfigurationId);
            _mapper.Map<UpdateInsurerConfiguration, InsurerConfiguration>(command, model);
            await _repository.UpdateAsync(model);
            await _unitOfWork.SaveAsync();
            return model;
        }
    }
}
