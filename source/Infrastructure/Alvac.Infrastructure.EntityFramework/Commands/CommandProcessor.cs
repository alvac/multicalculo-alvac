﻿using Alvac.Domain.Entities.Commands;
using System;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Command
{
    public class CommandProcessor : ICommandProcessor
    {
        #region Private Fields

        private readonly IServiceProvider _serviceProvider;

        #endregion Private Fields

        #region Contructor

        public CommandProcessor(IServiceProvider serviceProvider)
        {
            _serviceProvider = serviceProvider;
        }

        #endregion Contructor

        #region ICommandProcessor Members

        public async Task<TResult> ProcessAsync<TResult>(ICommand<TResult> command)
        {
            var handlerType = typeof(ICommandHandler<,>).MakeGenericType(command.GetType(), typeof(TResult));

            dynamic handler = _serviceProvider.GetService(handlerType);

            return await handler.HandleAsync((dynamic)command);
        }

        #endregion ICommandProcessor Members
    }
}