﻿using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.InsurerQuotations;
using Alvac.Domain.Entities.Repositories;
using Alvac.Domain.Enum;
using System;
using System.Threading.Tasks;

namespace Alvac.Infrastructure.EntityFramework.Commands
{
    public class InsurerQuotationCmdHandler :
        ICommandHandler<UpdateStatusInsurerQuotation, InsurerQuotation>
    {
        #region Private Fields

        private readonly IRepository<InsurerQuotation> _repository;
        private readonly IUnitOfWork _unitOfWork;

        #endregion Private Fields

        #region Constructors

        public InsurerQuotationCmdHandler(IUnitOfWork unitOfWork, IRepository<InsurerQuotation> repository)
        {
            _repository = repository;
            _unitOfWork = unitOfWork;
        }

        #endregion Constructors

        #region ICommandHandler Members

        public async Task<InsurerQuotation> HandleAsync(UpdateStatusInsurerQuotation command)
        {
            var model = await _repository.GetByIdAsync(command.InsurerQuotationId);

            if (model != null)
            {
                if (command.ConnectionId > 0)
                {
                    model.ConnectionId = command.ConnectionId;
                }

                model.Status = command.Status;
                model.OperationDescriptionCode = command.OperationDescriptionCode;
                model.OperationDescription = command.OperationDescription;
                model.LastUpdate = DateTimeOffset.Now;

                if (string.IsNullOrEmpty(command.Gateway))
                {
                    model.GatewayId = command.Gateway;
                }

                await _repository.UpdateAsync(model);
                await _unitOfWork.SaveAsync();
            }
            else
            {
                throw new AlvacException(string.Format("InsurerQuotation not found - InsurerQuotationId: {0}", command.InsurerQuotationId), DescriptionCode.NotFound);
            }

            return model;
        }

        #endregion ICommandHandler Members
    }
}