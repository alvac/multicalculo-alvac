namespace Alvac.Infrastructure.EntityFramework.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Init : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AccountConnections",
                c => new
                    {
                        AccountId = c.Int(nullable: false),
                        InsurerId = c.Int(nullable: false),
                        RequestType = c.Int(nullable: false),
                        ConnectionId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.AccountId, t.InsurerId, t.RequestType })
                .ForeignKey("dbo.Insurers", t => t.InsurerId, cascadeDelete: true)
                .ForeignKey("dbo.Accounts", t => t.AccountId, cascadeDelete: true)
                .ForeignKey("dbo.Connections", t => t.ConnectionId)
                .Index(t => t.AccountId)
                .Index(t => t.InsurerId)
                .Index(t => t.ConnectionId);
            
            CreateTable(
                "dbo.Accounts",
                c => new
                    {
                        AccountId = c.Int(nullable: false, identity: true),
                        LegalRepresentative = c.String(),
                        Qualifier = c.Int(nullable: false),
                        Susep = c.String(nullable: false),
                        PersonType = c.Int(nullable: false),
                        Document = c.String(),
                        Name = c.String(),
                        ZipCode = c.String(),
                        Street = c.String(),
                        Neighborhood = c.String(),
                        State = c.String(),
                        City = c.String(),
                        Email = c.String(),
                        ChargeEmail = c.String(),
                        Phone = c.String(),
                        MobilePhone = c.String(),
                        Site = c.String(),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.AccountId);
            
            CreateTable(
                "dbo.AccountConfigurations",
                c => new
                    {
                        AccountId = c.Int(nullable: false),
                        EmailReceipt = c.String(),
                        EmailBody = c.String(),
                        EmailSendToInsured = c.Boolean(nullable: false),
                        EmailHost = c.String(),
                        EmailUsername = c.String(),
                        EmailPassword = c.String(),
                        EmailPort = c.Int(nullable: false),
                        EmailEnableSsl = c.Boolean(nullable: false),
                        ReportTitle = c.String(),
                        ReportSubtitle = c.String(),
                        ReportLogo = c.String(),
                        ReportFooter = c.String(),
                        ReportUserMessage = c.String(),
                        ReportFontSize = c.Int(nullable: false),
                        ReportComments = c.String(),
                    })
                .PrimaryKey(t => t.AccountId)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.AccountInsurerTemplates",
                c => new
                    {
                        AccountInsurerTemplateId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        Name = c.String(),
                        Description = c.String(),
                        IsActive = c.Boolean(nullable: false),
                        Created = c.DateTimeOffset(nullable: false, precision: 7),
                        RequestType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AccountInsurerTemplateId)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .Index(t => t.AccountId);
            
            CreateTable(
                "dbo.AccountInsurerConfigurations",
                c => new
                    {
                        AccountInsurerConfigurationId = c.Int(nullable: false, identity: true),
                        InsurerConfigurationId = c.Int(nullable: false),
                        AccountInsurerTemplateId = c.Int(nullable: false),
                        Value = c.String(),
                    })
                .PrimaryKey(t => t.AccountInsurerConfigurationId)
                .ForeignKey("dbo.AccountInsurerTemplates", t => t.AccountInsurerTemplateId, cascadeDelete: true)
                .ForeignKey("dbo.InsurerConfigurations", t => t.InsurerConfigurationId, cascadeDelete: true)
                .Index(t => t.InsurerConfigurationId)
                .Index(t => t.AccountInsurerTemplateId);
            
            CreateTable(
                "dbo.InsurerConfigurations",
                c => new
                    {
                        InsurerConfigurationId = c.Int(nullable: false, identity: true),
                        InsurerId = c.Int(nullable: false),
                        Required = c.Boolean(nullable: false),
                        DisplayName = c.String(nullable: false),
                        Type = c.String(nullable: false),
                        Key = c.String(nullable: false),
                        Options = c.String(),
                        RequestType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InsurerConfigurationId)
                .ForeignKey("dbo.Insurers", t => t.InsurerId, cascadeDelete: true)
                .Index(t => t.InsurerId);
            
            CreateTable(
                "dbo.Insurers",
                c => new
                    {
                        InsurerId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.InsurerId);
            
            CreateTable(
                "dbo.Dispatchers",
                c => new
                    {
                        DispatcherId = c.Int(nullable: false, identity: true),
                        InsurerId = c.Int(nullable: false),
                        Technology = c.Int(nullable: false),
                        RequestType = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 255),
                        Description = c.String(nullable: false, maxLength: 255),
                        IsActive = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.DispatcherId)
                .ForeignKey("dbo.Insurers", t => t.InsurerId, cascadeDelete: true)
                .Index(t => t.InsurerId);
            
            CreateTable(
                "dbo.Connections",
                c => new
                    {
                        ConnectionId = c.Int(nullable: false, identity: true),
                        DispatcherId = c.Int(nullable: false),
                        Name = c.String(nullable: false, maxLength: 200),
                        MaxThroughput = c.Int(nullable: false),
                        LengthTimeUnitThroughput = c.Int(nullable: false),
                        WindowSize = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        InputQueueName = c.String(nullable: false),
                        OutputQueueName = c.String(nullable: false),
                        NotificationQueueName = c.String(nullable: false),
                        RetryMax = c.Int(),
                        RetryInterval = c.Int(),
                        IsActive = c.Boolean(nullable: false),
                        IsDefault = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.ConnectionId)
                .ForeignKey("dbo.Dispatchers", t => t.DispatcherId)
                .Index(t => t.DispatcherId);
            
            CreateTable(
                "dbo.ConnectionParameters",
                c => new
                    {
                        ConnectionParameterId = c.Int(nullable: false, identity: true),
                        ConnectionId = c.Int(nullable: false),
                        Key = c.String(nullable: false),
                        Value = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.ConnectionParameterId)
                .ForeignKey("dbo.Connections", t => t.ConnectionId)
                .Index(t => t.ConnectionId);
            
            CreateTable(
                "dbo.InsurerQuotations",
                c => new
                    {
                        InsurerQuotationId = c.Int(nullable: false, identity: true),
                        QuotationId = c.Int(nullable: false),
                        ConnectionId = c.Int(nullable: false),
                        InsurerId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        OperationDescriptionCode = c.Int(nullable: false),
                        OperationDescription = c.String(),
                        PremiumValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ValidUntil = c.DateTimeOffset(precision: 7),
                        GatewayId = c.String(),
                        InsurerBudgetCode = c.String(),
                        BudgetFile = c.String(),
                        RetryMax = c.Int(),
                        RetryAttempts = c.Int(),
                        RetryInterval = c.Int(),
                        RetryNext = c.DateTimeOffset(precision: 7),
                        LastUpdate = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.InsurerQuotationId)
                .ForeignKey("dbo.Connections", t => t.ConnectionId)
                .ForeignKey("dbo.Quotations", t => t.QuotationId)
                .ForeignKey("dbo.Insurers", t => t.InsurerId, cascadeDelete: true)
                .Index(t => t.QuotationId)
                .Index(t => t.ConnectionId)
                .Index(t => t.InsurerId);
            
            CreateTable(
                "dbo.Notifications",
                c => new
                    {
                        InsurerQuotationId = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        OperationDescriptionCode = c.Int(nullable: false),
                        OperationDescription = c.String(),
                        CorrelationId = c.String(),
                        CallType = c.Int(nullable: false),
                        Path = c.String(),
                        Created = c.DateTimeOffset(nullable: false, precision: 7),
                        LastUpdate = c.DateTimeOffset(precision: 7),
                    })
                .PrimaryKey(t => t.InsurerQuotationId)
                .ForeignKey("dbo.InsurerQuotations", t => t.InsurerQuotationId)
                .Index(t => t.InsurerQuotationId);
            
            CreateTable(
                "dbo.Payments",
                c => new
                    {
                        PaymentId = c.Int(nullable: false, identity: true),
                        InsurerQuotationId = c.Int(nullable: false),
                        Code = c.String(),
                        Description = c.String(),
                        FirstInstallmentValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        OtherInstallmentsValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        TotalInstallmentsValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.PaymentId)
                .ForeignKey("dbo.InsurerQuotations", t => t.InsurerQuotationId)
                .Index(t => t.InsurerQuotationId);
            
            CreateTable(
                "dbo.Quotations",
                c => new
                    {
                        QuotationId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        UserId = c.String(nullable: false, maxLength: 128),
                        Ip = c.String(),
                        Processed = c.Boolean(nullable: false),
                        OperationDescriptionCode = c.Int(nullable: false),
                        OperationDescription = c.String(),
                        RequestDateTime = c.DateTimeOffset(nullable: false, precision: 7),
                        LastUpdate = c.DateTimeOffset(nullable: false, precision: 7),
                        NotificationCorrelationId = c.String(),
                        NotificationCallType = c.Int(nullable: false),
                        NotificationPath = c.String(),
                    })
                .PrimaryKey(t => t.QuotationId)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId)
                .Index(t => t.AccountId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Insureds",
                c => new
                    {
                        QuotationId = c.Int(nullable: false),
                        WorkActivityId = c.Int(nullable: false),
                        FullName = c.String(),
                        Gender = c.Int(nullable: false),
                        Birthdate = c.DateTime(nullable: false),
                        PersonType = c.Int(nullable: false),
                        Document = c.String(),
                        MaritalStatus = c.Int(nullable: false),
                        ZipCode = c.String(),
                    })
                .PrimaryKey(t => t.QuotationId)
                .ForeignKey("dbo.WorkActivities", t => t.WorkActivityId, cascadeDelete: true)
                .ForeignKey("dbo.Quotations", t => t.QuotationId)
                .Index(t => t.QuotationId)
                .Index(t => t.WorkActivityId);
            
            CreateTable(
                "dbo.WorkActivities",
                c => new
                    {
                        WorkActivityId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.WorkActivityId);
            
            CreateTable(
                "dbo.MainDrivers",
                c => new
                    {
                        QuotationId = c.Int(nullable: false),
                        WorkActivityId = c.Int(nullable: false),
                        IsInsured = c.Boolean(nullable: false),
                        IsVehicleOwner = c.Boolean(nullable: false),
                        FullName = c.String(),
                        MobilePhoneNumber = c.String(),
                        PhoneNumber = c.String(),
                        Email = c.String(),
                        Gender = c.Int(nullable: false),
                        Birthdate = c.DateTime(nullable: false),
                        Document = c.String(),
                        MaritalStatus = c.Int(nullable: false),
                        ZipCode = c.String(),
                        DriverLicenseYears = c.Int(nullable: false),
                        DriverLicenseNumber = c.String(),
                        Schooling = c.Int(nullable: false),
                        RelationshipWithInsured = c.Int(nullable: false),
                        HasBeenStolenInTheLast24Months = c.Boolean(nullable: false),
                        ResidentialType = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuotationId)
                .ForeignKey("dbo.AutoQuotations", t => t.QuotationId)
                .ForeignKey("dbo.WorkActivities", t => t.WorkActivityId)
                .Index(t => t.QuotationId)
                .Index(t => t.WorkActivityId);
            
            CreateTable(
                "dbo.AdditionalDrivers",
                c => new
                    {
                        AdditionalDriverId = c.Int(nullable: false, identity: true),
                        QuotationId = c.Int(nullable: false),
                        FullName = c.String(),
                        Gender = c.Int(nullable: false),
                        BirthDate = c.DateTime(),
                        MaritalStatus = c.Int(nullable: false),
                        DriverLicenseYears = c.Int(),
                        RelationshipWithInsured = c.Int(nullable: false),
                        HasRegisteredVehicle = c.Boolean(nullable: false),
                        LivesWithMainDriver = c.Boolean(nullable: false),
                        NumberOfUseDaysPerWeek = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.AdditionalDriverId)
                .ForeignKey("dbo.AutoQuotations", t => t.QuotationId)
                .Index(t => t.QuotationId);
            
            CreateTable(
                "dbo.Insurances",
                c => new
                    {
                        QuotationId = c.Int(nullable: false),
                        CoveragePlanType = c.Int(nullable: false),
                        CurrentCoveragePlanType = c.Int(nullable: false),
                        CurrentInsuranceBonus = c.Int(nullable: false),
                        CurrentInsuranceNumber = c.String(),
                        ClaimQuantity = c.Int(nullable: false),
                        CurrentInsurancePolicyDate = c.DateTime(),
                        CurrentInsurancePolicyExpirationDate = c.DateTime(),
                        IsPolicyOwner = c.Boolean(nullable: false),
                        InsurancePolicyInitialDate = c.DateTime(nullable: false),
                        InsurancePolicyFinalDate = c.DateTime(nullable: false),
                        CurrentInsuranceCompany_InsurerId = c.Int(),
                    })
                .PrimaryKey(t => t.QuotationId)
                .ForeignKey("dbo.Insurers", t => t.CurrentInsuranceCompany_InsurerId)
                .ForeignKey("dbo.AutoQuotations", t => t.QuotationId)
                .Index(t => t.QuotationId)
                .Index(t => t.CurrentInsuranceCompany_InsurerId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        AccountId = c.Int(nullable: false),
                        IsActive = c.Boolean(nullable: false),
                        Name = c.String(),
                        Created = c.DateTimeOffset(nullable: false, precision: 7),
                        Email = c.String(maxLength: 256),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(),
                        SecurityStamp = c.String(),
                        PhoneNumber = c.String(),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .Index(t => t.AccountId)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex");
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128),
                        ClaimType = c.String(),
                        ClaimValue = c.String(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128),
                        ProviderKey = c.String(nullable: false, maxLength: 128),
                        UserId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128),
                        RoleId = c.String(nullable: false, maxLength: 128),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.VehicleInfos",
                c => new
                    {
                        QuotationId = c.Int(nullable: false),
                        Brand = c.String(),
                        Model = c.String(),
                        ManufacturingYear = c.Int(nullable: false),
                        ModelYear = c.Int(nullable: false),
                        FipeCode = c.String(),
                        Fuel = c.Int(nullable: false),
                        ModelDescription = c.String(),
                        IsNew = c.Boolean(nullable: false),
                        DevicesAlarm = c.Boolean(nullable: false),
                        IsArmored = c.Boolean(nullable: false),
                        ArmoryCoverage = c.Decimal(precision: 18, scale: 2),
                        HasNaturalGasKit = c.Boolean(nullable: false),
                        NaturalGasKitCoverage = c.Decimal(precision: 18, scale: 2),
                        PlateNumber = c.String(),
                        ChassisNumber = c.String(),
                        ValueCategory = c.Int(nullable: false),
                        DeterminedValue = c.Decimal(precision: 18, scale: 2),
                        ReferencedValue = c.Decimal(precision: 18, scale: 2),
                        ReferencedPercentage = c.Int(),
                        IsFinanced = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.QuotationId)
                .ForeignKey("dbo.AutoQuotations", t => t.QuotationId)
                .Index(t => t.QuotationId);
            
            CreateTable(
                "dbo.IgnitionBlockers",
                c => new
                    {
                        IgnitionBlockerId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.IgnitionBlockerId);
            
            CreateTable(
                "dbo.DeviceTrackers",
                c => new
                    {
                        DeviceTrackerId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.DeviceTrackerId);
            
            CreateTable(
                "dbo.VehicleOwners",
                c => new
                    {
                        QuotationId = c.Int(nullable: false),
                        FullName = c.String(),
                        Gender = c.Int(nullable: false),
                        Birthdate = c.DateTime(nullable: false),
                        PersonType = c.Int(nullable: false),
                        Document = c.String(),
                        MaritalStatus = c.Int(nullable: false),
                        ZipCode = c.String(),
                        VehiclesNumbers = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuotationId)
                .ForeignKey("dbo.AutoQuotations", t => t.QuotationId)
                .Index(t => t.QuotationId);
            
            CreateTable(
                "dbo.VehicleUses",
                c => new
                    {
                        QuotationId = c.Int(nullable: false),
                        UseType = c.Int(nullable: false),
                        OvernightZipCode = c.String(),
                        MovementZipCode = c.String(),
                        CommercialUseType = c.Int(nullable: false),
                        CommercialUsePeriod = c.Int(nullable: false),
                        GarageLocal = c.Int(nullable: false),
                        DailyDistance = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuotationId)
                .ForeignKey("dbo.AutoQuotations", t => t.QuotationId)
                .Index(t => t.QuotationId);
            
            CreateTable(
                "dbo.MainDriverDependents",
                c => new
                    {
                        MainDriverDependentId = c.Int(nullable: false, identity: true),
                        MainDriverId = c.Int(nullable: false),
                        Age = c.Int(nullable: false),
                        Gender = c.Int(nullable: false),
                        LivesWithMainDriver = c.Boolean(nullable: false),
                    })
                .PrimaryKey(t => t.MainDriverDependentId)
                .ForeignKey("dbo.MainDrivers", t => t.MainDriverId)
                .Index(t => t.MainDriverId);
            
            CreateTable(
                "dbo.Transactions",
                c => new
                    {
                        InsurerQuotationId = c.Int(nullable: false),
                        InvoiceId = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Created = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.InsurerQuotationId)
                .ForeignKey("dbo.Invoices", t => t.InvoiceId)
                .ForeignKey("dbo.InsurerQuotations", t => t.InsurerQuotationId)
                .Index(t => t.InsurerQuotationId)
                .Index(t => t.InvoiceId);
            
            CreateTable(
                "dbo.Invoices",
                c => new
                    {
                        InvoiceId = c.Int(nullable: false, identity: true),
                        SubscriptionId = c.Int(nullable: false),
                        Closing = c.DateTimeOffset(nullable: false, precision: 7),
                        Created = c.DateTimeOffset(nullable: false, precision: 7),
                        LastUpdate = c.DateTimeOffset(nullable: false, precision: 7),
                        Status = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.InvoiceId)
                .ForeignKey("dbo.Subscriptions", t => t.SubscriptionId)
                .Index(t => t.SubscriptionId);
            
            CreateTable(
                "dbo.Subscriptions",
                c => new
                    {
                        SubscriptionId = c.Int(nullable: false, identity: true),
                        AccountId = c.Int(nullable: false),
                        PlanId = c.Int(nullable: false),
                        RequestType = c.Int(nullable: false),
                        Type = c.Int(nullable: false),
                        Status = c.Int(nullable: false),
                        Expiration = c.DateTimeOffset(precision: 7),
                        Created = c.DateTimeOffset(nullable: false, precision: 7),
                        LastUpdate = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.SubscriptionId)
                .ForeignKey("dbo.Accounts", t => t.AccountId)
                .ForeignKey("dbo.Plans", t => t.PlanId)
                .Index(t => t.AccountId)
                .Index(t => t.PlanId);
            
            CreateTable(
                "dbo.Plans",
                c => new
                    {
                        PlanId = c.Int(nullable: false, identity: true),
                        Name = c.String(nullable: false, maxLength: 100),
                        RecurrenceType = c.Int(nullable: false),
                        Recurrence = c.Int(nullable: false),
                        Amount = c.Int(nullable: false),
                        Value = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AdditionalValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        Trial = c.Int(),
                        IsActive = c.Boolean(nullable: false),
                        LastUpdate = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.PlanId);
            
            CreateTable(
                "dbo.InsurerAutoQuotationParameters",
                c => new
                    {
                        InsurerAutoQuotationId = c.Int(nullable: false),
                        Parameters = c.String(),
                    })
                .PrimaryKey(t => t.InsurerAutoQuotationId)
                .ForeignKey("dbo.InsurerAutoQuotations", t => t.InsurerAutoQuotationId)
                .Index(t => t.InsurerAutoQuotationId);
            
            CreateTable(
                "dbo.InsurerValidators",
                c => new
                    {
                        InsurerValidatorId = c.Int(nullable: false, identity: true),
                        InsurerId = c.Int(nullable: false),
                        RequestType = c.Int(nullable: false),
                        Description = c.String(nullable: false),
                        FullAssemblyName = c.String(nullable: false),
                    })
                .PrimaryKey(t => t.InsurerValidatorId)
                .ForeignKey("dbo.Insurers", t => t.InsurerId, cascadeDelete: true)
                .Index(t => t.InsurerId);
            
            CreateTable(
                "dbo.Manufacturers",
                c => new
                    {
                        ManufacturerId = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        InsurerId = c.Int(),
                        CodeManufacturerInsurer = c.String(),
                    })
                .PrimaryKey(t => t.ManufacturerId)
                .ForeignKey("dbo.Insurers", t => t.InsurerId)
                .Index(t => t.InsurerId);
            
            CreateTable(
                "dbo.MatchManufacturers",
                c => new
                    {
                        MatchManufacturerId = c.Int(nullable: false, identity: true),
                        ManufacturerAlvacId = c.Int(nullable: false),
                        ManufacturerInsurerId = c.Int(nullable: false),
                        Insurer_InsurerId = c.Int(),
                    })
                .PrimaryKey(t => t.MatchManufacturerId)
                .ForeignKey("dbo.Insurers", t => t.Insurer_InsurerId)
                .ForeignKey("dbo.Manufacturers", t => t.ManufacturerAlvacId)
                .ForeignKey("dbo.Manufacturers", t => t.ManufacturerInsurerId)
                .Index(t => t.ManufacturerAlvacId)
                .Index(t => t.ManufacturerInsurerId)
                .Index(t => t.Insurer_InsurerId);
            
            CreateTable(
                "dbo.Vehicles",
                c => new
                    {
                        VehicleId = c.Int(nullable: false, identity: true),
                        FipeCode = c.String(),
                        ManufacturerId = c.Int(nullable: false),
                        InsurerId = c.Int(),
                        Model = c.String(),
                        InitialYear = c.Int(nullable: false),
                        FinalYear = c.Int(nullable: false),
                        Fuel = c.Int(nullable: false),
                        CodeVehicleInsurer = c.String(),
                        DateReference = c.String(),
                        GearBox = c.Int(),
                        CC = c.Decimal(precision: 18, scale: 2),
                        Doors = c.Int(),
                        Valves = c.Int(),
                        Traction = c.String(),
                        Passengers = c.Int(),
                        Axes = c.Int(),
                    })
                .PrimaryKey(t => t.VehicleId)
                .ForeignKey("dbo.Insurers", t => t.InsurerId)
                .ForeignKey("dbo.Manufacturers", t => t.ManufacturerId, cascadeDelete: true)
                .Index(t => t.ManufacturerId)
                .Index(t => t.InsurerId);
            
            CreateTable(
                "dbo.VehiclePrices",
                c => new
                    {
                        VehiclePriceId = c.Int(nullable: false, identity: true),
                        VehicleId = c.Int(nullable: false),
                        Year = c.Int(nullable: false),
                        Price = c.Decimal(nullable: false, precision: 18, scale: 2),
                    })
                .PrimaryKey(t => t.VehiclePriceId)
                .ForeignKey("dbo.Vehicles", t => t.VehicleId, cascadeDelete: true)
                .Index(t => t.VehicleId);
            
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128),
                        Name = c.String(nullable: false, maxLength: 256),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.Audits",
                c => new
                    {
                        AuditId = c.Int(nullable: false, identity: true),
                        UserName = c.String(nullable: false),
                        IPAddress = c.String(nullable: false),
                        AreaAccessed = c.String(nullable: false),
                        Data = c.String(),
                        HttpStatusCode = c.Int(nullable: false),
                        Timestamp = c.DateTimeOffset(nullable: false, precision: 7),
                    })
                .PrimaryKey(t => t.AuditId);
            
            CreateTable(
                "dbo.QuotationIgnitionBlockers",
                c => new
                    {
                        QuotationId = c.Int(nullable: false),
                        IgnitionBlockerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuotationId, t.IgnitionBlockerId })
                .ForeignKey("dbo.VehicleInfos", t => t.QuotationId, cascadeDelete: true)
                .ForeignKey("dbo.IgnitionBlockers", t => t.IgnitionBlockerId, cascadeDelete: true)
                .Index(t => t.QuotationId)
                .Index(t => t.IgnitionBlockerId);
            
            CreateTable(
                "dbo.QuotationDeviceTrackers",
                c => new
                    {
                        QuotationId = c.Int(nullable: false),
                        DeviceTrackerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.QuotationId, t.DeviceTrackerId })
                .ForeignKey("dbo.VehicleInfos", t => t.QuotationId, cascadeDelete: true)
                .ForeignKey("dbo.DeviceTrackers", t => t.DeviceTrackerId, cascadeDelete: true)
                .Index(t => t.QuotationId)
                .Index(t => t.DeviceTrackerId);
            
            CreateTable(
                "dbo.AutoQuotations",
                c => new
                    {
                        QuotationId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.QuotationId)
                .ForeignKey("dbo.Quotations", t => t.QuotationId)
                .Index(t => t.QuotationId);
            
            CreateTable(
                "dbo.InsurerAutoQuotations",
                c => new
                    {
                        InsurerQuotationId = c.Int(nullable: false),
                        PremiumValueCategory = c.Int(nullable: false),
                        DeductibleType = c.Int(nullable: false),
                        DeductibleValue = c.Decimal(nullable: false, precision: 18, scale: 2),
                        AssistanceType = c.Int(nullable: false),
                        CoveragePlanType = c.Int(nullable: false),
                        GlassesCoverages = c.Int(nullable: false),
                        MaterialDamages = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MoralDamages = c.Decimal(nullable: false, precision: 18, scale: 2),
                        BodyDamages = c.Decimal(nullable: false, precision: 18, scale: 2),
                        PersonalAccidentPassenger = c.Decimal(nullable: false, precision: 18, scale: 2),
                        MedicalExpenses = c.Decimal(nullable: false, precision: 18, scale: 2),
                        ExtraExpenses = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HasWinch = c.Boolean(nullable: false),
                        WinchKm = c.Decimal(nullable: false, precision: 18, scale: 2),
                        HasCourtesyCar = c.Boolean(nullable: false),
                        CourtesyCarDays = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.InsurerQuotationId)
                .ForeignKey("dbo.InsurerQuotations", t => t.InsurerQuotationId)
                .Index(t => t.InsurerQuotationId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.InsurerAutoQuotations", "InsurerQuotationId", "dbo.InsurerQuotations");
            DropForeignKey("dbo.AutoQuotations", "QuotationId", "dbo.Quotations");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropForeignKey("dbo.AccountConnections", "ConnectionId", "dbo.Connections");
            DropForeignKey("dbo.AccountConnections", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AccountInsurerConfigurations", "InsurerConfigurationId", "dbo.InsurerConfigurations");
            DropForeignKey("dbo.Manufacturers", "InsurerId", "dbo.Insurers");
            DropForeignKey("dbo.VehiclePrices", "VehicleId", "dbo.Vehicles");
            DropForeignKey("dbo.Vehicles", "ManufacturerId", "dbo.Manufacturers");
            DropForeignKey("dbo.Vehicles", "InsurerId", "dbo.Insurers");
            DropForeignKey("dbo.MatchManufacturers", "ManufacturerInsurerId", "dbo.Manufacturers");
            DropForeignKey("dbo.MatchManufacturers", "ManufacturerAlvacId", "dbo.Manufacturers");
            DropForeignKey("dbo.MatchManufacturers", "Insurer_InsurerId", "dbo.Insurers");
            DropForeignKey("dbo.InsurerValidators", "InsurerId", "dbo.Insurers");
            DropForeignKey("dbo.InsurerQuotations", "InsurerId", "dbo.Insurers");
            DropForeignKey("dbo.InsurerConfigurations", "InsurerId", "dbo.Insurers");
            DropForeignKey("dbo.Dispatchers", "InsurerId", "dbo.Insurers");
            DropForeignKey("dbo.InsurerAutoQuotationParameters", "InsurerAutoQuotationId", "dbo.InsurerAutoQuotations");
            DropForeignKey("dbo.Transactions", "InsurerQuotationId", "dbo.InsurerQuotations");
            DropForeignKey("dbo.Transactions", "InvoiceId", "dbo.Invoices");
            DropForeignKey("dbo.Invoices", "SubscriptionId", "dbo.Subscriptions");
            DropForeignKey("dbo.Subscriptions", "PlanId", "dbo.Plans");
            DropForeignKey("dbo.Subscriptions", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.InsurerQuotations", "QuotationId", "dbo.Quotations");
            DropForeignKey("dbo.Quotations", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.Insureds", "QuotationId", "dbo.Quotations");
            DropForeignKey("dbo.MainDrivers", "WorkActivityId", "dbo.WorkActivities");
            DropForeignKey("dbo.MainDriverDependents", "MainDriverId", "dbo.MainDrivers");
            DropForeignKey("dbo.VehicleUses", "QuotationId", "dbo.AutoQuotations");
            DropForeignKey("dbo.VehicleOwners", "QuotationId", "dbo.AutoQuotations");
            DropForeignKey("dbo.VehicleInfos", "QuotationId", "dbo.AutoQuotations");
            DropForeignKey("dbo.QuotationDeviceTrackers", "DeviceTrackerId", "dbo.DeviceTrackers");
            DropForeignKey("dbo.QuotationDeviceTrackers", "QuotationId", "dbo.VehicleInfos");
            DropForeignKey("dbo.QuotationIgnitionBlockers", "IgnitionBlockerId", "dbo.IgnitionBlockers");
            DropForeignKey("dbo.QuotationIgnitionBlockers", "QuotationId", "dbo.VehicleInfos");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUsers", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.MainDrivers", "QuotationId", "dbo.AutoQuotations");
            DropForeignKey("dbo.Insurances", "QuotationId", "dbo.AutoQuotations");
            DropForeignKey("dbo.Insurances", "CurrentInsuranceCompany_InsurerId", "dbo.Insurers");
            DropForeignKey("dbo.AdditionalDrivers", "QuotationId", "dbo.AutoQuotations");
            DropForeignKey("dbo.Insureds", "WorkActivityId", "dbo.WorkActivities");
            DropForeignKey("dbo.Quotations", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.Payments", "InsurerQuotationId", "dbo.InsurerQuotations");
            DropForeignKey("dbo.Notifications", "InsurerQuotationId", "dbo.InsurerQuotations");
            DropForeignKey("dbo.InsurerQuotations", "ConnectionId", "dbo.Connections");
            DropForeignKey("dbo.Connections", "DispatcherId", "dbo.Dispatchers");
            DropForeignKey("dbo.ConnectionParameters", "ConnectionId", "dbo.Connections");
            DropForeignKey("dbo.AccountConnections", "InsurerId", "dbo.Insurers");
            DropForeignKey("dbo.AccountInsurerConfigurations", "AccountInsurerTemplateId", "dbo.AccountInsurerTemplates");
            DropForeignKey("dbo.AccountInsurerTemplates", "AccountId", "dbo.Accounts");
            DropForeignKey("dbo.AccountConfigurations", "AccountId", "dbo.Accounts");
            DropIndex("dbo.InsurerAutoQuotations", new[] { "InsurerQuotationId" });
            DropIndex("dbo.AutoQuotations", new[] { "QuotationId" });
            DropIndex("dbo.QuotationDeviceTrackers", new[] { "DeviceTrackerId" });
            DropIndex("dbo.QuotationDeviceTrackers", new[] { "QuotationId" });
            DropIndex("dbo.QuotationIgnitionBlockers", new[] { "IgnitionBlockerId" });
            DropIndex("dbo.QuotationIgnitionBlockers", new[] { "QuotationId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropIndex("dbo.VehiclePrices", new[] { "VehicleId" });
            DropIndex("dbo.Vehicles", new[] { "InsurerId" });
            DropIndex("dbo.Vehicles", new[] { "ManufacturerId" });
            DropIndex("dbo.MatchManufacturers", new[] { "Insurer_InsurerId" });
            DropIndex("dbo.MatchManufacturers", new[] { "ManufacturerInsurerId" });
            DropIndex("dbo.MatchManufacturers", new[] { "ManufacturerAlvacId" });
            DropIndex("dbo.Manufacturers", new[] { "InsurerId" });
            DropIndex("dbo.InsurerValidators", new[] { "InsurerId" });
            DropIndex("dbo.InsurerAutoQuotationParameters", new[] { "InsurerAutoQuotationId" });
            DropIndex("dbo.Subscriptions", new[] { "PlanId" });
            DropIndex("dbo.Subscriptions", new[] { "AccountId" });
            DropIndex("dbo.Invoices", new[] { "SubscriptionId" });
            DropIndex("dbo.Transactions", new[] { "InvoiceId" });
            DropIndex("dbo.Transactions", new[] { "InsurerQuotationId" });
            DropIndex("dbo.MainDriverDependents", new[] { "MainDriverId" });
            DropIndex("dbo.VehicleUses", new[] { "QuotationId" });
            DropIndex("dbo.VehicleOwners", new[] { "QuotationId" });
            DropIndex("dbo.VehicleInfos", new[] { "QuotationId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUsers", new[] { "AccountId" });
            DropIndex("dbo.Insurances", new[] { "CurrentInsuranceCompany_InsurerId" });
            DropIndex("dbo.Insurances", new[] { "QuotationId" });
            DropIndex("dbo.AdditionalDrivers", new[] { "QuotationId" });
            DropIndex("dbo.MainDrivers", new[] { "WorkActivityId" });
            DropIndex("dbo.MainDrivers", new[] { "QuotationId" });
            DropIndex("dbo.Insureds", new[] { "WorkActivityId" });
            DropIndex("dbo.Insureds", new[] { "QuotationId" });
            DropIndex("dbo.Quotations", new[] { "UserId" });
            DropIndex("dbo.Quotations", new[] { "AccountId" });
            DropIndex("dbo.Payments", new[] { "InsurerQuotationId" });
            DropIndex("dbo.Notifications", new[] { "InsurerQuotationId" });
            DropIndex("dbo.InsurerQuotations", new[] { "InsurerId" });
            DropIndex("dbo.InsurerQuotations", new[] { "ConnectionId" });
            DropIndex("dbo.InsurerQuotations", new[] { "QuotationId" });
            DropIndex("dbo.ConnectionParameters", new[] { "ConnectionId" });
            DropIndex("dbo.Connections", new[] { "DispatcherId" });
            DropIndex("dbo.Dispatchers", new[] { "InsurerId" });
            DropIndex("dbo.InsurerConfigurations", new[] { "InsurerId" });
            DropIndex("dbo.AccountInsurerConfigurations", new[] { "AccountInsurerTemplateId" });
            DropIndex("dbo.AccountInsurerConfigurations", new[] { "InsurerConfigurationId" });
            DropIndex("dbo.AccountInsurerTemplates", new[] { "AccountId" });
            DropIndex("dbo.AccountConfigurations", new[] { "AccountId" });
            DropIndex("dbo.AccountConnections", new[] { "ConnectionId" });
            DropIndex("dbo.AccountConnections", new[] { "InsurerId" });
            DropIndex("dbo.AccountConnections", new[] { "AccountId" });
            DropTable("dbo.InsurerAutoQuotations");
            DropTable("dbo.AutoQuotations");
            DropTable("dbo.QuotationDeviceTrackers");
            DropTable("dbo.QuotationIgnitionBlockers");
            DropTable("dbo.Audits");
            DropTable("dbo.AspNetRoles");
            DropTable("dbo.VehiclePrices");
            DropTable("dbo.Vehicles");
            DropTable("dbo.MatchManufacturers");
            DropTable("dbo.Manufacturers");
            DropTable("dbo.InsurerValidators");
            DropTable("dbo.InsurerAutoQuotationParameters");
            DropTable("dbo.Plans");
            DropTable("dbo.Subscriptions");
            DropTable("dbo.Invoices");
            DropTable("dbo.Transactions");
            DropTable("dbo.MainDriverDependents");
            DropTable("dbo.VehicleUses");
            DropTable("dbo.VehicleOwners");
            DropTable("dbo.DeviceTrackers");
            DropTable("dbo.IgnitionBlockers");
            DropTable("dbo.VehicleInfos");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.Insurances");
            DropTable("dbo.AdditionalDrivers");
            DropTable("dbo.MainDrivers");
            DropTable("dbo.WorkActivities");
            DropTable("dbo.Insureds");
            DropTable("dbo.Quotations");
            DropTable("dbo.Payments");
            DropTable("dbo.Notifications");
            DropTable("dbo.InsurerQuotations");
            DropTable("dbo.ConnectionParameters");
            DropTable("dbo.Connections");
            DropTable("dbo.Dispatchers");
            DropTable("dbo.Insurers");
            DropTable("dbo.InsurerConfigurations");
            DropTable("dbo.AccountInsurerConfigurations");
            DropTable("dbo.AccountInsurerTemplates");
            DropTable("dbo.AccountConfigurations");
            DropTable("dbo.Accounts");
            DropTable("dbo.AccountConnections");
        }
    }
}
