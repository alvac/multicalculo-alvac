namespace Alvac.Infrastructure.EntityFramework.Migrations
{
    using Alvac.Domain.Entities;
    using Alvac.Domain.Enum;
    using Microsoft.AspNet.Identity;
    using Microsoft.AspNet.Identity.EntityFramework;
    using System;
    using System.Data.Entity;
    using System.Data.Entity.Migrations;
    using System.Linq;

    internal sealed class Configuration : DbMigrationsConfiguration<Alvac.Infrastructure.EntityFramework.AlvacContext>
    {
        public Configuration()
        {
            AutomaticMigrationsEnabled = false;
        }

        protected override void Seed(AlvacContext context)
        {
            #region IgnitionBlockers
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 1,
                Name = "Albany Link (Saty Albany)"
            });

            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 2,
                Name = "Bip Auto (Bipwherer)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 3,
                Name = "Block Fone Cell (Bethany)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 4,
                Name = "Block Sat (Sat Company)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 5,
                Name = "Carsystem (Car System)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 5,
                Name = "Cell Sat (Sat Company)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 6,
                Name = "Combat Avl (Combat Vip)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 7,
                Name = "Conect Car (Conectel)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 8,
                Name = "Digiblock (Digitalsat)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 9,
                Name = "Guard One (Guard One)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 10,
                Name = "Ms-30 (Mobi System)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 11,
                Name = "New Block (G3)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 12,
                Name = "Orbylock (Orbsystem)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 13,
                Name = "Pkrsat-Blocklink (Pkr)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 14,
                Name = "Positron Rd Link Via Sat (Pst)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 15,
                Name = "Sensoblock (Sensocar)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 16,
                Name = "Stopcar (Teletrim)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 17,
                Name = "Syslock (Pollus Sat)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 18,
                Name = "Tb Block (Logus Sat)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 19,
                Name = "Tecblock (Triadis)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 20,
                Name = "Teleblock (Pollus Sat)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 21,
                Name = "Viasat (Viasat)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 22,
                Name = "Graber (Graber)"
            });
            context.IgnitionBlockers.AddOrUpdate(x => x.IgnitionBlockerId, new IgnitionBlocker
            {
                IgnitionBlockerId = 23,
                Name = "N�o listado"
            });
            #endregion

            #region DeviceTrackers
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 1,
                Name = "3 S Smart (3 S)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 2,
                Name = "3 Tmv10 Gsm/Gprs (Tecno 3 T)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 3,
                Name = "Angels Personal Premium (Angels Brasil)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 4,
                Name = "Autocargo Gprs (Plantao)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 5,
                Name = "Autofinder Gps (Autofinder/Opentech)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 6,
                Name = "Autosat (Autosat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 7,
                Name = "Autosearch (Earthsearch)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 8,
                Name = "Autotrac (Autotrac)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 9,
                Name = "Baby Navgator Gsm/Gprs (Ariasat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 10,
                Name = "Brasiltrack Avl/Gps (Br Track)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 11,
                Name = "Btc 4000 (Brasil Track)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 12,
                Name = "Car System Gsm (Carsystem)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 13,
                Name = "Chipsat Mpa 35 I (Stv)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 14,
                Name = "Cielocel Gps (Cielo)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 15,
                Name = "Control Loc (Control Loc)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 16,
                Name = "Controlcell 4000 (Controlsat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 17,
                Name = "Coralsat (Coralsat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 18,
                Name = "Crown Wt 5000 (Crown Telecom)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 19,
                Name = "E Track Rs2000 (Telematix)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 20,
                Name = "E-Finder Gsm (Bergmann)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 21,
                Name = "Fleet Control (Seecomm)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 22,
                Name = "Fms-C (Wise Track)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 23,
                Name = "Forsat D+ (Fortress)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 24,
                Name = "Gardens Nav Track Digital Iii (Gardens)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 25,
                Name = "Gecell Gps (Gennari Peartree)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 26,
                Name = "Gp Sat (Sat Company)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 27,
                Name = "Gsm/Gprs (Graber)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 28,
                Name = "Gt Gprs Ka (Globaltech)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 29,
                Name = "Guard One (Guard One)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 30,
                Name = "Hunter Track (Hunter Sat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 31,
                Name = "Itd-800 (Sitrack)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 32,
                Name = "Ituran Localizador (Em Comodato)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 33,
                Name = "Ituran Localizador (Ituran)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 34,
                Name = "Ituran S-Gprs/Gps (Ituran)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 35,
                Name = "Jabursat/Onixsat (Jabursat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 36,
                Name = "Lidersat Match 300(Lider)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 37,
                Name = "Lo Jack (Em Comodato)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 38,
                Name = "Lo Jack Tracker Gps (Tracker)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 39,
                Name = "Lo Jack Tracker Local (Tracker)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 40,
                Name = "Lock Sat (Sat Company)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 41,
                Name = "Lock System Gps/Gprs/Gsm (Lock System)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 42,
                Name = "Log Sat (Logus Sat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 43,
                Name = "Maxtrack Mtc-400 (Graber)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 44,
                Name = "Maxtrack Mtc-400 Gsm/Gprs (Graber)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 45,
                Name = "Mtc 400 (Plantao)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 46,
                Name = "Mtc 400 - Sattelite Iv (Sattelite)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 47,
                Name = "Navtrack Iii (Alerta)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 48,
                Name = "Omnilink (Omnink)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 49,
                Name = "Omnisat (Autotrac)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 50,
                Name = "Orbicom (Braslaser)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 51,
                Name = "Gprs/Gps (Positron)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 52,
                Name = "Prisma 400 (Prisma Sat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 53,
                Name = "Protesat (Protesim)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 54,
                Name = "Ps Daf-V"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 55,
                Name = "Sascar Gsm (Sascar)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 56,
                Name = "Satcomm (Satcar)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 57,
                Name = "Satplus Gsm/Gprs (Satcar)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 58,
                Name = "Scorpion (Cerruns)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 59,
                Name = "Simtrack Gps/Gsm (Sim)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 60,
                Name = "Sis 101 (Rodosis)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 61,
                Name = "Sitrack Itd-700 (Sitrack)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 62,
                Name = "Sitrack Itd-800 (Sitrack)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 63,
                Name = "Skytrack Net (Skytrack)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 64,
                Name = "Sti Sat (Sti Sat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 65,
                Name = "Sysblock Gsm (Aj Sat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 66,
                Name = "Telecom Track Ii (Telecom Track)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 67,
                Name = "Teletrim Gsm (Teletrim Wireless)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 68,
                Name = "Tracsat Gps(Tracsat)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 69,
                Name = "Vigauto (Vigauto)"
            });
            context.DeviceTrackers.AddOrUpdate(x => x.DeviceTrackerId, new DeviceTracker
            {
                DeviceTrackerId = 70,
                Name = "N�o listado"
            });
            #endregion

            context.SaveChanges();

            #region Account

            var account = new Account
            {
                IsActive = true,
                Name = "Corretora Ant�nio Lucas",
                Susep = "07659289659"
            };

            context.Accounts.AddOrUpdate(x => x.Name, account);
            context.SaveChanges();

            account = context.Accounts.FirstOrDefault(x => x.Name == account.Name);
            #endregion

            #region Template

            var accountInsurerTemplate = new AccountInsurerTemplate
            {
                AccountId = account.AccountId,
                Name = "Template 1",
                Description = "Template com as configura��es de comis�o m�nima.",
                IsActive = true,
                Created = DateTime.UtcNow,
                RequestType = RequestType.AutoQuotation
            };

            context.AccountInsurerTemplates.AddOrUpdate(x => x.Name, accountInsurerTemplate);
            context.SaveChanges();

            #endregion

            #region Plan

            var plan = new Plan
            {
                Name = "Basic",
                Recurrence = 1,
                RecurrenceType = PlanRecurrenceType.Month,
                Amount = 350,
                Value = 0.34M,
                AdditionalValue = 0.60M,
                Trial = 15,
                LastUpdate = DateTime.UtcNow,
                IsActive = true
            };

            context.Plans.AddOrUpdate(x => x.Name, plan);
            context.SaveChanges();

            plan = context.Plans.FirstOrDefault(x => x.Name == plan.Name);
            #endregion

            #region Subscription

            var subscription = new Subscription
            {
                AccountId = account.AccountId,
                PlanId = plan.PlanId,
                RequestType = RequestType.AutoQuotation,
                Type = SubscriptionType.Normal,
                Status = SubscriptionStatus.Active,
                Created = DateTime.UtcNow,
                LastUpdate = DateTime.UtcNow
            };

            context.Subscriptions.AddOrUpdate(x => x.AccountId, subscription);
            context.SaveChanges();

            subscription = context.Subscriptions.FirstOrDefault(x => x.AccountId == account.AccountId);

            #endregion

            #region Invoice

            var invoice = new Invoice
            {
                SubscriptionId = subscription.SubscriptionId,
                Closing = DateTime.UtcNow.AddDays(plan.Recurrence),
                Created = DateTime.UtcNow,
                LastUpdate = DateTime.UtcNow,
                Status = InvoiceStatus.Open
            };

            context.Invoices.AddOrUpdate(x => new { x.SubscriptionId }, invoice);
            context.SaveChanges();

            #endregion

            #region User

            var manager = new UserManager<User>(new UserStore<User>(context));
            manager.Create(new User
            {
                AccountId = account.AccountId,
                Email = "lucas.cunha@alvac.com.br",
                UserName = "lucas.cunha@alvac.com.br",
                EmailConfirmed = true,
                IsActive = true
            }, "Alvac123");

            manager.Create(new User
            {
                AccountId = account.AccountId,
                Email = "lucas.cunha@alvac.com.br",
                UserName = "lucas.cunha@alvac.com.br",
                EmailConfirmed = true,
                IsActive = true
            }, "alvac123");

            var role = new RoleManager<IdentityRole>(new RoleStore<IdentityRole>(context));

            if (!role.RoleExists("Master"))
            {
                context.Roles.Add(new IdentityRole
                {
                    Id = Alvac.Domain.Enum.Role.Master,
                    Name = Role.MasterString
                });

                context.Roles.Add(new IdentityRole
                {
                    Id = Alvac.Domain.Enum.Role.Accounting,
                    Name = Role.AccountingString
                });

                context.Roles.Add(new IdentityRole
                {
                    Id = Alvac.Domain.Enum.Role.Internet,
                    Name = Role.InternetString
                });

                context.Roles.Add(new IdentityRole
                {
                    Id = Alvac.Domain.Enum.Role.Alvac,
                    Name = Role.AlvacString
                });

                context.SaveChanges();

                var user = manager.FindByEmail("lucas.cunha@alvac.com.br");
                manager.AddToRole(user.Id, "Master");
                manager.AddToRole(user.Id, "Alvac");

                var user2 = manager.FindByEmail("lucas.cunha@alvac.com.br");
                manager.AddToRole(user2.Id, "Accounting");
                context.SaveChanges();
            }

            #endregion

            #region WorkActivity

            var workActivity = new WorkActivity
            {
                Name = "Outras"
            };

            context.WorkActivities.AddOrUpdate(x => x.Name, workActivity);
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Administrador(a)" });
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Advogado(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Agr�nomo" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Analista de sistemas" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Arquiteto(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Banc�rio(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Bibliotec�rio(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Comiss�rio(a) de bordo" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Contador(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Corretor(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Dentista" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Diretor(a) de empresas" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Economista" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Enfermeiro(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Engenheiro(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Farmac�utico(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Fisioterapeuta" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Jornalista" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Mec�nico(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "M�dico(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Militar" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Motorista" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Nutricionista" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Padre" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Pastor(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Piloto de aeronaves" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Professor(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Professor(a) de educa��o f�sica" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Psicanalista" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Psic�logo(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Publicit�rio(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Representante comercial" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Sacerdote" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Securit�rio(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Servidor p�blico" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Soci�logo(a)" }); ;
            context.WorkActivities.AddOrUpdate(x => x.Name, new WorkActivity { Name = "Veterin�rio(a)" });
            context.SaveChanges();

            #endregion

        }
    }
}
