﻿using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Repositories;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Infrastructure.EntityFramework;
using Alvac.Infrastructure.Util.Http;
using Alvac.Infrastructure.Util.Logger;
using SimpleInjector;
using SimpleInjector.Extensions.ExecutionContextScoping;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SimpleInjector.Extensions;
using Alvac.Infrastructure.EntityFramework.Repositories;
using Alvac.Domain.Entities.Queries;
using Alvac.Infrastructure.EntityFramework.Queries;
using Alvac.Domain.Entities.Commands;
using Alvac.Infrastructure.EntityFramework.Command;
using Alvac.Domain.Services.Factories;
using Alvac.Infrastructure.Util.Queue;
using Alvac.Domain.Services.Notification;
using Alvac.Domain.Entities.Decorators;

namespace Alvac.Infrastructure.SimpleInjector
{
    public class NotificationProducerBootstrap
    {
        public static IContainerProvider Register()
        {
            var container = new Container();

            #region Core

            container.RegisterSingle<IServiceProvider>(container);
            container.RegisterSingle<IContainerProvider>(() => new ContainerProvider(container));
            container.RegisterSingle<IMapper, MapperService>();
            container.RegisterSingle<ILogger, AlvacLogger>();
            container.Register<IHttpClient>(() => { return new AlvacHttpClient(); }, new ExecutionContextScopeLifestyle(true));
            #endregion

            #region Data Base
            container.Register<DbContext, AlvacContext>(new ExecutionContextScopeLifestyle(true));
            

            container.Register<IUnitOfWork>(() => container.GetInstance<DbContext>() as IUnitOfWork);
            container.RegisterOpenGeneric(typeof(IRepository<>), typeof(EntityRepository<>));

            container.Register<IQueryProcessor, QueryProcessor>();
            container.RegisterManyForOpenGeneric(typeof(IQueryHandler<,>), typeof(AlvacContext).Assembly);

            container.Register<ICommandProcessor, CommandProcessor>();
            container.RegisterManyForOpenGeneric(typeof(ICommandHandler<,>), typeof(AlvacContext).Assembly);

            #endregion

            #region Service

            container.Register<INotificationProvider, NotificationProvider>();

            #endregion

            #region Billing



            #endregion

            #region Queue

            container.Register<IMessageQueueFactory, MessageQueueFactory>();

            #endregion

            #region Register Decorators

            container.RegisterDecorator(typeof(IQueryHandler<,>), typeof(ValidationQueryHandlerDecorator<,>));

            #endregion

            AutoMapperConfiguration.Configure();

            return new ContainerProvider(container);
        }
    }
}
