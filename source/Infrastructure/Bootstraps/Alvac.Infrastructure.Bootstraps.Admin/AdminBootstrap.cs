﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Infrastructure.SimpleInjector;
using Alvac.Infrastructure.Util.Http;
using Alvac.Infrastructure.Util.Logger;
using SimpleInjector;
using SimpleInjector.Integration.Web.Mvc;
using System.Configuration;
using System.Security.Claims;
using System.Threading;
using System.Web.Mvc;

namespace Alvac.Infrastructure.Bootstraps.Api
{
    public class AdminBootstrap
    {
        public static IContainerProvider Register()
        {
            var container = new Container();

            container.RegisterSingle<IContainerProvider>(() => new ContainerProvider(container));
            container.RegisterSingle<ILogger, AlvacLogger>();

            container.RegisterPerWebRequest<IHttpClient>(() => 
                {
                    var identity = ((ClaimsIdentity)Thread.CurrentPrincipal.Identity);
                    var token = identity != null && identity.IsAuthenticated ? identity.FindFirst("Token").Value : string.Empty;
                    return new AlvacHttpClient(ConfigurationManager.AppSettings.Get("apiManagerUrl"), token);
                });

            // Create a new SimpleInjectorDependencyResolver that wraps the, 
            // container, and register that resolver in MVC.          

            // This is an extension method from the integration package.
            container.RegisterMvcControllers();
            // This is an extension method from the integration package as well.
            container.RegisterMvcIntegratedFilterProvider();

            DependencyResolver.SetResolver(new SimpleInjectorDependencyResolver(container));

            AutoMapperConfiguration.Configure();

            return new ContainerProvider(container);
        }
    }
}
