﻿using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Decorators;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Repositories;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Infrastructure.EntityFramework;
using Alvac.Infrastructure.EntityFramework.Command;
using Alvac.Infrastructure.EntityFramework.Queries;
using Alvac.Infrastructure.EntityFramework.Repositories;
using Alvac.Infrastructure.Util.Logger;
using Alvac.Infrastructure.Util.Queue;
using SimpleInjector;
using SimpleInjector.Extensions;
using SimpleInjector.Extensions.ExecutionContextScoping;
using System;
using System.Data.Entity;

namespace Alvac.Infrastructure.SimpleInjector
{
    public class DispatcherProducerBootstrap
    {
        public static IContainerProvider Register()
        {
            var container = new Container();

            #region Core

            container.RegisterSingle<IServiceProvider>(container);
            container.RegisterSingle<IContainerProvider>(() => new ContainerProvider(container));
            container.RegisterSingle<IMapper, MapperService>();
            container.RegisterSingle<ILogger, AlvacLogger>();

            #endregion

            #region Data Base
            container.Register<DbContext, AlvacContext>(new ExecutionContextScopeLifestyle());

            container.Register<IUnitOfWork>(() => container.GetInstance<DbContext>() as IUnitOfWork);
            container.RegisterOpenGeneric(typeof(IRepository<>), typeof(EntityRepository<>));

            container.Register<IQueryProcessor, QueryProcessor>();
            container.RegisterManyForOpenGeneric(typeof(IQueryHandler<,>), typeof(AlvacContext).Assembly);

            container.Register<ICommandProcessor, CommandProcessor>();
            container.RegisterManyForOpenGeneric(typeof(ICommandHandler<,>), typeof(AlvacContext).Assembly);

            #endregion

            #region Service


            #endregion

            #region Dispatcher

            container.Register<IDispatcherFactory, DispatcherFactory>();

            #endregion

            #region Queue

            container.Register<IMessageQueueFactory, MessageQueueFactory>();

            #endregion

            #region Register Decorators

            container.RegisterDecorator(typeof(IQueryHandler<,>), typeof(ValidationQueryHandlerDecorator<,>));

            #endregion

            AutoMapperConfiguration.Configure();

            return new ContainerProvider(container);
        }
    }
}
