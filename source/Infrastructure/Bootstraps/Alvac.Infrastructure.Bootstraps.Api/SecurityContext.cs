﻿using Alvac.Domain.Entities;
using Alvac.Domain.Entities.Repositories;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading;
using System.Threading.Tasks;
using System.Web;

namespace Alvac.UI.Api.Models
{
    public class SecurityContext : ISecurityContext
    {
        private User _user;
        private IRepository<User> _userRepository;

        public SecurityContext(IRepository<User> userRepository)
        {
            _userRepository = userRepository;
        }

        public async Task<User> GetUserAsync()
        {
            if (_user == null)
            {
                var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;

                var userId = identity.Claims.FirstOrDefault(x => x.Type == ClaimTypes.NameIdentifier).Value;

                _user = await _userRepository.GetByIdAsync(userId);

                if (_user == null)
                {
                    throw new Exception(string.Format("Usuario não encontado. Id: {0}", userId));
                }
            }

            return _user;
        }


        public bool IsInRole(string role)
        {
            var identity = Thread.CurrentPrincipal.Identity as ClaimsIdentity;

            var roles = string.Join(",", identity.Claims.Where(x => x.Type == ClaimTypes.Role).Select(x => x.Value));

            return roles.IndexOf(role) != -1;
        }
    }
}