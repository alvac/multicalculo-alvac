﻿using Alvac.Application.Services;
using Alvac.Application.Services.Interfaces;
using Alvac.Application.Services.Quotation;
using Alvac.Application.Services.Quotation.Factories;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Decorators;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Repositories;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Infrastructure.EntityFramework;
using Alvac.Infrastructure.EntityFramework.Command;
using Alvac.Infrastructure.EntityFramework.Queries;
using Alvac.Infrastructure.EntityFramework.Repositories;
using Alvac.Infrastructure.SimpleInjector;
using Alvac.Infrastructure.Util.Email;
using Alvac.Infrastructure.Util.Logger;
using Alvac.Infrastructure.Util.Queue;
using Alvac.UI.Api.Models;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using SimpleInjector;
using SimpleInjector.Extensions;
using SimpleInjector.Integration.WebApi;
using System;
using System.Data.Entity;
using System.Web.Http;

namespace Alvac.Infrastructure.Bootstraps.Api
{
    public class ApiBootstrap
    {
        public static IContainerProvider Register(HttpConfiguration config)
        {
            var container = new Container();
            container.RegisterWebApiControllers(config);

            container.RegisterSingle<IMapper, MapperService>();
            container.RegisterSingle<IServiceProvider>(container);
            container.RegisterSingle<IContainerProvider>(() => new ContainerProvider(container));
            container.RegisterSingle<ILogger, AlvacLogger>();
            container.RegisterSingle<IQueryProcessor, QueryProcessor>();
            container.RegisterSingle<ICommandProcessor, CommandProcessor>();
            container.RegisterSingle<ITimeProvider, TimeProvider>();

            container.RegisterWebApiRequest<IEmailSender, MandrillEmailSender>();
            container.RegisterWebApiRequest<ISecurityContext, SecurityContext>();
            container.RegisterWebApiRequest<UserManager<Domain.Entities.User>>(() =>
                new UserManager<Domain.Entities.User>(new UserStore<Domain.Entities.User>(container.GetInstance<DbContext>()))
                {
                    EmailService = new ManagerEmailSender(container.GetInstance<IEmailSender>()),
                    UserTokenProvider = new ManagerUserTokenProvider()
                });

            container.RegisterManyForOpenGeneric(typeof(IQueryHandler<,>), typeof(AlvacContext).Assembly);
            container.RegisterManyForOpenGeneric(typeof(ICommandHandler<,>), typeof(AlvacContext).Assembly);

            container.RegisterWebApiRequest<DbContext, AlvacContext>();
            container.RegisterWebApiRequest<IUnitOfWork>(() => container.GetInstance<DbContext>() as IUnitOfWork);
            container.RegisterOpenGeneric(typeof(IRepository<>), typeof(EntityRepository<>), new WebApiRequestLifestyle());

            container.RegisterWebApiRequest<IMessageQueueFactory, MessageQueueFactory>();
            container.RegisterWebApiRequest<IQuotationServiceFactory, QuotationServiceFactory>();

            container.RegisterWebApiRequest<ISubscriptionAppService, SubscriptionAppService>();
            container.RegisterWebApiRequest<IAuditAppService, AuditAppService>();
            container.RegisterWebApiRequest<IDeviceTrackerAppService, DeviceTrackerAppService>();
            container.RegisterWebApiRequest<IIgnitionBlockerAppService, IgnitionBlockerAppService>();
            container.RegisterWebApiRequest<IAccountConfigurationAppService, AccountConfigurationAppService>();
            container.RegisterWebApiRequest<IUserAppService, UserAppService>();
            container.RegisterWebApiRequest<IReportAppService, ReportAppService>();
            container.RegisterWebApiRequest<IAccountAppService, AccountAppService>();
            container.RegisterWebApiRequest<IAccountInsurerTemplateAppService, AccountInsurerTemplateAppService>();
            container.RegisterWebApiRequest<IAccountInsurerConfigurationAppService, AccountInsurerConfigurationAppService>();
            container.RegisterWebApiRequest<IWorkActivityAppService, WorkActivityAppService>();
            container.RegisterWebApiRequest<IInsurerAppService, InsurerAppService>();
            container.RegisterWebApiRequest<IQuotationAppService, QuotationAppService>();
            container.RegisterWebApiRequest<IVehicleAppService, VehicleAppService>();
            container.RegisterWebApiRequest<IAccountConnectionAppService, AccountConnectionAppService>();
            container.RegisterWebApiRequest<IInsurerConfigurationAppService, InsurerConfigurationAppService>();
            container.RegisterWebApiRequest<IPlanAppService, PlanAppService>();
            container.RegisterWebApiRequest<IConnectionAppService, ConnectionAppService>();
            container.RegisterWebApiRequest<IDispatcherAppService, DispatcherAppService>();
            container.RegisterWebApiRequest<IConnectionParameterAppService, ConnectionParameterAppService>();
            container.RegisterWebApiRequest<IInsurerValidatorAppService, InsurerValidatorAppService>(); 
            container.RegisterWebApiRequest<IInvoiceAppService, InvoiceAppService>();
            

            //Register Decorators
            container.RegisterDecorator(typeof(IQueryHandler<,>), typeof(ValidationQueryHandlerDecorator<,>));

            config.DependencyResolver = new SimpleInjectorWebApiDependencyResolver(container);

            AutoMapperConfiguration.Configure();

            return new ContainerProvider(container);
        }
    }
}
