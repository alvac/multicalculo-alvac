﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract;
using Alvac.Domain.Enum;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Enum.Auto;
using Alvac.Domain.Contract.Extensions;
using System.Collections.Generic;
using System.Linq;

namespace Alvac.Test.Unit.Domain.Contract
{
    [TestClass]
    public class QuotationRequestTest
    {
        [TestInitialize]
        public void Setup()
        {

        }


        [TestMethod]
        public void QuotationApiRequest_AutoQuotation_Serialize_Successfully()
        {
            var quotationRequest = GetAutoQuotationApiRequest(RequestType.AutoQuotation);

            var autoQuotationRequest = quotationRequest.Request as AutoQuotationRequest;

            var autoRequest = AlvacJson<BaseRequest>.Serialize(autoQuotationRequest);

            string jsonRequest = AlvacJson<QuotationApiRequest>.Serialize(quotationRequest);

            var contains = jsonRequest.Contains("Antônio Lucas Veloso Alves Cunha");

            Assert.IsTrue(contains);

        }

        [TestMethod]
        public void QuotationApiRequest_AutoQuotation_Deserialize_Successfully()
        {
            var quotationApiRequest = GetAutoQuotationApiRequest(RequestType.AutoQuotation);

            string jsonRequest = AlvacJson<QuotationApiRequest>.Serialize(quotationApiRequest);

            var deserializedAlvacRequest = AlvacJson<AutoQuotationApiRequest>.Deserialize(jsonRequest);

            Assert.IsTrue(deserializedAlvacRequest != null && deserializedAlvacRequest.Request != null && deserializedAlvacRequest.Request is AutoQuotationRequest);
        }

        #region Private Methods

        private AutoQuotationApiRequest GetAutoQuotationApiRequest(RequestType requestType)
        {
            var quotationRequest = new AutoQuotationApiRequest(true);

            quotationRequest.DeductibleTypeList.Add(DeductibleType.Optional25Percent);
            quotationRequest.DeductibleTypeList.Add(DeductibleType.Reduced25Percent);

            quotationRequest.InsurerRequestList.Add(new InsurerRequest
            {
                Insurer = 1,
                Parameters = new Dictionary<string, object>()
            });

            quotationRequest.InsurerRequestList.Add(new InsurerRequest
            {
                Insurer = 2,
                Parameters = new Dictionary<string, object>()
            });

            switch (requestType)
            {
                case RequestType.AutoQuotation:

                    quotationRequest.Request = GetAutoQuotationRequest();

                    break;

                default:

                    throw new NotImplementedException(string.Format("Quotation not implemented by '{0}' request type", requestType.ToString()));
            }


            return quotationRequest;
        }

        private AutoQuotationRequest GetAutoQuotationRequest()
        {
            var autoQuotationRequest = new AutoQuotationRequest(true);

            autoQuotationRequest.Notification = new Notification
            {
                CorrelationId = "12345",
                CallType = CallType.Post,
                Path = "http://localhost:5050"
            };

            autoQuotationRequest.Retry = new RetryRequest
            {
                Max = 3
            };

            #region VehicleInfo

            /// <summary>
            /// Marca
            /// </summary>
            autoQuotationRequest.VehicleInfo.Brand = "Volkswagen";

            /// <summary>
            /// Modelo do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.Model = "Gol";

            /// <summary>
            /// Ano de fabricação veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.ManufacturingYear = 2010;

            /// <summary>
            /// Ano do modelo do veiculo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.ModelYear = 2011;

            /// <summary>
            /// Código fipe da versão do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.FipeCode = "005317-1";

            /// <summary>
            /// Tipo de combutível do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.Fuel = FuelType.Flex;

            /// <summary>
            /// Descrição do modelo do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.ModelDescription = "Gol ECOMOTION  1.0 Mi Total Flex 8V 4p";

            /// <summary>
            /// Veículo zero KM
            /// </summary>
            autoQuotationRequest.VehicleInfo.IsNew = false;

            /// <summary>
            /// Dispositivos de alarme.
            /// </summary>
            autoQuotationRequest.VehicleInfo.DevicesAlarm = true;

            /// <summary>
            /// Dispositivos de bloquio de ingnição.
            /// </summary>
            autoQuotationRequest.VehicleInfo.DevicesIgnitionBlocker = new int[0];

            /// <summary>
            /// Dispositivos rastreador.
            /// </summary>
            autoQuotationRequest.VehicleInfo.DevicesTracker = new int[0];

            /// <summary>
            /// Veículo blindado.
            /// </summary>
            autoQuotationRequest.VehicleInfo.IsArmored = false;

            /// <summary>
            /// Valor covertura blindagem.
            /// </summary>
            autoQuotationRequest.VehicleInfo.ArmoryCoverage = null;

            /// <summary>
            /// Possui kit gás
            /// </summary>
            autoQuotationRequest.VehicleInfo.HasNaturalGasKit = false;

            /// <summary>
            /// Valor covertura kit gas.
            /// </summary>
            autoQuotationRequest.VehicleInfo.NaturalGasKitCoverage = null;

            /// <summary>
            /// Dados de licença do veículo
            /// </summary>
            autoQuotationRequest.VehicleInfo.License = new VehicleLicenseRequest { ChassisNumber = "", PlateNumber = "HLG3378" };

            /// <summary>
            /// Dados referente ao tipo / cobertura do valor do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.Value = new VehicleValueRequest
            {
                ValueCategory = ValueCategory.Referenced,
                ReferencedValue = 21536,
                ReferencedPercentage = 100
            };

            #endregion

            #region VehicleUse

            /// <summary>
            /// Tipo de utilização do veiculo.
            /// </summary>
            autoQuotationRequest.VehicleUse.UseType = UseType.GoToJob | UseType.GoToSchool;

            /// <summary>
            /// Cep de circulação
            /// </summary>
            autoQuotationRequest.VehicleUse.MovementZipCode = "31250-340";

            /// <summary>
            /// Local ondse fica o veiculo na residencia
            /// </summary>
            autoQuotationRequest.VehicleUse.GarageLocal = Garage.Job | Garage.Residence;

            #endregion

            #region Insured

            /// <summary>
            /// Nome completo
            /// </summary>
            autoQuotationRequest.Insured.FullName = "Antônio Lucas Veloso Alves Cunha";

            /// <summary>
            /// Sexo
            /// </summary>
            autoQuotationRequest.Insured.Gender = Gender.Male;

            /// <summary>
            /// Data de nascimento.
            /// </summary>
            autoQuotationRequest.Insured.Birthdate = new DateTime(1986, 9, 6);

            /// <summary>
            /// Tipo de pessoa
            /// None = 0,
            /// Natural = 1,
            /// Legal = 2
            /// </summary>
            autoQuotationRequest.Insured.PersonType = PersonType.Natural;

            /// <summary>
            /// Cpf / Cnpj
            /// </summary>
            autoQuotationRequest.Insured.Document = "067.594.516-00";

            /// <summary>
            /// Estado civíl.
            /// </summary>
            autoQuotationRequest.Insured.MaritalStatus = MaritalStatus.Married;

            /// <summary>
            /// Informações endereço
            /// </summary>
            autoQuotationRequest.Insured.ZipCode = "31250-340";

            /// Profissão do segurado.
            autoQuotationRequest.Insured.WorkActivityId = 1;

            #endregion

            #region MainDriver

            /// <summary>
            /// Cliente que esta realizando a contação é o segurado.
            /// </summary>
            autoQuotationRequest.MainDriver.IsInsured = true;

            /// <summary>
            /// Cliente que esta realizando a contação é o dono do veículo.
            /// </summary>
            autoQuotationRequest.MainDriver.IsVehicleOwner = true;

            autoQuotationRequest.MainDriver.ResidentialType = ResidentialType.Apartment;

            /// <summary>
            /// Nome completo
            /// </summary>
            autoQuotationRequest.MainDriver.FullName = "Antônio Lucas Veloso Alves Cunha";

            /// <summary>
            /// Telefone celular do cliente que esta realizando a contação.
            /// </summary>
            autoQuotationRequest.MainDriver.MobilePhoneNumber = "+5531991178550";

            /// <summary>
            /// Telefone fixo do cliente que esta realizando a contação.
            /// </summary>
            autoQuotationRequest.MainDriver.PhoneNumber = "+553133885000";

            /// <summary>
            /// Email do cliente que esta realizando a contação.
            /// </summary>
            autoQuotationRequest.MainDriver.Email = "lucas.cunha@alvac.com.br";

            /// <summary>
            /// Sexo
            /// </summary>
            autoQuotationRequest.MainDriver.Gender = Gender.Male;

            /// <summary>
            /// Data de nascimento.
            /// </summary>
            autoQuotationRequest.MainDriver.BirthDate = new DateTime(1986, 9, 6);

            /// <summary>
            /// Cpf.
            /// </summary>
            autoQuotationRequest.MainDriver.Document = "067.594.516-00";

            /// <summary>
            /// Estado civíl.
            /// </summary>
            autoQuotationRequest.MainDriver.MaritalStatus = MaritalStatus.Married;

            /// <summary>
            /// Informações endereço
            /// </summary>
            autoQuotationRequest.MainDriver.ZipCode = "31250-340";

            /// <summary>
            /// Lista de dependentes (se possui).
            /// </summary>
            autoQuotationRequest.MainDriver.DependentList.Add(new DependentRequest
            {
                Age = 6,
                Gender = Gender.Male
            });

            /// <summary>
            /// Data de habilitação.
            /// </summary>
            autoQuotationRequest.MainDriver.DriverLicenseYears = 3;

            /// <summary>
            /// Número da carteira de habilitação
            /// </summary>
            autoQuotationRequest.MainDriver.DriverLicenseNumber = "03008499657";

            /// <summary>
            /// Descrição de atividade de trabalho.
            /// </summary>
            autoQuotationRequest.MainDriver.WorkActivityId = 1;

            /// <summary>
            /// Escolaridade
            /// </summary>
            autoQuotationRequest.MainDriver.Schooling = Schooling.PostGraduate;

            /// <summary>
            /// Relação com o segurado.
            /// </summary>
            autoQuotationRequest.MainDriver.RelationshipWithInsured = Relationship.OtherIndividual;

            /// <summary>
            /// Vítima de roubo de veículos nos últimos 24 meses.
            /// </summary>
            autoQuotationRequest.MainDriver.HasBeenStolenInTheLast24Months = false;

            #endregion

            #region  VehicleOwner

            /// <summary>
            /// Nome completo
            /// </summary>
            autoQuotationRequest.VehicleOwner.FullName = "Antônio Lucas Veloso Alves Cunha";

            /// <summary>
            /// Sexo
            /// </summary>
            autoQuotationRequest.VehicleOwner.Gender = Gender.Male;

            /// <summary>
            /// Data de nascimento.
            /// </summary>
            autoQuotationRequest.VehicleOwner.Birthdate = new DateTime(1986, 9, 6);

            /// <summary>
            /// Tipo de pessoa
            /// None = 0,
            /// Natural = 1,
            /// Legal = 2
            /// </summary>
            autoQuotationRequest.VehicleOwner.PersonType = PersonType.Natural;

            /// <summary>
            /// Cpf / Cnpj
            /// </summary>
            autoQuotationRequest.VehicleOwner.Document = "067.594.516-00";

            /// <summary>
            /// Estado civíl.
            /// </summary>
            autoQuotationRequest.VehicleOwner.MaritalStatus = MaritalStatus.Single;

            /// <summary>
            /// Informações endereço
            /// </summary>
            autoQuotationRequest.VehicleOwner.ZipCode = "31250-340";

            /// <summary>
            /// Número de veículos que possui.
            /// </summary>
            autoQuotationRequest.VehicleOwner.VehiclesNumbers = 1;

            #endregion

            #region Insurance

            /// <summary>
            /// Seguradora atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentInsuranceCompany = null;

            /// <summary>
            /// Tipo cobertura da apólice atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentCoveragePlanType = CoveragePlanType.None;

            /// <summary>
            /// Classe de Bônus  da apólice atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentInsuranceBonus = 0;

            /// <summary>
            /// Sinistros ocorreram na apólice
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.ClaimQuantity = 0;

            /// <summary>
            /// Data do início da apólice atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentInsurancePolicyDate = null;

            /// <summary>
            /// Data do vencimento da apólice atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentInsurancePolicyExpirationDate = null;

            /// <summary>
            /// Corretor detem a apólice.
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.IsPolicyOwner = false;

            /// <summary>
            /// Corretor detem a apólice.
            /// </summary>
            autoQuotationRequest.Insurance.InsurancePolicyInitialDate = DateTime.UtcNow;

            #endregion

            #region  AdditionalDrivers

            var additionalDriver = new AdditionalDriverRequest();

            /// <summary>
            /// Nome completo
            /// </summary>
            additionalDriver.FullName = "Antônio Lucas Veloso Alves Cunha";

            /// <summary>
            /// Sexo
            /// </summary>
            additionalDriver.Gender = Gender.Male;

            /// <summary>
            /// Data de nascimento.
            /// </summary>
            additionalDriver.BirthDate = new DateTime(1986, 9, 6);

            /// <summary>
            /// Estado civíl.
            /// </summary>
            additionalDriver.MaritalStatus = MaritalStatus.Married;

            /// <summary>
            /// Data de habilitação.
            /// </summary>
            additionalDriver.DriverLicenseYears = 4;

            /// <summary>
            /// Relação com o segurado.
            /// </summary>
            additionalDriver.RelationshipWithInsured = Relationship.Married;

            /// <summary>
            /// Possui veículo
            /// </summary>
            additionalDriver.HasRegisteredVehicle = false;

            /// <summary>
            /// Mora com o condutor principal.
            /// </summary>
            additionalDriver.LivesWithMainDriver = true;

            /// <summary>
            /// Número de dias que utiliza o veículo na semana.
            /// </summary>
            additionalDriver.NumberOfUseDaysPerWeek = 2;

            autoQuotationRequest.AdditionalDriverList.Add(additionalDriver);

            #endregion

            return autoQuotationRequest;
        }

        #endregion

    }
}
