﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.InsurerConfigurations;
using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using Alvac.Domain.Services.Factories;
using Alvac.Infrastructure.SimpleInjector;
using Alvac.Infrastructure.Util.Logger;
using Alvac.Service.Dispatchers.Mitsui.Auto.Ws;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Test.Console.Dispatcher.Mitsui.Auto.Ws
{
    class Program
    {
        static void Main(string[] args)
        {

            var teste = @"{
    'vehicleInfo': {
      'brand': 'AUDI',
      'model': 'A1 SPORTBACK 1.4 TFSI 122CV 5P S-TRONIC',
      'manufacturingYear': 2014,
      'modelYear': 2015,
      'fipeCode': '008173-6',
      'fuel': 'Gasoline',
      'isNew': true,
      'devicesAlarm': false,
      'devicesIgnitionBlocker': [],
      'devicesTracker': [],
      'isArmored': true,
      'hasNaturalGasKit': true,
      'license': {
        'plateNumber': '',
        'chassisNumber': '123456'
      },
      'value': {
        'valueCategory': 'Determined',
        'determinedValue': 83789.55,
        'referencedValue': 83713.0,
        'isFinanced': false
      }
    },
    'vehicleUse': {
      'useType': 'Leisure',
      'overnightZipCode': '31270190',
      'movementZipCode': '31270190',
      'commercialUseType': 'None',
      'commercialUsePeriod': 'None',
      'garageLocal': 'None',
      'dailyDistance': 31
    },
    'mainDriver': {
      'isInsured': true,
      'isVehicleOwner': true,
      'fullName': 'Breno Fernandes de Queiroz',
      'mobilePhoneNumber': '3194430522',
      'phoneNumber': '3134973612',
      'email': 'brenoqueiroz@gmail.com',
      'gender': 'Female',
      'birthDate': '1988-04-17T03:00:00Z',
      'document': '07657118695',
      'maritalStatus': 'Single',
      'zipCode': '31270190',
      'dependentList': [],
      'driverLicenseYears': 4,
      'driverLicenseNumber': '123456798',
      'workActivityId': 5,
      'schooling': 'PostGraduate',
      'relationshipWithInsured': 'Self',
      'hasBeenStolenInTheLast24Months': false,
      'residentialType': 'Home'
    },
    'insured': {
      'fullName': 'Breno Fernandes de Queiroz',
      'gender': 'Female',
      'birthdate': '1988-04-17T03:00:00Z',
      'personType': 'Natural',
      'document': '07657118695',
      'maritalStatus': 'Single',
      'zipCode': '31270190',
      'workActivityId': 5
    },
    'vehicleOwner': {
      'fullName': 'Breno Fernandes de Queiroz',
      'gender': 'Female',
      'birthdate': '1988-04-17T03:00:00Z',
      'personType': 'Natural',
      'document': '07657118695',
      'maritalStatus': 'Single',
      'zipCode': '31270190',
      'vehiclesNumbers': 0
    },
    'insurance': {
      'coveragePlanType': 'Comprehensive',
      'deductibleType': 'Required',
      'renewal': {
        'currentInsuranceCompany': 'None',
        'currentCoveragePlanType': 'None',
        'currentInsuranceBonus': 0,
        'claimQuantity': 0,
        'isPolicyOwner': false
      },
      'insurancePolicyInitialDate': '2015-06-06T13:59:11.855Z',
      'insurancePolicyFinalDate': '2016-06-06T13:59:11.855Z'
    },
    'additionalDriverList': [],
    'ip': '104.41.9.254/NX',
    'accountId': 1,
    'userId': 'e7197301-a5d7-496b-825c-cb532f2a391e',
    'identifier': 1456,
    'requestDateTime': '2015-06-06T13:59:49.9527238+00:00',
    'notification': {
      'correlationId': 'brenoqueiroz@gmail.com',
      'callType': 'Post',
      'path': 'http://app-Alvac.azurewebsites.net/api/notification'
    },
    'requestType': 'AutoQuotation'
  }";


            var request = AlvacJson<AutoQuotationRequest>.Deserialize(teste);

            var provider = DispatcherProducerBootstrap.Register();
            var factory = provider.GetService<IDispatcherFactory>();

            using (provider.BeginScope())
            {
                var parameters = new Dictionary<string, object>();
                parameters.Add("url", @"https://homologacao2.msig.com.br/wsmitsui/msskit.asmx?op=Calcular");
                parameters.Add("user", "0109674");
                parameters.Add("password", "mitsui");

                var queryProcessor = provider.GetService<IQueryProcessor>();

                var insurerConfigurations = queryProcessor.ProcessAsync(new FindInsurerConfigurationByInsurer
                                                {
                                                    Insurer = 3
                                                }).Result;

                var dispatcher = factory.Create("Alvac.Service.Dispatchers.Mitsui.Auto.Ws.MitsuiAutoWsDispatcher, Alvac.Service.Dispatchers.Mitsui.Auto.Ws", parameters, insurerConfigurations);

                var autoQuotationRequest = GetAutoQuotationRequest();
                var insurerParameters = GetInsurerParameters();

                var baseResponse = dispatcher.ProcessAsync(request, insurerParameters).Result;
                System.Console.ReadLine();
            }
        }

        private static AutoQuotationRequest GetAutoQuotationRequest()
        {
            var autoQuotationRequest = new AutoQuotationRequest(true);

            autoQuotationRequest.Notification = new Notification
            {
                CorrelationId = "12345",
                CallType = CallType.Post,
                Path = "http://localhost:5050"
            };

            autoQuotationRequest.Retry = new RetryRequest
            {
                Max = 3
            };

            #region VehicleInfo

            /// <summary>
            /// Marca
            /// </summary>
            autoQuotationRequest.VehicleInfo.Brand = "Fiat";

            /// <summary>
            /// Modelo do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.Model = "Grand Siena Essence 1.6 Flex 16V";

            /// <summary>
            /// Ano de fabricação veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.ManufacturingYear = 2014;

            /// <summary>
            /// Ano do modelo do veiculo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.ModelYear = 2014;

            /// <summary>
            /// Código fipe da versão do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.FipeCode = "0013803";

            /// <summary>
            /// Tipo de combutível do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.Fuel = FuelType.Flex;

            /// <summary>
            /// Descrição do modelo do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.ModelDescription = "Grand Siena Essence 1.6 Flex 16V";

            /// <summary>
            /// Veículo zero KM
            /// </summary>
            autoQuotationRequest.VehicleInfo.IsNew = false;

            /// <summary>
            /// Dispositivos de alarme.
            /// </summary>
            autoQuotationRequest.VehicleInfo.DevicesAlarm = true;
            autoQuotationRequest.VehicleInfo.License.ChassisNumber = "123456";

            /// <summary>
            /// Dispositivos de bloquio de ingnição.
            /// </summary>
            autoQuotationRequest.VehicleInfo.DevicesIgnitionBlocker = new int[0];

            /// <summary>
            /// Dispositivos rastreador.
            /// </summary>
            autoQuotationRequest.VehicleInfo.DevicesTracker = new int[0];

            /// <summary>
            /// Veículo blindado.
            /// </summary>
            autoQuotationRequest.VehicleInfo.IsArmored = false;

            /// <summary>
            /// Valor covertura blindagem.
            /// </summary>
            autoQuotationRequest.VehicleInfo.ArmoryCoverage = null;

            /// <summary>
            /// Possui kit gás
            /// </summary>
            autoQuotationRequest.VehicleInfo.HasNaturalGasKit = false;

            /// <summary>
            /// Valor covertura kit gas.
            /// </summary>
            autoQuotationRequest.VehicleInfo.NaturalGasKitCoverage = null;

            /// <summary>
            /// Dados de licença do veículo
            /// </summary>
            autoQuotationRequest.VehicleInfo.License = new VehicleLicenseRequest { ChassisNumber = "", PlateNumber = "HLG3378" };

            /// <summary>
            /// Dados referente ao tipo / cobertura do valor do veículo.
            /// </summary>
            autoQuotationRequest.VehicleInfo.Value = new VehicleValueRequest
            {
                ValueCategory = ValueCategory.Referenced,
                ReferencedValue = 40975,
                ReferencedPercentage = 100
            };

            #endregion

            #region VehicleUse

            /// <summary>
            /// Tipo de utilização do veiculo.
            /// </summary>
            autoQuotationRequest.VehicleUse.UseType = UseType.GoToJob | UseType.GoToSchool;

            /// <summary>
            /// Cep de circulação
            /// </summary>
            autoQuotationRequest.VehicleUse.OvernightZipCode = "31250340";

            /// <summary>
            /// Cep de circulação
            /// </summary>
            autoQuotationRequest.VehicleUse.MovementZipCode = "31250340";

            /// <summary>
            /// Local ondse fica o veiculo na residencia
            /// </summary>
            autoQuotationRequest.VehicleUse.GarageLocal = Garage.Job | Garage.Residence;

            #endregion

            #region Insured

            /// <summary>
            /// Nome completo
            /// </summary>
            autoQuotationRequest.Insured.FullName = "Bruno Fernando Oliveira Brandes";

            /// <summary>
            /// Sexo
            /// </summary>
            autoQuotationRequest.Insured.Gender = Gender.Male;

            /// <summary>
            /// Data de nascimento.
            /// </summary>
            autoQuotationRequest.Insured.Birthdate = new DateTime(1986, 9, 6);

            /// <summary>
            /// Tipo de pessoa
            /// None = 0,
            /// Natural = 1,
            /// Legal = 2
            /// </summary>
            autoQuotationRequest.Insured.PersonType = PersonType.Natural;

            /// <summary>
            /// Cpf / Cnpj
            /// </summary>
            autoQuotationRequest.Insured.Document = "06759451600";

            /// <summary>
            /// Estado civíl.
            /// </summary>
            autoQuotationRequest.Insured.MaritalStatus = MaritalStatus.Married;

            /// <summary>
            /// Informações endereço
            /// </summary>
            autoQuotationRequest.Insured.ZipCode = "31250340";

            /// Profissão do segurado.
            autoQuotationRequest.Insured.WorkActivityId = 1;

            #endregion

            #region MainDriver

            /// <summary>
            /// Cliente que esta realizando a contação é o segurado.
            /// </summary>
            autoQuotationRequest.MainDriver.IsInsured = true;

            /// <summary>
            /// Cliente que esta realizando a contação é o dono do veículo.
            /// </summary>
            autoQuotationRequest.MainDriver.IsVehicleOwner = true;

            autoQuotationRequest.MainDriver.ResidentialType = ResidentialType.Apartment;

            /// <summary>
            /// Nome completo
            /// </summary>
            autoQuotationRequest.MainDriver.FullName = "Bruno Fernando Oliveira Brandes";

            /// <summary>
            /// Telefone celular do cliente que esta realizando a contação.
            /// </summary>
            autoQuotationRequest.MainDriver.MobilePhoneNumber = "+553185168771";

            /// <summary>
            /// Telefone fixo do cliente que esta realizando a contação.
            /// </summary>
            autoQuotationRequest.MainDriver.PhoneNumber = "+553132456976";

            /// <summary>
            /// Email do cliente que esta realizando a contação.
            /// </summary>
            autoQuotationRequest.MainDriver.Email = "brunobrandes.as@gmail.com";

            /// <summary>
            /// Sexo
            /// </summary>
            autoQuotationRequest.MainDriver.Gender = Gender.Male;

            /// <summary>
            /// Data de nascimento.
            /// </summary>
            autoQuotationRequest.MainDriver.BirthDate = new DateTime(1986, 9, 6);

            /// <summary>
            /// Cpf.
            /// </summary>
            autoQuotationRequest.MainDriver.Document = "06759451600";

            /// <summary>
            /// Estado civíl.
            /// </summary>
            autoQuotationRequest.MainDriver.MaritalStatus = MaritalStatus.Married;

            /// <summary>
            /// Informações endereço
            /// </summary>
            autoQuotationRequest.MainDriver.ZipCode = "31250340";

            /// <summary>
            /// Lista de dependentes (se possui).
            /// </summary>
            autoQuotationRequest.MainDriver.DependentList.Add(new DependentRequest
            {
                Age = 6,
                Gender = Gender.Male
            });

            /// <summary>
            /// Data de habilitação.
            /// </summary>
            autoQuotationRequest.MainDriver.DriverLicenseYears = 3;

            /// <summary>
            /// Número da carteira de habilitação
            /// </summary>
            autoQuotationRequest.MainDriver.DriverLicenseNumber = "03008499657";

            /// <summary>
            /// Descrição de atividade de trabalho.
            /// </summary>
            autoQuotationRequest.MainDriver.WorkActivityId = 1;

            /// <summary>
            /// Escolaridade
            /// </summary>
            autoQuotationRequest.MainDriver.Schooling = Schooling.PostGraduate;

            /// <summary>
            /// Relação com o segurado.
            /// </summary>
            autoQuotationRequest.MainDriver.RelationshipWithInsured = Relationship.OtherIndividual;

            /// <summary>
            /// Vítima de roubo de veículos nos últimos 24 meses.
            /// </summary>
            autoQuotationRequest.MainDriver.HasBeenStolenInTheLast24Months = false;

            #endregion

            #region  VehicleOwner

            /// <summary>
            /// Nome completo
            /// </summary>
            autoQuotationRequest.VehicleOwner.FullName = "Bruno Fernando Oliveira Brandes";

            /// <summary>
            /// Sexo
            /// </summary>
            autoQuotationRequest.VehicleOwner.Gender = Gender.Male;

            /// <summary>
            /// Data de nascimento.
            /// </summary>
            autoQuotationRequest.VehicleOwner.Birthdate = new DateTime(1986, 9, 6);

            /// <summary>
            /// Tipo de pessoa
            /// None = 0,
            /// Natural = 1,
            /// Legal = 2
            /// </summary>
            autoQuotationRequest.VehicleOwner.PersonType = PersonType.Natural;

            /// <summary>
            /// Cpf / Cnpj
            /// </summary>
            autoQuotationRequest.VehicleOwner.Document = "06759451600";

            /// <summary>
            /// Estado civíl.
            /// </summary>
            autoQuotationRequest.VehicleOwner.MaritalStatus = MaritalStatus.Single;

            /// <summary>
            /// Informações endereço
            /// </summary>
            autoQuotationRequest.VehicleOwner.ZipCode = "31250340";

            /// <summary>
            /// Número de veículos que possui.
            /// </summary>
            autoQuotationRequest.VehicleOwner.VehiclesNumbers = 1;

            #endregion

            #region Insurance

            /// <summary>
            /// Seguradora atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentInsuranceCompany = null;

            autoQuotationRequest.Insurance.DeductibleType = DeductibleType.Required;

            /// <summary>
            /// Tipo cobertura da apólice atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentCoveragePlanType = CoveragePlanType.None;

            /// <summary>
            /// Classe de Bônus  da apólice atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentInsuranceBonus = 0;

            /// <summary>
            /// Sinistros ocorreram na apólice
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.ClaimQuantity = 0;

            /// <summary>
            /// Data do início da apólice atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentInsurancePolicyDate = null;

            /// <summary>
            /// Data do vencimento da apólice atual
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.CurrentInsurancePolicyExpirationDate = null;

            /// <summary>
            /// Corretor detem a apólice.
            /// </summary>
            autoQuotationRequest.Insurance.Renewal.IsPolicyOwner = false;

            /// <summary>
            /// Corretor detem a apólice.
            /// </summary>
            autoQuotationRequest.Insurance.InsurancePolicyInitialDate = DateTime.Now;


            /// </summary>
            autoQuotationRequest.Insurance.InsurancePolicyFinalDate = DateTime.Now.AddYears(1);



            #endregion

            #region  AdditionalDrivers

            var additionalDriver = new AdditionalDriverRequest();

            /// <summary>
            /// Nome completo
            /// </summary>
            additionalDriver.FullName = "Bruno Fernando Oliveira Brandes";

            /// <summary>
            /// Sexo
            /// </summary>
            additionalDriver.Gender = Gender.Male;

            /// <summary>
            /// Data de nascimento.
            /// </summary>
            additionalDriver.BirthDate = new DateTime(1986, 9, 6);

            /// <summary>
            /// Estado civíl.
            /// </summary>
            additionalDriver.MaritalStatus = MaritalStatus.Married;

            /// <summary>
            /// Data de habilitação.
            /// </summary>
            additionalDriver.DriverLicenseYears = 4;

            /// <summary>
            /// Relação com o segurado.
            /// </summary>
            additionalDriver.RelationshipWithInsured = Relationship.Married;

            /// <summary>
            /// Possui veículo
            /// </summary>
            additionalDriver.HasRegisteredVehicle = false;

            /// <summary>
            /// Mora com o condutor principal.
            /// </summary>
            additionalDriver.LivesWithMainDriver = true;

            /// <summary>
            /// Número de dias que utiliza o veículo na semana.
            /// </summary>
            additionalDriver.NumberOfUseDaysPerWeek = 2;

            autoQuotationRequest.AdditionalDriverList.Add(additionalDriver);

            #endregion

            return autoQuotationRequest;
        }

        private static IDictionary<string, object> GetInsurerParameters()
        {
            var result = new Dictionary<string, object>();
            result.Add("usuario", "0124927");
            result.Add("contratoParceiro", "0124927");
            result.Add("dM", "60.000,00");
            result.Add("dC", "60.000,00");
            result.Add("danosMorais", "10.000,00");
            result.Add("appM", "Não Contratado");
            result.Add("appI", "Não Contratado");
            result.Add("despesasExtras", "Não Contratado");
            result.Add("despesasComplementares", "Não Contratado");
            result.Add("planofuneral", "Não Contratado");
            result.Add("vidros", "Não Contratado");
            result.Add("assistencia24Horas", "Não Contratado");
            result.Add("padraoCarroReserva", "Não Contratado");
            result.Add("reboque", "Não Contratado");
            result.Add("porcentagemComissao", "10");
            result.Add("tipoIsencao", "Sem Isenção");
            result.Add("tipoAplicacaoAgravo", "Desconto");
            result.Add("porcentagemDescontoEspecial", "0");
            result.Add("chassiRemarcado", "Sim");
            return result;
        }
    }
}

