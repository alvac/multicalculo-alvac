﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Entities.Commands.AccountConfigurations;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/account-configuration")]
    public class AccountConfigurationController : ApiController
    {
        private readonly IAccountConfigurationAppService _accountConfigurationAppService;

        public AccountConfigurationController(IAccountConfigurationAppService accountConfigurationAppService)
        {
            _accountConfigurationAppService = accountConfigurationAppService;
        }

        [Route("")]
        /// <summary>
        /// Obtem a configuração
        /// </summary>
        public async Task<IHttpActionResult> GetConfiguration()
        {
            var response = await _accountConfigurationAppService.GetConfigurationAsync();
            return Ok(response);
        }

        [Route("email")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        /// <summary>
        /// Atualiza a configuração de email
        /// </summary>
        public async Task<IHttpActionResult> PutEmailConfiguration(UpdateEmailConfiguration updateEmailConfiguration)
        {
            await _accountConfigurationAppService.UpdateEmailConfigurationAsync(updateEmailConfiguration);
            return Ok();
        }

        [Route("email")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        /// <summary>
        /// Cria a configuração de email
        /// </summary>
        public async Task<IHttpActionResult> PostEmailConfiguration(CreateEmailConfiguration createEmailConfiguration)
        {
            await _accountConfigurationAppService.CreateEmailConfigurationAsync(createEmailConfiguration);
            return Ok();
        }

        [Route("report")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<IHttpActionResult> PutReportConfiguration(UpdateReportConfiguration updateReportConfiguration)
        {
            await _accountConfigurationAppService.UpdateReportConfigurationAsync(updateReportConfiguration);
            return Ok();
        }

        [Route("report")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<IHttpActionResult> PostReportConfiguration(CreateReportConfiguration createReportConfiguration)
        {
            await _accountConfigurationAppService.CreateReportConfigurationAsync(createReportConfiguration);
            return Ok();
        }
    }
}
