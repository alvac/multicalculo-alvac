﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.AccountInsurerConfigurations;
using Alvac.Domain.Entities.Commands.AccountInsurerTemplates;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Query;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/template")]
    public class TemplateController : ApiController
    {
        private readonly IAccountInsurerTemplateAppService _accountInsurerTemplateAppService;
        private readonly ISecurityContext _securityContext;
        private readonly IAccountInsurerConfigurationAppService _accountInsurerConfigurationAppService;

        public TemplateController(IAccountInsurerTemplateAppService accountInsurerTemplateAppService,
            IAccountInsurerConfigurationAppService accountInsurerConfigurationAppService,
            ISecurityContext securityContext)
        {
            _securityContext = securityContext;
            _accountInsurerConfigurationAppService = accountInsurerConfigurationAppService;
            _accountInsurerTemplateAppService = accountInsurerTemplateAppService;
        }

        [Route("")]
        [EnableQuery(EnsureStableOrdering = false)]
        public async Task<IQueryable<Alvac.Domain.DataTransferObject.AccountInsurerTemplate>> GetInsurerTemplatesByAccountId()
        {
            return await _accountInsurerTemplateAppService.GetAccountInsurerTemplate();
        }

        [Route("{accountInsurerTemplateId}/insurers/{insurer}/parameters")]
        public async Task<IEnumerable<AccountInsurerConfigurationParameters>> GetAccountInsurerConfigurationParameters(int insurer, int accountInsurerTemplateId)
        {
            var accountInsurerConfigurationParameters = await _accountInsurerConfigurationAppService.GetAccountInsurerConfigurationParameters(insurer, accountInsurerTemplateId);
            return accountInsurerConfigurationParameters;
        }

        [Route("parameters")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<IHttpActionResult> PostAccountInsurerConfigurationParameters(List<CreateAccountInsurerConfiguration> createAccountInsurerConfigurationCommand)
        {
            await _accountInsurerConfigurationAppService.CreateAccountInsurerConfigurationParameters(createAccountInsurerConfigurationCommand);
            return Ok();
        }

        [Route("{accountInsurerTemplateId}")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<IHttpActionResult> DeleteTemplate(int accountInsurerTemplateId)
        {
            await _accountInsurerTemplateAppService.DeleteTemplate(accountInsurerTemplateId);
            return Ok();
        }

        [Route("")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<IHttpActionResult> PostTemplate(CreateAccountInsurerTemplate command)
        {
            var entity = await _accountInsurerTemplateAppService.CreateTemplate(command);
            return Ok(new
            {
                id = entity.AccountInsurerTemplateId
            });
        }

        [Route("{accountInsurerTemplateId}/status")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<IHttpActionResult> PutUpdateStatusTemplate(UpdateStatusAccountInsurerTemplate command)
        {
            var entity = await _accountInsurerTemplateAppService.UpdateStatusAccountInsurerTemplate(command);
            return Ok();
        }
    }
}
