﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Linq.Expressions;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Extensions;
using System.Web.OData.Query;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/quotation/auto")]
    public class AutoQuotationController : ApiController
    {
        #region Private Fields

        private readonly IQuotationAppService _quotationAppService;
        private readonly ILogger _logger;
        private readonly ISecurityContext _securityContext;

        #endregion

        #region Constructor

        public AutoQuotationController(IQuotationAppService quotationAppService, ILogger logger, ISecurityContext securityContext)
        {
            _quotationAppService = quotationAppService;
            _logger = logger;
            _securityContext = securityContext;
        }

        #endregion

        #region Actions

        [Route("")]
        public async Task<IHttpActionResult> PostAutoQuotation(AutoQuotationApiRequest request)
        {
            QuotationApiResponse quotationApiResponse = null;

            try
            {
                string json = AlvacJson<AutoQuotationApiRequest>.Serialize(request as AutoQuotationApiRequest);

                await _logger.WriteVerboseAsync("AutoQuotationController", "PostAutoQuotationAsync", string.Format("Begin auto quotation api request - Request: {0}", json));

                quotationApiResponse = await _quotationAppService.ProcessAsync(request, HttpContext.Current != null ? HttpContext.Current.Request.UserHostAddress : string.Empty);

                await _logger.WriteVerboseAsync("AutoQuotationController", "PostAutoQuotationAsync", string.Format("Auto quotation api request enqueue sucessfully - Request: {0}", json));
            }
            catch (Exception ex)
            {
                return InternalServerError(ex);
            }

            return this.ApiResponse(quotationApiResponse);
        }

        [Route("")]
        [EnableQuery(EnsureStableOrdering = false)]
        public async Task<IQueryable<SimpleAutoQuotation>> GetAutoQuotations()
        {
            return await _quotationAppService.GetAutoQuotationsAsync();
        }

        #endregion
    }
}
