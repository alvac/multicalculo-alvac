﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;
using System.Web.Mvc;

namespace Alvac.UI.Api.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            return Content("It's working.");
        }

        public FileResult Report()
        {
            string binPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "bin");
            var assembly = Assembly.Load(System.IO.File.ReadAllBytes(binPath + "\\Alvac.UI.Api.dll"));

            using (var stream = assembly.GetManifestResourceStream("Alvac.UI.Api.Reports.Contracts.Default.rdlc"))
            {
                var viewer = new ReportViewer();
                viewer.LocalReport.EnableExternalImages = true;
                viewer.LocalReport.LoadReportDefinition(stream);

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                viewer.LocalReport.DataSources.Add(new ReportDataSource("Contract", new List<object>()
                {
                    new 
                    {
                        Nome = "Alvac Consultoria e Sistemas Ltda",
                        Endereco = "Rua Barão Homem de Melo",
                        Bairro = "Estoril",
                        Cidade = "Belo Horizonte",
                        UF = "MG",
                        CEP = "30494270",
                        CnpjCpf = "08.337.320/000",
                        InscricaoEstadual = "",
                        Email = "Alvac@Alvac.com.br",
                        Representante = "PEDRO HENRIQUE PINTO DA CUNHA MENEZES",
                        CPFRepresentante = "",
                        Telefone = "31",
                        NUsuarios = "60",
                        Plano = "Pós-pago: 100 cotações a R$80 até 700 a R$169 - 700 cotações",
                        Valor = "Calculável",
                        DataVenda = "18/01/2015",
                        SiteAlvac = "www.Alvac.com.br",
                        TelAlvac = "(31) 9443-0522",
                        FormaPagamento = "Boleto bancario"
                    }
                }));

                viewer.LocalReport.Refresh();

                byte[] bytes = viewer.LocalReport.Render(
                    "PDF", null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);

                return File(bytes, "application/pdf");
            }
        }
    }
}
