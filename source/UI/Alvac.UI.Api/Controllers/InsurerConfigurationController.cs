﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.InsurerConfigurations;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [RoutePrefix("api/insurer-configuration")]
    public class InsurerConfigurationController : ApiController
    {
        #region Private Fields

        private readonly IInsurerConfigurationAppService _insurerConfigurationAppService;

        #endregion

        #region Constructor

        public InsurerConfigurationController(IInsurerConfigurationAppService insurerConfigurationAppService)
        {
            _insurerConfigurationAppService = insurerConfigurationAppService;
        }

        #endregion

        #region Actions

        [Route("{id}")]
        public async Task<InsurerConfiguration> GetInsurerConfigurationById(int id)
        {
            var insurerConfigurations = await _insurerConfigurationAppService.GetInsurerConfigurationById(id);

            if (insurerConfigurations != null)
            {
                return insurerConfigurations;
            }
            else
            {
                throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
            }
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> DeleteInsurerConfigurationById(int id)
        {
            await _insurerConfigurationAppService.DeleteInsurerConfiguration(new DeleteInsurerConfiguration
                {
                    InsurerConfigurationId = id
                });

            return Ok();
        }

        [Route("")]
        public async Task<IHttpActionResult> PostInsurerConfigurationById(CreateInsurerConfiguration command)
        {
            await _insurerConfigurationAppService.CreateInsurerConfiguration(command);
            return Ok();
        }

        [Route("{insurerConfigurationId}")]
        public async Task<IHttpActionResult> PutInsurerConfigurationById(int insurerConfigurationId, UpdateInsurerConfiguration command)
        {
            await _insurerConfigurationAppService.UpdateInsurerConfiguration(command);
            return Ok();
        }

        [Route("insurer/{insurer}/request-type/{requestType}")]
        public async Task<IEnumerable<InsurerConfiguration>> GetInsurerConfigurationByInsurer(int insurer, RequestType requestType)
        {
            var insurerConfigurations = await _insurerConfigurationAppService.GetInsurerConfigurationByInsurer(insurer, requestType);

            if (insurerConfigurations != null && insurerConfigurations.Count() > 0)
            {
                return insurerConfigurations.OrderByDescending(x => x.DisplayName);
            }
            else
            {
                throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
            }
        }


        #endregion
    }
}