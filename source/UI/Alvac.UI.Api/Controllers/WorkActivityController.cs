﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/work-activity")]
    public class WorkActivityController : ApiController
    {
        private readonly IWorkActivityAppService _workActivityAppService;

        public WorkActivityController(IWorkActivityAppService workActivityAppService)
        {
            _workActivityAppService = workActivityAppService;
        }

        [Route("")]
        public async Task<IEnumerable<WorkActivity>> GetAll()
        {
            var works = await _workActivityAppService.GetWorkActivitiesAsync();
            return works;
        }
    }
}
