﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/quotation")]
    public class QuotationController : ApiController
    {
        private readonly IQuotationAppService _quotationAppService;

        public QuotationController(IQuotationAppService quotationAppService)
        {
            _quotationAppService = quotationAppService;
        }

        [Route("{id}/response")]
        public async Task<IEnumerable<InsurerQuotation>> GetQuotationResponse(int id)
        {
            var result = await _quotationAppService.GetQuotationAsync(id);
            return result;
        }
    }
}
