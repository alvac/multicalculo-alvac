﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/insurer")]
    public class InsurerController : ApiController
    {
        private readonly IInsurerAppService _insurerAppService;

        public InsurerController(IInsurerAppService insurerAppService)
        {
            _insurerAppService = insurerAppService;
        }

        [Route("")]
        public async Task<IEnumerable<Insurer>> GetInsurers()
        {
            var response = await _insurerAppService.GetInsurersAsync();
            return response;
        }
    }
}
