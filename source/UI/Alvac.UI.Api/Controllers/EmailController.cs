﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject.Commands.Emails;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/email")]
    public class EmailController : ApiController
    {
        private readonly IQuotationAppService _quotationAppService;

        public EmailController(IQuotationAppService quotationAppService)
        {
            _quotationAppService = quotationAppService;
        }

        public async Task<IHttpActionResult> PostEmail(SendQuotationByEmail command)
        {
            await _quotationAppService.SendQuotationResponseByEmailAsync(command);
            return Ok();
        }
    }
}
