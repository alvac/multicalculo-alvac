﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Plans;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
    [RoutePrefix("api/plan")]
    public class PlanController : ApiController
    {
        #region Private Fields

        private readonly IPlanAppService _planAppService;

        #endregion

        #region Constructor

        public PlanController(IPlanAppService planAppService)
        {
            _planAppService = planAppService;
        }

        #endregion

        #region Actions

        [Route("{id}")]
        public async Task<Plan> GetPlanById(int id)
        {
            try
            {
                var plan = await _planAppService.GetByIdAsync(id);

                if (plan != null)
                {
                    return plan;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        public async Task<IEnumerable<Plan>> GetPlans()
        {
            try
            {
                var plans = await _planAppService.GetAllAsync();

                if (plans != null)
                {
                    return plans;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        public async Task<IHttpActionResult> PostPlan(CreatePlan command)
        {
            await _planAppService.CreatePlanAsync(command);
            return Ok();
        }

        [Route("")]
        public async Task<IHttpActionResult> PutPlan(UpdatePlan command)
        {
            await _planAppService.UpdatePlanAsync(command);
            return Ok();
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> Deleteplan(int id)
        {
            await _planAppService.DeletePlanAsync(id);
            return Ok();
        }
        #endregion
    }
}