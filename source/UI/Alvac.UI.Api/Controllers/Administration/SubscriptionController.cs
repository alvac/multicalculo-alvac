﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Subscriptions;
using Alvac.UI.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers.Administration
{
    [RoutePrefix("api/subscription")]
    [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
    public class SubscriptionController : ApiController
    {
        private readonly ISubscriptionAppService _subscriptionAppService;

        public SubscriptionController(ISubscriptionAppService subscriptionAppService)
        {
            _subscriptionAppService = subscriptionAppService;
        }

        [Route("{subscriptionId}")]
        public async Task<Subscription> Get(int subscriptionId)
        {
            return await _subscriptionAppService.GetSubscription(subscriptionId);
        }

        [Route("account/{accountId}")]
        [CustomEnableQueryAttribute(EnsureStableOrdering = false)]
        public async Task<IQueryable<Subscription>> GetByAccountId(int accountId)
        {
            return await _subscriptionAppService.GetSubscriptionsByAccountAsync(accountId);
        }

        [Route("account/{accountId}")]
        public async Task<IHttpActionResult> PostSubscription(CreateSubscription command)
        {
            await _subscriptionAppService.CreateSubscriptionAsync(command);
            return Ok();
        }

        [Route("{subscriptionId}")]
        public async Task<IHttpActionResult> PutSubscription(int subscriptionId, UpdateSubscription command)
        {
            await _subscriptionAppService.UpdateSubscriptionAsync(command);
            return Ok();
        }
    }
}
