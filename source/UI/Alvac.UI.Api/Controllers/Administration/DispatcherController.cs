﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Dispatchers;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
    [RoutePrefix("api/dispatcher")]
    public class DispatchersController : ApiController
    {
        #region Private Fields

        private readonly IDispatcherAppService _dispatcherAppService;

        #endregion

        #region Constructor

        public DispatchersController(IDispatcherAppService dispatcherAppService)
        {
            _dispatcherAppService = dispatcherAppService;
        }

        #endregion
        
        #region Actions

        [Route("{id}")]
        public async Task<Dispatcher> GetDispatcherById(int id)
        {
            try
            {
                var dispatcher = await _dispatcherAppService.GetByIdAsync(id);

                if (dispatcher != null)
                {
                    return dispatcher;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        public async Task<IEnumerable<Dispatcher>> GetDispatchers()
        {
            try
            {
                var dispatcher = await _dispatcherAppService.GetAllAsync();

                if (dispatcher != null)
                {
                    return dispatcher;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        public async Task<IHttpActionResult> PostDispatcher(CreateDispatcher command)
        {
            await _dispatcherAppService.CreateDispatcherAsync(command);
            return Ok();
        }

        [Route("")]
        public async Task<IHttpActionResult> PutDispatcher(UpdateDispatcher command)
        {
            await _dispatcherAppService.UpdateDispatcherAsync(command);
            return Ok();
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> DeleteDispatcher(int id)
        {
            await _dispatcherAppService.DeleteDispatcherAsync(id);
            return Ok();
        }

        #endregion
    }
}
