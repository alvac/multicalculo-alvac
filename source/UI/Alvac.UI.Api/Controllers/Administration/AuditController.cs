﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Enum;
using Alvac.UI.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers.Administration
{
    [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
    [RoutePrefix("api/audit")]
    public class AuditController : ApiController
    {
        private readonly IAuditAppService _auditAppService;

        public AuditController(IAuditAppService auditAppService)
        {
            _auditAppService = auditAppService;
        }

        [Route("")]
        [CustomEnableQueryAttribute(EnsureStableOrdering = false)]
        public async Task<IQueryable<Audit>> GetAudits()
        {
            try
            {
                var model = await _auditAppService.GetAllAsync();

                if (model != null)
                {
                    return model;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }
    }
}
