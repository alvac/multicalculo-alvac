﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using Alvac.UI.Api.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;

namespace Alvac.UI.Api.Controllers.Administration
{
    [RoutePrefix("api/invoice")]
    [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
    public class InvoiceController : ApiController
    {
        private readonly IInvoiceAppService _invoiceAppService;

        public InvoiceController(IInvoiceAppService invoiceAppService)
        {
            _invoiceAppService = invoiceAppService;
        }

        [Route("subscription/{subscriptionId}")]
        [EnableQuery(EnsureStableOrdering = false)]
        public async Task<IQueryable<Invoice>> GetBySubscriptionId(int subscriptionId)
        {
            return await _invoiceAppService.GetInvoicesBySubscriptionAsync(subscriptionId);
        }
    }
}
