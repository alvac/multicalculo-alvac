﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Connections;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;

namespace Alvac.UI.Api.Controllers
{
    [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
    [RoutePrefix("api/connection")]
    public class ConnectionsController : ApiController
    {
        #region Private Fields

        private readonly IConnectionAppService _connectionAppService;

        #endregion

        #region Constructor

        public ConnectionsController(IConnectionAppService connectionAppService)
        {
            _connectionAppService = connectionAppService;
        }

        #endregion

        #region Actions

        [Route("{id}")]
        public async Task<Connection> GetConnectionById(int id)
        {
            try
            {
                var connection = await _connectionAppService.GetByIdAsync(id);

                if (connection != null)
                {
                    return connection;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        [EnableQuery(EnsureStableOrdering = false)]
        public async Task<IQueryable<Connection>> GetConnections()
        {
            try
            {
                var connections = await _connectionAppService.GetAllAsync();

                if (connections != null)
                {
                    return connections;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        public async Task<IHttpActionResult> PostConnection(CreateConnection command)
        {
            await _connectionAppService.CreateConnectionAsync(command);
            return Ok();
        }

        [Route("")]
        public async Task<IHttpActionResult> PutConnection(UpdateConnection command)
        {
            await _connectionAppService.UpdateConnectionAsync(command);
            return Ok();
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> DeleteConnection(int id)
        {
            await _connectionAppService.DeleteConnectionAsync(id);
            return Ok();
        }

        #endregion
    }
}
