﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.ConnectionParameters;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.OData;

namespace Alvac.UI.Api.Controllers.Administration
{
    [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
    [RoutePrefix("api/connection-parameter")]
    public class ConnectionParameterController : ApiController
    {
        #region Private Fields

        private readonly IConnectionParameterAppService _connectionParameterAppService;

        #endregion

        #region Constructor

        public ConnectionParameterController(IConnectionParameterAppService connectionParameterAppService)
        {
            _connectionParameterAppService = connectionParameterAppService;
        }

        #endregion

        #region Actions

        [Route("{id}")]
        public async Task<ConnectionParameter> GetConnectionParameterById(int id)
        {
            try
            {
                var connectionParameter = await _connectionParameterAppService.GetByIdAsync(id);

                if (connectionParameter != null)
                {
                    return connectionParameter;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("connection/{connectionId}")]
        public async Task<IQueryable<ConnectionParameter>> GetConnectionParameterByConnectionId(int connectionId)
        {
            try
            {
                var connectionParameters = await _connectionParameterAppService.GetByConnectionIdAsync(connectionId);

                if (connectionParameters != null)
                {
                    return connectionParameters;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        [EnableQuery(EnsureStableOrdering = false)]
        public async Task<IQueryable<ConnectionParameter>> GetConnectionParameters()
        {
            try
            {
                var connections = await _connectionParameterAppService.GetAllAsync();

                if (connections != null)
                {
                    return connections;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        public async Task<IHttpActionResult> PostConnectionParamter(CreateConnectionParameter command)
        {
            await _connectionParameterAppService.CreateConnectionAsync(command);
            return Ok();
        }

        [Route("")]
        public async Task<IHttpActionResult> PutConnectionParamter(UpdateConnectionParameter command)
        {
            await _connectionParameterAppService.UpdateConnectionAsync(command);
            return Ok();
        }

        [Route("{id}")]
        public async Task<IHttpActionResult> DeleteConnectionParamter(int id)
        {
            await _connectionParameterAppService.DeleteConnectionAsync(id);
            return Ok();
        }

        #endregion
    }
}