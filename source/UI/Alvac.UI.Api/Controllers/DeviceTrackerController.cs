﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/device-tracker")]
    public class DeviceTrackerController : ApiController
    {
        private readonly IDeviceTrackerAppService _deviceTrackerAppService;

        public DeviceTrackerController(IDeviceTrackerAppService deviceTrackerAppService)
        {
            _deviceTrackerAppService = deviceTrackerAppService;
        }

        [Route("")]
        public async Task<IEnumerable<DeviceTracker>> GetTrackers()
        {
            var response = await _deviceTrackerAppService.GetAllAsync();
            return response;
        }
    }
}
