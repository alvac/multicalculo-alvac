﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/ignition-blocker")]
    public class IgnitionBlockerController : ApiController
    {
        private readonly IIgnitionBlockerAppService _ignitionBlockerAppService;

        public IgnitionBlockerController(IIgnitionBlockerAppService ignitionBlockerAppService)
        {
            _ignitionBlockerAppService = ignitionBlockerAppService;
        }

        [Route("")]
        public async Task<IEnumerable<IgnitionBlocker>> GetBlockers()
        {
            var response = await _ignitionBlockerAppService.GetAllAsync();
            return response;
        }
    }
}
