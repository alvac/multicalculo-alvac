﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Reports;
using Alvac.Domain.Services.Reports.Queries;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/report")]
    public class ReportController : ApiController
    {
        private readonly IReportAppService _reportService;

        public ReportController(IReportAppService reportService)
        {
            _reportService = reportService;
        }

        [Route("auto-quotation/general")]
        public async Task<ReportGeneral> PostGeneral(ReportGeneralQuery query)
        {
            var response = await _reportService.GetGeneralAsync(query);
            return response;
        }

        [Route("auto-quotation/user")]
        public async Task<List<ReportQuotationGroupByUser>> PostUser(ReportGroupByUserQuery query)
        {
            var response = await _reportService.GetGroupByUserAsync(query);
            return response;
        }
    }
}
