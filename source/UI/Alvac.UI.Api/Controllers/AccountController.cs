﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;
using System.Web.Http.ModelBinding;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.Owin.Security;
using Microsoft.Owin.Security.Cookies;
using Microsoft.Owin.Security.OAuth;
using Alvac.UI.Api.Models;
using Alvac.UI.Api.Providers;
using Alvac.UI.Api.Results;
using Alvac.Domain.Enum;
using Alvac.Application.Services.Interfaces;
using System.Linq;
using Alvac.Domain.Services.Interfaces;
using System.Web.OData.Query;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Accounts;
using Alvac.Domain.Contract.Exceptions;
using System.Web.OData;
using System.IO;
using System.Net;
using System.Net.Http.Headers;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    public class AccountController : ApiController
    {
        #region Private Fields

        private readonly IAccountAppService _accountAppService;

        #endregion

        #region Constructor

        public AccountController(IAccountAppService accountAppService)
        {
            _accountAppService = accountAppService;
        }

        #endregion

        #region Actions

        [Route("self")]
        public async Task<Account> GetSelf()
        {
            try
            {
                var account = await _accountAppService.GetCurrentAccountAsync();

                if (account != null)
                {
                    return account;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("self/contract")]
        public async Task<HttpResponseMessage> GetContract()
        {
            try
            {
                var account = await _accountAppService.GetCurrentAccountAsync();

                if (account != null)
                {
                    var contract = ReportHelper.Generate("Alvac.UI.Api.Reports.Contracts.Default.rdlc", "Contract",
                        new List<object>()
                        {
                            new 
                            {
                                Nome = account.Name,
                                Endereco = account.Street,
                                Bairro = account.Neighborhood,
                                Cidade = account.City,
                                UF = account.State,
                                CEP = account.ZipCode,
                                CnpjCpf = account.Document,
                                InscricaoEstadual = "",
                                Email = account.Email,
                                Representante = account.LegalRepresentative,
                                CPFRepresentante = "",
                                Telefone = account.Phone,
                                NUsuarios = "60",
                                Plano = "Pós-pago: 100 cotações a R$80 até 700 a R$169 - 700 cotações",
                                Valor = "Calculável",
                                DataVenda = "18/01/2015",
                                SiteAlvac = "www.Alvac.com.br",
                                TelAlvac = "(31) 9443-0522",
                                FormaPagamento = "Boleto Bancário"
                            }
                        }, null);

                    var result = Request.CreateResponse(HttpStatusCode.OK);
                    result.Content = new StreamContent(new MemoryStream(contract));
                    result.Content.Headers.ContentDisposition = new ContentDispositionHeaderValue("attachment");
                    result.Content.Headers.ContentDisposition.FileName = "contrato.pdf";
                    return result;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        public async Task<IHttpActionResult> Put(UpdateAccount command)
        {
            await _accountAppService.UpdateAsync(command);
            return Ok();
        }

        [Route("")]
        [CustomEnableQueryAttribute(EnsureStableOrdering = false)]
        [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
        public async Task<IQueryable<Account>> GetAccounts()
        {
            try
            {
                var accounts = await _accountAppService.GetAccountsAsync();

                if (accounts != null)
                {
                    return accounts;
                }
                else
                {
                    throw new AlvacException("Não encontrado", DescriptionCode.NotFound);
                }
            }
            catch (Exception ex)
            {
                throw new AlvacException(ex.Message, DescriptionCode.InternalServerError);
            }
        }

        [Route("")]
        [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
        public async Task<IHttpActionResult> Post(CreateAccount command)
        {
            await _accountAppService.CreateAsync(command);
            return Ok();
        }

        [Route("{id}")]
        [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
        public async Task<IHttpActionResult> Delete(int id)
        {
            await _accountAppService.DeleteAsync(id);
            return Ok();
        }

        [Route("{id}")]
        [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
        public async Task<Account> GetAccountById(int id)
        {
            return await _accountAppService.GetAccountByIdAsync(id);
        }
        #endregion
    }
}
