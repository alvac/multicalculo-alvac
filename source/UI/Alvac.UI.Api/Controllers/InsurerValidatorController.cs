﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [RoutePrefix("api/insurer-validator")]
    public class InsurerValidatorController : ApiController
    {
        #region Private Fields

        private readonly IInsurerValidatorAppService _insurerValidatorAppService;

        #endregion

        #region Constructor

        public InsurerValidatorController(IInsurerValidatorAppService insurerValidatorAppService)
        {
            _insurerValidatorAppService = insurerValidatorAppService;
        }

        #endregion

        [Route("validate")]
        public async Task<InsurerValidatorResponse> Validate(InsurerQuotationRequest insurerQuotationRequest)
        {
            return await _insurerValidatorAppService.Validate(insurerQuotationRequest);
        }
    }
}
