﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/vehicle")]
    public class VehicleController : ApiController
    {
        private readonly IVehicleAppService _vehicleAppService;

        public VehicleController(IVehicleAppService vehicleAppService)
        {
            _vehicleAppService = vehicleAppService;
        }

        [Route("manufacturer")]
        public async Task<IEnumerable<Manufacturer>> GetBrands()
        {
            var response = await _vehicleAppService.GetManufacturersAsync();
            return response;
        }

        [Route("insurer/{insurer}/vehicle/{vehicleId}")]
        public async Task<IEnumerable<Vehicle>> GetVehicles(int insurer, int vehicleId)
        {
            var response = await _vehicleAppService.GetVehicleByIdInsurerAsync(insurer, vehicleId);
            return response;
        }

        [Route("manufacturer/{manufacturer}/year/{modelYear}")]
        public async Task<IEnumerable<Vehicle>> GetVehiclesByYear(int manufacturer, int modelYear)
        {
            var response = await _vehicleAppService.GetVehicleByManufacturerAsync(manufacturer, modelYear);
            return response;
        }

        [Route("manufacturer/{manufacturer}")]
        public async Task<IEnumerable<Vehicle>> GetVehiclesByModel(int manufacturer, string model)
        {
            var response = await _vehicleAppService.GetVehicleByManufacturerAsync(manufacturer, model);
            return response;
        }
    }
}
