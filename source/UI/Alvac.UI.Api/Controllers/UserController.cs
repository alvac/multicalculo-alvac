﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Contract;
using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Users;
using Alvac.Domain.Enum;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.OData;
using System.Web.OData.Query;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/user")]
    public class UserController : ApiController
    {
        private readonly IUserAppService _userAppService;

        public UserController(IUserAppService userAppService)
        {
            _userAppService = userAppService;
        }

        #region Actions

        [Route("roles")]
        [AllowAnonymous]
        public async Task<IQueryable<Alvac.Domain.DataTransferObject.Role>> GetRoles()
        {
            return await _userAppService.GetRolesAsync();
        }

        [Route("confirm-email")]
        [AllowAnonymous]
        public async Task<IHttpActionResult> PutConfirmEmail(ConfirmEmail confirmEmail)
        {
            await _userAppService.ConfirmEmailAsync(confirmEmail);
            return Ok();
        }

        [Route("email/{email}/{id}")]
        public async Task<IHttpActionResult> GetIsUniqueEmail(string email, string id)
        {
            var response = await _userAppService.IsUniqueEmailAsync(email, id);

            if (response)
            {
                return Ok();
            }

            return Conflict();
        }

        [Route("self")]
        public async Task<User> GetUserSelf()
        {
            var response = await _userAppService.GetUserSelfAsync();
            return response;
        }

        [Route("self")]
        public async Task<User> PutUserSelf(UpdateUser user)
        {
            var response = await _userAppService.UpdateUserAsync(user);
            return response;
        }

        [Route("")]
        [EnableQuery(EnsureStableOrdering = false)]
        public async Task<IQueryable<Alvac.Domain.DataTransferObject.User>> GetUsers()
        {
            return await _userAppService.GetUsersAsync();
        }

        [Route("{id}")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<User> GetUser(string id)
        {
            var response = await _userAppService.GetUserAsync(id);
            return response;
        }

        [Route("")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<IHttpActionResult> PostUser(CreateUser user)
        {
            var response = await _userAppService.CreateUserAsync(user);
            return Ok();
        }

        [Route("{id}")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<IHttpActionResult> PutUser(UpdateUser user, string id)
        {
            Asserts.AreEquals(user.Id, id);

            var response = await _userAppService.UpdateUserAsync(user);
            return Ok();
        }

        [Route("{id}")]
        [Authorize(Roles = Domain.Enum.Role.MasterString)]
        public async Task<IHttpActionResult> DeleteUser(string id)
        {
            await _userAppService.DeleteUserAsync(id);
            return Ok();
        }

        #endregion
    }
}
