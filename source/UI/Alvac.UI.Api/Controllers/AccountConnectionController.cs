﻿using Alvac.Application.Services.Interfaces;
using Alvac.Domain.Entities.Commands.AccountConnections;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web.Http;

namespace Alvac.UI.Api.Controllers
{
    [Authorize]
    [RoutePrefix("api/account")]
    public class AccountConnectionController : ApiController
    {
        private readonly IAccountConnectionAppService _accountConnectionService;

        public AccountConnectionController(IAccountConnectionAppService accountConnectionService)
        {
            _accountConnectionService = accountConnectionService;
        }

        [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
        [Route("{accountId}/insurers/connections")]
        public async Task<IEnumerable<Domain.DataTransferObject.AccountConnection>> GetInsurersByAccount(int accountId)
        {
            var response = await _accountConnectionService.GetInsurersByAccountAsync(accountId);
            return response;
        }

        [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
        [Route("{accountId}/insurers/connections/{connectionId}")]
        public async Task<IHttpActionResult> DeleteAccountConnection(int accountId, int connectionId)
        {
            var response = await _accountConnectionService.DeleteAccountConnection(new DeleteAccountConnection
                {
                    AccountId = accountId,
                    ConnectionId = connectionId
                });
            return Ok();
        }

        [Authorize(Roles = Alvac.Domain.Enum.Role.AlvacString)]
        [Route("{accountId}/insurers/connections/{connectionId}")]
        public async Task<IHttpActionResult> PostAccountConnection(CreateAccountConnection command)
        {
            var response = await _accountConnectionService.CreateAccountConnection(command);
            return Ok();
        }

        [Route("insurers/connections/{requestType}")]
        public async Task<IEnumerable<Domain.DataTransferObject.AccountConnection>> GetInsurers(RequestType requestType)
        {
            var response = await _accountConnectionService.GetInsurersAsync(requestType);
            return response;
        }
    }
}
