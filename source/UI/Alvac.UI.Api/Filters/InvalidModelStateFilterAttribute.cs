﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Web;
using System.Web.Http.Controllers;
using System.Web.Http.Filters;
using System.Net.Http;
using Alvac.Domain.Services.Logger;

namespace Alvac.UI.Api.Filters
{
    [AttributeUsage(AttributeTargets.Class | AttributeTargets.Method, AllowMultiple = false, Inherited = true)]
    public class InvalidModelStateFilterAttribute : ActionFilterAttribute
    {
        private readonly ILogger _logger;

        public InvalidModelStateFilterAttribute(ILogger logger)
        {
            _logger = logger;
        }

        public override void OnActionExecuting(HttpActionContext actionContext)
        {
            _logger.WriteVerboseAsync(string.Format("{0} - {1}", actionContext.ActionDescriptor.ControllerDescriptor.ControllerName, actionContext.ActionDescriptor.ActionName), "OnActionExecuting",
                string.Format("{0} - {1}", actionContext.Request.RequestUri.ToString(), actionContext.RequestContext.Principal.Identity.Name));

            if (!actionContext.ModelState.IsValid)
            {
                _logger.WriteInformationAsync(string.Format("{0} - {1}", actionContext.ActionDescriptor.ControllerDescriptor.ControllerName, actionContext.ActionDescriptor.ActionName), "", "ModelState is invalid");
                actionContext.Response = actionContext.Request.CreateErrorResponse(HttpStatusCode.BadRequest, actionContext.ModelState);
            }
        }
    }
}