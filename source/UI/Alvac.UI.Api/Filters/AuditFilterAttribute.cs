﻿using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Audits;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http.Filters;

namespace Alvac.UI.Api.Filters
{
    public class AuditFilterAttribute : ActionFilterAttribute
    {
        private readonly ICommandProcessor _commandProcessor;

        public AuditFilterAttribute(ICommandProcessor commandProcessor)
        {
            _commandProcessor = commandProcessor;
        }

        public override async Task OnActionExecutedAsync(HttpActionExecutedContext actionExecutedContext, System.Threading.CancellationToken cancellationToken)
        {
            if (actionExecutedContext.Request.Method == HttpMethod.Post 
                || actionExecutedContext.Request.Method == HttpMethod.Put 
                || actionExecutedContext.Request.Method == HttpMethod.Delete)
            {
                var command = new CreateAudit
                {
                    UserName = (actionExecutedContext.ActionContext.RequestContext.Principal.Identity.IsAuthenticated)
                        ? actionExecutedContext.ActionContext.RequestContext.Principal.Identity.Name
                        : "Anonymous",
                    IPAddress = HttpContext.Current.Request.ServerVariables["HTTP_X_FORWARDED_FOR"] ?? HttpContext.Current.Request.UserHostAddress,
                    AreaAccessed = HttpContext.Current.Request.RawUrl,
                    HttpStatusCode = actionExecutedContext.Response != null ? (int)actionExecutedContext.Response.StatusCode : 500
                };

                var reqStream = await actionExecutedContext.Request.Content.ReadAsStreamAsync();

                if (reqStream.CanSeek)
                {
                    reqStream.Position = 0;
                }

                command.Data = await actionExecutedContext.Request.Content.ReadAsStringAsync();
                await _commandProcessor.ProcessAsync(command);
            }

            await base.OnActionExecutedAsync(actionExecutedContext, cancellationToken);
        }
    }
}