﻿using Alvac.Domain.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http.Filters;

namespace Alvac.UI.Api.Filters
{
    public class CustomExceptionFilterAttribute : ActionFilterAttribute
    {
        private readonly ILogger _logger;

        public CustomExceptionFilterAttribute(ILogger logger)
        {
            _logger = logger;
        }

        public override void OnActionExecuted(HttpActionExecutedContext actionExecutedContext)
        {
            if (actionExecutedContext.Exception != null)
            {
                _logger.WriteErrorAsync(actionExecutedContext.ActionContext.ControllerContext.ControllerDescriptor.ControllerName,
                    actionExecutedContext.ActionContext.ActionDescriptor.ActionName,
                    actionExecutedContext.Exception);

                System.Diagnostics.Trace.TraceError(actionExecutedContext.Exception.Message);
            }

            base.OnActionExecuted(actionExecutedContext);
        }
    }
}