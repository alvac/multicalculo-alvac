﻿using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Services.Logger;
using Alvac.Infrastructure.Bootstraps.Api;
using Alvac.UI.Api.Models;
using Microsoft.Owin.Security.OAuth;
using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using Newtonsoft.Json.Serialization;
using System.Globalization;
using System.Web.Http;
using Swashbuckle;
using Swashbuckle.Application;
using Alvac.UI.Api.Filters;

namespace Alvac.UI.Api
{
    public static class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.Formatters.Remove(GlobalConfiguration.Configuration.Formatters.XmlFormatter);

            var formatters = config.Formatters;
            var jsonFormatter = formatters.JsonFormatter;
            var settings = jsonFormatter.SerializerSettings;

            settings.NullValueHandling = NullValueHandling.Ignore;
            settings.Formatting = Formatting.Indented;
            settings.ContractResolver = new CamelCasePropertyNamesContractResolver();
            settings.Converters.Add(new StringEnumConverter());
            settings.Converters.Add(new DateTimeOffSetConverter());
            settings.Culture = new CultureInfo("pt-BR");

            // Web API configuration and services
            // Configure Web API to use only bearer token authentication.
            config.SuppressDefaultHostAuthentication();
            config.Filters.Add(new HostAuthenticationFilter(OAuthDefaults.AuthenticationType));
            
            config.MessageHandlers.Add(new ODataResponseHandler());

            // Web API routes
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/{controller}/{id}",
                defaults: new { id = RouteParameter.Optional }
            );

            var container = ApiBootstrap.Register(config);
            config.Filters.Add(new CustomExceptionFilterAttribute(container.GetService<ILogger>()));
            config.Filters.Add(new InvalidModelStateFilterAttribute(container.GetService<ILogger>()));
            config.Filters.Add(new AuditFilterAttribute(container.GetService<ICommandProcessor>()));
        }
    }
}