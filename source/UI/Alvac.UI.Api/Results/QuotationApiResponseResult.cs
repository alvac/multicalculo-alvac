﻿using Alvac.Domain.Contract.Response;
using System.Collections.Generic;
using System.Diagnostics.Contracts;
using System.Net;
using System.Net.Http;
using System.Net.Http.Formatting;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

namespace Alvac.UI.Api.Results
{
    public class QuotationApiResponseResult : IHttpActionResult
    {
        private QuotationApiResponse _response;
        private ApiController _controller;

        public QuotationApiResponseResult(QuotationApiResponse response, ApiController controller)
        {
            _response = response;
            _controller = controller;
        }

        public Task<HttpResponseMessage> ExecuteAsync(System.Threading.CancellationToken cancellationToken)
        {
            var status = _response.Result.Code == Domain.Enum.Code.Success ? HttpStatusCode.OK : (HttpStatusCode)(int)_response.Result.DescriptionCode;

            return new NegotiatedContentResult<QuotationApiResponse>(status, _response, _controller).ExecuteAsync(cancellationToken);
        }
    }
}