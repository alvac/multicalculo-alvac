﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alvac.UI.Api.Models
{
    public class ODataResponse
    {
        public int Count { get; set; }
        public IEnumerable Data { get; set; }
    }
}