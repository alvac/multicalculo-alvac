﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.OData;

namespace Alvac.UI.Api.Models
{
    public class CustomEnableQueryAttribute : EnableQueryAttribute
    {
        public override IQueryable ApplyQuery(IQueryable queryable, System.Web.OData.Query.ODataQueryOptions queryOptions)
        {
            if (queryOptions.Request.RequestUri.ToString().Contains("$inlinecount=allpages"))
            {
                dynamic query = queryable;
                var count = Queryable.Count(query);
                var response = base.ApplyQuery(queryable, queryOptions);

                return new List<ODataResponse>
                                {
                                    new ODataResponse
                                    {
                                        Count = count,
                                        Data = response
                                    }
                                }.AsQueryable();
            }

            return base.ApplyQuery(queryable, queryOptions); ;
        }

        public override void ValidateQuery(System.Net.Http.HttpRequestMessage request, System.Web.OData.Query.ODataQueryOptions queryOptions)
        {
            if (!request.RequestUri.ToString().Contains("$inlinecount=allpages"))
            {
                base.ValidateQuery(request, queryOptions);
            }
        }
    }
}