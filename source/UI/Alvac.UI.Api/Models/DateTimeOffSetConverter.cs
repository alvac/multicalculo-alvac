﻿using Newtonsoft.Json.Converters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alvac.UI.Api.Models
{
    public class DateTimeOffSetConverter : IsoDateTimeConverter
    {
        public override object ReadJson(Newtonsoft.Json.JsonReader reader, Type objectType, object existingValue, Newtonsoft.Json.JsonSerializer serializer)
        {
            object baseResult = base.ReadJson(reader, objectType, existingValue, serializer);
            DateTimeOffset? date = baseResult as DateTimeOffset?;

            if (date.HasValue)
            {
                int timeZoneOffSet = 0;

                try
                {
                    var header = HttpContext.Current.Request.Headers.Get("TimezoneOffsetMinutes");
                    timeZoneOffSet = int.Parse(header) * -1;
                }
                catch
                {
                }

                var response = date.Value.ToOffset(TimeSpan.FromMinutes(timeZoneOffSet));
                return response;
            }

            return baseResult;
        }
    }
}