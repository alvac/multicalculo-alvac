﻿using Microsoft.Reporting.WebForms;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Web;

namespace Alvac.UI.Api.Models
{
    public class ReportHelper
    {
        public static byte[] Generate(string reportName, string sourceName, object source, Dictionary<string, string> parameters)
        {
            string binPath = Path.Combine(System.AppDomain.CurrentDomain.BaseDirectory, "bin");
            var assembly = Assembly.Load(System.IO.File.ReadAllBytes(binPath + "\\Alvac.UI.Api.dll"));

            using (var stream = assembly.GetManifestResourceStream(reportName))
            {
                var viewer = new ReportViewer();
                viewer.LocalReport.EnableExternalImages = true;
                viewer.LocalReport.LoadReportDefinition(stream);

                if (parameters != null)
                {
                    List<ReportParameter> pr = new List<ReportParameter>();

                    foreach (var item in parameters)
                    {
                        pr.Add(new ReportParameter(item.Key, item.Value));
                    }

                    viewer.LocalReport.SetParameters(pr);
                }

                Warning[] warnings;
                string[] streamids;
                string mimeType;
                string encoding;
                string filenameExtension;

                viewer.LocalReport.DataSources.Add(new ReportDataSource(sourceName, source));
                viewer.LocalReport.Refresh();

                byte[] bytes = viewer.LocalReport.Render(
                    "PDF", null, out mimeType, out encoding, out filenameExtension,
                    out streamids, out warnings);

                return bytes;
            }
        }
    }
}