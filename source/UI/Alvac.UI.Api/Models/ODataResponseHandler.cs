﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web;
using System.Web.Http;

namespace Alvac.UI.Api.Models
{
    public class ODataResponseHandler : DelegatingHandler
    {
        protected override System.Threading.Tasks.Task<HttpResponseMessage> SendAsync(HttpRequestMessage request, System.Threading.CancellationToken cancellationToken)
        {
            return base.SendAsync(request, cancellationToken).ContinueWith(task =>
                {
                    if (ResponseIsValid(task.Result) && request.RequestUri.ToString().Contains("$inlinecount=allpages"))
                    {
                        dynamic obj;
                        task.Result.TryGetContentValue(out obj);
                        if(obj is IQueryable)
                        {
                            task.Result.Content = new ObjectContent<ODataResponse>(((IQueryable<ODataResponse>)obj).FirstOrDefault(), GlobalConfiguration.Configuration.Formatters[0]);
                        }
                    }
                    
                    return task.Result;
                });
        }

        private bool ResponseIsValid(HttpResponseMessage response)
        {
            if (response == null || response.StatusCode != HttpStatusCode.OK || !(response.Content is ObjectContent)) return false;
            return true;
        }

    }
}