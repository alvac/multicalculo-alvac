﻿using Alvac.Domain.Contract.Response;
using Alvac.UI.Api.Models;
using Alvac.UI.Api.Results;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Http;

namespace System.Web.Http
{
    public static class ApiControllerExtensions
    {
        public static QuotationApiResponseResult ApiResponse(this ApiController controller, QuotationApiResponse response)
        {
            return new QuotationApiResponseResult(response, controller);
        }
    }
}