﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alvac.Ui.Admin.Models
{
    public class DataTableResponse<T>
    {
        public DataTableResponse(int draw, ODataResponse<T> response)
        {
            this.draw = draw;
            data = response.Data;
            recordsTotal = response.Count;
            recordsFiltered = response.Count;
        }

        public int draw { get; set; }
        public int recordsTotal { get; set; }
        public int recordsFiltered { get; set; }
        public T data { get; set; }
    }
}