﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace Alvac.Ui.Admin.Models
{
    public class AuthBindingModel
    {
        [Required(ErrorMessage = "Digite o seu usuario")]
        [EmailAddress(ErrorMessage = "Digite um email valido")]
        public string UserName { get; set; }

        [Required(ErrorMessage = "Digite a senha")]
        public string Password { get; set; }
    }
}