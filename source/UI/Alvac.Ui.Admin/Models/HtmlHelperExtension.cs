﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Linq;
using System.Linq.Expressions;
using System.Web;
using System.Web.Mvc;
using System.Web.Mvc.Html;

namespace System.Web.Mvc.Html
{
    public static class HtmlHelperExtension
    {
        public static MvcHtmlString EnumFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, string optionLabel)
        {
            return EnumFor(htmlHelper, expression, optionLabel, null);
        }

        public static MvcHtmlString EnumFor(this HtmlHelper htmlHelper, Type enumType, string name, string optionLabel, object htmlAttributes)
        {
            var values = Enum.GetValues(enumType);

            var items = new List<SelectListItem>();

            foreach (var item in values)
            {
                if (item.ToString() != "None")
                {
                    items.Add(new SelectListItem
                                {
                                    Text = GetEnumDescription(item),
                                    Value = item.ToString()
                                });
                }
            }

            return htmlHelper.DropDownList(name, items, optionLabel, htmlAttributes);
        }

        public static MvcHtmlString EnumFor<TModel, TEnum>(this HtmlHelper<TModel> htmlHelper, Expression<Func<TModel, TEnum>> expression, string optionLabel, object htmlAttributes)
        {
            ModelMetadata metadata = ModelMetadata.FromLambdaExpression(expression, htmlHelper.ViewData);
            Type enumType = GetNonNullableModelType(metadata);
            IEnumerable<TEnum> values = Enum.GetValues(enumType).Cast<TEnum>();

            IEnumerable<SelectListItem> items = from value in values
                                                where value.ToString() != "None"
                                                select new SelectListItem
                                                {
                                                    Text = GetEnumDescription(value),
                                                    Value = value.ToString(),
                                                    Selected = value.Equals(metadata.Model)
                                                };


            return htmlHelper.DropDownListFor(expression, items, optionLabel, htmlAttributes);
        }

        public static string GetEnumDescription<TEnum>(TEnum value)
        {
            var fi = value.GetType().GetField(value.ToString());

            DescriptionAttribute[] attributes = (DescriptionAttribute[])fi.GetCustomAttributes(typeof(DescriptionAttribute), false);

            if ((attributes != null) && (attributes.Length > 0))
                return attributes[0].Description;
            else
                return value.ToString();
        }

        private static Type GetNonNullableModelType(ModelMetadata modelMetadata)
        {
            Type realModelType = modelMetadata.ModelType;

            Type underlyingType = Nullable.GetUnderlyingType(realModelType);
            if (underlyingType != null)
            {
                realModelType = underlyingType;
            }
            return realModelType;
        }

    }
}