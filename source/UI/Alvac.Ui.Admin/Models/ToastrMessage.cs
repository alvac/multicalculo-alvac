﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Web;

namespace Alvac.Ui.Admin.Models
{
    public class ToastrMessage
    {
        public ToastrMessage(TraceLevel level, string message)
        {
            Level = level;
            Message = message;
        }

        public ToastrMessage(TraceLevel level)
        {
            Level = level;
            Message = string.Empty;
        }

        public TraceLevel Level { get; set; }
        public string Message { get; set; }
    }
}