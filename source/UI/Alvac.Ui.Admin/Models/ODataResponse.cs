﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alvac.Ui.Admin.Models
{
    public class ODataResponse<T>
    {

        public int Count { get; set; }
        public T Data { get; set; }
    }
}