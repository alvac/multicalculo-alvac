﻿using System.Web;
using System.Web.Mvc;

namespace Alvac.Ui.Admin
{
    public class FilterConfig
    {
        public static void RegisterGlobalFilters(GlobalFilterCollection filters)
        {
            filters.Add(new HandleErrorAttribute());
            filters.Add(new AuthorizeAttribute()
                {
                    Roles = "Alvac"
                });
        }
    }
}
