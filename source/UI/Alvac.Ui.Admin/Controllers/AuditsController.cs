﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Services.Interfaces;
using Alvac.Ui.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Alvac.Ui.Admin.Controllers
{
    public class AuditsController : Controller
    {
        private readonly IHttpClient _httpClient;

        public AuditsController(IHttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        // GET: Audit
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> LoadTable(int draw, int start, int length)
        {
            var filter = Request.QueryString.Get("search[value]");

            string query = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(filter))
                {
                    query += "&$filter=contains(UserName,'" + filter + "') or contains(IPAddress,'" + filter + "')  or contains(AreaAccessed,'" + filter + "')";
                }

                var model = await _httpClient.GetAsync<ODataResponse<IEnumerable<Audit>>>(string.Format("audit?$inlinecount=allpages&$orderby=Timestamp desc&$top={0}&$skip={1}{2}", length, start, query));
                return Json(new DataTableResponse<IEnumerable<Audit>>(draw, model), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return View();
            }
        }
    }
}