﻿using Alvac.Domain.DataTransferObject;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Alvac.Domain.Entities.Commands.ConnectionParameters;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System.Threading.Tasks;
using Alvac.Ui.Admin.Models;
using System.Diagnostics;

namespace Alvac.Ui.Admin.Controllers
{
    public class ConnectionParametersController : CrudController<IEnumerable<ConnectionParameter>, CreateConnectionParameter, ConnectionParameter, UpdateConnectionParameter>
    {
        public ConnectionParametersController(IHttpClient httpClient, ILogger logger)
            : base("connection-parameter", httpClient, logger)
        {

        }

        public override async Task<ActionResult> Index()
        {
            var response = await _httpClient.GetAsync<IEnumerable<ConnectionParameter>>(string.Format("{0}/connection/{1}", _path, Request.QueryString["connectionId"].ToString()));
            return View(response);
        }

        public override async Task<ActionResult> Create(CreateConnectionParameter command)
        {
            try
            {
                var response = await base.Create(command);

                if (!ModelState.IsValid)
                {
                    TempData["toastr"] = new ToastrMessage(TraceLevel.Error);
                }
            }
            catch (Exception)
            {
                TempData["toastr"] = new ToastrMessage(TraceLevel.Error);
            }


            return RedirectToAction("Index", "ConnectionParameters", new { connectionId = command.ConnectionId });
        }
    }
}