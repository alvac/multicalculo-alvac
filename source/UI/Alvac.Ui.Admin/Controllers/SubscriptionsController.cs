﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Subscriptions;
using Alvac.Domain.Services.Interfaces;
using Alvac.Ui.Admin.Models;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Alvac.Ui.Admin.Controllers
{
    public class SubscriptionsController : Controller
    {
        private readonly IHttpClient _httpClient;

        public SubscriptionsController(IHttpClient client)
        {
            _httpClient = client;
        }

        public async Task<ActionResult> Create(int accountId)
        {
            await LoadPlans();
            var model = new CreateSubscription
            {
                AccountId = accountId
            };

            return View(model);
        }

        [HttpPost]
        public async Task<ActionResult> Create(CreateSubscription command)
        {
            var response = await _httpClient.PostAsync(string.Format("subscription/account/{0}", command.AccountId), command);
            if (response.IsSuccessStatusCode)
            {
                TempData["toastr"] = new ToastrMessage(TraceLevel.Verbose, "Criado com sucesso");
                return RedirectToAction("Details", "Accounts", new { id = command.AccountId });
            }

            await LoadPlans();
            ModelState.AddModelError(string.Empty, await response.Content.ReadAsStringAsync());
            return View(command);
        }

        public async Task<ActionResult> Edit(int id)
        {
            await LoadPlans();

            var model = await _httpClient.GetAsync<Subscription>(string.Format("subscription/{0}", id));
            var command = Mapper.Map<UpdateSubscription>(model);
            return View(command);
        }

        [HttpPost]
        public async Task<ActionResult> Edit(UpdateSubscription command)
        {
            var response = await _httpClient.PutAsync(string.Format("subscription/{0}", command.SubscriptionId), command);
            if (response.IsSuccessStatusCode)
            {
                TempData["toastr"] = new ToastrMessage(TraceLevel.Verbose, "Criado com sucesso");
                return RedirectToAction("Details", "Accounts", new { id = command.AccountId });
            }

            await LoadPlans();
            ModelState.AddModelError(string.Empty, await response.Content.ReadAsStringAsync());
            return View(command);
        }

        private async Task LoadPlans()
        {
            var plans = await _httpClient.GetAsync<IEnumerable<Plan>>(string.Format("plan"));
            ViewBag.Plans = new SelectList(plans, "PlanId", "Name");
        }
    }
}