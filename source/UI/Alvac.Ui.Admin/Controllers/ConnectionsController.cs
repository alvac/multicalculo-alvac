﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Connections;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Ui.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Alvac.Ui.Admin.Controllers
{
    public class ConnectionsController : CrudController<IEnumerable<Connection>, CreateConnection, Connection, UpdateConnection>
    {
        public ConnectionsController(IHttpClient httpClient, ILogger logger)
            : base("connection", httpClient, logger)
        {

        }

        public override async Task<ActionResult> Create()
        {
            await LoadDispatchersAsync();
            return await base.Create();
        }

        public override async Task<ActionResult> Create(CreateConnection command)
        {
            var response = await base.Create(command);

            if (!ModelState.IsValid)
            {
                await LoadDispatchersAsync();
            }

            return response;
        }

        public override async Task<ActionResult> Edit(object id)
        {
            await LoadDispatchersAsync();
            return await base.Edit(id);
        }

        public override async Task<ActionResult> Edit(UpdateConnection command)
        {
            var response = await base.Edit(command);

            if (!ModelState.IsValid)
            {
                await LoadDispatchersAsync();
            }

            return response;
        }
        
        private async Task LoadDispatchersAsync()
        {
            var dispatchers = await _httpClient.GetAsync<IEnumerable<Dispatcher>>("dispatcher");
            ViewBag.Dispatchers = new SelectList(dispatchers, "DispatcherId", "Name");
        }
    }
}