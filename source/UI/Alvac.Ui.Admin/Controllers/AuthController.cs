﻿using Alvac.Ui.Admin.Models;
using Microsoft.Rest;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Net.Http;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;
using System.Linq;
using Microsoft.Owin.Security;
using System.Security.Claims;

namespace Alvac.Ui.Admin.Controllers
{
    [AllowAnonymous]
    public class AuthController : Controller
    {
        private Uri _baseUri = new Uri(ConfigurationManager.AppSettings.Get("apiManagerUrl"));

        // GET: Auth
        public ActionResult Index()
        {
            return View();
        }

        public async Task<ActionResult> Login(AuthBindingModel model)
        {
            if (ModelState.IsValid)
            {
                using (var client = new HttpClient())
                {
                    var response = await client.PostAsync(_baseUri.ToString() + "token",
                        new StringContent(string.Format("grant_type=password&username={0}&password={1}", model.UserName, model.Password)));

                    var strResponse = await response.Content.ReadAsStringAsync();

                    var token = JsonConvert.DeserializeObject<TokenResponse>(strResponse);

                    var claims = new List<Claim>()
                            {
                                new Claim(ClaimTypes.Name, model.UserName),
                                new Claim("Token", token.AccessToken)
                            };

                    token.Role.Split(',').ToList().ForEach(x => claims.Add(new Claim(ClaimTypes.Role, x)));

                    HttpContext.GetOwinContext().Authentication.SignIn(new ClaimsIdentity(claims, "ApplicationCookie"));
                    return RedirectToAction("Index", "Home");
                }
            }

            return View("Index", model);
        }

        public ActionResult Logout()
        {
            HttpContext.GetOwinContext().Authentication.SignOut("ApplicationCookie");
            return RedirectToAction("Index", "Auth");
        }
    }
}