﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.AccountConnections;
using Alvac.Domain.Entities.Commands.Accounts;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Ui.Admin.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Alvac.Ui.Admin.Controllers
{
    public class AccountsController : CrudController<IEnumerable<Account>, CreateAccount, Account, UpdateAccount>
    {
        public AccountsController(IHttpClient httpClient, ILogger logger)
            : base("account", httpClient, logger)
        {
        }

        public override Task<ActionResult> Index()
        {
            return Task.FromResult<ActionResult>(View());
        }

        public async Task<ActionResult> Details(int id)
        {
            var model = await _httpClient.GetAsync<Account>(string.Format("account/{0}", id));
            return View(model);
        }

        public async Task<ActionResult> LoadTable(int draw, int start, int length)
        {
            var filter = Request.QueryString.Get("search[value]");

            string query = string.Empty;

            try
            {
                if (!string.IsNullOrEmpty(filter))
                {
                    query += "&$filter=contains(Name,'" + filter + "') or contains(Email,'" + filter + "')  or contains(LegalRepresentative,'" + filter + "') or contains(Document,'" + filter + "')";
                }

                var accounts = await _httpClient.GetAsync<ODataResponse<IEnumerable<Account>>>(string.Format("account?$inlinecount=allpages&$top={0}&$skip={1}{2}", length, start, query));
                return Json(new DataTableResponse<IEnumerable<Account>>(draw, accounts), JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return View();
            }
        }

        #region Connections
        public async Task<ActionResult> Connections(int id)
        {
            await LoadInsurersAsync();
            var model = await _httpClient.GetAsync<IEnumerable<AccountConnection>>(string.Format("account/{0}/insurers/connections", id));
            ViewBag.AccountId = id;
            return View("_Connections", model);
        }

        public async Task<ActionResult> DeleteConnection(DeleteAccountConnection command)
        {
            try
            {
                var model = await _httpClient.DeleteAsync(string.Format("account/{0}/insurers/connections/{1}", command.AccountId, command.ConnectionId));
                return Content("Conexão deletada");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        public async Task<ActionResult> CreateConnection(CreateAccountConnection command)
        {
            try
            {
                var model = await _httpClient.PostAsync(string.Format("account/{0}/insurers/connections/{1}", command.AccountId, command.ConnectionId), command);
                return Content("Conexão criada");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        public async Task<ActionResult> LoadConnections(RequestType requestType, int insurer)
        {
            string query = string.Empty;

            try
            {
                query += "$filter=RequestType eq Alvac.Domain.Enum.RequestType'" + (int)requestType + "' and InsurerId eq  " + (int)insurer;

                var connections = await _httpClient.GetAsync<IEnumerable<Connection>>(string.Format("connection?{0}", query));

                return Json(connections, JsonRequestBehavior.AllowGet);
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }
        #endregion

        public async Task<ActionResult> Subscriptions(int id)
        {
            ViewBag.AccountId = id;
            var model = await _httpClient.GetAsync<IEnumerable<Subscription>>(string.Format("subscription/account/{0}", id));
            return PartialView("_Subscriptions", model);
        }

        public async Task LoadInsurersAsync()
        {
            var insurers = await _httpClient.GetAsync<IEnumerable<Insurer>>("insurer");
            ViewBag.Insurers = new SelectList(insurers, "Id", "Name");
        }
    }
}