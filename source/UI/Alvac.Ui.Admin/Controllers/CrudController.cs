﻿using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Ui.Admin.Models;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Alvac.Ui.Admin.Controllers
{
    public class CrudController<IndexQuery, CreateCommand, UpdateQuery, UpdateCommand> : Controller
    {
        protected readonly string _path;
        private readonly ILogger _logger;

        protected readonly IHttpClient _httpClient;

        public CrudController(string path, IHttpClient httpClient, ILogger logger)
        {
            _path = path;
            _httpClient = httpClient;
            _logger = logger;
        }

        public virtual async Task<ActionResult> Index()
        {
            var response = await _httpClient.GetAsync<IndexQuery>(_path);
            return View(response);
        }

        public virtual Task<ActionResult> Create()
        {
            return Task.FromResult<ActionResult>(View());
        }

        [HttpPost]
        public virtual async Task<ActionResult> Create(CreateCommand command)
        {
            if (ModelState.IsValid)
            {
                var response = await _httpClient.PostAsync(_path, command);
                if (response.IsSuccessStatusCode)
                {
                    TempData["toastr"] = new ToastrMessage(TraceLevel.Verbose, "Criado com sucesso");
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, await response.Content.ReadAsStringAsync());
                return View(command);
            }

            return View(command);
        }

        public virtual async Task<ActionResult> Edit(object id)
        {
            var response = await _httpClient.GetAsync<UpdateQuery>(string.Format("{0}/{1}", _path, id));
            var command = Mapper.Map<UpdateCommand>(response);
            return View(command);
        }

        [HttpPost]
        public virtual async Task<ActionResult> Edit(UpdateCommand command)
        {
            if (ModelState.IsValid)
            {
                var response = await _httpClient.PutAsync(_path, command);
                if (response.IsSuccessStatusCode)
                {
                    TempData["toastr"] = new ToastrMessage(TraceLevel.Verbose, "Atualizado com sucesso");
                    return RedirectToAction("Index");
                }

                ModelState.AddModelError(string.Empty, await response.Content.ReadAsStringAsync());
                return View(command);
            }

            return View(command);
        }

        [HttpGet]
        public virtual async Task<ActionResult> Delete(string id)
        {
            var response = await _httpClient.GetAsync<UpdateQuery>(string.Format("{0}/{1}", _path, id));
            return View(response);
        }

        [HttpPost]
        public virtual async Task<ActionResult> PostDelete(string id)
        {
            var response = await _httpClient.DeleteAsync(string.Format("{0}/{1}", _path, id));

            if (response.IsSuccessStatusCode)
            {
                TempData["toastr"] = new ToastrMessage(TraceLevel.Verbose, "Deletado com sucesso");
                return RedirectToAction("Index");
            }

            return RedirectToAction("Delete", new { id = id });
        }
    }
}