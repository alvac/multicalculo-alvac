﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Dispatchers;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Threading.Tasks;

namespace Alvac.Ui.Admin.Controllers
{
    public class DispatchersController : CrudController<IEnumerable<Dispatcher>, CreateDispatcher, Dispatcher, UpdateDispatcher>
    {
        public DispatchersController(IHttpClient httpClient, ILogger logger)
            : base("dispatcher", httpClient, logger)
        {
        }

        public override async Task<ActionResult> Create()
        {
            await LoadInsurersAsync();
            return await base.Create();
        }

        public override async Task<ActionResult> Create(CreateDispatcher command)
        {
            await LoadInsurersAsync();
            return await base.Create(command);
        }

        public override async Task<ActionResult> Edit(object id)
        {
            await LoadInsurersAsync();
            return await base.Edit(id);
        }

        public override async Task<ActionResult> Edit(UpdateDispatcher command)
        {
            await LoadInsurersAsync();
            return await base.Edit(command);
        }

        public async Task LoadInsurersAsync()
        {
            var insurers = await _httpClient.GetAsync<IEnumerable<Insurer>>("insurer");
            ViewBag.Insurers = new SelectList(insurers, "Id", "Name");
        }
    }
}