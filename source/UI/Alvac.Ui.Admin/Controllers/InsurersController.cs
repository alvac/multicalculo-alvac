﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.InsurerConfigurations;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Interfaces;
using Omu.ValueInjecter;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Alvac.Ui.Admin.Controllers
{
    public class InsurersController : Controller
    {
        private readonly IHttpClient _httpClient;

        public InsurersController(IHttpClient client)
        {
            _httpClient = client;
        }

        // GET: Insurers
        public async Task<ActionResult> Index()
        {
            var insurers = await _httpClient.GetAsync<IEnumerable<Insurer>>("insurer");
            return View(insurers);
        }

        public async Task<ActionResult> Configurations(int id, RequestType requestType)
        {
            var model = await _httpClient.GetAsync<IEnumerable<InsurerConfiguration>>(string.Format("insurer-configuration/insurer/{0}/request-type/{1}", id, requestType));
            ViewBag.RequestType = requestType;
            ViewBag.Insurer = id;
            return View(model);
        }

        public async Task<ActionResult> EditConfiguration(int id)
        {
            var model = await _httpClient.GetAsync<InsurerConfiguration>(string.Format("insurer-configuration/{0}", id));
            ViewBag.RequestType = model.RequestType;
            ViewBag.Insurer = model.InsurerId;
            return View(Mapper.Map<UpdateInsurerConfiguration>(model));
        }

        [HttpPost]
        public async Task<ActionResult> EditConfiguration(UpdateInsurerConfiguration command, int insurer, RequestType requestType)
        {
            var model = await _httpClient.PutAsync(string.Format("insurer-configuration/{0}", command.InsurerConfigurationId), command);
            return RedirectToAction("Configurations", new { id = insurer, requestType = requestType });
        }

        public async Task<ActionResult> DeleteConfiguration(DeleteInsurerConfiguration command)
        {
            try
            {
                var model = await _httpClient.DeleteAsync(string.Format("insurer-configuration/{0}", command.InsurerConfigurationId));
                return Content("Configuração deletada");
            }
            catch (Exception ex)
            {
                return Content(ex.Message);
            }
        }

        public ActionResult CreateConfiguration(int insurer, RequestType requestType)
        {
            return View(new CreateInsurerConfiguration
                {
                    Insurer = insurer,
                    RequestType = requestType
                });
        }

        [HttpPost]
        public async Task<ActionResult> CreateConfiguration(CreateInsurerConfiguration command)
        {
            var model = await _httpClient.PostAsync(string.Format("insurer-configuration"), command);
            return RedirectToAction("Configurations", new { id = command.Insurer, requestType = command.RequestType });
        }
    }
}