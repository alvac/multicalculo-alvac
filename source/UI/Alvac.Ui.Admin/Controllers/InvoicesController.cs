﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Mvc;

namespace Alvac.Ui.Admin.Controllers
{
    public class InvoicesController : Controller
    {
        private readonly IHttpClient _httpClient;

        public InvoicesController(IHttpClient client)
        {
            _httpClient = client;
        }

        public async Task<ActionResult> Index(int subscriptionId)
        {
            ViewBag.SubscriptionId = subscriptionId;
            var model = await _httpClient.GetAsync<IEnumerable<Invoice>>(string.Format("invoice/subscription/{0}?&$orderby=Closing desc", subscriptionId));
            return View(model);
        }

        public ActionResult Create(int subscriptionId)
        {
            return View();
        }
    }
}