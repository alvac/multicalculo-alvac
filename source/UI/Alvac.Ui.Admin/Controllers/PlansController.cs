﻿using Alvac.Domain.DataTransferObject;
using Alvac.Domain.Entities.Commands.Plans;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Ui.Admin.Models;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Configuration;
using System.Threading.Tasks;
using System.Web.Mvc;

namespace Alvac.Ui.Admin.Controllers
{
    public class PlansController : CrudController<IEnumerable<Plan>, CreatePlan, Plan, UpdatePlan>
    {
        #region Private Fields

        private readonly ILogger _logger;

        #endregion

        public PlansController(ILogger logger, IHttpClient httpClient)
            : base("plan", httpClient, logger)
        {
            _logger = logger;
        }
    }
}