﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('initCtrl', ['$rootScope', '$scope', 'authService', '$location', 'localStorageService', '$q', 'alertService', '$templateCache',
        function ($rootScope, $scope, authService, $location, localStorageService, $q, alertService, $templateCache) {

            $rootScope.authentication = authService.authentication;

            $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
                if (next.$$route) {
                    $rootScope.title = next.$$route.title;
                }

                alertService.unblock();
            });

            $rootScope.$on('$routeChangeStart', function () {
                if (typeof (current) !== 'undefined') {
                    $templateCache.remove(current.templateUrl);
                }

                alertService.block();
            });

            $rootScope.$on('$routeChangeError', function () {
                alertService.unblock();
            });

            angular.element(document).ready(function () {
                $("#main").show();
            });

            $rootScope.connection = null;

            $rootScope.$on("loadSignalr", function () {
                var connection = $rootScope.connection;

                if (!connection) {
                    var proxy = null;
                    connection = $.hubConnection();
                    connection.qs = { 'userId': authService.authentication.userName };
                    connection.logging = true;

                    connection.stateChanged(function (change) {
                        console.log('stateChanged from:' + change.oldState + ' to: ' + change.newState);
                    });

                    connection.reconnected(function () {
                        console.log('reconnected signalr');
                    });

                    connection.disconnected(function () {
                        console.log('disconnected signalr');
                        setTimeout(function () { connection.start(); }, 5000);
                    });

                    connection.connectionSlow(function () {
                        console.log('We are currently experiencing difficulties with the connection.');
                    });

                    connection.error(function (error) {
                        console.log('SignalR error: ' + error);
                    });

                    proxy = connection.createHubProxy('QuotationHub');

                    // Listen to the 'quotationResponse' event that will be pushed from SignalR server
                    proxy.on('quotationResponse', function (quotationResponse) {
                        $rootScope.$emit("quotationResponse", quotationResponse);
                    });

                    // Connecting to SignalR server        
                    connection.start()
                    .done(function () { console.log('Now connected'); })
                    .fail(function () { console.log('Could not connect'); });

                    $rootScope.connection = connection;
                }
            });
        }]);
})();