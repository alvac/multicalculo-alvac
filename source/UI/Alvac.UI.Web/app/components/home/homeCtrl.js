﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('homeCtrl', ['$rootScope', '$scope', 'autoQuotationService', 'alertService', 'quotations',
        function ($rootScope, $scope, autoQuotationService, alertService, quotations) {

            $scope.areaLabels = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho"];
            $scope.areaSeries = ['Bradesco', 'Alfa', 'Azul', 'Chubb', 'Zurich'];
            $scope.areaData = [
              [65, 59, 80, 81, 56, 55, 40],
              [28, 48, 70, 11, 86, 27, 90],
              [38, 38, 80, 11, 36, 33, 55],
              [48, 18, 90, 12, 46, 65, 44],
              [58, 28, 4, 29, 66, 17, 90]
            ];

            $scope.pieLabels = ["Automóvel ", "Vida", "Residencial"];
            $scope.pieData = [3870, 1200, 320];

            $scope.barLabels = ['NOV/2014', 'DEZ/2014', 'JAN/2015'];
            $scope.barSeries = ['Vendas'];
            $scope.barOptions = {
                scaleLabel: "<%= Number(value).toMoney() %>",
                tooltipTemplate: "<%= Number(value).toMoney() %>",
                responsive: true
            };
            $scope.barData = [
              [5800, 19840.55, 25000.10]
            ];

            $scope.lastQuotations = quotations;

            $rootScope.$on("quotationResponse", function (event, notificationRequest) {
                var found = false;
                for (var index = 0; index < $scope.lastQuotations.length; index++) {
                    for (var i = 0; i < $scope.lastQuotations[index].insurerQuotation.length; i++) {

                        if (found) break;

                        if ($scope.lastQuotations[index].insurerQuotation[i].insurerQuotationId == notificationRequest.response.identifier) {
                            found = true;

                            var quotation = $scope.lastQuotations[index].insurerQuotation[i];
                            quotation.status = notificationRequest.response.result.code;
                            quotation.operationDescription = notificationRequest.response.result.description;
                        }
                    }

                    if (found) break;
                }

                $scope.loadCount();
            });

            $scope.loadCount = function () {
                if ($scope.lastQuotations) {
                    for (var i = 0; i < $scope.lastQuotations.length; i++) {
                        $scope.lastQuotations[i].count = $scope.lastQuotations[i].insurerQuotation.filter(function (element) {
                            return element.status == 'Success' || element.status == 'Failure';
                        }).length;
                    }
                }
            };

            $scope.loadCount();
        }]);
})();