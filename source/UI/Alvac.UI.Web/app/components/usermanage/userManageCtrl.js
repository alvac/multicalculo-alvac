﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('userManageCtrl', ['$rootScope', '$scope', 'userService', '$location', 'alertService', 'roles', 'user',
        function ($rootScope, $scope, userService, $location, alertService, roles, user) {

            $scope.pageTitle = "Editando usuário";

            $scope.roles = roles;
            $scope.model = user;

            $scope.save = function () {

                $scope.form.submitted = true;

                if ($scope.form.$valid) {
                    alertService.block();

                    userService.update($scope.model.id, $scope.model).then(function (response) {
                        $location.path("/corretora/usuarios");
                        alertService.success("Usuário atualizado com sucesso.");
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.message);
                }
            }
        }]);
})();