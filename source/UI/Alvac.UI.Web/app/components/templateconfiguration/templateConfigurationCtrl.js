﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('templateConfigurationCtrl', ['$rootScope', '$scope', '$modalInstance', 'insurer',
        function ($rootScope, $scope, $modalInstance, insurer) {

            $scope.authentication = $rootScope.authentication;
            $scope.insurer = angular.copy(insurer);

            $scope.ok = function () {
                $scope.templateConfigurationForm.submitted = true;

                if ($scope.templateConfigurationForm.$valid) {
                    $modalInstance.close($scope.insurer);
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);
})();