﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('autoQuotationListCtrl', ['$rootScope', '$scope', 'autoQuotationService', '$location', 'alertService',
        function ($rootScope, $scope, autoQuotationService, $location, alertService) {

            $scope.pageTitle = "Lista de cotações";

            $scope.currentPage = 0;
            $scope.pageLength = 20;
            $scope.count = 0;

            $scope.filters = {
                beginDate: moment().subtract(1, 'months').toDate(),
                endDate: new Date()
            };

            $scope.nextPage = function () {
                $scope.currentPage = $scope.currentPage + 1;
                $scope.loadTable();
            };

            $scope.previousPage = function () {
                if ($scope.currentPage == 0) {
                    return;
                }

                $scope.currentPage = $scope.currentPage - 1;
                $scope.loadTable();
            };

            $scope.goToDetails = function (id) {
                $location.path("/cotacao/automovel/" + id);
            };

            $rootScope.$on("quotationResponse", function (event, notificationRequest) {
                var found = false;
                for (var index = 0; index < $scope.lastQuotations.length; index++) {
                    for (var i = 0; i < $scope.lastQuotations[index].insurerQuotation.length; i++) {

                        if (found) break;

                        if ($scope.lastQuotations[index].insurerQuotation[i].insurerQuotationId == notificationRequest.response.identifier) {
                            found = true;

                            var quotation = $scope.lastQuotations[index].insurerQuotation[i];
                            quotation.status = notificationRequest.response.result.code;
                            quotation.operationDescription = notificationRequest.response.result.description;
                        }
                    }

                    if (found) break;
                }
            });

            $scope.$watchCollection("filters", function () {
                $scope.loadTable();
            });

            $scope.loadTable = function () {
                alertService.block();

                var beginDate = moment($scope.filters.beginDate).set('hour', 0).set('minute', 0).set('second', 0);
                var endDate = moment($scope.filters.endDate).set('hour', 23).set('minute', 59).set('second', 59);

                autoQuotationService.getQuotations(beginDate, endDate, $scope.filters.text,
                        $scope.pageLength, $scope.pageLength * $scope.currentPage).then(function (response) {
                            $scope.lastQuotations = response;
                            $scope.count = response.length;
                            alertService.unblock();
                        }, function () {
                            alertService.unblock();
                        });
            };

            $scope.loadTable();
        }]);
})();