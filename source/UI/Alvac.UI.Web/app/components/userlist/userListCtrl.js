﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('userListCtrl', ['$rootScope', '$scope', 'userService', '$location', 'alertService',
        function ($rootScope, $scope, userService, $location, alertService) {

            alertService.block();
            $scope.pageTitle = "Gerenciamento de usuários";

            $scope.goToDetails = function (id) {
                $location.path("/corretora/usuarios/cadastro/" + id);
            };

            $scope.loadTable = function () {
                userService.getUsers().then(function (response) {
                    $scope.users = response;
                    alertService.unblock();
                }, handleError);
            };

            $scope.delete = function (user) {
                alertService.confirm("Confirmação de exclusão", "Tem certeza que deseja deletar esse usuário?", function () {
                    alertService.block();

                    userService.remove(user.id).then(function () {
                        alertService.success("Usuário deletado com sucesso.");
                        $scope.users.remove(user);
                        alertService.unblock();
                    }, handleError);
                });
            };

            $scope.loadTable();

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.data.message);
                }
            }

        }]);
})();