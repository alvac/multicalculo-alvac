﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('navbarCtrl', ['$rootScope', '$scope', 'authService', '$location', '$window',
        function ($rootScope, $scope, authService, $location, $window) {
            $scope.authentication = authService.authentication;

            $scope.logOut = function () {
                authService.logOut();
                $window.location.href = "/auth";
            };

            $scope.menu = [
                {
                    "title": "<i class='glyphicon glyphicon-home'></i> Página Inicial",
                    "href": "/dashboard"
                },
                {
                    "title": "<i class='glyphicon glyphicon-list-alt'></i> Cotações <span class='caret'></span>",
                    "submenu": [
                        {
                            "title": "Nova Cotação",
                            "href": "/cotacao/automovel/cadastro"
                        },
                        {
                            "title": "Minhas Cotações",
                            "href": "/cotacao/automovel"
                        }]
                },
                {
                    "title": "<i class='glyphicon glyphicon-briefcase'></i> Corretora <span class='caret'></span>",
                    "submenu": [
                          {
                              "title": "Dados cadastrais",
                              "href": "/corretora/cadastro",
                          },
                          {
                              "title": "Gerenciamento de usuários",
                              "href": "/corretora/usuarios",
                          },
                          {
                              "title": "Contrato",
                              "href": "/corretora/contrato"
                          }
                    ]
                },
                {
                    "title": "<i class='glyphicon glyphicon-cog'></i> Configurações <span class='caret'></span>",
                    "submenu": [
                        {
                            "title": "Seguradoras",
                            "href": "/configuracao/seguradoras/templates",
                        },
                        {
                            "title": "Relatórios",
                            "href": "/configuracao/relatorios"
                        },
                        {
                            "title": "Email de Proposta",
                            "href": "/configuracao/email"
                        }]
                },
                {
                    "title": "<i class='glyphicon glyphicon-stats'></i> Relatórios <span class='caret'></span>",
                    "submenu": [
                        {
                            "title": "Geral",
                            "href": "/relatorios/geral"
                        },
                        {
                            "title": "Usuários",
                            "href": "/relatorios/usuario"
                        }]
                },

                {
                    "title": "<i class='glyphicon glyphicon-question-sign'></i> Service Desk",
                    "href": "/service-desk",
                    "class": "visible-lg-block"
                }
            ];
        }]);
})();