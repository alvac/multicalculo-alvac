﻿(function () {
    'use strict';

    angular.module('AlvacApp').directive('menu', function () {
        return {
            restrict: 'A',
            scope: {
                menu: '=menu',
                cls: '=ngClass'
            },
            replace: true,
            template: '<ul><li ng-repeat="item in menu" menu-item="item"></li></ul>',
            link: function (scope, element, attrs) {
                element.addClass(attrs.class);
                element.addClass(scope.cls);
            }
        };
    });

    angular.module('AlvacApp').directive('menuItem', ["$compile", function ($compile) {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                item: '=menuItem'
            },
            template: '<li active-link={{item.navBar}} class={{item.class}}><a href={{item.href}} ng-bind-html="item.title"></a></li>',
            link: function (scope, element, attrs) {

                if (scope.item.roles && scope.item.roles.indexOf(scope.$root.authentication.role) == -1) {
                    element.empty();
                    return;
                }

                if (scope.item.header) {
                    element.addClass('nav-header');
                    element.text(scope.item.header);
                }
                if (scope.item.divider) {
                    element.addClass('divider');
                    element.empty();
                }
                if (scope.item.submenu) {
                    element.addClass('dropdown');

                    element.empty();
                    var $a = $('<a class="dropdown-toggle cursor" data-toggle="dropdown" role="button" aria-expanded="false" ng-bind-html="item.title"></a>');
                    element.append($a);

                    var $submenu = $('<div menu="item.submenu" class="dropdown-menu"></div>');
                    element.append($submenu);
                }

                if (scope.item.click) {
                    element.find('a').attr('ng-click', 'item.click()');
                }

                $compile(element.contents())(scope);
            }
        };
    }]);

    angular.module('AlvacApp').directive('activeLink', ['$location', function (location) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, controller) {
                var anchor = element.children('a');
                scope.location = location;

                var navBar = angular.element(".navbar");

                scope.$watch('location.path()', function (newPath) {
                    if (anchor.attr("href") === newPath) {

                        //METRO MENU
                        //navBar.removeClass();
                        //navBar.addClass("navbar");
                        //if (attrs.activeLink) {
                        //    navBar.addClass(attrs.activeLink);
                        //}
                        //else {
                        //    navBar.addClass("navbar-default");
                        //}


                        var submenu = element.parent();
                        if (submenu.hasClass('dropdown-menu')) {
                            submenu.parent().addClass('active');
                            element.addClass('active');
                        }
                        else {
                            element.addClass('active');
                        }
                    } else {
                        element.removeClass('active');
                    }
                });
            }
        };
    }]);

})();