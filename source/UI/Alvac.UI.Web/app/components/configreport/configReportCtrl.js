﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('configReportCtrl', ['$rootScope', '$scope', 'accountConfiguration', 'alertService', 'accountConfigurationService',
        function ($rootScope, $scope, accountConfiguration, alertService, accountConfigurationService) {

            $scope.model = accountConfiguration;

            $scope.save = function () {
                alertService.block();

                if ($scope.model.accountId) {
                    accountConfigurationService.updateReport($scope.model).then(function (response) {
                        alertService.success("Configuração salva.");
                        alertService.unblock();
                    }, handleError);
                }
                else {
                    accountConfigurationService.createReport($scope.model).then(function (response) {
                        alertService.success("Configuração salva.");
                        alertService.unblock();
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.unblock();
            }
        }
    ]);
})();