﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('contractCtrl', ['$rootScope', '$scope', 'accountService', 'alertService', '$window',
        function ($rootScope, $scope, accountService, alertService, $window) {

            $scope.download = function () {
                alertService.block();

                accountService.getContract().then(function (data) {
                    alertService.unblock();

                    var file = new Blob([data], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);
                    $window.open(fileURL);
                }, function () {
                    alertService.unblock();
                });
            }
        }]);
})();