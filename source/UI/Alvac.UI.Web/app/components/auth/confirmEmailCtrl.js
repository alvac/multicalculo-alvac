﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('confirmEmailCtrl', ['$rootScope', '$scope', 'authService', 'alertService', '$location', '$routeParams', 'userService',
        function ($rootScope, $scope, authService, alertService, $location, $routeParams, userService) {

            $scope.model = {};
            $scope.model.token = $routeParams.token;
            $scope.model.email = $routeParams.email;

            $scope.confirmEmail = function () {

                if ($scope.form.$valid) {
                    alertService.block();
                    userService.confirmEmail($scope.model).then(function (response) {
                        alertService.unblock();
                        authService.login({ userName: $scope.model.email, password: $scope.model.password }).then(function () {
                            $location.path("/");
                        }, function (err) {
                            alertService.error(err);
                            alertService.unblock();
                        });

                    }, function () {
                        alertService.unblock();
                    });
                }
            };
        }]);
})();