﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('authService', ['$http', 'settings', '$q', 'localStorageService', '$rootScope', '$location',
        function ($http, settings, $q, localStorageService, $rootScope, $location) {

            var serviceBase = settings.apiBaseUri;

            var _authentication = {
                isAuth: false,
                userName: "",
                useRefreshTokens: false,
                role: "",
                isMaster: function () {
                    return this.role.indexOf("Master") != -1;
                }
            };

            var _saveRegistration = function (registration) {

                _logOut();

                return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
                    return response;
                });
            };

            var _login = function (loginData) {

                var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

                if (loginData.useRefreshTokens) {
                    data = data + "&client_id=self";
                }

                var deferred = $q.defer();

                $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                    if (loginData.useRefreshTokens) {
                        localStorageService.set('authorizationData', {
                            token: response.access_token,
                            userName: loginData.userName,
                            refreshToken: response.refresh_token,
                            useRefreshTokens: true,
                            role: response.role
                        });
                    }
                    else {
                        localStorageService.set('authorizationData', {
                            token: response.access_token,
                            userName: response.userName,
                            refreshToken: "",
                            useRefreshTokens: false,
                            role: response.role
                        });
                    }

                    _authentication.isAuth = true;
                    _authentication.userName = response.userName;
                    _authentication.role = response.role;
                    _authentication.isAdmin = response.role.indexOf("Master") != -1;
                    _authentication.useRefreshTokens = loginData.useRefreshTokens;
                    deferred.resolve(response);

                }).error(function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });

                return deferred.promise;

            };

            var _logOut = function () {

                localStorageService.remove('authorizationData');

                _authentication.isAuth = false;
                _authentication.userName = "";
                _authentication.useRefreshTokens = false;
                _authentication.role = "";
            };

            var _fillAuthData = function () {
                if (!_authentication.isAuth) {
                    var authData = localStorageService.get('authorizationData');
                    if (authData) {
                        _authentication.isAuth = true;
                        _authentication.userName = authData.userName;
                        _authentication.role = authData.role;
                        _authentication.useRefreshTokens = authData.useRefreshTokens;
                        $rootScope.$emit("loadSignalr");
                    }
                    else {
                        _logOut();
                    }
                }
            };

            var _isAuth = function () {
                if (!_authentication || !_authentication.isAuth) {
                    _logOut();
                    $location.path('/auth');
                    return false;
                }

                return true;
            };

            var _refreshToken = function () {
                var deferred = $q.defer();

                var authData = localStorageService.get('authorizationData');

                if (authData) {

                    if (authData.useRefreshTokens) {

                        var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=self";

                        localStorageService.remove('authorizationData');

                        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });

                            deferred.resolve(response);

                        }).error(function (err, status) {
                            _logOut();
                            deferred.reject(err);
                        });
                    }
                }

                return deferred.promise;
            };

            return {
                saveRegistration: _saveRegistration,
                login: _login,
                logOut: _logOut,
                fillAuthData: _fillAuthData,
                authentication: _authentication,
                refreshToken: _refreshToken,
                isAuth: _isAuth
            };
        }]);
})();
