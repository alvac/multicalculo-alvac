﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('authCtrl', ['$rootScope', '$scope', 'authService', 'alertService', '$location',
        function ($rootScope, $scope, authService, alertService, $location) {

            $scope.signIn = function () {
                if ($scope.form.$valid) {
                    alertService.block();
                    authService.login($scope.model).then(function (response) {
                        $location.path("/");
                    }, function (err) {
                        alertService.error(err);
                        alertService.unblock();
                    });
                }
            };
        }]);
})();