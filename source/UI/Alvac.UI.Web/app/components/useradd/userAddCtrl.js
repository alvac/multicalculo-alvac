﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('userAddCtrl', ['$rootScope', '$scope', 'userService', '$location', 'alertService', 'roles',
        function ($rootScope, $scope, userService, $location, alertService, roles) {

            $scope.pageTitle = "Cadastro de usuário";

            $scope.roles = roles;
            $scope.model = {
                isActive: true
            };

            $scope.save = function () {

                $scope.form.submitted = true;

                if ($scope.form.$valid) {
                    userService.create($scope.model).then(function (response) {
                        $location.path("/corretora/usuarios");
                        alertService.success("Usuário criado com sucesso.");
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.message);
                }
            }
        }]);
})();