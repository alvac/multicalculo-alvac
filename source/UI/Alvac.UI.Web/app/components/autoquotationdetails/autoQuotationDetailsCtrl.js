﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('autoQuotationDetailsCtrl', ['$rootScope', '$scope', 'alertService', 'autoQuotationService', '$modal',
        'quotations', '$routeParams', 'emailService',
        function ($rootScope, $scope, alertService, autoQuotationService, $modal,
            quotations, $routeParams, emailService) {

            $scope.quotationId = $routeParams.id;
            $scope.pageTitle = "Resultado da cotação";

            $scope.selectedResponse = [];
            $scope.model = {
                insurerQuotations: []
            };
            $scope.openEmail = function () {
                $scope.modal = $modal.open({
                    templateUrl: 'send-email.html',
                    scope: $scope
                });
            };

            $scope.closeEmail = function () {
                $scope.modal.dismiss('cancel');
            }

            $scope.sendEmail = function () {
                alertService.block();

                for (var i = 0; i < $scope.selectedResponse.length; i++) {
                    $scope.model.insurerQuotations.push($scope.selectedResponse[i].insurerQuotationId);
                }

                emailService.sendEmail($scope.model).then(function () {
                    alertService.unblock();
                    alertService.success("Email enviado!");
                    $scope.modal.dismiss('email sent');
                }, function () {
                    alertService.unblock();
                });
            };

            $rootScope.$on("quotationResponse", function (event, notificationRequest) {
                for (var index = 0; index < $scope.quotations.length; index++) {
                    if ($scope.quotations[index].insurerQuotationId == notificationRequest.response.identifier) {

                        var quotation = $scope.quotations[index];

                        quotation.status = notificationRequest.response.result.code;
                        quotation.operationDescription = notificationRequest.response.result.description;

                        if (quotation.status == 'Success') {
                            quotation.insurerBudgetCode = notificationRequest.response.insurerBudgetCode;
                            quotation.premiumValue = notificationRequest.response.premiumValue;
                            quotation.deductibleValue = notificationRequest.response.deductibleValue;
                            quotation.materialDamages = notificationRequest.response.coverage.materialDamages;
                            quotation.bodyDamages = notificationRequest.response.coverage.moralDamages;
                            quotation.moralDamages = notificationRequest.response.coverage.bodyDamages;
                            quotation.personalAccidentPassenger = notificationRequest.response.coverage.personalAccidentPassenger;
                            alertService.success("A cotação da " + quotation.insurer + " foi concluida.");
                        }

                        break;
                    }
                }
            });

            $scope.quotations = quotations;
        }]);
})();