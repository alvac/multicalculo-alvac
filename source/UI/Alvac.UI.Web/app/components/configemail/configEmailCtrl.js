﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('configEmailCtrl', ['$rootScope', '$scope', 'accountConfiguration', 'alertService', 'accountConfigurationService',
        function ($rootScope, $scope, accountConfiguration, alertService, accountConfigurationService) {

            $scope.model = accountConfiguration;

            $scope.save = function () {
                alertService.block();

                if ($scope.model.accountId) {
                    accountConfigurationService.updateEmail($scope.model).then(function (response) {
                        alertService.success("Configuração salva.");
                        alertService.unblock();
                    }, handleError);
                }
                else {
                    accountConfigurationService.createEmail($scope.model).then(function (response) {
                        alertService.success("Configuração salva.");
                        alertService.unblock();
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.warning("Ocorreu alguma falha para atualizar, tente novamente em alguns minutos.");
                alertService.unblock();
            }
        }
    ]);
})();