﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('reportAllUsersCtrl', ['$rootScope', '$scope', 'reportService', '$location', 'alertService',
        function ($rootScope, $scope, reportService, $location, alertService) {

            $scope.query = {};

            $scope.generateReport = function () {
                alertService.block();
                reportService.getAutoQuotationGeneral($scope.query).then(function (response) {

                    $scope.totalPieLabels = [];
                    $scope.totalPieData = [];

                    for (var i = 0; i < response.insurerQuotations.length; i++) {
                        $scope.totalPieLabels.push(response.insurerQuotations[i].insurerDisplay);
                        $scope.totalPieData.push(response.insurerQuotations[i].count);
                    }

                    $scope.statusPieLabels = [];
                    $scope.statusPieData = [];

                    for (var i = 0; i < response.insurerQuotationsStatus.length; i++) {
                        $scope.statusPieLabels.push(response.insurerQuotationsStatus[i].descriptionCode);
                        $scope.statusPieData.push(response.insurerQuotationsStatus[i].count);
                    }

                    $scope.report = response;

                    alertService.unblock();
                });
            };
        }]);
})();