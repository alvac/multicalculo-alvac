﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('reportByUsersCtrl', ['$rootScope', '$scope', 'reportService', '$location', 'alertService',
        function ($rootScope, $scope, reportService, $location, alertService) {

            $scope.query = {};

            $scope.generateReport = function () {
                $scope.report = {};
                alertService.block();

                var successQuery = angular.copy($scope.query);
                successQuery.status = 4; //SUCCESS

                $scope.successData = [];
                $scope.successLabels = [];
                reportService.getAutoQuotationGroupByUser(successQuery).then(function (response) {
                    $scope.report.success = response;

                    for (var i = 0; i < response.length; i++) {
                        $scope.successLabels.push(response[i].userName);
                        $scope.successData.push(response[i].count);
                    }

                    alertService.unblock();
                });

                $scope.errorData = [];
                $scope.errorLabels = [];

                var errorQuery = angular.copy($scope.query);
                errorQuery.status = 5; //FAILURE

                reportService.getAutoQuotationGroupByUser(errorQuery).then(function (response) {
                    $scope.report.error = response;

                    for (var i = 0; i < response.length; i++) {
                        $scope.errorLabels.push(response[i].userName);
                        $scope.errorData.push(response[i].count);
                    }

                    alertService.unblock();
                });
            };
        }]);
})();