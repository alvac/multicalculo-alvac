﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('accountCtrl', ['$rootScope', '$scope', '$location', 'accountService', 'account', 'alertService',
        function ($rootScope, $scope, $location, accountService, account, alertService) {
            $scope.pageTitle = "Dados cadastrais";

            $scope.model = account;

            $scope.update = function () {

                $scope.accountForm.submitted = true;

                if ($scope.accountForm.$valid) {
                    alertService.block();

                    accountService.update($scope.model).then(function (response) {
                        alertService.success("Conta editada com sucesso!");
                        alertService.unblock();
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.message);
                }
            }
        }]);
})();