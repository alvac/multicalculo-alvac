﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('templateListCtrl', ['$rootScope', '$scope', 'templates', 'insurers', '$modal', 'templateService', 'alertService',
        function ($rootScope, $scope, templates, insurers, $modal, templateService, alertService) {
            $scope.templates = templates;
            $scope.insurers = insurers;

            $scope.delete = function (template) {
                alertService.confirm("Confirmação de exclusão", "Tem certeza que deseja deletar esse template?", function () {
                    alertService.block();

                    templateService.remove(template.accountInsurerTemplateId).then(function () {
                        alertService.success("Template deletado com sucesso.");
                        $scope.templates.remove(template);

                        alertService.unblock();
                    }, handleError);
                });
            };

            angular.forEach($scope.templates, function (value, key) {
                $scope.$watch("templates[" + key + "]", function (newValue, oldValue) {
                    if (newValue.isActive != oldValue.isActive) {
                        alertService.block();
                        templateService.putStatusTemplate(newValue.accountInsurerTemplateId, newValue.isActive).then(function () {
                            alertService.unblock();
                        }, function () {
                            alertService.unblock();
                        });
                    }
                }, true);
            });

            $scope.newTemplate = function () {
                var newModalInstance = $modal.open({
                    templateUrl: '/app/components/templateadd/templateAddView.html',
                    controller: 'templateAddCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    keyboard: false
                });

                newModalInstance.result.then(function (item) {
                    $scope.templates.unshift(item);
                }, function () {
                });
            };

            $scope.openTemplateConfiguration = function (insurer, selectedTemplate) {
                alertService.block();

                templateService.getInsurerParameters(insurer.id, selectedTemplate.accountInsurerTemplateId).then(function (response) {
                    insurer.parameters = response;
                    alertService.unblock();

                    var modalInstance = $modal.open({
                        templateUrl: '/app/components/templateconfiguration/templateConfigurationView.html',
                        controller: 'templateConfigurationCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            insurer: function () {
                                return insurer;
                            }
                        }
                    });

                    modalInstance.result.then(function (item) {
                        alertService.block();
                        templateService.postParameters(item.parameters).then(function (response) {
                            alertService.unblock();
                        }, handleError);

                    }, function () {

                    });

                }, handleError);
            };

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.message);
                }
            }

        }]);
})();