﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('additionalDriverCtrl', ['$modalInstance', '$scope', 'item', function ($modalInstance, $scope, item) {

        $scope.item = angular.copy(item);

        $scope.ok = function () {
            $scope.additionalDriverForm.submitted = true;

            if ($scope.additionalDriverForm.$valid) {
                $modalInstance.close($scope.item);
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
})();