﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('dependentCtrl', ['$modalInstance', '$scope', 'item', function ($modalInstance, $scope, item) {

        $scope.item = angular.copy(item);

        $scope.ok = function () {
            $scope.dependentForm.submitted = true;

            if ($scope.dependentForm.$valid) {
                $modalInstance.close($scope.item);
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
})();