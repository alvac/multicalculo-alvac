﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('autoQuotationCtrl',
        ['$rootScope', '$scope', 'accountConnectionService', 'alertService', 'manufacturers', 'autoQuotationService', '$modal', '$location', 'workActivities', 'authService', 'templates',
            'insurers', 'templateService', 'deviceTrackers', 'ignitionBlockers',
        function ($rootScope, $scope, accountConnectionService, alertService, manufacturers, autoQuotationService, $modal, $location, workActivities, authService, templates,
            insurers, templateService, deviceTrackers, ignitionBlockers) {

            $scope.pageTitle = "Nova cotação";

            $scope.tabs = {
                vehicle: true,
                mainDriver: false,
                insured: false,
                vehicleOwner: false,
                insurance: false,
                insurers: false
            };

            $scope.previouslyInsurers = insurers;
            $scope.templates = templates;

            $scope.loadParamaters = function ($event, insurer, selectedTemplate) {
                var checkbox = $event.target;
                if (checkbox.checked) {
                    alertService.block();
                    templateService.getInsurerParameters(insurer.id, selectedTemplate.accountInsurerTemplateId).then(function (response) {
                        insurer.parameters = response;
                    }, handleError);

                    autoQuotationService.getInsurerVehicles(insurer.id, $scope.selected.Vehicle.vehicleId).then(function (response) {
                        insurer.vehicles = response;

                        if (response.length == 1) {
                            insurer.vehicle = response[0].vehicleId;
                        }

                        alertService.unblock();
                    }, handleError);

                }
                else {
                    insurer.parameters = null;
                    insurer.vehicles = null;
                }
            };

            $scope.openTemplateConfiguration = function (insurer) {
                var modalInstance = $modal.open({
                    templateUrl: '/app/components/templateconfiguration/templateConfigurationView.html',
                    controller: 'templateConfigurationCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        insurer: function () {
                            return insurer;
                        }
                    }
                });

                modalInstance.result.then(function (item) {
                    insurer.parameters = item.parameters;
                }, function () {
                });
            };

            $scope.formProgressBar = 0;

            $scope.model = {
                InsurerRequestList: [],
                DeductibleTypeList: [],
                Request: {
                    VehicleInfo: {
                        Value: {
                            IsFinanced: false,
                        },
                        License: {},
                        IsNew: false,
                        HasNaturalGasKit: false,
                        IsArmored: false,

                    },
                    VehicleUse: {
                        UseType: 0,
                        GarageLocal: 0,
                        VehiclesNumbers: 1
                    },
                    MainDriver: {
                        DependentList: []
                    },
                    Insured: {},
                    VehicleOwner: {},
                    Insurance: {
                        CoveragePlanType: 1,
                        Renewal: {},
                        InsurancePolicyInitialDate: new Date()
                    },
                    AdditionalDriverList: []
                }
            };

            $scope.currentInsurancePolicyDateMin = moment().subtract(365, 'days').format('YYYY-MM-DD');
            $scope.currentInsurancePolicyDateMax = moment().add(45, 'days').format('YYYY-MM-DD');

            $scope.insurancePolicyInitialDateMin = moment().subtract(7, 'days').format('YYYY-MM-DD');
            $scope.insurancePolicyInitialDateax = moment().add(45, 'days').format('YYYY-MM-DD');

            $scope.$watch("model.Request.Insurance.InsurancePolicyInitialDate", function (newValue) {
                if (newValue) {
                    var date = new Date(newValue);
                    date.setFullYear(date.getFullYear() + 1);
                    $scope.model.Request.Insurance.InsurancePolicyFinalDate = date;
                }
            });

            $scope.$watch("model.Request.Insurance.Renewal.CurrentInsurancePolicyDate", function (newValue) {
                if (newValue) {
                    var date = new Date(newValue);
                    date.setFullYear(date.getFullYear() + 1);
                    $scope.model.Request.Insurance.Renewal.CurrentInsurancePolicyExpirationDate = date;
                }
            });

            $scope.workActivities = workActivities;
            $scope.devicesIgnitionBlockers = ignitionBlockers;
            $scope.devicesTrackers = deviceTrackers;

            $scope.selectedInsurers = [];

            $scope.toggleGarage = function ($event) {
                var checkbox = $event.target;
                if (checkbox.checked) {
                    $scope.model.Request.VehicleUse.GarageLocal += parseInt(checkbox.value);
                }
                else {
                    $scope.model.Request.VehicleUse.GarageLocal -= parseInt(checkbox.value);
                }
            };

            $scope.toggleUseType = function ($event) {
                var checkbox = $event.target;
                if (checkbox.checked) {
                    $scope.model.Request.VehicleUse.UseType += parseInt(checkbox.value);
                }
                else {
                    $scope.model.Request.VehicleUse.UseType -= parseInt(checkbox.value);
                }
            };

            $scope.deductibleTypes = [
                { id: 1, name: "Obrigatória" },
                { id: 2, name: "Reduzida 50%" },
                { id: 3, name: "Reduzida 25%" },
                { id: 4, name: "Facultativa 100%" },
                { id: 5, name: "Facultativa 50%" }
            ];

            $scope.manufacturers = manufacturers;
            $scope.selected = {};

            $scope.$watch("selected.Brand", function (newValue) {
                if (newValue) {
                    $scope.model.Request.VehicleInfo.Brand = newValue.name;
                    $scope.selected.Vehicle = null;
                    $scope.model.Request.VehicleInfo.ModelYear = "";
                }
            });

            $scope.manufacturingYears = [];
            var year = new Date().getFullYear();
            for (var i = 0; i < 15; i++) {
                $scope.manufacturingYears.push(year - i);
            }

            $scope.$watch("selected.ManufacturingYear", function (newValue) {
                $scope.model.Request.VehicleInfo.ModelYear = "";
                $scope.selected.Vehicle = null;
            });

            $scope.$watch("model.Request.VehicleInfo.ModelYear", function (newValue) {

                if (newValue) {
                    alertService.block();
                    autoQuotationService.getVehicles(newValue, encodeURIComponent($scope.selected.Brand.manufacturerId)).then(function (response) {
                        if (response && response.length > 0) {
                            $scope.vehicles = response;
                        }
                        else {
                            alertService.warning("Nenhum modelo encontrado.");
                        }

                        alertService.unblock();
                    }, handleError);
                }

                $scope.selected.Vehicle = null;
                $scope.model.Request.VehicleInfo.FipeCode = null;
                $scope.model.Request.VehicleInfo.Fuel = null;
                $scope.model.Request.VehicleInfo.Value.ReferencedValue = null;
            });

            $scope.$watch("selected.Vehicle", function (newValue) {
                if (newValue) {
                    $scope.model.Request.VehicleInfo.FipeCode = newValue.fipeCode;
                    $scope.model.Request.VehicleInfo.Fuel = newValue.fuel;
                    $scope.model.Request.VehicleInfo.Model = newValue.model;
                    $scope.model.Request.VehicleInfo.Value.ReferencedValue = newValue.vehiclePrices.filter(function (element) {
                        return element.year == $scope.model.Request.VehicleInfo.ModelYear;
                    })[0].price;

                    $scope.vehicleZero = newValue.vehiclePrices.filter(function (element) {
                        return element.year === 0;
                    })[0];

                    if (!$scope.vehicleZero) {
                        model.Request.VehicleInfo.IsNew = false;
                    }
                }
            });

            $scope.$watch("model.Request.VehicleInfo.IsNew", function (newValue) {
                $scope.model.Request.VehicleInfo.FipeCode = $scope.selected.Vehicle.fipeCode;
                $scope.model.Request.VehicleInfo.Fuel = $scope.selected.Vehicle.fuel;
                $scope.model.Request.VehicleInfo.Model = $scope.selected.Vehicle.model;

                if (newValue && newValue == "true") {
                    $scope.model.Request.VehicleInfo.Value.ReferencedValue = $scope.vehicleZero.price;
                }
                else if ($scope.selected.Vehicle) {
                    $scope.model.Request.VehicleInfo.Value.ReferencedValue = $scope.selected.Vehicle.vehiclePrices.filter(function (element) {
                        return element.year == $scope.model.Request.VehicleInfo.ModelYear;
                    })[0].price;
                }
            });

            $scope.$watch("model.Request.VehicleInfo.HasNaturalGasKit", function () {
                $scope.model.Request.VehicleInfo.NaturalGasKitCoverage = null;
            });

            $scope.$watch("model.Request.VehicleInfo.IsArmored", function () {
                $scope.model.Request.VehicleInfo.ArmoryCoverage = null;
            });

            $scope.$watch("model.Request.VehicleInfo.Value.ValueCategory", function () {
                $scope.model.Request.VehicleInfo.Value.DeterminedValue = null;
                $scope.model.Request.VehicleInfo.Value.ReferencedPercentage = null;
            });


            accountConnectionService.getInsurers('AutoQuotation').then(function (response) {
                $scope.insurers = response;
            });

            $scope.openModalDependent = function (item) {
                var modalInstance = $modal.open({
                    templateUrl: '/app/components/autoquotation/dependentView.html',
                    controller: 'dependentCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            return item;
                        }
                    }
                });

                modalInstance.result.then(function (addedDependent) {
                    if (addedDependent.index >= 0) {
                        $scope.model.Request.MainDriver.DependentList[addedDependent.index] = addedDependent;
                    }
                    else {
                        $scope.model.Request.MainDriver.DependentList.push(addedDependent);
                        var index = $scope.model.Request.MainDriver.DependentList.indexOf(addedDependent);
                        addedDependent.index = index;
                    }
                }, function () {
                });
            };

            $scope.removeDependent = function (item) {
                var index = $scope.model.Request.MainDriver.DependentList.indexOf(item);
                $scope.model.Request.MainDriver.DependentList.splice(index, 1);
            };

            $scope.openModalAdditionalDriver = function (item) {
                var modalInstance = $modal.open({
                    templateUrl: '/app/components/autoquotation/additionalDriverView.html',
                    controller: 'additionalDriverCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            return item;
                        }
                    }
                });

                modalInstance.result.then(function (additionalDriver) {
                    if (additionalDriver.index >= 0) {
                        $scope.model.Request.AdditionalDriverList[additionalDriver.index] = additionalDriver;
                    }
                    else {
                        $scope.model.Request.AdditionalDriverList.push(additionalDriver);
                        var index = $scope.model.Request.AdditionalDriverList.indexOf(additionalDriver);
                        additionalDriver.index = index;
                    }
                }, function () {
                });
            };

            $scope.removeAdditionalDriver = function (item) {
                var index = $scope.model.Request.AdditionalDriverList.indexOf(item);
                $scope.model.Request.AdditionalDriverList.splice(index, 1);
            };

            $scope.isVehicleValid = function () {
                $scope.form.vehicleForm.submitted = true;

                if ($scope.form.vehicleForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 16;
                    $scope.tabs.mainDriver = true;
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.isMainDriverValid = function () {
                $scope.form.mainDriverForm.submitted = true;

                if ($scope.form.mainDriverForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 32;
                    $scope.tabs.insured = true;

                    if ($scope.model.Request.MainDriver.IsInsured == "true") {
                        $scope.model.Request.Insured.PersonType = 1;
                        $scope.model.Request.Insured.FullName = $scope.model.Request.MainDriver.FullName;
                        $scope.model.Request.Insured.Gender = $scope.model.Request.MainDriver.Gender;
                        $scope.model.Request.Insured.BirthDate = $scope.model.Request.MainDriver.BirthDate;
                        $scope.model.Request.Insured.Document = $scope.model.Request.MainDriver.Document;
                        $scope.model.Request.Insured.MaritalStatus = $scope.model.Request.MainDriver.MaritalStatus;
                        $scope.model.Request.Insured.ZipCode = $scope.model.Request.MainDriver.ZipCode;
                        $scope.model.Request.Insured.WorkActivityId = $scope.model.Request.MainDriver.WorkActivityId;
                    }

                    if ($scope.model.Request.MainDriver.IsVehicleOwner == "true") {
                        $scope.model.Request.VehicleOwner.PersonType = "1";
                        $scope.model.Request.VehicleOwner.FullName = $scope.model.Request.MainDriver.FullName;
                        $scope.model.Request.VehicleOwner.Gender = $scope.model.Request.MainDriver.Gender;
                        $scope.model.Request.VehicleOwner.BirthDate = $scope.model.Request.MainDriver.BirthDate;
                        $scope.model.Request.VehicleOwner.Document = $scope.model.Request.MainDriver.Document;
                        $scope.model.Request.VehicleOwner.MaritalStatus = $scope.model.Request.MainDriver.MaritalStatus;
                        $scope.model.Request.VehicleOwner.ZipCode = $scope.model.Request.MainDriver.ZipCode;
                    }
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.isInsuredValid = function () {
                $scope.form.insuredForm.submitted = true;

                if ($scope.form.insuredForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 48;
                    $scope.tabs.vehicleOwner = true;
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.isVehicleOwnerValid = function () {
                $scope.form.vehicleOwnerForm.submitted = true;

                if ($scope.form.vehicleOwnerForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 64;
                    $scope.tabs.insurance = true;
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.isInsuranceValid = function () {
                $scope.form.insuranceForm.submitted = true;

                if ($scope.form.insuranceForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 80;
                    $scope.tabs.insurers = true;
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.quotation = function () {
                if ($scope.form.$valid) {
                    alertService.block();

                    $scope.model.Request.VehicleInfo.Value.ReferencedPercentage = $scope.model.Request.VehicleInfo.Value.ReferencedPercentage * 100;

                    $scope.model.InsurerRequestList = [];

                    angular.forEach($scope.selectedInsurers, function (value, key) {
                        var parameters = {};

                        angular.forEach(value.parameters, function (item, paramKey) {
                            parameters[item.key] = item.value;
                        });

                        parameters["vehicle"] = value.vehicle;

                        $scope.model.InsurerRequestList.push({ Insurer: value.id, Parameters: parameters });
                    });

                    $scope.model.Request.Notification = {
                        CorrelationId: authService.authentication.userName,
                        CallType: 1,
                        Path: $location.protocol() + '://' + $location.host() + "/api/notification"
                    };

                    autoQuotationService.quotation($scope.model).then(function (response) {
                        $location.path("/cotacao/automovel/" + response.quotationId);
                        alertService.success("Seu cálculo esta sendo processado.");
                    }, function (err) {
                        alertService.error('Ocorreu algum erro tenta novamente');
                        alertService.unblock();
                    });
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            function scrollTop() {
                $("html, body").animate({ scrollTop: 0 }, 200);
            }

            function handleError(response) {
                alertService.unblock();
                alertService.error('Ocorreu alguma falha, favor tentar novamente.');
            }
        }]);
})();