﻿(function () {
    'use strict';

    angular.module('AlvacApp').controller('templateAddCtrl', ['$rootScope', '$scope', 'templateService', 'alertService', '$modalInstance',
        function ($rootScope, $scope, templateService, alertService, $modalInstance) {
            $scope.ok = function () {
                $scope.templateForm.submitted = true;

                if ($scope.templateForm.$valid) {
                    alertService.block();

                    templateService.create($scope.model).then(function (response) {
                        $scope.model.accountInsurerTemplateId = response.data.id;
                        $modalInstance.close($scope.model);
                        alertService.success("Template criado com sucesso");
                        alertService.unblock();
                    }, handleError);
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            function handleError(response) {
                alertService.unblock();
                alertService.error(respose.message);
            }
        }
    ]);
})();