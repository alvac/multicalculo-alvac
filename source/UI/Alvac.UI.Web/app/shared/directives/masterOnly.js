﻿(function () {
    'use strict';

    angular.module('AlvacApp')
        .directive('masterOnly', [function () {
            return {
                restrict: 'A',
                controller: 'initCtrl',
                link: function (scope, element, attrs, modelCtrl) {

                    if (attrs.masterOnly == "" || attrs.masterOnly == "disabled") {
                        if (!scope.authentication.isMaster()) {
                            element.attr("disabled", "disabled");
                        }
                    }
                    else if (attrs.masterOnly == "show") {
                        if (!scope.authentication.isMaster()) {
                            element.css("display", "none");
                        }
                    }
                }
            };
        }]);
})();