﻿(function () {
    'use strict';

    angular.module('AlvacApp')
        .directive('jqSelect2', function () {
            return {
                restrict: 'A',
                scope: {
                    ngModel: '='
                },
                link: function ($scope, element, attrs) {
                    $(element).select2({
                        placeholder: "Selecione"
                    });

                    $(element).val($scope.ngModel);
                }
            };
        });
})();