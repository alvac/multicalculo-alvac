﻿(function () {
    'use strict';

    angular.module('AlvacApp').directive('uniqueEmail', ['$q', 'userService', function ($q, userService) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {

                ctrl.$asyncValidators.uniqueEmail = function (modelValue, viewValue) {

                    if (ctrl.$isEmpty(modelValue)) {
                        return $q.when();
                    }

                    var keyProperty = scope.$eval(attrs.uniqueEmail);
                    return userService.isUniqueEmail(modelValue, keyProperty);
                };
            }
        };
    }]);
})();