﻿(function () {
    'use strict';

    angular.module('AlvacApp')
        .directive('numberOnly', function () {
            return {
                restrict: 'A',
                scope: {},
                link: function ($scope, element, attrs) {

                    $(element).on('change keyup', function () {
                        // Remove invalid characters
                        var sanitized = $(this).val().replace(/[^-.0-9]/g, '');
                        // Remove non-leading minus signs
                        sanitized = sanitized.replace(/(.)-+/g, '$1');
                        // Remove the first point if there is more than one
                        sanitized = sanitized.replace(/\.(?=.*\.)/g, '');
                        // Update value
                        $(this).val(sanitized);
                    });
                }
            };
        });
})();