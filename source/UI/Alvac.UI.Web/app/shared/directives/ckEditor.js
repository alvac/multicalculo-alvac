﻿(function () {
    'use strict';

    angular.module('AlvacApp')
        .directive('ckEditor', function () {
            return {
                restrict: 'A',
                scope: {
                    model: '=ngModel'
                },
                link: function ($scope, element, attrs) {

                    CKEDITOR.replace(attrs.name);

                    CKEDITOR.focusManager._.blurDelay = 10;

                    CKEDITOR.instances[attrs.name].on('blur', function (e) {
                        if (e.editor.checkDirty()) {
                            $scope.$apply(function () {
                                $scope.model = e.editor.getData();
                            });
                        }
                    });
                }
            };
        });
})();