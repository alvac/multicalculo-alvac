﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('ignitionBlockerService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAll: function () {
                return $http.get(serviceBase + "ignition-blocker").then(function (response) {
                    return response.data;
                });
            },
        };
    }]);
})();