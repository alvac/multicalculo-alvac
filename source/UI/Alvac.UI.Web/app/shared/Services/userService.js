﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('userService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {

            getUser: function(id){
                return $http.get(serviceBase + "user/" + id).then(function (response) {
                    return response.data;
                });
            },

            getUsers: function () {

                return $http.get(serviceBase + "user?$filter=IsActive eq true").then(function (response) {
                    return response.data;
                });
            },

            getRoles: function () {

                return $http.get(serviceBase + "user/roles").then(function (response) {
                    return response.data;
                });
            },

            confirmEmail: function (model) {
                return $http.put(serviceBase + "user/confirm-email", model);
            },
            
            isUniqueEmail: function (email, id) {
                var url = serviceBase + 'user/email/' + encodeURIComponent(email);

                if (id) {
                    url = url + '/' + id;
                }

                return $http.get(url).then(function (results) {
                    return results.data;
                });
            },

            create: function (model) {
                return $http.post(serviceBase + "user", model);
            },

            update: function (id, model) {
                return $http.put(serviceBase + "user/" + id, model);
            },

            remove: function (id) {
                return $http.delete(serviceBase + "user/" + id);
            },
        };
    }]);
})();