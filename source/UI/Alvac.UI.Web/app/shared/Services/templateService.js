﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('templateService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getTemplates: function () {
                return $http.get(serviceBase + "template").then(function (response) {
                    return response.data;
                });
            },

            getActiveTemplates: function (requestType) {
                return $http.get(serviceBase + "template?$filter=IsActive eq true and RequestType eq Alvac.Domain.Enum.RequestType'" + requestType + "'").then(function (response) {
                    return response.data;
                });
            },

            getInsurerParameters: function (insurerId, accountInsurerTemplateId) {
                return $http.get(serviceBase + "template/" + accountInsurerTemplateId + "/insurers/" + insurerId + "/parameters").then(function (response) {
                    return response.data;
                });
            },

            postParameters: function (model) {
                return $http.post(serviceBase + "template/parameters", model);
            },

            create: function (model) {
                return $http.post(serviceBase + "template", model);
            },

            remove: function (id) {
                return $http.delete(serviceBase + "template/" + id);
            },

            putStatusTemplate: function (accountInsurerTemplateId, status) {
                return $http.put(serviceBase + "template/" + accountInsurerTemplateId + "/status",
                    { accountInsurerTemplateId: accountInsurerTemplateId, isActive: status });
            },
        };
    }]);
})();