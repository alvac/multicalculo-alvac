﻿(function () {
    'use strict';
    angular.module('AlvacApp').factory('alertService', ['toaster', function (toaster) {

        var serviceServiceFactory = {};

        var _success = function (text) {
            toaster.pop("success", "", text);
        };

        var _warning = function (text) {
            toaster.pop("warning", "", text);
        };

        var _error = function (text) {
            toaster.pop("error", "", text);
        };

        var _confirm = function (title, message, successCallback, cancelCallback) {
            bootbox.dialog({
                message: message,
                title: title,
                buttons: {
                    cancel: {
                        label: "Cancelar",
                        className: "btn-danger",
                        callback: cancelCallback
                    },
                    success: {
                        label: "Confirmar",
                        className: "btn-success",
                        callback: successCallback
                    }
                }
            });
        };

        var _block = function (element) {
            if (element) {
                $(element).block({ message: null });
            }
            else {
                var imgSrc = '/assets/img/loader.gif';
                $.blockUI({
                    message: '<img src="' + imgSrc + '" id="spinner" align="absmiddle"> Aguarde um momento...',
                    overlayCSS: {
                        backgroundColor: '#000',
                        opacity: 0.05,
                        cursor: 'wait',
                        baseZ: 10000
                    }
                });

                setTimeout("$('#spinner').attr('src', '" + imgSrc + "');", 100);
            }
        };

        var _unblock = function (element) {
            if (element) {
                $(element).unblock();
            }
            else {
                $.unblockUI();
            }
        };

        serviceServiceFactory.success = _success;
        serviceServiceFactory.warning = _warning;
        serviceServiceFactory.error = _error;
        serviceServiceFactory.confirm = _confirm;
        serviceServiceFactory.block = _block;
        serviceServiceFactory.unblock = _unblock;
        return serviceServiceFactory;
    }]);
})();