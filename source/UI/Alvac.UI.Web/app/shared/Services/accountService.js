﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('accountService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAccount: function () {
                return $http.get(serviceBase + "account/self").then(function (response) {
                    return response.data;
                });
            },

            getContract: function () {
                return $http.get(serviceBase + "account/self/contract", { responseType: 'arraybuffer' }).then(function (response) {
                    return response.data;
                });
            },

            update: function (model) {
                return $http.put(serviceBase + "account/", model);
            }

        };
    }]);
})();