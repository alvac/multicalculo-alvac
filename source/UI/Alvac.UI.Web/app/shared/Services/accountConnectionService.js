﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('accountConnectionService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {

            getInsurers: function (requestType) {

                return $http.get(serviceBase + "account/insurers/connections/" + requestType).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);

})();