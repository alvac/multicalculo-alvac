﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('reportService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAutoQuotationGeneral: function (query) {
                return $http.post(serviceBase + "report/auto-quotation/general", query).then(function (response) {
                    return response.data;
                });
            },

            getAutoQuotationGroupByUser: function (query) {
                return $http.post(serviceBase + "report/auto-quotation/user", query).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
})();