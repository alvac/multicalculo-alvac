﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('autoQuotationService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getManufacturers: function () {
                return $http.get(serviceBase + "vehicle/manufacturer", { cache: true }).then(function (response) {
                    return response.data;
                });
            },

            getVehicles: function (modelYear, manufacturer) {
                return $http.get(serviceBase + "vehicle/manufacturer/" + manufacturer + '/year/' + modelYear, { cache: true }).then(function (response) {
                    return response.data;
                });
            },

            getInsurerVehicles: function (insurer, vehicleId) {
                return $http.get(serviceBase + "vehicle/insurer/" + insurer + '/vehicle/' + vehicleId).then(function (response) {
                    return response.data;
                });
            },

            getVehiclesByModel: function (model, manufacturer) {
                return $http.get(serviceBase + "vehicle/manufacturer/" + manufacturer + '?model=' + model, { cache: true }).then(function (response) {
                    return response.data;
                });
            },

            getQuotationResponse: function (id) {
                return $http.get(serviceBase + "quotation/" + id + '/response').then(function (response) {
                    return response.data;
                });
            },

            getQuotations: function (beginDate, endDate, text, take, skip) {
                var uri = serviceBase + "quotation/auto";

                uri += "?$skip=" + (skip ? skip : 0);

                if (take) {
                    uri += "&$top=" + take;
                }

                uri += "&$filter=RequestDateTime ge " + beginDate.toJSON();
                uri += " and RequestDateTime le " + endDate.toJSON();

                if (text) {
                    uri += " and (contains(FullName, '" + text + "') or contains(Document, '" + text + "'))";
                }

                return $http.get(uri).then(function (response) {
                    return response.data;
                });
            },

            getLastQuotations: function (take, skip) {
                var uri = serviceBase + "quotation/auto";

                uri += "?$skip=" + (skip ? skip : 0);

                if (take) {
                    uri += "&$top=" + take;
                }

                return $http.get(uri).then(function (response) {
                    return response.data;
                });
            },

            quotation: function (model) {
                model.Request.requestType = "AutoQuotation";
                return $http.post(serviceBase + "quotation/auto", model).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
})();