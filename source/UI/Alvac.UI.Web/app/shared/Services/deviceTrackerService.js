﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('deviceTrackerService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAll: function () {
                return $http.get(serviceBase + "device-tracker").then(function (response) {
                    return response.data;
                });
            },
        };
    }]);
})();