﻿(function () {
    'use strict';
    angular.module('AlvacApp').factory('emailService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            sendEmail: function (model) {
                return $http.post(serviceBase + "email", model);
            }
        };

    }]);
})();