﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('insurerService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getInsurers: function () {
                return $http.get(serviceBase + "insurer", { cache: true }).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
})();