﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('accountConfigurationService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getConfiguration: function () {
                return $http.get(serviceBase + "account-configuration").then(function (response) {
                    return response.data;
                });
            },

            updateEmail: function (model) {
                return $http.put(serviceBase + "account-configuration/email", model);
            },

            createEmail: function (model) {
                return $http.post(serviceBase + "account-configuration/email", model);
            },

            updateReport: function (model) {
                return $http.put(serviceBase + "account-configuration/report", model);
            },

            createReport: function (model) {
                return $http.post(serviceBase + "account-configuration/report", model);
            }
        };
    }]);
})();