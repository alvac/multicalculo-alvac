﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('workActivityService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAll: function () {
                return $http.get(serviceBase + "work-activity", { cache: true }).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
})();