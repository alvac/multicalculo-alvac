﻿(function () {
    'use strict';

    angular.module('AlvacApp').factory('offSetInterceptorService', [function () {

        var interceptorServiceFactory = {};

        var _request = function (config) {

            config.headers = config.headers || {};
            config.headers.TimezoneOffsetMinutes = new Date().getTimezoneOffset();
            return config;
        }

        interceptorServiceFactory.request = _request;

        return interceptorServiceFactory;
    }]);
})();