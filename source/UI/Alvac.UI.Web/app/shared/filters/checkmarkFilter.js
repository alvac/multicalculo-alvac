﻿(function () {
    'use strict';

    angular.module('AlvacApp').filter('checkmark', function () {
        return function (input) {
            return input === "true" ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>';
        };
    });
})();