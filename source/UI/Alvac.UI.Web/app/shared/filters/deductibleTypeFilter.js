﻿(function () {
    'use strict';

    angular.module('AlvacApp').filter('deductibleType', function () {
        return function (input) {
            switch (input) {
                case "Required":
                    input = "Obrigatória";
                    break;
                case "Reduced50Percent":
                    input = "Reduzida 50%";
                    break;
                case "Reduced25Percent":
                    input = "Reduzida 25%";
                    break;
                case "Optional50Percent":
                    input = "Facultativa 50%";
                    break;
                case "Optional25Percent":
                    input = "Facultativa 25%";
                    break;
            }

            return input;
        };
    });
})();