﻿(function () {
    'use strict';

    angular.module('AlvacApp').config(['$routeProvider', '$compileProvider', '$locationProvider',
        function ($routeProvider, $compileProvider, $locationProvider) {
            var viewBase = '/app/components/';

            $compileProvider.debugInfoEnabled(false);
            $locationProvider.html5Mode(true);

            var extRouteProvider = angular.extend({}, $routeProvider, {
                when: function (path, route) {
                    route.resolve = (route.resolve) ? route.resolve : {};
                    angular.extend(route.resolve, {
                        isAuth: ['authService', '$q', '$location',
                            function (authService, $q, $location) {
                                var deferred = $q.defer();
                                authService.fillAuthData();

                                if (authService.isAuth() || $location.path() === "/auth") {
                                    deferred.resolve(true);
                                }
                                else {
                                    deferred.reject(false);
                                }

                                return deferred.promise;
                            }]
                    });

                    $routeProvider.when(path, route);
                    return this;
                }
            });

            extRouteProvider
                .when('/dashboard', {
                    title: 'Alvac - Cotação e Comparação Online de Seguros',
                    controller: 'homeCtrl',
                    templateUrl: viewBase + 'home/homeView.html',
                    resolve: {
                        quotations: ["autoQuotationService", function (autoQuotationService) {
                            return autoQuotationService.getLastQuotations(15, 0);
                        }]
                    }
                })
                .when('/cotacao/automovel', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'autoQuotationListCtrl',
                    templateUrl: viewBase + 'autoquotationlist/autoQuotationListView.html'
                })
                .when('/cotacao/automovel/cadastro', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'autoQuotationCtrl',
                    templateUrl: viewBase + 'autoquotation/autoQuotationView.html',
                    resolve: {
                        manufacturers: ["autoQuotationService", function (autoQuotationService) {
                            return autoQuotationService.getManufacturers();
                        }],
                        workActivities: ["workActivityService", function (workActivityService) {
                            return workActivityService.getAll();
                        }],
                        templates: ["templateService", function (templateService) {
                            return templateService.getActiveTemplates(1);
                        }],
                        insurers: ["insurerService", function (insurerService) {
                            return insurerService.getInsurers();
                        }],
                        deviceTrackers: ["deviceTrackerService", function (deviceTrackerService) {
                            return deviceTrackerService.getAll();
                        }],
                        ignitionBlockers: ["ignitionBlockerService", function (ignitionBlockerService) {
                            return ignitionBlockerService.getAll();
                        }]
                    }
                })
                .when('/cotacao/automovel/:id', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'autoQuotationDetailsCtrl',
                    templateUrl: viewBase + 'autoquotationdetails/autoQuotationDetailsView.html',
                    resolve: {
                        quotations: ["$route", "autoQuotationService", function ($route, autoQuotationService) {
                            return autoQuotationService.getQuotationResponse($route.current.params.id);
                        }]
                    }
                })
                .when('/corretora/usuarios', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'userListCtrl',
                    templateUrl: viewBase + 'userlist/userListView.html'
                })
                .when('/corretora/usuarios/cadastro', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'userAddCtrl',
                    templateUrl: viewBase + 'useradd/userAddView.html',
                    resolve: {
                        roles: ["userService", function (userService) {
                            return userService.getRoles();
                        }]
                    }
                })
                .when('/corretora/usuarios/cadastro/:id', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'userEditCtrl',
                    templateUrl: viewBase + 'useredit/userEditView.html',
                    resolve: {
                        roles: ["userService", function (userService) {
                            return userService.getRoles();
                        }],
                        user: ["userService", "$route", function (userService, $route) {
                            return userService.getUser($route.current.params.id);
                        }]
                    }
                })
                .when('/corretora/cadastro', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'accountCtrl',
                    templateUrl: viewBase + 'account/accountView.html',
                    resolve: {
                        account: ["accountService", function (accountService) {
                            return accountService.getAccount();
                        }]
                    }
                })
                 .when('/corretora/contrato', {
                     title: 'Alvac - Cotação de Seguro Automóvel',
                     controller: 'contractCtrl',
                     templateUrl: viewBase + 'contract/contractView.html'
                 })
                .when('/configuracao/seguradoras/templates', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'templateListCtrl',
                    templateUrl: viewBase + 'templatelist/templateListView.html',
                    resolve: {
                        templates: ["templateService", function (templateService) {
                            return templateService.getTemplates();
                        }],
                        insurers: ["accountConnectionService", function (accountConnectionService) {
                            return accountConnectionService.getInsurers('AutoQuotation');
                        }]
                    }
                })
                .when('/configuracao/email', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'configEmailCtrl',
                    templateUrl: viewBase + 'configemail/configEmailView.html',
                    resolve: {
                        accountConfiguration: ["accountConfigurationService", function (accountConfigurationService) {
                            return accountConfigurationService.getConfiguration();
                        }]
                    }
                })
                .when('/configuracao/relatorios', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'configReportCtrl',
                    templateUrl: viewBase + 'configreport/configReportView.html',
                    resolve: {
                        accountConfiguration: ["accountConfigurationService", function (accountConfigurationService) {
                            return accountConfigurationService.getConfiguration();
                        }]
                    }
                })
                .when('/relatorios/geral', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'reportAllUsersCtrl',
                    templateUrl: viewBase + 'reportallusers/reportAllUsersView.html'
                })
                .when('/relatorios/usuario', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'reportByUsersCtrl',
                    templateUrl: viewBase + 'reportbyusers/reportByUsersView.html'
                });

            $routeProvider.when('/auth', {
                title: 'Bem-vindo ao Alvac',
                controller: 'authCtrl',
                templateUrl: viewBase + 'auth/authView.html',
            });

            $routeProvider.when('/auth/:email/token/:token', {
                title: 'Bem-vindo ao Alvac',
                controller: 'confirmEmailCtrl',
                templateUrl: viewBase + '/auth/confirmEmail.html'
            });

            extRouteProvider.otherwise({ redirectTo: "/dashboard" });
        }]);
})();