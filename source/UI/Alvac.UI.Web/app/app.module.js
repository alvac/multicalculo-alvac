(function () {
    'use strict';

    var AlvacApp = angular.module('AlvacApp', ['ngRoute', 'ngAnimate', 'LocalStorageModule', 'angular-loading-bar', 'toaster', 'ui.bootstrap', 'checklist-model',
    'ui.select', 'ngSanitize', 'ui.utils', 'ui.utils.masks', 'chart.js', 'frapontillo.bootstrap-switch', 'ngLocale', 'ui.bootstrap.datetimepicker']);

    var serviceBase = 'http://localhost:50396/';

    AlvacApp.constant('settings', {
        apiBaseUri: serviceBase + "api/",
        signalRBaseUri: serviceBase
    });

    AlvacApp.run(['$location', '$rootScope',
        function ($location, $rootScope) {

            $rootScope.app = {};
        }]);

    AlvacApp.config(['$httpProvider', 'uiSelectConfig', function ($httpProvider, uiSelectConfig) {
        uiSelectConfig.theme = 'bootstrap';
        uiSelectConfig.resetSearchInput = true;
        $httpProvider.interceptors.push('authInterceptorService');
        $httpProvider.interceptors.push('offSetInterceptorService');
    }]);
})();