﻿using Alvac.UI.Web.Hubs;
using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;

namespace Alvac.UI.Web.Controllers
{
    [RoutePrefix("api/notification")]
    public class NotificationController : ApiController
    {
        [Route("")]
        public IHttpActionResult PostNotification([FromBody]dynamic model)
        {
            var context = GlobalHost.ConnectionManager.GetHubContext<QuotationHub>();
            context.Clients.Group(model.correlationId.ToString()).quotationResponse(model);
            return Ok();
        }
    }
}
