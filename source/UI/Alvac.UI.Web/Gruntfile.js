﻿module.exports = function (grunt) {
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            compile: {
                src: 'assets/css/site.less',
                dest: 'assets/dist/site.css'
            }
        },
        concat: {
            css: {
                src: [
                    'assets/dist/site.css',
                    'bower_components/angularjs-toaster/toaster.css',
                    'bower_components/angular-loading-bar/build/loading-bar.min.css',
                    'bower_components/angular-ui-select/dist/select.min.css',
                    'bower_components/angular-chart.js/dist/angular-chart.css',
                    'bower_components/datatables/media/css/jquery.dataTables.min.css',
                    'bower_components/bootstrap-switch/dist/css/bootstrap3/bootstrap-switch.min.css',
                    'bower_components/angular-bootstrap-datetimepicker/src/css/datetimepicker.css'
                ],
                dest: 'assets/dist/combined.css'
            },
            jsmodules: {
                src: [
                    'app/angular-locale_pt-br.js',
                    'app/app.module.js',
                    'app/app.route.js',
                    'app/initCtrl.js'
                ],
                dest: 'assets/dist/app.js'
            },
            jscomponents: {
                src: [
                    'app/components/**/*.js',
                    'app/shared/services/*.js',
                    'app/shared/filters/*.js',
                    'app/shared/directives/*.js',
                    'app/shared/interceptors/*.js'
                ],
                dest: 'assets/dist/components.js'
            },
            external: {
                src: [
                    'bower_components/angular-ui-utils/ui-utils.min.js',
                    'bower_components/chartjs/Chart.min.js',
                    'bower_components/angular-chart.js/dist/angular-chart.js',
                    'bower_components/angularjs-toaster/toaster.js',
                    'bower_components/angular-local-storage/dist/angular-local-storage.min.js',
                    'bower_components/angular-loading-bar/build/loading-bar.min.js',
                    'bower_components/checklist-model/checklist-model.js',
                    'bower_components/angular-ui-select/dist/select.min.js',
                    'bower_components/angular-input-masks/releases/masks.min.js',
                    'bower_components/blockui/jquery.blockUI.js',
                    'bower_components/angular-datatables/dist/angular-datatables.min.js',
                    'bower_components/bootbox/bootbox.js',
                    'bower_components/bootstrap-switch/dist/js/bootstrap-switch.js',
                    'bower_components/angular-bootstrap-switch/dist/angular-bootstrap-switch.js',
                    'assets/js/uibootstrap/ui-bootstrap-tpls-0.12.0.min.js',
                    'assets/js/jquery.signalR-2.2.0.min.js',
                    'bower_components/angular-bootstrap-datetimepicker/src/js/datetimepicker.js'
                ],
                dest: 'assets/dist/external.js'
            }
        },
        cssmin: {
            css: {
                files: {
                    'assets/dist/combined.css': ['assets/dist/combined.css']
                }
            }
        },
        uglify: {
            jsmodules: {
                files: {
                    'assets/dist/app.js': ['assets/dist/app.js']
                }
            },
            jscomponents: {
                files: {
                    'assets/dist/components.js': ['assets/dist/components.js'],
                    'assets/dist/external.js': ['assets/dist/external.js']
                }
            }
        },
        'string-replace': {
            dev: {
                files: {
                    'app/app.module.js': 'app/app.module.js',
                },
                options: {
                    replacements: [
                      {
                          pattern: 'var serviceBase = \'https://microsoft-apiapp37dce4952f834c67996305734fc0303e.azurewebsites.net\/\';',
                          replacement: 'var serviceBase = \'http://localhost:50396/\';'
                      }]
                }
            },
            prod: {
                files: {
                    'app/app.module.js': 'app/app.module.js',
                },
                options: {
                    replacements: [
                      {
                          pattern: 'var serviceBase = \'http://localhost:50396\/\';',
                          replacement: 'var serviceBase = \'https://microsoft-apiapp37dce4952f834c67996305734fc0303e.azurewebsites.net/\';',
                      }
                    ]
                }
            }
        }
    });

    grunt.loadNpmTasks('grunt-contrib-concat');
    grunt.loadNpmTasks('grunt-contrib-uglify');
    grunt.loadNpmTasks('grunt-contrib-cssmin');
    grunt.loadNpmTasks('grunt-string-replace');
    grunt.loadNpmTasks('grunt-contrib-less');

    grunt.registerTask('dev', 'development', ['less:compile', 'string-replace:dev', 'concat:css', 'concat:external', 'cssmin:css', 'concat:jsmodules', 'concat:jscomponents']);

    grunt.registerTask('prod', 'production', ['less:compile', 'string-replace:prod', 'concat:css', 'concat:external', 'cssmin:css', 'concat:jsmodules', 'concat:jscomponents', 'uglify:jsmodules', 'uglify:jscomponents']);

    grunt.registerTask('default', 'Default task: dev', 'dev');
}