﻿describe('Formulario de cotação', function () {

    it('deve preencher os dados do veiculo', function () {

        browser.get('http://localhost:50183/cotacao/automovel');

        element(by.model('selected.Brand')).click();
        var brandElement = element(by.model('selected.Brand')).element(by.tagName('input'));
        brandElement.sendKeys('BMW');
        brandElement.sendKeys(protractor.Key.ENTER);

        element(by.model('selected.Vehicle')).click();
        var vehicleElement = element(by.model('selected.Vehicle')).element(by.tagName('input'));
        vehicleElement.sendKeys('118IA');
        vehicleElement.sendKeys(protractor.Key.ENTER);

        element(by.model('selected.VehicleDetails')).click();
        var vehicleYear = element(by.model('selected.VehicleDetails')).element(by.tagName('input'));
        vehicleYear.sendKeys('2011');
        vehicleYear.sendKeys(protractor.Key.ENTER);

        element(by.model('model.Request.VehicleInfo.ModelYear')).element(by.cssContainingText('option', '2012')).click();

        element.all(by.model('model.Request.VehicleInfo.IsNew')).get(1).click();

        element(by.model('model.Request.VehicleInfo.License.PlateNumber')).sendKeys('HAO0180');
        element(by.model('model.Request.VehicleInfo.License.ChassisNumber')).sendKeys('123456789');

        element.all(by.model('model.Request.VehicleInfo.Value.IsFinanced')).get(1).click();
        element.all(by.model('model.Request.VehicleInfo.HasNaturalGasKit')).get(1).click();
        element.all(by.model('model.Request.VehicleInfo.IsArmored')).get(1).click();
        element.all(by.model('model.Request.VehicleInfo.Value.ValueCategory')).get(1).click();
        element(by.model('model.Request.VehicleInfo.Value.ReferencedValue')).sendKeys(7800000);

        element(by.model('model.Request.VehicleUse.OvernightZipCode')).sendKeys(30525430);
        element(by.model('model.Request.VehicleUse.MovementZipCode')).sendKeys(30525430);
        element(by.model('hasLeisure')).click();
        element(by.model('model.Request.VehicleUse.DailyDistance')).sendKeys(45);

        element(by.css('[data-ng-click="isVehicleValid()"]')).click();
    });

    it('deve preencher os dados dos condutores', function () {
        element.all(by.model('model.Request.MainDriver.IsInsured')).get(1).click();
        element.all(by.model('model.Request.MainDriver.IsVehicleOwner')).get(1).click();

        element(by.model('model.Request.MainDriver.FullName')).sendKeys("Antônio Lucas Veloso Alves Cunha");
        element(by.model('model.Request.MainDriver.MobilePhoneNumber')).sendKeys("31991178550");
        element(by.model('model.Request.MainDriver.PhoneNumber')).sendKeys("31991178550");
        element(by.model('model.Request.MainDriver.Email')).sendKeys("lucas.cunha@alvac.com.br");

        element(by.model('model.Request.MainDriver.Gender')).element(by.cssContainingText('option', 'Masculino')).click();
        element(by.model('model.Request.MainDriver.BirthDate')).sendKeys("18041986");
        element(by.model('model.Request.MainDriver.Document')).sendKeys("07659289659");

        element(by.model('model.Request.MainDriver.MaritalStatus')).element(by.cssContainingText('option', 'Solteiro(a)')).click();
        element(by.model('model.Request.MainDriver.ZipCode')).sendKeys("30525430");
        element(by.model('model.Request.MainDriver.DriverLicenseDate')).sendKeys("01012000");
        element(by.model('model.Request.MainDriver.DriverLicenseNumber')).sendKeys("123456");

        element(by.model('model.Request.MainDriver.WorkActivityId')).element(by.cssContainingText('option', 'Outras')).click();
        element(by.model('model.Request.MainDriver.Schooling')).element(by.cssContainingText('option', 'Superior')).click();
        element(by.model('model.Request.MainDriver.RelationshipWithInsured')).element(by.cssContainingText('option', 'Filho/Enteado')).click();

        element.all(by.model('model.Request.MainDriver.HasBeenStolenInTheLast24Months')).get(1).click();
        element(by.model('model.Request.MainDriver.ResidentialType')).element(by.cssContainingText('option', 'Casa')).click();

        element(by.css('[data-ng-click="isMainDriverValid()"]')).click();
    });

    it('deve preencher os dados do segurado', function () {
        element(by.model('model.Request.Insured.FullName')).sendKeys("Antônio Lucas Veloso Alves Cunha");
        element(by.model('model.Request.Insured.Gender')).element(by.cssContainingText('option', 'Masculino')).click();
        element(by.model('model.Request.Insured.BirthDate')).sendKeys("18041986");
        element.all(by.model('model.Request.Insured.PersonType')).get(0).click();
        element(by.model('model.Request.Insured.Document')).sendKeys("07659289659");

        element(by.model('model.Request.Insured.MaritalStatus')).element(by.cssContainingText('option', 'Solteiro(a)')).click();
        element(by.model('model.Request.Insured.ZipCode')).sendKeys("30525430");
        element(by.model('model.Request.Insured.WorkActivityId')).element(by.cssContainingText('option', 'Outras')).click();
        element(by.css('[data-ng-click="isInsuredValid()"]')).click();
    });

    it('deve preencher os dados do proprietario', function () {
        element(by.model('model.Request.VehicleOwner.FullName')).sendKeys("Antônio Lucas Veloso Alves Cunha");
        element(by.model('model.Request.VehicleOwner.Gender')).element(by.cssContainingText('option', 'Masculino')).click();
        element(by.model('model.Request.VehicleOwner.BirthDate')).sendKeys("18041986");
        element.all(by.model('model.Request.VehicleOwner.PersonType')).get(0).click();
        element(by.model('model.Request.VehicleOwner.Document')).sendKeys("07659289659");

        element(by.model('model.Request.VehicleOwner.MaritalStatus')).element(by.cssContainingText('option', 'Solteiro(a)')).click();
        element(by.model('model.Request.VehicleOwner.ZipCode')).sendKeys("30525430");
        element(by.model('model.Request.VehicleUse.VehiclesNumbers')).sendKeys("2");
        element(by.css('[data-ng-click="isVehicleOwnerValid()"]')).click();
    });

    it('deve preencher os dados do seguro', function () {
        element.all(by.model('isRenewal')).get(0).click();
        element(by.model('model.Request.Insurance.InsurancePolicyInitialDate')).sendKeys("11022015");
        element(by.css('[data-ng-click="isInsuranceValid()"]')).click();

        browser.debugger();
    });
});