﻿exports.config = {
    seleniumAddress: 'http://localhost:4444/wd/hub',
    specs: ['specs/*-spec.js'],

    onPrepare: function () {
        browser.driver.get('http://localhost:50183/auth');

        browser.driver.findElement(by.id('username')).sendKeys('antoniolucas');
        browser.driver.findElement(by.id('password')).sendKeys('alvac123');
        browser.driver.findElement(by.id('signIn')).click();

        browser.driver.wait(function () {
            return browser.driver.getCurrentUrl().then(function (url) {
                return /dashboard/.test(url);
            });
        });
    }
};