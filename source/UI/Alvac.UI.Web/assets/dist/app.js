angular.module("ngLocale", [], ["$provide", function ($provide) {
    var PLURAL_CATEGORY = { ZERO: "zero", ONE: "one", TWO: "two", FEW: "few", MANY: "many", OTHER: "other" };
    $provide.value("$locale", {
        "DATETIME_FORMATS": {
            "AMPMS": [
              "AM",
              "PM"
            ],
            "DAY": [
              "domingo",
              "segunda-feira",
              "ter\u00e7a-feira",
              "quarta-feira",
              "quinta-feira",
              "sexta-feira",
              "s\u00e1bado"
            ],
            "ERANAMES": [
              "Antes de Cristo",
              "Ano do Senhor"
            ],
            "ERAS": [
              "a.C.",
              "d.C."
            ],
            "FIRSTDAYOFWEEK": 6,
            "MONTH": [
              "janeiro",
              "fevereiro",
              "mar\u00e7o",
              "abril",
              "maio",
              "junho",
              "julho",
              "agosto",
              "setembro",
              "outubro",
              "novembro",
              "dezembro"
            ],
            "SHORTDAY": [
              "dom",
              "seg",
              "ter",
              "qua",
              "qui",
              "sex",
              "s\u00e1b"
            ],
            "SHORTMONTH": [
              "jan",
              "fev",
              "mar",
              "abr",
              "mai",
              "jun",
              "jul",
              "ago",
              "set",
              "out",
              "nov",
              "dez"
            ],
            "WEEKENDRANGE": [
              5,
              6
            ],
            "fullDate": "EEEE, d 'de' MMMM 'de' y",
            "longDate": "d 'de' MMMM 'de' y",
            "medium": "d 'de' MMM 'de' y HH:mm:ss",
            "mediumDate": "d 'de' MMM 'de' y",
            "mediumTime": "HH:mm:ss",
            "short": "dd/MM/yy HH:mm",
            "shortDate": "dd/MM/yy",
            "shortTime": "HH:mm"
        },
        "NUMBER_FORMATS": {
            "CURRENCY_SYM": "R$",
            "DECIMAL_SEP": ",",
            "GROUP_SEP": ".",
            "PATTERNS": [
              {
                  "gSize": 3,
                  "lgSize": 3,
                  "maxFrac": 3,
                  "minFrac": 0,
                  "minInt": 1,
                  "negPre": "-",
                  "negSuf": "",
                  "posPre": "",
                  "posSuf": ""
              },
              {
                  "gSize": 3,
                  "lgSize": 3,
                  "maxFrac": 2,
                  "minFrac": 2,
                  "minInt": 1,
                  "negPre": "\u00a4-",
                  "negSuf": "",
                  "posPre": "\u00a4",
                  "posSuf": ""
              }
            ]
        },
        "id": "pt-br",
        "pluralCat": function (n, opt_precision) { if (n >= 0 && n <= 2 && n != 2) { return PLURAL_CATEGORY.ONE; } return PLURAL_CATEGORY.OTHER; }
    });
}]);
(function () {
    'use strict';

    var AlvacApp = angular.module('AlvacApp', ['ngRoute', 'ngAnimate', 'LocalStorageModule', 'angular-loading-bar', 'toaster', 'ui.bootstrap', 'checklist-model',
    'ui.select', 'ngSanitize', 'ui.utils', 'ui.utils.masks', 'chart.js', 'frapontillo.bootstrap-switch', 'ngLocale', 'ui.bootstrap.datetimepicker']);

    var serviceBase = 'http://localhost:50396/';

    AlvacApp.constant('settings', {
        apiBaseUri: serviceBase + "api/",
        signalRBaseUri: serviceBase
    });

    AlvacApp.run(['$location', '$rootScope',
        function ($location, $rootScope) {

            $rootScope.app = {};
        }]);

    AlvacApp.config(['$httpProvider', 'uiSelectConfig', function ($httpProvider, uiSelectConfig) {
        uiSelectConfig.theme = 'bootstrap';
        uiSelectConfig.resetSearchInput = true;
        $httpProvider.interceptors.push('authInterceptorService');
        $httpProvider.interceptors.push('offSetInterceptorService');
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').config(['$routeProvider', '$compileProvider', '$locationProvider',
        function ($routeProvider, $compileProvider, $locationProvider) {
            var viewBase = '/app/components/';

            $compileProvider.debugInfoEnabled(false);
            $locationProvider.html5Mode(true);

            var extRouteProvider = angular.extend({}, $routeProvider, {
                when: function (path, route) {
                    route.resolve = (route.resolve) ? route.resolve : {};
                    angular.extend(route.resolve, {
                        isAuth: ['authService', '$q', '$location',
                            function (authService, $q, $location) {
                                var deferred = $q.defer();
                                authService.fillAuthData();

                                if (authService.isAuth() || $location.path() === "/auth") {
                                    deferred.resolve(true);
                                }
                                else {
                                    deferred.reject(false);
                                }

                                return deferred.promise;
                            }]
                    });

                    $routeProvider.when(path, route);
                    return this;
                }
            });

            extRouteProvider
                .when('/dashboard', {
                    title: 'Alvac - Cotação e Comparação Online de Seguros',
                    controller: 'homeCtrl',
                    templateUrl: viewBase + 'home/homeView.html',
                    resolve: {
                        quotations: ["autoQuotationService", function (autoQuotationService) {
                            return autoQuotationService.getLastQuotations(15, 0);
                        }]
                    }
                })
                .when('/cotacao/automovel', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'autoQuotationListCtrl',
                    templateUrl: viewBase + 'autoquotationlist/autoQuotationListView.html'
                })
                .when('/cotacao/automovel/cadastro', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'autoQuotationCtrl',
                    templateUrl: viewBase + 'autoquotation/autoQuotationView.html',
                    resolve: {
                        manufacturers: ["autoQuotationService", function (autoQuotationService) {
                            return autoQuotationService.getManufacturers();
                        }],
                        workActivities: ["workActivityService", function (workActivityService) {
                            return workActivityService.getAll();
                        }],
                        templates: ["templateService", function (templateService) {
                            return templateService.getActiveTemplates(1);
                        }],
                        insurers: ["insurerService", function (insurerService) {
                            return insurerService.getInsurers();
                        }],
                        deviceTrackers: ["deviceTrackerService", function (deviceTrackerService) {
                            return deviceTrackerService.getAll();
                        }],
                        ignitionBlockers: ["ignitionBlockerService", function (ignitionBlockerService) {
                            return ignitionBlockerService.getAll();
                        }]
                    }
                })
                .when('/cotacao/automovel/:id', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'autoQuotationDetailsCtrl',
                    templateUrl: viewBase + 'autoquotationdetails/autoQuotationDetailsView.html',
                    resolve: {
                        quotations: ["$route", "autoQuotationService", function ($route, autoQuotationService) {
                            return autoQuotationService.getQuotationResponse($route.current.params.id);
                        }]
                    }
                })
                .when('/corretora/usuarios', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'userListCtrl',
                    templateUrl: viewBase + 'userlist/userListView.html'
                })
                .when('/corretora/usuarios/cadastro', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'userAddCtrl',
                    templateUrl: viewBase + 'useradd/userAddView.html',
                    resolve: {
                        roles: ["userService", function (userService) {
                            return userService.getRoles();
                        }]
                    }
                })
                .when('/corretora/usuarios/cadastro/:id', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'userEditCtrl',
                    templateUrl: viewBase + 'useredit/userEditView.html',
                    resolve: {
                        roles: ["userService", function (userService) {
                            return userService.getRoles();
                        }],
                        user: ["userService", "$route", function (userService, $route) {
                            return userService.getUser($route.current.params.id);
                        }]
                    }
                })
                .when('/corretora/cadastro', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'accountCtrl',
                    templateUrl: viewBase + 'account/accountView.html',
                    resolve: {
                        account: ["accountService", function (accountService) {
                            return accountService.getAccount();
                        }]
                    }
                })
                 .when('/corretora/contrato', {
                     title: 'Alvac - Cotação de Seguro Automóvel',
                     controller: 'contractCtrl',
                     templateUrl: viewBase + 'contract/contractView.html'
                 })
                .when('/configuracao/seguradoras/templates', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'templateListCtrl',
                    templateUrl: viewBase + 'templatelist/templateListView.html',
                    resolve: {
                        templates: ["templateService", function (templateService) {
                            return templateService.getTemplates();
                        }],
                        insurers: ["accountConnectionService", function (accountConnectionService) {
                            return accountConnectionService.getInsurers('AutoQuotation');
                        }]
                    }
                })
                .when('/configuracao/email', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'configEmailCtrl',
                    templateUrl: viewBase + 'configemail/configEmailView.html',
                    resolve: {
                        accountConfiguration: ["accountConfigurationService", function (accountConfigurationService) {
                            return accountConfigurationService.getConfiguration();
                        }]
                    }
                })
                .when('/configuracao/relatorios', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'configReportCtrl',
                    templateUrl: viewBase + 'configreport/configReportView.html',
                    resolve: {
                        accountConfiguration: ["accountConfigurationService", function (accountConfigurationService) {
                            return accountConfigurationService.getConfiguration();
                        }]
                    }
                })
                .when('/relatorios/geral', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'reportAllUsersCtrl',
                    templateUrl: viewBase + 'reportallusers/reportAllUsersView.html'
                })
                .when('/relatorios/usuario', {
                    title: 'Alvac - Cotação de Seguro Automóvel',
                    controller: 'reportByUsersCtrl',
                    templateUrl: viewBase + 'reportbyusers/reportByUsersView.html'
                });

            $routeProvider.when('/auth', {
                title: 'Bem-vindo ao Alvac',
                controller: 'authCtrl',
                templateUrl: viewBase + 'auth/authView.html',
            });

            $routeProvider.when('/auth/:email/token/:token', {
                title: 'Bem-vindo ao Alvac',
                controller: 'confirmEmailCtrl',
                templateUrl: viewBase + '/auth/confirmEmail.html'
            });

            extRouteProvider.otherwise({ redirectTo: "/dashboard" });
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('initCtrl', ['$rootScope', '$scope', 'authService', '$location', 'localStorageService', '$q', 'alertService', '$templateCache',
        function ($rootScope, $scope, authService, $location, localStorageService, $q, alertService, $templateCache) {

            $rootScope.authentication = authService.authentication;

            $rootScope.$on('$routeChangeSuccess', function (event, next, current) {
                if (next.$$route) {
                    $rootScope.title = next.$$route.title;
                }

                alertService.unblock();
            });

            $rootScope.$on('$routeChangeStart', function () {
                if (typeof (current) !== 'undefined') {
                    $templateCache.remove(current.templateUrl);
                }

                alertService.block();
            });

            $rootScope.$on('$routeChangeError', function () {
                alertService.unblock();
            });

            angular.element(document).ready(function () {
                $("#main").show();
            });

            $rootScope.connection = null;

            $rootScope.$on("loadSignalr", function () {
                var connection = $rootScope.connection;

                if (!connection) {
                    var proxy = null;
                    connection = $.hubConnection();
                    connection.qs = { 'userId': authService.authentication.userName };
                    connection.logging = true;

                    connection.stateChanged(function (change) {
                        console.log('stateChanged from:' + change.oldState + ' to: ' + change.newState);
                    });

                    connection.reconnected(function () {
                        console.log('reconnected signalr');
                    });

                    connection.disconnected(function () {
                        console.log('disconnected signalr');
                        setTimeout(function () { connection.start(); }, 5000);
                    });

                    connection.connectionSlow(function () {
                        console.log('We are currently experiencing difficulties with the connection.');
                    });

                    connection.error(function (error) {
                        console.log('SignalR error: ' + error);
                    });

                    proxy = connection.createHubProxy('QuotationHub');

                    // Listen to the 'quotationResponse' event that will be pushed from SignalR server
                    proxy.on('quotationResponse', function (quotationResponse) {
                        $rootScope.$emit("quotationResponse", quotationResponse);
                    });

                    // Connecting to SignalR server        
                    connection.start()
                    .done(function () { console.log('Now connected'); })
                    .fail(function () { console.log('Could not connect'); });

                    $rootScope.connection = connection;
                }
            });
        }]);
})();