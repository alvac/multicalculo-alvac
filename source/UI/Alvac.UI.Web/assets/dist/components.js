(function () {
    'use strict';

    angular.module('AlvacApp').controller('accountCtrl', ['$rootScope', '$scope', '$location', 'accountService', 'account', 'alertService',
        function ($rootScope, $scope, $location, accountService, account, alertService) {
            $scope.pageTitle = "Dados cadastrais";

            $scope.model = account;

            $scope.update = function () {

                $scope.accountForm.submitted = true;

                if ($scope.accountForm.$valid) {
                    alertService.block();

                    accountService.update($scope.model).then(function (response) {
                        alertService.success("Conta editada com sucesso!");
                        alertService.unblock();
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.message);
                }
            }
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('authCtrl', ['$rootScope', '$scope', 'authService', 'alertService', '$location',
        function ($rootScope, $scope, authService, alertService, $location) {

            $scope.signIn = function () {
                if ($scope.form.$valid) {
                    alertService.block();
                    authService.login($scope.model).then(function (response) {
                        $location.path("/");
                    }, function (err) {
                        alertService.error(err);
                        alertService.unblock();
                    });
                }
            };
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('authService', ['$http', 'settings', '$q', 'localStorageService', '$rootScope', '$location',
        function ($http, settings, $q, localStorageService, $rootScope, $location) {

            var serviceBase = settings.apiBaseUri;

            var _authentication = {
                isAuth: false,
                userName: "",
                useRefreshTokens: false,
                role: "",
                isMaster: function () {
                    return this.role.indexOf("Master") != -1;
                }
            };

            var _saveRegistration = function (registration) {

                _logOut();

                return $http.post(serviceBase + 'api/account/register', registration).then(function (response) {
                    return response;
                });
            };

            var _login = function (loginData) {

                var data = "grant_type=password&username=" + loginData.userName + "&password=" + loginData.password;

                if (loginData.useRefreshTokens) {
                    data = data + "&client_id=self";
                }

                var deferred = $q.defer();

                $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                    if (loginData.useRefreshTokens) {
                        localStorageService.set('authorizationData', {
                            token: response.access_token,
                            userName: loginData.userName,
                            refreshToken: response.refresh_token,
                            useRefreshTokens: true,
                            role: response.role
                        });
                    }
                    else {
                        localStorageService.set('authorizationData', {
                            token: response.access_token,
                            userName: response.userName,
                            refreshToken: "",
                            useRefreshTokens: false,
                            role: response.role
                        });
                    }

                    _authentication.isAuth = true;
                    _authentication.userName = response.userName;
                    _authentication.role = response.role;
                    _authentication.isAdmin = response.role.indexOf("Master") != -1;
                    _authentication.useRefreshTokens = loginData.useRefreshTokens;
                    deferred.resolve(response);

                }).error(function (err, status) {
                    _logOut();
                    deferred.reject(err);
                });

                return deferred.promise;

            };

            var _logOut = function () {

                localStorageService.remove('authorizationData');

                _authentication.isAuth = false;
                _authentication.userName = "";
                _authentication.useRefreshTokens = false;
                _authentication.role = "";
            };

            var _fillAuthData = function () {
                if (!_authentication.isAuth) {
                    var authData = localStorageService.get('authorizationData');
                    if (authData) {
                        _authentication.isAuth = true;
                        _authentication.userName = authData.userName;
                        _authentication.role = authData.role;
                        _authentication.useRefreshTokens = authData.useRefreshTokens;
                        $rootScope.$emit("loadSignalr");
                    }
                    else {
                        _logOut();
                    }
                }
            };

            var _isAuth = function () {
                if (!_authentication || !_authentication.isAuth) {
                    _logOut();
                    $location.path('/auth');
                    return false;
                }

                return true;
            };

            var _refreshToken = function () {
                var deferred = $q.defer();

                var authData = localStorageService.get('authorizationData');

                if (authData) {

                    if (authData.useRefreshTokens) {

                        var data = "grant_type=refresh_token&refresh_token=" + authData.refreshToken + "&client_id=self";

                        localStorageService.remove('authorizationData');

                        $http.post(serviceBase + 'token', data, { headers: { 'Content-Type': 'application/x-www-form-urlencoded' } }).success(function (response) {

                            localStorageService.set('authorizationData', { token: response.access_token, userName: response.userName, refreshToken: response.refresh_token, useRefreshTokens: true });

                            deferred.resolve(response);

                        }).error(function (err, status) {
                            _logOut();
                            deferred.reject(err);
                        });
                    }
                }

                return deferred.promise;
            };

            return {
                saveRegistration: _saveRegistration,
                login: _login,
                logOut: _logOut,
                fillAuthData: _fillAuthData,
                authentication: _authentication,
                refreshToken: _refreshToken,
                isAuth: _isAuth
            };
        }]);
})();

(function () {
    'use strict';

    angular.module('AlvacApp').controller('confirmEmailCtrl', ['$rootScope', '$scope', 'authService', 'alertService', '$location', '$routeParams', 'userService',
        function ($rootScope, $scope, authService, alertService, $location, $routeParams, userService) {

            $scope.model = {};
            $scope.model.token = $routeParams.token;
            $scope.model.email = $routeParams.email;

            $scope.confirmEmail = function () {

                if ($scope.form.$valid) {
                    alertService.block();
                    userService.confirmEmail($scope.model).then(function (response) {
                        alertService.unblock();
                        authService.login({ userName: $scope.model.email, password: $scope.model.password }).then(function () {
                            $location.path("/");
                        }, function (err) {
                            alertService.error(err);
                            alertService.unblock();
                        });

                    }, function () {
                        alertService.unblock();
                    });
                }
            };
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('additionalDriverCtrl', ['$modalInstance', '$scope', 'item', function ($modalInstance, $scope, item) {

        $scope.item = angular.copy(item);

        $scope.ok = function () {
            $scope.additionalDriverForm.submitted = true;

            if ($scope.additionalDriverForm.$valid) {
                $modalInstance.close($scope.item);
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('autoQuotationCtrl',
        ['$rootScope', '$scope', 'accountConnectionService', 'alertService', 'manufacturers', 'autoQuotationService', '$modal', '$location', 'workActivities', 'authService', 'templates',
            'insurers', 'templateService', 'deviceTrackers', 'ignitionBlockers',
        function ($rootScope, $scope, accountConnectionService, alertService, manufacturers, autoQuotationService, $modal, $location, workActivities, authService, templates,
            insurers, templateService, deviceTrackers, ignitionBlockers) {

            $scope.pageTitle = "Nova cotação";

            $scope.tabs = {
                vehicle: true,
                mainDriver: false,
                insured: false,
                vehicleOwner: false,
                insurance: false,
                insurers: false
            };

            $scope.previouslyInsurers = insurers;
            $scope.templates = templates;

            $scope.loadParamaters = function ($event, insurer, selectedTemplate) {
                var checkbox = $event.target;
                if (checkbox.checked) {
                    alertService.block();
                    templateService.getInsurerParameters(insurer.id, selectedTemplate.accountInsurerTemplateId).then(function (response) {
                        insurer.parameters = response;
                    }, handleError);

                    autoQuotationService.getInsurerVehicles(insurer.id, $scope.selected.Vehicle.vehicleId).then(function (response) {
                        insurer.vehicles = response;

                        if (response.length == 1) {
                            insurer.vehicle = response[0].vehicleId;
                        }

                        alertService.unblock();
                    }, handleError);

                }
                else {
                    insurer.parameters = null;
                    insurer.vehicles = null;
                }
            };

            $scope.openTemplateConfiguration = function (insurer) {
                var modalInstance = $modal.open({
                    templateUrl: '/app/components/templateconfiguration/templateConfigurationView.html',
                    controller: 'templateConfigurationCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    keyboard: false,
                    resolve: {
                        insurer: function () {
                            return insurer;
                        }
                    }
                });

                modalInstance.result.then(function (item) {
                    insurer.parameters = item.parameters;
                }, function () {
                });
            };

            $scope.formProgressBar = 0;

            $scope.model = {
                InsurerRequestList: [],
                DeductibleTypeList: [],
                Request: {
                    VehicleInfo: {
                        Value: {
                            IsFinanced: false,
                        },
                        License: {},
                        IsNew: false,
                        HasNaturalGasKit: false,
                        IsArmored: false,

                    },
                    VehicleUse: {
                        UseType: 0,
                        GarageLocal: 0,
                        VehiclesNumbers: 1
                    },
                    MainDriver: {
                        DependentList: []
                    },
                    Insured: {},
                    VehicleOwner: {},
                    Insurance: {
                        CoveragePlanType: 1,
                        Renewal: {},
                        InsurancePolicyInitialDate: new Date()
                    },
                    AdditionalDriverList: []
                }
            };

            $scope.currentInsurancePolicyDateMin = moment().subtract(365, 'days').format('YYYY-MM-DD');
            $scope.currentInsurancePolicyDateMax = moment().add(45, 'days').format('YYYY-MM-DD');

            $scope.insurancePolicyInitialDateMin = moment().subtract(7, 'days').format('YYYY-MM-DD');
            $scope.insurancePolicyInitialDateax = moment().add(45, 'days').format('YYYY-MM-DD');

            $scope.$watch("model.Request.Insurance.InsurancePolicyInitialDate", function (newValue) {
                if (newValue) {
                    var date = new Date(newValue);
                    date.setFullYear(date.getFullYear() + 1);
                    $scope.model.Request.Insurance.InsurancePolicyFinalDate = date;
                }
            });

            $scope.$watch("model.Request.Insurance.Renewal.CurrentInsurancePolicyDate", function (newValue) {
                if (newValue) {
                    var date = new Date(newValue);
                    date.setFullYear(date.getFullYear() + 1);
                    $scope.model.Request.Insurance.Renewal.CurrentInsurancePolicyExpirationDate = date;
                }
            });

            $scope.workActivities = workActivities;
            $scope.devicesIgnitionBlockers = ignitionBlockers;
            $scope.devicesTrackers = deviceTrackers;

            $scope.selectedInsurers = [];

            $scope.toggleGarage = function ($event) {
                var checkbox = $event.target;
                if (checkbox.checked) {
                    $scope.model.Request.VehicleUse.GarageLocal += parseInt(checkbox.value);
                }
                else {
                    $scope.model.Request.VehicleUse.GarageLocal -= parseInt(checkbox.value);
                }
            };

            $scope.toggleUseType = function ($event) {
                var checkbox = $event.target;
                if (checkbox.checked) {
                    $scope.model.Request.VehicleUse.UseType += parseInt(checkbox.value);
                }
                else {
                    $scope.model.Request.VehicleUse.UseType -= parseInt(checkbox.value);
                }
            };

            $scope.deductibleTypes = [
                { id: 1, name: "Obrigatória" },
                { id: 2, name: "Reduzida 50%" },
                { id: 3, name: "Reduzida 25%" },
                { id: 4, name: "Facultativa 100%" },
                { id: 5, name: "Facultativa 50%" }
            ];

            $scope.manufacturers = manufacturers;
            $scope.selected = {};

            $scope.$watch("selected.Brand", function (newValue) {
                if (newValue) {
                    $scope.model.Request.VehicleInfo.Brand = newValue.name;
                    $scope.selected.Vehicle = null;
                    $scope.model.Request.VehicleInfo.ModelYear = "";
                }
            });

            $scope.manufacturingYears = [];
            var year = new Date().getFullYear();
            for (var i = 0; i < 15; i++) {
                $scope.manufacturingYears.push(year - i);
            }

            $scope.$watch("selected.ManufacturingYear", function (newValue) {
                $scope.model.Request.VehicleInfo.ModelYear = "";
                $scope.selected.Vehicle = null;
            });

            $scope.$watch("model.Request.VehicleInfo.ModelYear", function (newValue) {

                if (newValue) {
                    alertService.block();
                    autoQuotationService.getVehicles(newValue, encodeURIComponent($scope.selected.Brand.manufacturerId)).then(function (response) {
                        if (response && response.length > 0) {
                            $scope.vehicles = response;
                        }
                        else {
                            alertService.warning("Nenhum modelo encontrado.");
                        }

                        alertService.unblock();
                    }, handleError);
                }

                $scope.selected.Vehicle = null;
                $scope.model.Request.VehicleInfo.FipeCode = null;
                $scope.model.Request.VehicleInfo.Fuel = null;
                $scope.model.Request.VehicleInfo.Value.ReferencedValue = null;
            });

            $scope.$watch("selected.Vehicle", function (newValue) {
                if (newValue) {
                    $scope.model.Request.VehicleInfo.FipeCode = newValue.fipeCode;
                    $scope.model.Request.VehicleInfo.Fuel = newValue.fuel;
                    $scope.model.Request.VehicleInfo.Model = newValue.model;
                    $scope.model.Request.VehicleInfo.Value.ReferencedValue = newValue.vehiclePrices.filter(function (element) {
                        return element.year == $scope.model.Request.VehicleInfo.ModelYear;
                    })[0].price;

                    $scope.vehicleZero = newValue.vehiclePrices.filter(function (element) {
                        return element.year === 0;
                    })[0];

                    if (!$scope.vehicleZero) {
                        model.Request.VehicleInfo.IsNew = false;
                    }
                }
            });

            $scope.$watch("model.Request.VehicleInfo.IsNew", function (newValue) {
                $scope.model.Request.VehicleInfo.FipeCode = $scope.selected.Vehicle.fipeCode;
                $scope.model.Request.VehicleInfo.Fuel = $scope.selected.Vehicle.fuel;
                $scope.model.Request.VehicleInfo.Model = $scope.selected.Vehicle.model;

                if (newValue && newValue == "true") {
                    $scope.model.Request.VehicleInfo.Value.ReferencedValue = $scope.vehicleZero.price;
                }
                else if ($scope.selected.Vehicle) {
                    $scope.model.Request.VehicleInfo.Value.ReferencedValue = $scope.selected.Vehicle.vehiclePrices.filter(function (element) {
                        return element.year == $scope.model.Request.VehicleInfo.ModelYear;
                    })[0].price;
                }
            });

            $scope.$watch("model.Request.VehicleInfo.HasNaturalGasKit", function () {
                $scope.model.Request.VehicleInfo.NaturalGasKitCoverage = null;
            });

            $scope.$watch("model.Request.VehicleInfo.IsArmored", function () {
                $scope.model.Request.VehicleInfo.ArmoryCoverage = null;
            });

            $scope.$watch("model.Request.VehicleInfo.Value.ValueCategory", function () {
                $scope.model.Request.VehicleInfo.Value.DeterminedValue = null;
                $scope.model.Request.VehicleInfo.Value.ReferencedPercentage = null;
            });

            accountConnectionService.getInsurers('AutoQuotation').then(function (response) {
                $scope.insurers = response;
            });

            $scope.openModalDependent = function (item) {
                var modalInstance = $modal.open({
                    templateUrl: '/app/components/autoquotation/dependentView.html',
                    controller: 'dependentCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            return item;
                        }
                    }
                });

                modalInstance.result.then(function (addedDependent) {
                    if (addedDependent.index >= 0) {
                        $scope.model.Request.MainDriver.DependentList[addedDependent.index] = addedDependent;
                    }
                    else {
                        $scope.model.Request.MainDriver.DependentList.push(addedDependent);
                        var index = $scope.model.Request.MainDriver.DependentList.indexOf(addedDependent);
                        addedDependent.index = index;
                    }
                }, function () {
                });
            };

            $scope.removeDependent = function (item) {
                var index = $scope.model.Request.MainDriver.DependentList.indexOf(item);
                $scope.model.Request.MainDriver.DependentList.splice(index, 1);
            };

            $scope.openModalAdditionalDriver = function (item) {
                var modalInstance = $modal.open({
                    templateUrl: '/app/components/autoquotation/additionalDriverView.html',
                    controller: 'additionalDriverCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    resolve: {
                        item: function () {
                            return item;
                        }
                    }
                });

                modalInstance.result.then(function (additionalDriver) {
                    if (additionalDriver.index >= 0) {
                        $scope.model.Request.AdditionalDriverList[additionalDriver.index] = additionalDriver;
                    }
                    else {
                        $scope.model.Request.AdditionalDriverList.push(additionalDriver);
                        var index = $scope.model.Request.AdditionalDriverList.indexOf(additionalDriver);
                        additionalDriver.index = index;
                    }
                }, function () {
                });
            };

            $scope.removeAdditionalDriver = function (item) {
                var index = $scope.model.Request.AdditionalDriverList.indexOf(item);
                $scope.model.Request.AdditionalDriverList.splice(index, 1);
            };

            $scope.isVehicleValid = function () {
                $scope.form.vehicleForm.submitted = true;

                if ($scope.form.vehicleForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 16;
                    $scope.tabs.mainDriver = true;
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.isMainDriverValid = function () {
                $scope.form.mainDriverForm.submitted = true;

                if ($scope.form.mainDriverForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 32;
                    $scope.tabs.insured = true;

                    if ($scope.model.Request.MainDriver.IsInsured == "true") {
                        $scope.model.Request.Insured.PersonType = 1;
                        $scope.model.Request.Insured.FullName = $scope.model.Request.MainDriver.FullName;
                        $scope.model.Request.Insured.Gender = $scope.model.Request.MainDriver.Gender;
                        $scope.model.Request.Insured.BirthDate = $scope.model.Request.MainDriver.BirthDate;
                        $scope.model.Request.Insured.Document = $scope.model.Request.MainDriver.Document;
                        $scope.model.Request.Insured.MaritalStatus = $scope.model.Request.MainDriver.MaritalStatus;
                        $scope.model.Request.Insured.ZipCode = $scope.model.Request.MainDriver.ZipCode;
                        $scope.model.Request.Insured.WorkActivityId = $scope.model.Request.MainDriver.WorkActivityId;
                    }

                    if ($scope.model.Request.MainDriver.IsVehicleOwner == "true") {
                        $scope.model.Request.VehicleOwner.PersonType = "1";
                        $scope.model.Request.VehicleOwner.FullName = $scope.model.Request.MainDriver.FullName;
                        $scope.model.Request.VehicleOwner.Gender = $scope.model.Request.MainDriver.Gender;
                        $scope.model.Request.VehicleOwner.BirthDate = $scope.model.Request.MainDriver.BirthDate;
                        $scope.model.Request.VehicleOwner.Document = $scope.model.Request.MainDriver.Document;
                        $scope.model.Request.VehicleOwner.MaritalStatus = $scope.model.Request.MainDriver.MaritalStatus;
                        $scope.model.Request.VehicleOwner.ZipCode = $scope.model.Request.MainDriver.ZipCode;
                    }
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.isInsuredValid = function () {
                $scope.form.insuredForm.submitted = true;

                if ($scope.form.insuredForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 48;
                    $scope.tabs.vehicleOwner = true;
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.isVehicleOwnerValid = function () {
                $scope.form.vehicleOwnerForm.submitted = true;

                if ($scope.form.vehicleOwnerForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 64;
                    $scope.tabs.insurance = true;
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.isInsuranceValid = function () {
                $scope.form.insuranceForm.submitted = true;

                if ($scope.form.insuranceForm.$valid) {
                    scrollTop();
                    $scope.formProgressBar = 80;
                    $scope.tabs.insurers = true;
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            $scope.quotation = function () {
                if ($scope.form.$valid) {
                    alertService.block();

                    $scope.model.Request.VehicleInfo.Value.ReferencedPercentage = $scope.model.Request.VehicleInfo.Value.ReferencedPercentage * 100;

                    $scope.model.InsurerRequestList = [];

                    angular.forEach($scope.selectedInsurers, function (value, key) {
                        var parameters = {};

                        angular.forEach(value.parameters, function (item, paramKey) {
                            parameters[item.key] = item.value;
                        });

                        parameters["vehicle"] = value.vehicle;

                        $scope.model.InsurerRequestList.push({ Insurer: value.id, Parameters: parameters });
                    });

                    $scope.model.Request.Notification = {
                        CorrelationId: authService.authentication.userName,
                        CallType: 1,
                        Path: $location.protocol() + '://' + $location.host() + "/api/notification"
                    };

                    autoQuotationService.quotation($scope.model).then(function (response) {
                        $location.path("/cotacao/automovel/" + response.quotationId);
                        alertService.success("Seu cálculo esta sendo processado.");
                    }, function (err) {
                        alertService.error('Ocorreu algum erro tenta novamente');
                        alertService.unblock();
                    });
                }
                else {
                    alertService.error('Corrija os erros neste formulário');
                }
            };

            function scrollTop() {
                $("html, body").animate({ scrollTop: 0 }, 200);
            }

            function handleError(response) {
                alertService.unblock();
                alertService.error('Ocorreu alguma falha, favor tentar novamente.');
            }
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('dependentCtrl', ['$modalInstance', '$scope', 'item', function ($modalInstance, $scope, item) {

        $scope.item = angular.copy(item);

        $scope.ok = function () {
            $scope.dependentForm.submitted = true;

            if ($scope.dependentForm.$valid) {
                $modalInstance.close($scope.item);
            }
        };

        $scope.cancel = function () {
            $modalInstance.dismiss('cancel');
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('autoQuotationDetailsCtrl', ['$rootScope', '$scope', 'alertService', 'autoQuotationService', '$modal',
        'quotations', '$routeParams', 'emailService',
        function ($rootScope, $scope, alertService, autoQuotationService, $modal,
            quotations, $routeParams, emailService) {

            $scope.quotationId = $routeParams.id;
            $scope.pageTitle = "Resultado da cotação";

            $scope.selectedResponse = [];
            $scope.model = {
                insurerQuotations: []
            };
            $scope.openEmail = function () {
                $scope.modal = $modal.open({
                    templateUrl: 'send-email.html',
                    scope: $scope
                });
            };

            $scope.closeEmail = function () {
                $scope.modal.dismiss('cancel');
            }

            $scope.sendEmail = function () {
                alertService.block();

                for (var i = 0; i < $scope.selectedResponse.length; i++) {
                    $scope.model.insurerQuotations.push($scope.selectedResponse[i].insurerQuotationId);
                }

                emailService.sendEmail($scope.model).then(function () {
                    alertService.unblock();
                    alertService.success("Email enviado!");
                    $scope.modal.dismiss('email sent');
                }, function () {
                    alertService.unblock();
                });
            };

            $rootScope.$on("quotationResponse", function (event, notificationRequest) {
                for (var index = 0; index < $scope.quotations.length; index++) {
                    if ($scope.quotations[index].insurerQuotationId == notificationRequest.response.identifier) {

                        var quotation = $scope.quotations[index];

                        quotation.status = notificationRequest.response.result.code;
                        quotation.operationDescription = notificationRequest.response.result.description;

                        if (quotation.status == 'Success') {
                            quotation.insurerBudgetCode = notificationRequest.response.insurerBudgetCode;
                            quotation.premiumValue = notificationRequest.response.premiumValue;
                            quotation.deductibleValue = notificationRequest.response.deductibleValue;
                            quotation.materialDamages = notificationRequest.response.coverage.materialDamages;
                            quotation.bodyDamages = notificationRequest.response.coverage.moralDamages;
                            quotation.moralDamages = notificationRequest.response.coverage.bodyDamages;
                            quotation.personalAccidentPassenger = notificationRequest.response.coverage.personalAccidentPassenger;
                            alertService.success("A cotação da " + quotation.insurer + " foi concluida.");
                        }

                        break;
                    }
                }
            });

            $scope.quotations = quotations;
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('autoQuotationListCtrl', ['$rootScope', '$scope', 'autoQuotationService', '$location', 'alertService',
        function ($rootScope, $scope, autoQuotationService, $location, alertService) {

            $scope.pageTitle = "Lista de cotações";

            $scope.currentPage = 0;
            $scope.pageLength = 20;
            $scope.count = 0;

            $scope.filters = {
                beginDate: moment().subtract(1, 'months').toDate(),
                endDate: new Date()
            };

            $scope.nextPage = function () {
                $scope.currentPage = $scope.currentPage + 1;
                $scope.loadTable();
            };

            $scope.previousPage = function () {
                if ($scope.currentPage == 0) {
                    return;
                }

                $scope.currentPage = $scope.currentPage - 1;
                $scope.loadTable();
            };

            $scope.goToDetails = function (id) {
                $location.path("/cotacao/automovel/" + id);
            };

            $rootScope.$on("quotationResponse", function (event, notificationRequest) {
                var found = false;
                for (var index = 0; index < $scope.lastQuotations.length; index++) {
                    for (var i = 0; i < $scope.lastQuotations[index].insurerQuotation.length; i++) {

                        if (found) break;

                        if ($scope.lastQuotations[index].insurerQuotation[i].insurerQuotationId == notificationRequest.response.identifier) {
                            found = true;

                            var quotation = $scope.lastQuotations[index].insurerQuotation[i];
                            quotation.status = notificationRequest.response.result.code;
                            quotation.operationDescription = notificationRequest.response.result.description;
                        }
                    }

                    if (found) break;
                }
            });

            $scope.$watchCollection("filters", function () {
                $scope.loadTable();
            });

            $scope.loadTable = function () {
                alertService.block();

                var beginDate = moment($scope.filters.beginDate).set('hour', 0).set('minute', 0).set('second', 0);
                var endDate = moment($scope.filters.endDate).set('hour', 23).set('minute', 59).set('second', 59);

                autoQuotationService.getQuotations(beginDate, endDate, $scope.filters.text,
                        $scope.pageLength, $scope.pageLength * $scope.currentPage).then(function (response) {
                            $scope.lastQuotations = response;
                            $scope.count = response.length;
                            alertService.unblock();
                        }, function () {
                            alertService.unblock();
                        });
            };

            $scope.loadTable();
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('configEmailCtrl', ['$rootScope', '$scope', 'accountConfiguration', 'alertService', 'accountConfigurationService',
        function ($rootScope, $scope, accountConfiguration, alertService, accountConfigurationService) {

            $scope.model = accountConfiguration;

            $scope.save = function () {
                alertService.block();

                if ($scope.model.accountId) {
                    accountConfigurationService.updateEmail($scope.model).then(function (response) {
                        alertService.success("Configuração salva.");
                        alertService.unblock();
                    }, handleError);
                }
                else {
                    accountConfigurationService.createEmail($scope.model).then(function (response) {
                        alertService.success("Configuração salva.");
                        alertService.unblock();
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.warning("Ocorreu alguma falha para atualizar, tente novamente em alguns minutos.");
                alertService.unblock();
            }
        }
    ]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('configReportCtrl', ['$rootScope', '$scope', 'accountConfiguration', 'alertService', 'accountConfigurationService',
        function ($rootScope, $scope, accountConfiguration, alertService, accountConfigurationService) {

            $scope.model = accountConfiguration;

            $scope.save = function () {
                alertService.block();

                if ($scope.model.accountId) {
                    accountConfigurationService.updateReport($scope.model).then(function (response) {
                        alertService.success("Configuração salva.");
                        alertService.unblock();
                    }, handleError);
                }
                else {
                    accountConfigurationService.createReport($scope.model).then(function (response) {
                        alertService.success("Configuração salva.");
                        alertService.unblock();
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.unblock();
            }
        }
    ]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('contractCtrl', ['$rootScope', '$scope', 'accountService', 'alertService', '$window',
        function ($rootScope, $scope, accountService, alertService, $window) {

            $scope.download = function () {
                alertService.block();

                accountService.getContract().then(function (data) {
                    alertService.unblock();

                    var file = new Blob([data], { type: 'application/pdf' });
                    var fileURL = URL.createObjectURL(file);
                    $window.open(fileURL);
                }, function () {
                    alertService.unblock();
                });
            }
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('homeCtrl', ['$rootScope', '$scope', 'autoQuotationService', 'alertService', 'quotations',
        function ($rootScope, $scope, autoQuotationService, alertService, quotations) {

            $scope.areaLabels = ["Janeiro", "Fevereiro", "Março", "Abril", "Maio", "Junho", "Julho"];
            $scope.areaSeries = ['Bradesco', 'Alfa', 'Azul', 'Chubb', 'Zurich'];
            $scope.areaData = [
              [65, 59, 80, 81, 56, 55, 40],
              [28, 48, 70, 11, 86, 27, 90],
              [38, 38, 80, 11, 36, 33, 55],
              [48, 18, 90, 12, 46, 65, 44],
              [58, 28, 4, 29, 66, 17, 90]
            ];

            $scope.pieLabels = ["Automóvel ", "Vida", "Residencial"];
            $scope.pieData = [3870, 1200, 320];

            $scope.barLabels = ['NOV/2014', 'DEZ/2014', 'JAN/2015'];
            $scope.barSeries = ['Vendas'];
            $scope.barOptions = {
                scaleLabel: "<%= Number(value).toMoney() %>",
                tooltipTemplate: "<%= Number(value).toMoney() %>",
                responsive: true
            };
            $scope.barData = [
              [5800, 19840.55, 25000.10]
            ];

            $scope.lastQuotations = quotations;

            $rootScope.$on("quotationResponse", function (event, notificationRequest) {
                var found = false;
                for (var index = 0; index < $scope.lastQuotations.length; index++) {
                    for (var i = 0; i < $scope.lastQuotations[index].insurerQuotation.length; i++) {

                        if (found) break;

                        if ($scope.lastQuotations[index].insurerQuotation[i].insurerQuotationId == notificationRequest.response.identifier) {
                            found = true;

                            var quotation = $scope.lastQuotations[index].insurerQuotation[i];
                            quotation.status = notificationRequest.response.result.code;
                            quotation.operationDescription = notificationRequest.response.result.description;
                        }
                    }

                    if (found) break;
                }

                $scope.loadCount();
            });

            $scope.loadCount = function () {
                if ($scope.lastQuotations) {
                    for (var i = 0; i < $scope.lastQuotations.length; i++) {
                        $scope.lastQuotations[i].count = $scope.lastQuotations[i].insurerQuotation.filter(function (element) {
                            return element.status == 'Success' || element.status == 'Failure';
                        }).length;
                    }
                }
            };

            $scope.loadCount();
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('navbarCtrl', ['$rootScope', '$scope', 'authService', '$location', '$window',
        function ($rootScope, $scope, authService, $location, $window) {
            $scope.authentication = authService.authentication;

            $scope.logOut = function () {
                authService.logOut();
                $window.location.href = "/auth";
            };

            $scope.menu = [
                {
                    "title": "<i class='glyphicon glyphicon-home'></i> Página Inicial",
                    "href": "/dashboard"
                },
                {
                    "title": "<i class='glyphicon glyphicon-list-alt'></i> Cotações <span class='caret'></span>",
                    "submenu": [
                        {
                            "title": "Nova Cotação",
                            "href": "/cotacao/automovel/cadastro"
                        },
                        {
                            "title": "Minhas Cotações",
                            "href": "/cotacao/automovel"
                        }]
                },
                {
                    "title": "<i class='glyphicon glyphicon-briefcase'></i> Corretora <span class='caret'></span>",
                    "submenu": [
                          {
                              "title": "Dados cadastrais",
                              "href": "/corretora/cadastro",
                          },
                          {
                              "title": "Gerenciamento de usuários",
                              "href": "/corretora/usuarios",
                          },
                          {
                              "title": "Contrato",
                              "href": "/corretora/contrato"
                          }
                    ]
                },
                {
                    "title": "<i class='glyphicon glyphicon-cog'></i> Configurações <span class='caret'></span>",
                    "submenu": [
                        {
                            "title": "Seguradoras",
                            "href": "/configuracao/seguradoras/templates",
                        },
                        {
                            "title": "Relatórios",
                            "href": "/configuracao/relatorios"
                        },
                        {
                            "title": "Email de Proposta",
                            "href": "/configuracao/email"
                        }]
                },
                {
                    "title": "<i class='glyphicon glyphicon-stats'></i> Relatórios <span class='caret'></span>",
                    "submenu": [
                        {
                            "title": "Geral",
                            "href": "/relatorios/geral"
                        },
                        {
                            "title": "Usuários",
                            "href": "/relatorios/usuario"
                        }]
                },

                {
                    "title": "<i class='glyphicon glyphicon-question-sign'></i> Service Desk",
                    "href": "/service-desk",
                    "class": "visible-lg-block"
                }
            ];
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').directive('menu', function () {
        return {
            restrict: 'A',
            scope: {
                menu: '=menu',
                cls: '=ngClass'
            },
            replace: true,
            template: '<ul><li ng-repeat="item in menu" menu-item="item"></li></ul>',
            link: function (scope, element, attrs) {
                element.addClass(attrs.class);
                element.addClass(scope.cls);
            }
        };
    });

    angular.module('AlvacApp').directive('menuItem', ["$compile", function ($compile) {
        return {
            restrict: 'A',
            replace: true,
            scope: {
                item: '=menuItem'
            },
            template: '<li active-link={{item.navBar}} class={{item.class}}><a href={{item.href}} ng-bind-html="item.title"></a></li>',
            link: function (scope, element, attrs) {

                if (scope.item.roles && scope.item.roles.indexOf(scope.$root.authentication.role) == -1) {
                    element.empty();
                    return;
                }

                if (scope.item.header) {
                    element.addClass('nav-header');
                    element.text(scope.item.header);
                }
                if (scope.item.divider) {
                    element.addClass('divider');
                    element.empty();
                }
                if (scope.item.submenu) {
                    element.addClass('dropdown');

                    element.empty();
                    var $a = $('<a class="dropdown-toggle cursor" data-toggle="dropdown" role="button" aria-expanded="false" ng-bind-html="item.title"></a>');
                    element.append($a);

                    var $submenu = $('<div menu="item.submenu" class="dropdown-menu"></div>');
                    element.append($submenu);
                }

                if (scope.item.click) {
                    element.find('a').attr('ng-click', 'item.click()');
                }

                $compile(element.contents())(scope);
            }
        };
    }]);

    angular.module('AlvacApp').directive('activeLink', ['$location', function (location) {
        return {
            restrict: 'A',
            link: function (scope, element, attrs, controller) {
                var anchor = element.children('a');
                scope.location = location;

                var navBar = angular.element(".navbar");

                scope.$watch('location.path()', function (newPath) {
                    if (anchor.attr("href") === newPath) {

                        //METRO MENU
                        //navBar.removeClass();
                        //navBar.addClass("navbar");
                        //if (attrs.activeLink) {
                        //    navBar.addClass(attrs.activeLink);
                        //}
                        //else {
                        //    navBar.addClass("navbar-default");
                        //}


                        var submenu = element.parent();
                        if (submenu.hasClass('dropdown-menu')) {
                            submenu.parent().addClass('active');
                            element.addClass('active');
                        }
                        else {
                            element.addClass('active');
                        }
                    } else {
                        element.removeClass('active');
                    }
                });
            }
        };
    }]);

})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('reportAllUsersCtrl', ['$rootScope', '$scope', 'reportService', '$location', 'alertService',
        function ($rootScope, $scope, reportService, $location, alertService) {

            $scope.query = {};

            $scope.generateReport = function () {
                alertService.block();
                reportService.getAutoQuotationGeneral($scope.query).then(function (response) {

                    $scope.totalPieLabels = [];
                    $scope.totalPieData = [];

                    for (var i = 0; i < response.insurerQuotations.length; i++) {
                        $scope.totalPieLabels.push(response.insurerQuotations[i].insurerDisplay);
                        $scope.totalPieData.push(response.insurerQuotations[i].count);
                    }

                    $scope.statusPieLabels = [];
                    $scope.statusPieData = [];

                    for (var i = 0; i < response.insurerQuotationsStatus.length; i++) {
                        $scope.statusPieLabels.push(response.insurerQuotationsStatus[i].descriptionCode);
                        $scope.statusPieData.push(response.insurerQuotationsStatus[i].count);
                    }

                    $scope.report = response;

                    alertService.unblock();
                });
            };
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('reportByUsersCtrl', ['$rootScope', '$scope', 'reportService', '$location', 'alertService',
        function ($rootScope, $scope, reportService, $location, alertService) {

            $scope.query = {};

            $scope.generateReport = function () {
                $scope.report = {};
                alertService.block();

                var successQuery = angular.copy($scope.query);
                successQuery.status = 4; //SUCCESS

                $scope.successData = [];
                $scope.successLabels = [];
                reportService.getAutoQuotationGroupByUser(successQuery).then(function (response) {
                    $scope.report.success = response;

                    for (var i = 0; i < response.length; i++) {
                        $scope.successLabels.push(response[i].userName);
                        $scope.successData.push(response[i].count);
                    }

                    alertService.unblock();
                });

                $scope.errorData = [];
                $scope.errorLabels = [];

                var errorQuery = angular.copy($scope.query);
                errorQuery.status = 5; //FAILURE

                reportService.getAutoQuotationGroupByUser(errorQuery).then(function (response) {
                    $scope.report.error = response;

                    for (var i = 0; i < response.length; i++) {
                        $scope.errorLabels.push(response[i].userName);
                        $scope.errorData.push(response[i].count);
                    }

                    alertService.unblock();
                });
            };
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('templateAddCtrl', ['$rootScope', '$scope', 'templateService', 'alertService', '$modalInstance',
        function ($rootScope, $scope, templateService, alertService, $modalInstance) {
            $scope.ok = function () {
                $scope.templateForm.submitted = true;

                if ($scope.templateForm.$valid) {
                    alertService.block();

                    templateService.create($scope.model).then(function (response) {
                        $scope.model.accountInsurerTemplateId = response.data.id;
                        $modalInstance.close($scope.model);
                        alertService.success("Template criado com sucesso");
                        alertService.unblock();
                    }, handleError);
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };

            function handleError(response) {
                alertService.unblock();
                alertService.error(respose.message);
            }
        }
    ]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('templateConfigurationCtrl', ['$rootScope', '$scope', '$modalInstance', 'insurer',
        function ($rootScope, $scope, $modalInstance, insurer) {

            $scope.authentication = $rootScope.authentication;
            $scope.insurer = angular.copy(insurer);

            $scope.ok = function () {
                $scope.templateConfigurationForm.submitted = true;

                if ($scope.templateConfigurationForm.$valid) {
                    $modalInstance.close($scope.insurer);
                }
            };

            $scope.cancel = function () {
                $modalInstance.dismiss('cancel');
            };
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('templateListCtrl', ['$rootScope', '$scope', 'templates', 'insurers', '$modal', 'templateService', 'alertService',
        function ($rootScope, $scope, templates, insurers, $modal, templateService, alertService) {
            $scope.templates = templates;
            $scope.insurers = insurers;

            $scope.delete = function (template) {
                alertService.confirm("Confirmação de exclusão", "Tem certeza que deseja deletar esse template?", function () {
                    alertService.block();

                    templateService.remove(template.accountInsurerTemplateId).then(function () {
                        alertService.success("Template deletado com sucesso.");
                        $scope.templates.remove(template);

                        alertService.unblock();
                    }, handleError);
                });
            };

            angular.forEach($scope.templates, function (value, key) {
                $scope.$watch("templates[" + key + "]", function (newValue, oldValue) {
                    if (newValue.isActive != oldValue.isActive) {
                        alertService.block();
                        templateService.putStatusTemplate(newValue.accountInsurerTemplateId, newValue.isActive).then(function () {
                            alertService.unblock();
                        }, function () {
                            alertService.unblock();
                        });
                    }
                }, true);
            });

            $scope.newTemplate = function () {
                var newModalInstance = $modal.open({
                    templateUrl: '/app/components/templateadd/templateAddView.html',
                    controller: 'templateAddCtrl',
                    size: 'lg',
                    backdrop: 'static',
                    keyboard: false
                });

                newModalInstance.result.then(function (item) {
                    $scope.templates.unshift(item);
                }, function () {
                });
            };

            $scope.openTemplateConfiguration = function (insurer, selectedTemplate) {
                alertService.block();

                templateService.getInsurerParameters(insurer.id, selectedTemplate.accountInsurerTemplateId).then(function (response) {
                    insurer.parameters = response;
                    alertService.unblock();

                    var modalInstance = $modal.open({
                        templateUrl: '/app/components/templateconfiguration/templateConfigurationView.html',
                        controller: 'templateConfigurationCtrl',
                        size: 'lg',
                        backdrop: 'static',
                        keyboard: false,
                        resolve: {
                            insurer: function () {
                                return insurer;
                            }
                        }
                    });

                    modalInstance.result.then(function (item) {
                        alertService.block();
                        templateService.postParameters(item.parameters).then(function (response) {
                            alertService.unblock();
                        }, handleError);

                    }, function () {

                    });

                }, handleError);
            };

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.message);
                }
            }

        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('userAddCtrl', ['$rootScope', '$scope', 'userService', '$location', 'alertService', 'roles',
        function ($rootScope, $scope, userService, $location, alertService, roles) {

            $scope.pageTitle = "Cadastro de usuário";

            $scope.roles = roles;
            $scope.model = {
                isActive: true
            };

            $scope.save = function () {

                $scope.form.submitted = true;

                if ($scope.form.$valid) {
                    userService.create($scope.model).then(function (response) {
                        $location.path("/corretora/usuarios");
                        alertService.success("Usuário criado com sucesso.");
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.message);
                }
            }
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('userEditCtrl', ['$rootScope', '$scope', 'userService', '$location', 'alertService', 'roles', 'user',
        function ($rootScope, $scope, userService, $location, alertService, roles, user) {

            $scope.pageTitle = "Editando usuário";

            $scope.roles = roles;
            $scope.model = user;

            $scope.save = function () {

                $scope.form.submitted = true;

                if ($scope.form.$valid) {
                    alertService.block();

                    userService.update($scope.model.id, $scope.model).then(function (response) {
                        $location.path("/corretora/usuarios");
                        alertService.success("Usuário atualizado com sucesso.");
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.message);
                }
            }
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('userListCtrl', ['$rootScope', '$scope', 'userService', '$location', 'alertService',
        function ($rootScope, $scope, userService, $location, alertService) {

            alertService.block();
            $scope.pageTitle = "Gerenciamento de usuários";

            $scope.goToDetails = function (id) {
                $location.path("/corretora/usuarios/cadastro/" + id);
            };

            $scope.loadTable = function () {
                userService.getUsers().then(function (response) {
                    $scope.users = response;
                    alertService.unblock();
                }, handleError);
            };

            $scope.delete = function (user) {
                alertService.confirm("Confirmação de exclusão", "Tem certeza que deseja deletar esse usuário?", function () {
                    alertService.block();

                    userService.remove(user.id).then(function () {
                        alertService.success("Usuário deletado com sucesso.");
                        $scope.users.remove(user);
                        alertService.unblock();
                    }, handleError);
                });
            };

            $scope.loadTable();

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.data.message);
                }
            }

        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').controller('userManageCtrl', ['$rootScope', '$scope', 'userService', '$location', 'alertService', 'roles', 'user',
        function ($rootScope, $scope, userService, $location, alertService, roles, user) {

            $scope.pageTitle = "Editando usuário";

            $scope.roles = roles;
            $scope.model = user;

            $scope.save = function () {

                $scope.form.submitted = true;

                if ($scope.form.$valid) {
                    alertService.block();

                    userService.update($scope.model.id, $scope.model).then(function (response) {
                        $location.path("/corretora/usuarios");
                        alertService.success("Usuário atualizado com sucesso.");
                    }, handleError);
                }
            };

            function handleError(response) {
                alertService.unblock();
                if (response) {
                    alertService.error(respose.message);
                }
            }
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('accountConfigurationService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getConfiguration: function () {
                return $http.get(serviceBase + "account-configuration").then(function (response) {
                    return response.data;
                });
            },

            updateEmail: function (model) {
                return $http.put(serviceBase + "account-configuration/email", model);
            },

            createEmail: function (model) {
                return $http.post(serviceBase + "account-configuration/email", model);
            },

            updateReport: function (model) {
                return $http.put(serviceBase + "account-configuration/report", model);
            },

            createReport: function (model) {
                return $http.post(serviceBase + "account-configuration/report", model);
            }
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('accountConnectionService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {

            getInsurers: function (requestType) {

                return $http.get(serviceBase + "account/insurers/connections/" + requestType).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);

})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('accountService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAccount: function () {
                return $http.get(serviceBase + "account/self").then(function (response) {
                    return response.data;
                });
            },

            getContract: function () {
                return $http.get(serviceBase + "account/self/contract", { responseType: 'arraybuffer' }).then(function (response) {
                    return response.data;
                });
            },

            update: function (model) {
                return $http.put(serviceBase + "account/", model);
            }

        };
    }]);
})();
(function () {
    'use strict';
    angular.module('AlvacApp').factory('alertService', ['toaster', function (toaster) {

        var serviceServiceFactory = {};

        var _success = function (text) {
            toaster.pop("success", "", text);
        };

        var _warning = function (text) {
            toaster.pop("warning", "", text);
        };

        var _error = function (text) {
            toaster.pop("error", "", text);
        };

        var _confirm = function (title, message, successCallback, cancelCallback) {
            bootbox.dialog({
                message: message,
                title: title,
                buttons: {
                    cancel: {
                        label: "Cancelar",
                        className: "btn-danger",
                        callback: cancelCallback
                    },
                    success: {
                        label: "Confirmar",
                        className: "btn-success",
                        callback: successCallback
                    }
                }
            });
        };

        var _block = function (element) {
            if (element) {
                $(element).block({ message: null });
            }
            else {
                var imgSrc = '/assets/img/loader.gif';
                $.blockUI({
                    message: '<img src="' + imgSrc + '" id="spinner" align="absmiddle"> Aguarde um momento...',
                    overlayCSS: {
                        backgroundColor: '#000',
                        opacity: 0.05,
                        cursor: 'wait',
                        baseZ: 10000
                    }
                });

                setTimeout("$('#spinner').attr('src', '" + imgSrc + "');", 100);
            }
        };

        var _unblock = function (element) {
            if (element) {
                $(element).unblock();
            }
            else {
                $.unblockUI();
            }
        };

        serviceServiceFactory.success = _success;
        serviceServiceFactory.warning = _warning;
        serviceServiceFactory.error = _error;
        serviceServiceFactory.confirm = _confirm;
        serviceServiceFactory.block = _block;
        serviceServiceFactory.unblock = _unblock;
        return serviceServiceFactory;
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('autoQuotationService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getManufacturers: function () {
                return $http.get(serviceBase + "vehicle/manufacturer", { cache: true }).then(function (response) {
                    return response.data;
                });
            },

            getVehicles: function (modelYear, manufacturer) {
                return $http.get(serviceBase + "vehicle/manufacturer/" + manufacturer + '/year/' + modelYear, { cache: true }).then(function (response) {
                    return response.data;
                });
            },

            getInsurerVehicles: function (insurer, vehicleId) {
                return $http.get(serviceBase + "vehicle/insurer/" + insurer + '/vehicle/' + vehicleId).then(function (response) {
                    return response.data;
                });
            },

            getVehiclesByModel: function (model, manufacturer) {
                return $http.get(serviceBase + "vehicle/manufacturer/" + manufacturer + '?model=' + model, { cache: true }).then(function (response) {
                    return response.data;
                });
            },

            getQuotationResponse: function (id) {
                return $http.get(serviceBase + "quotation/" + id + '/response').then(function (response) {
                    return response.data;
                });
            },

            getQuotations: function (beginDate, endDate, text, take, skip) {
                var uri = serviceBase + "quotation/auto";

                uri += "?$skip=" + (skip ? skip : 0);

                if (take) {
                    uri += "&$top=" + take;
                }

                uri += "&$filter=RequestDateTime ge " + beginDate.toJSON();
                uri += " and RequestDateTime le " + endDate.toJSON();

                if (text) {
                    uri += " and (contains(FullName, '" + text + "') or contains(Document, '" + text + "'))";
                }

                return $http.get(uri).then(function (response) {
                    return response.data;
                });
            },

            getLastQuotations: function (take, skip) {
                var uri = serviceBase + "quotation/auto";

                uri += "?$skip=" + (skip ? skip : 0);

                if (take) {
                    uri += "&$top=" + take;
                }

                return $http.get(uri).then(function (response) {
                    return response.data;
                });
            },

            quotation: function (model) {
                model.Request.requestType = "AutoQuotation";
                return $http.post(serviceBase + "quotation/auto", model).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('deviceTrackerService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAll: function () {
                return $http.get(serviceBase + "device-tracker").then(function (response) {
                    return response.data;
                });
            },
        };
    }]);
})();
(function () {
    'use strict';
    angular.module('AlvacApp').factory('emailService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            sendEmail: function (model) {
                return $http.post(serviceBase + "email", model);
            }
        };

    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('ignitionBlockerService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAll: function () {
                return $http.get(serviceBase + "ignition-blocker").then(function (response) {
                    return response.data;
                });
            },
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('insurerService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getInsurers: function () {
                return $http.get(serviceBase + "insurer", { cache: true }).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('reportService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAutoQuotationGeneral: function (query) {
                return $http.post(serviceBase + "report/auto-quotation/general", query).then(function (response) {
                    return response.data;
                });
            },

            getAutoQuotationGroupByUser: function (query) {
                return $http.post(serviceBase + "report/auto-quotation/user", query).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('templateService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getTemplates: function () {
                return $http.get(serviceBase + "template").then(function (response) {
                    return response.data;
                });
            },

            getActiveTemplates: function (requestType) {
                return $http.get(serviceBase + "template?$filter=IsActive eq true and RequestType eq Alvac.Domain.Enum.RequestType'" + requestType + "'").then(function (response) {
                    return response.data;
                });
            },

            getInsurerParameters: function (insurerId, accountInsurerTemplateId) {
                return $http.get(serviceBase + "template/" + accountInsurerTemplateId + "/insurers/" + insurerId + "/parameters").then(function (response) {
                    return response.data;
                });
            },

            postParameters: function (model) {
                return $http.post(serviceBase + "template/parameters", model);
            },

            create: function (model) {
                return $http.post(serviceBase + "template", model);
            },

            remove: function (id) {
                return $http.delete(serviceBase + "template/" + id);
            },

            putStatusTemplate: function (accountInsurerTemplateId, status) {
                return $http.put(serviceBase + "template/" + accountInsurerTemplateId + "/status",
                    { accountInsurerTemplateId: accountInsurerTemplateId, isActive: status });
            },
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('userService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {

            getUser: function(id){
                return $http.get(serviceBase + "user/" + id).then(function (response) {
                    return response.data;
                });
            },

            getUsers: function () {

                return $http.get(serviceBase + "user?$filter=IsActive eq true").then(function (response) {
                    return response.data;
                });
            },

            getRoles: function () {

                return $http.get(serviceBase + "user/roles").then(function (response) {
                    return response.data;
                });
            },

            confirmEmail: function (model) {
                return $http.put(serviceBase + "user/confirm-email", model);
            },
            
            isUniqueEmail: function (email, id) {
                var url = serviceBase + 'user/email/' + encodeURIComponent(email);

                if (id) {
                    url = url + '/' + id;
                }

                return $http.get(url).then(function (results) {
                    return results.data;
                });
            },

            create: function (model) {
                return $http.post(serviceBase + "user", model);
            },

            update: function (id, model) {
                return $http.put(serviceBase + "user/" + id, model);
            },

            remove: function (id) {
                return $http.delete(serviceBase + "user/" + id);
            },
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('workActivityService', ['$http', 'settings', function ($http, settings) {

        var serviceBase = settings.apiBaseUri;

        return {
            getAll: function () {
                return $http.get(serviceBase + "work-activity", { cache: true }).then(function (response) {
                    return response.data;
                });
            }
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').filter('checkmark', function () {
        return function (input) {
            return input === "true" ? '<i class="glyphicon glyphicon-ok"></i>' : '<i class="glyphicon glyphicon-remove"></i>';
        };
    });
})();
(function () {
    'use strict';

    angular.module('AlvacApp').filter('deductibleType', function () {
        return function (input) {
            switch (input) {
                case "Required":
                    input = "Obrigatória";
                    break;
                case "Reduced50Percent":
                    input = "Reduzida 50%";
                    break;
                case "Reduced25Percent":
                    input = "Reduzida 25%";
                    break;
                case "Optional50Percent":
                    input = "Facultativa 50%";
                    break;
                case "Optional25Percent":
                    input = "Facultativa 25%";
                    break;
            }

            return input;
        };
    });
})();
(function () {
    'use strict';

    angular.module('AlvacApp').filter('role', function () {
        return function (input) {
            switch (input) {
                case "Accounting":
                    input = "Corretor";
                    break;
            }

            return input;
        };
    });
})();
(function () {
    'use strict';

    angular.module('AlvacApp')
        .directive('capitalize', function () {
            return {
                require: 'ngModel',
                link: function (scope, element, attrs, modelCtrl) {

                    var capitalize = function (inputValue) {
                        if (inputValue === undefined) inputValue = '';
                        var capitalized = inputValue.toUpperCase();
                        if (capitalized !== inputValue) {
                            modelCtrl.$setViewValue(capitalized);
                            modelCtrl.$render();
                        }
                        return capitalized;
                    };

                    modelCtrl.$parsers.push(capitalize);
                    capitalize(scope[attrs.ngModel]);
                }
            };
        });
})();
(function () {
    'use strict';

    angular.module('AlvacApp')
        .directive('ckEditor', function () {
            return {
                restrict: 'A',
                scope: {
                    model: '=ngModel'
                },
                link: function ($scope, element, attrs) {

                    CKEDITOR.replace(attrs.name);

                    CKEDITOR.focusManager._.blurDelay = 10;

                    CKEDITOR.instances[attrs.name].on('blur', function (e) {
                        if (e.editor.checkDirty()) {
                            $scope.$apply(function () {
                                $scope.model = e.editor.getData();
                            });
                        }
                    });
                }
            };
        });
})();
(function () {
    'use strict';

    angular.module('AlvacApp').directive('compareTo', [function () {
        return {
            require: "ngModel",
            scope: {
                otherModelValue: "=compareTo"
            },
            link: function (scope, element, attributes, ngModel) {

                ngModel.$validators.compareTo = function (modelValue) {
                    return modelValue == scope.otherModelValue;
                };

                scope.$watch("otherModelValue", function () {
                    ngModel.$validate();
                });
            }
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp')
        .directive('jqSelect2', function () {
            return {
                restrict: 'A',
                scope: {
                    ngModel: '='
                },
                link: function ($scope, element, attrs) {
                    $(element).select2({
                        placeholder: "Selecione"
                    });

                    $(element).val($scope.ngModel);
                }
            };
        });
})();
(function () {
    'use strict';

    angular.module('AlvacApp')
        .directive('masterOnly', [function () {
            return {
                restrict: 'A',
                controller: 'initCtrl',
                link: function (scope, element, attrs, modelCtrl) {

                    if (attrs.masterOnly == "" || attrs.masterOnly == "disabled") {
                        if (!scope.authentication.isMaster()) {
                            element.attr("disabled", "disabled");
                        }
                    }
                    else if (attrs.masterOnly == "show") {
                        if (!scope.authentication.isMaster()) {
                            element.css("display", "none");
                        }
                    }
                }
            };
        }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp')
        .directive('numberOnly', function () {
            return {
                restrict: 'A',
                scope: {},
                link: function ($scope, element, attrs) {

                    $(element).on('change keyup', function () {
                        // Remove invalid characters
                        var sanitized = $(this).val().replace(/[^-.0-9]/g, '');
                        // Remove non-leading minus signs
                        sanitized = sanitized.replace(/(.)-+/g, '$1');
                        // Remove the first point if there is more than one
                        sanitized = sanitized.replace(/\.(?=.*\.)/g, '');
                        // Update value
                        $(this).val(sanitized);
                    });
                }
            };
        });
})();
(function () {
    'use strict';

    angular.module('AlvacApp').directive('uniqueEmail', ['$q', 'userService', function ($q, userService) {
        return {
            require: 'ngModel',
            link: function (scope, elm, attrs, ctrl) {

                ctrl.$asyncValidators.uniqueEmail = function (modelValue, viewValue) {

                    if (ctrl.$isEmpty(modelValue)) {
                        return $q.when();
                    }

                    var keyProperty = scope.$eval(attrs.uniqueEmail);
                    return userService.isUniqueEmail(modelValue, keyProperty);
                };
            }
        };
    }]);
})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('authInterceptorService', ['$q', '$injector', '$location', 'localStorageService', function ($q, $injector, $location, localStorageService) {

        var authInterceptorServiceFactory = {};

        var _request = function (config) {

            config.headers = config.headers || {};

            var authData = localStorageService.get('authorizationData');

            if (authData) {
                config.headers.Authorization = 'Bearer ' + authData.token;
            }

            return config;
        };

        var _responseError = function (rejection) {
            if (rejection.status === 401) {
                var authService = $injector.get('authService');
                var alertService = $injector.get('alertService');
                var authData = localStorageService.get('authorizationData');

                if (authData) {
                    if (authData.useRefreshTokens) {
                        $location.path('/refresh');
                        return $q.reject(rejection);
                    }
                }

                $(".modal").modal('hide');
                alertService.unblock();
                authService.logOut();
                $location.path('/Auth');
            }
            return $q.reject(rejection);
        };

        authInterceptorServiceFactory.request = _request;
        authInterceptorServiceFactory.responseError = _responseError;

        return authInterceptorServiceFactory;
    }]);

})();
(function () {
    'use strict';

    angular.module('AlvacApp').factory('offSetInterceptorService', [function () {

        var interceptorServiceFactory = {};

        var _request = function (config) {

            config.headers = config.headers || {};
            config.headers.TimezoneOffsetMinutes = new Date().getTimezoneOffset();
            return config;
        }

        interceptorServiceFactory.request = _request;

        return interceptorServiceFactory;
    }]);
})();