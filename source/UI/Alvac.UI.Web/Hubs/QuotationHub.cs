﻿using Microsoft.AspNet.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace Alvac.UI.Web.Hubs
{
    public class QuotationHub : Hub
    {
        public override System.Threading.Tasks.Task OnConnected()
        {
            Groups.Add(Context.ConnectionId, Context.Request.QueryString.Get("userId"));
            return base.OnConnected();
        }
    }
}