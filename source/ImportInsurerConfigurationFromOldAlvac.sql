SELECT 'INSERT INTO [dbo].[InsurerConfigurations] VALUES (7, ' + CAST(c.Obrigatorio as varchar(MAX)) + ', ''' + c.Descricao + ''', ''System.String'', ''' + c.Codigo + ''', null)'
FROM ck_configuracao c
WHERE ConfiguracaoId not in (SELECT ConfiguracaoId FROM ck_configuracao_valor)
AND c.SeguradoraId = 1

SELECT 
'INSERT INTO [dbo].[InsurerConfigurations] VALUES (7, ' + CAST(c.Obrigatorio as varchar(MAX)) + ', ''' + c.Descricao + ''', ''System.String'', ''' + c.Codigo + ''', ''' + 
STUFF((SELECT  '|' + ti.Valor 
FROM ck_configuracao_valor ti 
WHERE ti.ConfiguracaoId = cv.ConfiguracaoId 
FOR XML PATH('')), 1, 1, '') + ''')'
FROM ck_configuracao_valor cv
JOIN ck_configuracao c on c.ConfiguracaoId = cv.ConfiguracaoId
AND c.SeguradoraId = 1
GROUP BY cv.ConfiguracaoId, c.Obrigatorio, c.Descricao, c.Codigo