﻿using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Contract.Response.Auto;
using Alvac.Domain.Enum;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Bradesco.Auto.Ws
{
    public class BradescoAutoWsDispatcher : DispatcherBase
    {
        #region DispatcherBase Members

        public override async Task<BaseResponse> ProcessAsync(BaseRequest request, IDictionary<string, object> insurerParameters)
        {
            Random random = new Random();

            var autoQuotationResponse = request.CreateResponse(Code.Success, "Auto quotation bradesco web-service successfuly.") as AutoQuotationResponse;
            autoQuotationResponse.PremiumValue = random.Next(987, 1985);
            autoQuotationResponse.Coverage = new CoverageResponse
            {
                BodyDamages = random.Next(15000, 60000),
                MaterialDamages = random.Next(15000, 60000),
                MoralDamages = random.Next(15000, 60000),
                PersonalAccidentPassenger = random.Next(15000, 60000)
            };
            autoQuotationResponse.DeductibleValue = random.Next(15000, 60000);

            await Task.Delay(new Random().Next(5000, 30000));

            return autoQuotationResponse;
        }
        
        #endregion

       
    }
}
