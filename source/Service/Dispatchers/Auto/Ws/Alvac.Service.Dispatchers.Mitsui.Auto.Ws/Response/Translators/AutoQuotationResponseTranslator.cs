﻿using Alvac.Domain.Contract.Response;
using Alvac.Domain.Contract.Response.Auto;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Response.Translators
{
    public class AutoQuotationResponseTranslator : IResponseTranslator<Calculo>
    {
        #region IResponseTranslator Members

        public void ToDispatcherResponse(Calculo dispatcherResponse, ref BaseResponse baseResponse)
        {
            Check.NotNull(baseResponse, "dispatcherResponse");
            Check.NotSameType(baseResponse, typeof(AutoQuotationResponse));

            var autoResponse = baseResponse as AutoQuotationResponse;
            autoResponse.DeductibleValue = decimal.Parse(dispatcherResponse.Coberturas.FirstOrDefault(x => x.codigo == "00001").vlr_franquia, CultureInfo.GetCultureInfo("en-GB"));
            autoResponse.Coverage = GetCoverageResponse(dispatcherResponse.Coberturas); 

            /// TODO: Get Values
            //autoResponse.HasWinch = ;
            //autoResponse.WinchKm  = ;
            //autoResponse.HasCourtesyCar  = ;
            //autoResponse.CourtesyCarDays  = ;
        }

        #endregion

        #region Private FIelds

        private CoverageResponse GetCoverageResponse(CalculoCoberturas[] coberturas)
        {
            return new CoverageResponse
            {
                MoralDamages = coberturas.Any(x => x.codigo == "00008")
                                ? decimal.Parse(coberturas.FirstOrDefault(x => x.codigo == "00008").valor_lmi, CultureInfo.GetCultureInfo("en-GB"))
                                : 0,

                BodyDamages = coberturas.Any(x => x.codigo == "00004")
                                ? decimal.Parse(coberturas.FirstOrDefault(x => x.codigo == "00004").valor_lmi, CultureInfo.GetCultureInfo("en-GB"))
                                : 0,

                MaterialDamages = coberturas.Any(x => x.codigo == "00003")
                                ? decimal.Parse(coberturas.FirstOrDefault(x => x.codigo == "00003").valor_lmi, CultureInfo.GetCultureInfo("en-GB"))
                                : 0,

                MedicalExpenses = coberturas.Any(x => x.codigo == "00007")
                                ? decimal.Parse(coberturas.FirstOrDefault(x => x.codigo == "00007").valor_lmi, CultureInfo.GetCultureInfo("en-GB"))
                                : 0,

                PersonalAccidentPassenger = coberturas.Any(x => x.codigo == "00006")
                                ? decimal.Parse(coberturas.FirstOrDefault(x => x.codigo == "00006").valor_lmi, CultureInfo.GetCultureInfo("en-GB"))
                                : 0
            };
        }

        #endregion
    }
}
