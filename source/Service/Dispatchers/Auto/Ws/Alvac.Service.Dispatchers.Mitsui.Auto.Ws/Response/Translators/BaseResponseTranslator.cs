﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Enum;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Response.Translators
{
    public class BaseResponseTranslator : IResponseTranslator<Calculo>
    {
        #region IResponseTranslator Members

        public void ToDispatcherResponse(Calculo dispatcherResponse, ref BaseResponse baseResponse)
        {
            Check.NotNull(dispatcherResponse, "dispatcherResponse");
            Check.NotNull(dispatcherResponse.Contrato, "dispatcherResponse.Contrato");

            baseResponse.GatewayId = dispatcherResponse.Contrato.contrato;
            baseResponse.InsurerBudgetCode = dispatcherResponse.Contrato.contrato;
            baseResponse.PremiumValue = decimal.Parse(dispatcherResponse.Contrato.VlrPremioTotal, CultureInfo.GetCultureInfo("en-GB"));
            baseResponse.ResponseDateTime = DateTimeOffset.Now;
            baseResponse.ValidUntil = DateTime.ParseExact(dispatcherResponse.Contrato.valido_ate, "dd/MM/yyyy", CultureInfo.InvariantCulture);

            baseResponse.Result = new OperationResult
            {
                Code = Code.Success,
                DescriptionCode = DescriptionCode.Successfully,
                Description = "Calculo realizado com sucesso."
            };
        }

        #endregion
    }
}
