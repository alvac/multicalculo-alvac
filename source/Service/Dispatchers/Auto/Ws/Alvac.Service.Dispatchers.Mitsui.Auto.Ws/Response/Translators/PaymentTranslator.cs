﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Response.Translators
{
    public class PaymentTranslator : IResponseTranslator<Calculo>
    {
        #region IResponseTranslator Members

        public void ToDispatcherResponse(Calculo dispatcherResponse, ref BaseResponse baseResponse)
        {
            Check.NotNull(dispatcherResponse, "dispatcherResponse");
            Check.NotNull(dispatcherResponse.Parcelas, "dispatcherResponse.Parcelas");

            baseResponse.Payment = new List<PaymentResponse>();

            foreach (var item in dispatcherResponse.Parcelas)
            {
                var paymentResponse = new PaymentResponse
                {
                    Code = item.codigo,
                    Description = item.descricao,
                    FirstInstallmentValue = !string.IsNullOrEmpty(item.vlr_primeira) ? decimal.Parse(item.vlr_primeira, CultureInfo.GetCultureInfo("en-GB")) : 0,
                    OtherInstallmentsValue = !string.IsNullOrEmpty(item.vlr_demais) ? decimal.Parse(item.vlr_demais, CultureInfo.GetCultureInfo("en-GB")) : 0,
                    TotalInstallmentsValue = !string.IsNullOrEmpty(item.vlr_total) ? decimal.Parse(item.vlr_total, CultureInfo.GetCultureInfo("en-GB")) : 0
                };

                baseResponse.Payment.Add(paymentResponse);
            }
        }

        #endregion
    }
}
