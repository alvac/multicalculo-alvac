﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Contract.Response.Auto;
using Alvac.Domain.Enum;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Service.Dispatchers.Mitsui.Auto.Ws.MitsuiServiceReference;
using Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.ServiceModel;
using System.ServiceModel.Description;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws
{
    public class MitsuiAutoWsDispatcher : DispatcherBase
    {
        #region Private Fields

        private readonly ILogger _logger;
        private readonly IEnumerable<IRequestTranslator<Root>> _mitsuiQuotationRequestTranslator;
        private readonly IEnumerable<IResponseTranslator<Calculo>> _mitsuiQuotationResponseTranslator;

        #endregion

        #region Constructor

        public MitsuiAutoWsDispatcher(IEnumerable<IRequestTranslator<Root>> mitsuiQuotationRequestTranslator,
            IEnumerable<IResponseTranslator<Calculo>> mitsuiQuotationResponseTranslator,
            ILogger logger)
        {
            _mitsuiQuotationRequestTranslator = mitsuiQuotationRequestTranslator;
            _mitsuiQuotationResponseTranslator = mitsuiQuotationResponseTranslator;
            _logger = logger;
        }

        #endregion

        #region DispatcherBase Members

        public override async Task<BaseResponse> ProcessAsync(BaseRequest request, IDictionary<string, object> userInsurerParameters)
        {
            BaseResponse result = null;
            ExceptionDispatchInfo exception = null;

            try
            {
                Check.NotNull(request, "request");
                Check.NotSameType(request, typeof(AutoQuotationRequest));

                var autoQuotationRequest = request as AutoQuotationRequest;

                try
                {
                    var mitsuiQuotationRequest = new Root();

                    foreach (var item in _mitsuiQuotationRequestTranslator)
                    {
                        await _logger.WriteVerboseAsync("MitsuiAutoWsDispatcher", "ProcessAsync", "Iniciando Translator:" + item.GetType().ToString());

                        await item.ToDispatcherRequestAsync(request, userInsurerParameters, mitsuiQuotationRequest);

                        await _logger.WriteVerboseAsync("MitsuiAutoWsDispatcher", "ProcessAsync", "Fim Translator:" + item.GetType().ToString());
                    }

                    var mitsuiQuotationRequestXml = AlvacXml<Root>.SerializeRemoveXmlTag(mitsuiQuotationRequest);

                    var client = GetMitsuiClient();

                    var mitsuiQuotationResponseXml = await client.CalcularAsync(mitsuiQuotationRequestXml);

                    if (!string.IsNullOrEmpty(mitsuiQuotationResponseXml))
                    {
                        await _logger.WriteInformationAsync("MitsuiAutoWsDispatcher", "ProcessAsync", "Response:" + mitsuiQuotationResponseXml);

                        var mitsuiQuotationResponse = AlvacXml<Calculo>.Deserialize(mitsuiQuotationResponseXml);

                        if (mitsuiQuotationResponse.Status != null)
                        {
                            if (mitsuiQuotationResponse.Status.Realizado.ToLower() == "s")
                            {
                                result = new AutoQuotationResponse(request.Identifier);

                                foreach (var item in _mitsuiQuotationResponseTranslator)
                                {
                                    await _logger.WriteVerboseAsync("MitsuiAutoWsDispatcher", "ProcessAsync", "Iniciando Response Translator:" + item.GetType().ToString());

                                    item.ToDispatcherResponse(mitsuiQuotationResponse, ref result);

                                    await _logger.WriteVerboseAsync("MitsuiAutoWsDispatcher", "ProcessAsync", "Fim Response Translator:" + item.GetType().ToString());
                                }
                            }
                            else
                            {
                                result = request.CreateResponse(Code.Failure, GetMitsuiErrorDescription(mitsuiQuotationResponse.Erros));
                            }
                        }
                    }
                    else
                    {
                        throw new Exception("Resposta do calculo Mitsui é nula ou vazia.");
                    }
                }
                catch (AlvacException ex)
                {
                    exception = ExceptionDispatchInfo.Capture(ex);
                    result = request.CreateResponse(Code.Failure, ex.DescriptionCode, ex.Message);
                }
                catch (Exception ex)
                {
                    exception = ExceptionDispatchInfo.Capture(ex);
                    result = request.CreateResponse(Code.Failure, ex.Message);
                }
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
                result = new AutoQuotationResponse();
                result.Result = new OperationResult
                {
                    Code = Code.Failure,
                    DescriptionCode = DescriptionCode.InternalServerError,
                    Message = ex.Message
                };
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("MitsuiAutoWsDispatcher", "ProcessAsync", exception.SourceException);
            }

            return result;
        }
        
        #endregion

        #region Private Methods

        private msskitSoapClient GetMitsuiClient()
        {
            var mitsuiParameters = new MitsuiConnectionParameters(this.DispatcherData.Parameters);

            BasicHttpBinding binding = new BasicHttpBinding() { SendTimeout = TimeSpan.FromSeconds(60) };

            Uri uri = new Uri(mitsuiParameters.Url);

            if (uri.Scheme == Uri.UriSchemeHttps)
            {
                binding.Security.Mode = BasicHttpSecurityMode.Transport;
            }

            EndpointAddress address = new EndpointAddress(uri);

            return new msskitSoapClient(binding, address);
        }

        private string GetMitsuiErrorDescription(CalculoErros[] erros)
        {

            if (erros != null && erros.Count() > 0)
            {
                return string.Join(Environment.NewLine, erros.Select(x => x.descricao));
            }
            else
            {
                return "Mitsui retornou um erro sem descrição.";
            }
        }

        #endregion
    }
}


