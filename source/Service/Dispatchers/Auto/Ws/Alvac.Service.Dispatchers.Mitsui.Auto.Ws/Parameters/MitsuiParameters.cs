﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Text;
using Alvac.Domain.Services.Parameters;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Parameters
{
    public class MitsuiParameters : ParameterBase
    {
        #region Contructor

        public MitsuiParameters(IDictionary<string, object> parameters) :
            base(parameters)
        {
            User = GetValue<string>("Usuario");
            PartnerAgreement = GetValue<string>("ContratoParceiro");
            RcfDm = GetValue<decimal>("DM");
            RcfDc = GetValue<decimal>("DC");
            MoralDamages = GetValue<string>("DanosMorais");
            AppM = GetValue<string>("AppM");
            AppI = GetValue<string>("AppI");
            ExtraExpenses = GetValue<string>("DespesasExtras");
            ComplementaryExpenses = GetValue<string>("DespesasComplementares");
            ComplementaryExpenses = GetValue<string>("DespesasMedicas");
            FuneralPlan = GetValue<string>("Planofuneral");
            Glass = GetValue<string>("Vidros");
            TwentyFourHoursAssistance = GetValue<string>("Assistencia24Horas");
            CourtesyCar = GetValue<string>("PadraoCarroReserva");
            Trailer = GetValue<string>("Reboque");
            Commission = GetValue<decimal>("PorcentagemComissao");
            ExemptionType = GetValue<string>("TipoIsencao");
            ApplicationType = GetValue<string>("TipoAplicacaoAgravo");
            SpecialDiscount = GetValue<string>("PorcentagemDescontoEspecial");
            ChassisRescheduled = GetValue<string>("ChassiRemarcado");
        }

        #endregion

        public string User { get; set; }

        public string PartnerAgreement { get; set; }

        public decimal RcfDm { get; set; }

        public decimal RcfDc { get; set; }

        public string MoralDamages { get; set; }

        public string AppM { get; set; }

        public string AppI { get; set; }

        public string ExtraExpenses { get; set; }

        public string ComplementaryExpenses { get; set; }

        public string MedicalExpenses { get; set; }

        public string FuneralPlan { get; set; }

        public string Glass { get; set; }

        public string TwentyFourHoursAssistance { get; set; }

        public string CourtesyCar { get; set; }

        public string Trailer { get; set; }

        public decimal Commission { get; set; }

        public string ExemptionType { get; set; }

        public string ApplicationType { get; set; }

        public string SpecialDiscount { get; set; }

        public string ChassisRescheduled { get; set; }
    }
}


