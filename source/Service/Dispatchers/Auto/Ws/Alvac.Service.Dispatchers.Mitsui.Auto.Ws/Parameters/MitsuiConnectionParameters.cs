﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using Alvac.Domain.Services.Parameters;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Parameters
{
    public class MitsuiConnectionParameters : ParameterBase
    {
        #region Contructor

        public MitsuiConnectionParameters(IDictionary<string, object> parameters) :
            base(parameters)
        {
            if (parameters == null || !parameters.Any())
            {
                throw new Exception("Parâmetros de conexão Mitsui não configurados.");
            }

            this.Url = GetValue<string>("url");
            this.User = GetValue<string>("user");
            this.Password = GetValue<string>("password");
        }

        #endregion

        #region Public Properties

        /// <summary>
        /// Url do web-service mitsui
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Usuário do web-service mitsui
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Senha do web-service mitsui
        /// </summary>
        public string Password { get; set; }

        #endregion
    }
}


