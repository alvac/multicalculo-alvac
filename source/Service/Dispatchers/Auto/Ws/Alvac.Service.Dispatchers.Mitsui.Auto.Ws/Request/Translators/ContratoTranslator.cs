﻿using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Request.Translators
{
    public class ContratoTranslator : IRequestTranslator<Root>
    {
        private readonly ILogger _logger;

        public ContratoTranslator(ILogger logger)
        {
            _logger = logger;
        }

        public async Task ToDispatcherRequestAsync(Alvac.Domain.Contract.Request.BaseRequest baseRequest, IDictionary<string, object> insurerParameters, Root dispatcherRequest)
        {
            Check.NotNull(baseRequest, "baseRequest");
            Check.NotSameType(baseRequest, typeof(AutoQuotationRequest));

            ExceptionDispatchInfo exception = null;

            try
            {
                var autoQuotationRequest = baseRequest as AutoQuotationRequest;
                var parameters = new MitsuiParameters(insurerParameters);

                dispatcherRequest.contrato = new RootContrato
                {
                    corretor = parameters.User,
                    // Único produto usado.
                    produto = "31001",
                    data_ini_segr = autoQuotationRequest.Insurance.InsurancePolicyInitialDate.ToString("dd/MM/yyyy"),
                    data_fim_segr = autoQuotationRequest.Insurance.InsurancePolicyFinalDate.ToString("dd/MM/yyyy"),
                    segurado = autoQuotationRequest.Insured.FullName,
                    CPFSegurado = autoQuotationRequest.Insured.Document,
                    // Alvac só trabalha com tipo de calculo anual.
                    tipo_calculo = "AN"
                };
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("MitsuiAutoWsDispatcher", "AgravdescTranslator", exception.SourceException);
                exception.Throw();
            }
        }
    }
}
