﻿using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Enum;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Request.Translators
{
    public class EmissaoTranslator : IRequestTranslator<Root>
    {
        private readonly ILogger _logger;

        public EmissaoTranslator(ILogger logger)
        {
            _logger = logger;
        }

        public async Task ToDispatcherRequestAsync(Alvac.Domain.Contract.Request.BaseRequest baseRequest, IDictionary<string, object> insurerParameters, Root dispatcherRequest)
        {
            Check.NotNull(baseRequest, "baseRequest");
            Check.NotSameType(baseRequest, typeof(AutoQuotationRequest));

            ExceptionDispatchInfo exception = null;

            try
            {
                var autoQuotationRequest = baseRequest as AutoQuotationRequest;
                var parameters = new MitsuiParameters(insurerParameters);

                var result = new RootEmissao
                {
                    tipo_isencao = parameters.ExemptionType,
                    tipo_pess = autoQuotationRequest.Insured.PersonType == PersonType.Legal ? "J" : "F",
                    cod_ciarenov = "1007", //GetMitsuiInsurerRenovationCode(autoQuotationRequest.Insurance.Renewal.CurrentInsuranceCompany),
                    apolice_renov = autoQuotationRequest.Insurance.Renewal.CurrentInsuranceNumber,
                    propostakit = ""
                };

                #region Exemption Type

                var exemptionType = parameters.ExemptionType.ToLower();

                if (exemptionType == "sem isenção")
                {
                    result.tipo_isencao = "1";
                }
                else if (exemptionType == "isento de custo")
                {
                    result.tipo_isencao = "2";
                }
                else if (exemptionType == "isento de iof")
                {
                    result.tipo_isencao = "3";
                }
                else if (exemptionType == "isento de custo e iof")
                {
                    result.tipo_isencao = "4";
                }

                #endregion

                #region Renewal

                if (autoQuotationRequest.Insurance.Renewal == null)
                {
                    result.tipo_renovacao = "1";
                }
                else
                {
                    if (autoQuotationRequest.Insurance.Renewal.CurrentInsuranceCompany == 7) //TODO: ver melhor maneira
                    {
                        if (autoQuotationRequest.Insurance.Renewal.ClaimQuantity <= 0)
                        {
                            result.tipo_renovacao = "2";
                        }
                        else
                        {
                            result.tipo_renovacao = "3";
                        }
                    }
                    else
                    {
                        if (autoQuotationRequest.Insurance.Renewal.ClaimQuantity <= 0)
                        {
                            result.tipo_renovacao = "4";
                        }
                        else
                        {
                            result.tipo_renovacao = "5";
                        }
                    }
                }

                #endregion

                dispatcherRequest.emissao = result;
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("MitsuiAutoWsDispatcher", "AgravdescTranslator", exception.SourceException);
                exception.Throw();
            }
        }

        #region Private Methods

        private string GetMitsuiInsurerRenovationCode(int insurer)
        {
            switch (insurer)
            {
                default:
                    throw new AlvacException("Seguradora não encontrada para renovação na mitsui.", DescriptionCode.InternalServerError);
            }
        }

        #endregion
    }
}
