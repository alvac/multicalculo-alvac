﻿using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Parameters;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Request.Translators
{
    public class AgravdescTranslator : IRequestTranslator<Root>
    {
        private readonly ILogger _logger;

        public AgravdescTranslator(ILogger logger)
        {
            _logger = logger;
        }

        public async Task ToDispatcherRequestAsync(Alvac.Domain.Contract.Request.BaseRequest baseRequest, IDictionary<string, object> insurerParameters, Root dispatcherRequest)
        {
            ExceptionDispatchInfo exception = null;
            try
            {
                var parameters = new MitsuiParameters(insurerParameters);

                var result = new RootAgravdesc
                {
                    tipoaplic = parameters.ApplicationType,
                    pct = parameters.SpecialDiscount.ToString(CultureInfo.GetCultureInfo("en-GB"))
                };

                if (parameters.ApplicationType.ToLower() == "desconto")
                {
                    result.tipoaplic = "D";
                }
                else
                {
                    result.tipoaplic = "A";
                }

                dispatcherRequest.agravdesc = result;
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if(exception != null)
            {
                await _logger.WriteErrorAsync("MitsuiAutoWsDispatcher", "AgravdescTranslator", exception.SourceException);
                exception.Throw();
            }
        }
    }
}
