﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Contract.Request.Auto;
using Alvac.Domain.Enum;
using Alvac.Domain.Enum.Auto;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Service.Dispatchers.Mitsui.Auto.Ws.AlvacWebBasicoReference;
using Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Parameters;
using Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Vehicle;
using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Text.RegularExpressions;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Request.Translators
{
    public class ItemTranslator : IRequestTranslator<Root>
    {
        private readonly ILogger _logger;

        public ItemTranslator(ILogger logger)
        {
            _logger = logger;
        }

        public async Task ToDispatcherRequestAsync(Alvac.Domain.Contract.Request.BaseRequest baseRequest, IDictionary<string, object> insurerParameters, Root dispatcherRequest)
        {
            Check.NotNull(baseRequest, "baseRequest");
            Check.NotSameType(baseRequest, typeof(AutoQuotationRequest));

            ExceptionDispatchInfo exception = null;

            try
            {
                var autoQuotationRequest = baseRequest as AutoQuotationRequest;
                var parameters = new MitsuiParameters(insurerParameters);

                var result = new RootItem
                {
                    ZeroKm = autoQuotationRequest.VehicleInfo.IsNew ? "S" : "N",

                    // Único tipo de uso que a Alvac trabalha.
                    uso = "01"
                };

                var AlvacBasicVehicle = GetAlvacBasicVehicle(autoQuotationRequest);

                if (AlvacBasicVehicle != null)
                {
                    result.veiculo = AlvacBasicVehicle.CodigoVeiculoSeguradora;
                    result.portas = AlvacBasicVehicle.Portas.ToString();
                    result.passageiros = AlvacBasicVehicle.Passageiros.ToString();

                    result.ano_fabricao = autoQuotationRequest.VehicleInfo.ManufacturingYear.ToString();
                    result.ano_modelo = autoQuotationRequest.VehicleInfo.ModelYear.ToString();

                    result.combustivel = AlvacBasicVehicle.Combustivel;
                    result.categoria = AlvacBasicVehicle.Categoria;

                    result.modalidade = autoQuotationRequest.VehicleInfo.Value.DeterminedValue.HasValue ? "2" : "1";
                }

                result.condutor = new RootItemCondutor[1];
                result.condutor[0] = new RootItemCondutor();
                result.condutor[0].nome = autoQuotationRequest.MainDriver.FullName;
                result.condutor[0].cpf = autoQuotationRequest.MainDriver.Document;

                if (autoQuotationRequest.VehicleInfo.Value.ValueCategory == ValueCategory.Determined)
                {
                    result.fator_ajuste = "100";
                }
                else
                {
                    if (autoQuotationRequest.VehicleInfo.ModelYear >= DateTime.Today.Year - 2)
                    {
                        result.fator_ajuste = autoQuotationRequest.VehicleInfo.Value.ReferencedPercentage.Value.ToString(CultureInfo.GetCultureInfo("en-GB"));
                    }
                    else
                    {
                        if (autoQuotationRequest.VehicleInfo.Value.ReferencedPercentage.HasValue && autoQuotationRequest.VehicleInfo.Value.ReferencedPercentage.Value > 100
                            && autoQuotationRequest.Insurance.Renewal != null)
                        {
                            throw new AlvacException("Para veículos com idade superior a 2 anos, o limite máximo de contratação é de 100% da FIPE para seguros novos.", DescriptionCode.InvalidRequest);
                        }
                        else
                        {
                            result.fator_ajuste = autoQuotationRequest.VehicleInfo.Value.ReferencedPercentage.Value.ToString(CultureInfo.GetCultureInfo("en-GB"));
                        }
                    }
                }

                result.bonus = autoQuotationRequest.Insurance.Renewal.CurrentInsuranceBonus.ToString();
                result.tipo_franquia = ((int)autoQuotationRequest.Insurance.DeductibleType).ToString();
                result.cep_pernoite = autoQuotationRequest.VehicleUse.OvernightZipCode;

                // Plano Compreensivo, único na Alvac.
                result.plano = "00001";

                result.placa = autoQuotationRequest.VehicleInfo.License.PlateNumber;
                result.chassi = autoQuotationRequest.VehicleInfo.License.ChassisNumber;
                result.KitGas = autoQuotationRequest.VehicleInfo.HasNaturalGasKit ? "1" : "2";
                result.Blindagem = autoQuotationRequest.VehicleInfo.IsArmored ? "1" : "2";

                //Chassi Remarcado setado como "Não desejo informar"
                result.ChassiRemarcado = "3";

                result.acessorio = GetAcessorio(autoQuotationRequest, parameters).ToArray();
                result.cobertura = GetCobertura(autoQuotationRequest, parameters).ToArray();
                result.perfil = GetPerfil(autoQuotationRequest, parameters).ToArray();

                dispatcherRequest.item = result;
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("MitsuiAutoWsDispatcher", "AgravdescTranslator", exception.SourceException);
                exception.Throw();
            }
        }

        private AlvacBasicVehicle GetAlvacBasicVehicle(AutoQuotationRequest autoQuotationRequest)
        {
            using (var servicosWebBasico = new ServicosWebBasico())
            {
                ///TODO: Teste na "mão"
                string webBasicoResponse = servicosWebBasico.ObtemVeiculo("Grand Siena Essence 1.6 Flex 16V", 2014, true, 41, true);
                autoQuotationRequest.VehicleInfo.ModelYear = 2014;

                if (!string.IsNullOrEmpty(webBasicoResponse))
                {
                    return AlvacJson<AlvacBasicVehicle>.Deserialize(webBasicoResponse);
                }
                else
                {
                    throw new Exception("ServicosWebBasicoClient - ObtemVeiculo respondeu nulo ou vazio.");
                }
            }
        }

        private List<RootItemAcessorio> GetAcessorio(AutoQuotationRequest autoQuotationRequest, MitsuiParameters parameters)
        {
            var result = new List<RootItemAcessorio>();

            if (autoQuotationRequest.VehicleInfo.HasNaturalGasKit)
            {
                var gasKitAcessorio = new RootItemAcessorio
                {
                    codigo = "00301",
                    cobertura = "00402",
                    grupo = "CA",
                    valor_lmi = autoQuotationRequest.VehicleInfo.NaturalGasKitCoverage.HasValue ?
                        autoQuotationRequest.VehicleInfo.NaturalGasKitCoverage.Value.ToString(CultureInfo.GetCultureInfo("en-GB")) : "0.00"
                };

                result.Add(gasKitAcessorio);
            }

            if (autoQuotationRequest.VehicleInfo.IsArmored)
            {
                var armoredAcessorio = new RootItemAcessorio
                {
                    codigo = "00900",
                    cobertura = "00014",
                    //TODO: Validar tag grupo.
                    grupo = "CA",
                    valor_lmi = autoQuotationRequest.VehicleInfo.ArmoryCoverage.HasValue ?
                        autoQuotationRequest.VehicleInfo.ArmoryCoverage.Value.ToString(CultureInfo.GetCultureInfo("en-GB")) : "0.00"
                };

                result.Add(armoredAcessorio);
            }

            return result;
        }

        private List<RootItemPerfil> GetPerfil(AutoQuotationRequest autoQuotationRequest, MitsuiParameters parameters)
        {
            var result = new List<RootItemPerfil>();

            var mainDriverFullName = new RootItemPerfil()
            {
                pergunta = "01",
                resposta = autoQuotationRequest.MainDriver.FullName
            };

            result.Add(mainDriverFullName);

            var mainDriverBirthDate = new RootItemPerfil()
            {
                pergunta = "02",
                resposta = autoQuotationRequest.MainDriver.BirthDate.ToString("dd/MM/yyyy")
            };

            result.Add(mainDriverBirthDate);

            var mainDriverGender = new RootItemPerfil()
            {
                pergunta = "03",
                resposta = autoQuotationRequest.MainDriver.Gender == Gender.Male ? "1" : "2"
            };

            result.Add(mainDriverGender);

            var maritalStatus = "";

            switch (autoQuotationRequest.MainDriver.MaritalStatus)
            {
                case MaritalStatus.Married:
                    maritalStatus = "1";
                    break;
                case MaritalStatus.Single:
                    maritalStatus = "2";
                    break;
                case MaritalStatus.Divorced:
                    maritalStatus = "3";
                    break;
                case MaritalStatus.Separated:
                    maritalStatus = "3";
                    break;

                default:
                    maritalStatus = "4";
                    break;
            }

            var mainDriverMaritalStatus = new RootItemPerfil()
            {
                pergunta = "04",
                resposta = maritalStatus
            };

            result.Add(mainDriverMaritalStatus);

            var mainDriverDriverLicenseYears = new RootItemPerfil()
            {
                pergunta = "05",
                resposta = DateTime.Now.AddYears((autoQuotationRequest.MainDriver.DriverLicenseYears * -1)).ToString("dd/MM/yyyy")
            };

            result.Add(mainDriverDriverLicenseYears);

            /// Se possui dependente é o mesmo mora com condutor principal.
            var hasDependent = autoQuotationRequest.MainDriver.DependentList != null && autoQuotationRequest.MainDriver.DependentList.Count > 0
                && autoQuotationRequest.MainDriver.DependentList.Where(x => x.LivesWithMainDriver).Count() > 0;

            /// Se possui condutor adicional residente entre 18 e 24 anos
            var hasCoverageDependent = autoQuotationRequest.AdditionalDriverList != null && autoQuotationRequest.AdditionalDriverList.Where(x => x.LivesWithMainDriver
                && ((DateTime.Now.Year - x.BirthDate.Year) > 18 && (DateTime.Now.Year - x.BirthDate.Year) < 24)).Count() > 0;

            var coverageDependent = "1";

            if (hasDependent)
            {
                if (!hasCoverageDependent)
                {
                    coverageDependent = "2";
                }
                else
                {
                    coverageDependent = "3";
                }
            }

            var mainDriverdepentent = new RootItemPerfil()
            {
                pergunta = "06",
                resposta = coverageDependent
            };

            result.Add(mainDriverdepentent);

            var relationship = "";

            switch (autoQuotationRequest.MainDriver.RelationshipWithInsured)
            {
                case Relationship.Self:
                    relationship = "1";
                    break;
                case Relationship.Child:
                    relationship = "2";
                    break;
                case Relationship.Married:
                    relationship = "3";
                    break;
                case Relationship.Parent:
                    relationship = "4";
                    break;
                case Relationship.Driver:
                    relationship = "5";
                    break;
                case Relationship.Partner:
                    relationship = "6";
                    break;
                default:
                    relationship = "7";
                    break;
            }

            var relationshipWithInsured = new RootItemPerfil()
            {
                pergunta = "07",
                resposta = relationship
            };

            result.Add(relationshipWithInsured);

            ///TODO: Get endereço condutor pelo cep de pernoite.
            var mainDriverAddress = new RootItemPerfil()
            {
                pergunta = "08",
                resposta = "1"
            };
            result.Add(mainDriverAddress);

            var garage = "";

            /// Vaido opção de escola, pois a interface permite o usuário marcar esta opção.
            switch (autoQuotationRequest.VehicleUse.GarageLocal)
            {
                case Garage.Residence | Garage.Job:
                    garage = "3";
                    break;
                case Garage.Residence:
                    garage = "1";
                    break;
                case Garage.Residence | Garage.School:
                    garage = "1";
                    break;
                case Garage.Job:
                    garage = "2";
                    break;
                case Garage.Job | Garage.School:
                    garage = "2";
                    break;
                default:
                    garage = "4";
                    break;
            }

            var mainDriverGarage = new RootItemPerfil()
            {
                pergunta = "10",
                resposta = garage
            };

            result.Add(mainDriverGarage);

            ///TODO: Get utilização do veiculo.
            var veihcleUse = new RootItemPerfil()
            {
                pergunta = "11",
                resposta = "1"
            };

            result.Add(veihcleUse);

            return result;
        }

        private List<RootItemCobertura> GetCobertura(AutoQuotationRequest autoQuotationRequest, MitsuiParameters parameters)
        {
            var result = new List<RootItemCobertura>();

            var colision = new RootItemCobertura()
            {
                codigo = "00001",
                grupo = "CA",
                valor_lmi = "0"
            };

            result.Add(colision);

            var rcfDm = new RootItemCobertura()
            {
                codigo = "00003",
                grupo = "RC",
                valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", parameters.RcfDm)
            };

            result.Add(rcfDm);

            var rcfDc = new RootItemCobertura()
            {
                codigo = "00004",
                grupo = "RC",
                valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", parameters.RcfDc)
            };

            result.Add(rcfDc);

            if (!string.IsNullOrEmpty(parameters.AppM) && parameters.AppM.ToLower() != "não contratado")
            {
                var appM = new RootItemCobertura()
                {
                    codigo = "00005",
                    grupo = "AP",
                    valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", decimal.Parse(parameters.AppM, CultureInfo.GetCultureInfo("pt-BR")))
                };

                result.Add(appM);
            }

            if (!string.IsNullOrEmpty(parameters.AppI) && parameters.AppI.ToLower() != "não contratado")
            {
                var appI = new RootItemCobertura()
                {
                    codigo = "00006",
                    grupo = "AP",
                    valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", decimal.Parse(parameters.AppI, CultureInfo.GetCultureInfo("pt-BR")))
                };

                result.Add(appI);
            }


            if (!string.IsNullOrEmpty(parameters.MedicalExpenses) && parameters.MedicalExpenses.ToLower() != "não contratado")
            {
                var medicalExpenses = new RootItemCobertura()
                {
                    codigo = "00007",
                    grupo = "AP",
                    valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", decimal.Parse(parameters.MedicalExpenses))
                };

                result.Add(medicalExpenses);
            }

            if (!string.IsNullOrEmpty(parameters.MoralDamages) && parameters.MoralDamages.ToLower() != "não contratado")
            {
                var moralDamages = new RootItemCobertura()
                {
                    codigo = "00008",
                    grupo = "AP",
                    valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", decimal.Parse(parameters.MoralDamages, CultureInfo.GetCultureInfo("pt-BR")))
                };

                result.Add(moralDamages);
            }

            if (autoQuotationRequest.VehicleInfo.IsArmored && autoQuotationRequest.VehicleInfo.ArmoryCoverage.HasValue)
            {
                var amored = new RootItemCobertura()
                {
                    codigo = "00014",
                    grupo = "CA",
                    valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", autoQuotationRequest.VehicleInfo.ArmoryCoverage.Value)
                };

                result.Add(amored);
            }

            if (!string.IsNullOrEmpty(parameters.ExtraExpenses) && parameters.ExtraExpenses.ToLower() != "não contratado")
            {
                var extraExpenses = new RootItemCobertura()
                {
                    codigo = "00015",
                    grupo = "AP",
                    valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", decimal.Parse(parameters.ExtraExpenses, CultureInfo.GetCultureInfo("pt-BR")))
                };

                result.Add(extraExpenses);
            }

            if (autoQuotationRequest.VehicleInfo.DevicesIgnitionBlocker != null && autoQuotationRequest.VehicleInfo.DevicesIgnitionBlocker.Count() > 0)
            {
                var devicesIgnitionBlocker = new RootItemCobertura()
                {
                    codigo = "00017",
                    grupo = "AS",
                    valor_lmi = "0"
                };

                result.Add(devicesIgnitionBlocker);
            }

            if (!string.IsNullOrEmpty(parameters.ComplementaryExpenses) && parameters.ComplementaryExpenses.ToLower() != "não contratado")
            {
                var complementaryExpenses = new RootItemCobertura()
                {
                    codigo = "00124",
                    grupo = "CA",
                    valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", decimal.Parse(parameters.ComplementaryExpenses, CultureInfo.GetCultureInfo("pt-BR")))
                };

                result.Add(complementaryExpenses);
            }

            if (autoQuotationRequest.VehicleInfo.HasNaturalGasKit && autoQuotationRequest.VehicleInfo.NaturalGasKitCoverage.HasValue)
            {
                var amored = new RootItemCobertura()
                {
                    codigo = "00402",
                    grupo = "CA",
                    valor_lmi = string.Format(CultureInfo.GetCultureInfo("en-GB"), "{0:F}", autoQuotationRequest.VehicleInfo.NaturalGasKitCoverage.Value)
                };

                result.Add(amored);
            }

            #region Courtesy Car

            if (!string.IsNullOrEmpty(parameters.CourtesyCar))
            {
                var regex = new Regex(@"\d+");
                Match match = regex.Match(parameters.CourtesyCar);

                if (match.Success)
                {
                    var days = int.Parse(match.Value);
                    var ar = parameters.CourtesyCar.ToLower().Contains("ar");
                    string code = string.Empty;

                    if (days == 7)
                    {
                        code = "00926";

                        if (ar)
                        {
                            code = "00917";
                        }
                    }
                    else if (days == 15)
                    {
                        code = "00927";

                        if (ar)
                        {
                            code = "00916";
                        }
                    }
                    else if (days == 30)
                    {
                        code = "00928";

                        if (ar)
                        {
                            code = "00915";
                        }
                    }

                    if (!string.IsNullOrEmpty(code))
                    {
                        var courtesyCar = new RootItemCobertura()
                        {
                            codigo = code,
                            grupo = "AS",
                            valor_lmi = ""
                        };

                        result.Add(courtesyCar);
                    }
                }
            }

            #endregion

            #region Trailer

            if (!string.IsNullOrEmpty(parameters.Trailer) && parameters.Trailer.ToLower() != "não contratado")
            {
                var trailer = new RootItemCobertura()
                {
                    grupo = "CA",
                    valor_lmi = ""
                };

                var trailerValue = parameters.Trailer.ToLower();

                if (trailerValue == "reboque adicional de 200km")
                {
                    trailer.codigo = "00919";
                    result.Add(trailer);
                }
                else if (trailerValue == "reboque adicional de 400km")
                {
                    trailer.codigo = "00922";
                    result.Add(trailer);
                }
                else if (trailerValue == "reboque adicional de 600km")
                {
                    trailer.codigo = "00923";
                    result.Add(trailer);
                }
                else if (trailerValue == "reboque adicional de 800km")
                {
                    trailer.codigo = "00924";
                    result.Add(trailer);
                }
                else if (trailerValue == "reboque adicional KM Ilimitado")
                {
                    trailer.codigo = "01189";
                    result.Add(trailer);
                }
            }

            #endregion

            #region Glass

            if (!string.IsNullOrEmpty(parameters.Glass) && parameters.Glass.ToLower() != "não contratado")
            {
                var glass = new RootItemCobertura()
                {
                    grupo = "CA",
                    valor_lmi = ""
                };

                var glassLower = parameters.Glass.ToLower();

                if (glassLower == "vidros básica")
                {
                    glass.codigo = "00932";
                    result.Add(glass);
                }
                else if (glassLower == "vidros completo")
                {
                    glass.codigo = "00933";
                    result.Add(glass);
                }
                else if (glassLower == "vidros blindados")
                {
                    glass.codigo = "00948";
                    result.Add(glass);
                }

            }

            #endregion

            return result;
        }

    }
}
