﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Mitsui.Auto.Ws.Vehicle
{
    public class AlvacBasicVehicle
    {
        public int Id { get; set; }

        public string Fabricante { get; set; }

        public string Seguradora { get; set; }

        public string Fipe { get; set; }

        public string Modelo { get; set; }

        public int AnoInicial { get; set; }

        public int AnoFinal { get; set; }

        public string Combustivel { get; set; }

        public string Cambio { get; set; }

        public int Cilindradas { get; set; }

        public long Portas { get; set; }

        public long Valvulas { get; set; }

        public string Tracao { get; set; }

        public string CodigoVeiculoSeguradora { get; set; }

        public string Categoria { get; set; }

        public int Passageiros { get; set; }
    }
}
