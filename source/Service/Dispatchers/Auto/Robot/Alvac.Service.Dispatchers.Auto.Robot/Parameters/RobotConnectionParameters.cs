﻿using Alvac.Domain.Services.Parameters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Dispatchers.Auto.Robot.Parameters
{
    public class RobotConnectionParameters : ParameterBase
    {
        #region Public Properties

        /// <summary>
        /// Url do web-service cotação estrutura antiga Alvac
        /// </summary>
        public string Url { get; set; }

        /// <summary>
        /// Usuário do web-service cotação estrutura antiga Alvac
        /// </summary>
        public string User { get; set; }

        /// <summary>
        /// Senha do web-service cotação estrutura antiga Alvac
        /// </summary>
        public string Password { get; set; }

        #endregion

        #region Contructor

        public RobotConnectionParameters(IDictionary<string, object> parameters) :
            base(parameters)
        {
        }

        #endregion


    }
}
