﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Enum;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Domain.Services.Notification;
using Alvac.Domain.Services.Pipeline;
using Alvac.Domain.Services.Pipeline.Producer;
using Alvac.Domain.Services.Queue;
using System;
using System.Collections.Concurrent;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Alvac.Domain.Contract.Extensions;
using Alvac.Domain.Contract.Exceptions;

namespace Alvac.Service.Notification.Producer.WorkerRole
{
    public class NotificationProducerPipeline : INotificationProducerPipeline
    {
        #region Private Fields

        private readonly IContainerProvider _containerProvider;
        private readonly IMessageQueueFactory _messageQueueFactory;
        private readonly ILogger _logger;

        private NotificationParameter _notificationParameter;

        private bool _isRunning;

        protected ConcurrentDictionary<int, PipelineLink<PipelineMessageNotificationProducer>> _links;

        protected BufferBlock<QueueMessage<NotificationRequest>> _buffer;
        protected TransformBlock<QueueMessage<NotificationRequest>, PipelineMessageNotificationProducer> _builderPipelineMessageBlock;
        protected TransformBlock<PipelineMessageNotificationProducer, PipelineMessageNotificationProducer> _getCallNotificationParametersBlock;
        protected ActionBlock<PipelineMessageNotificationProducer> _deliveryNotificationRequestBlock;

        #endregion

        #region Contructor

        public NotificationProducerPipeline(IContainerProvider containerProvider, IMessageQueueFactory messageQueueFactory, ILogger logger)
        {
            _containerProvider = containerProvider;
            _messageQueueFactory = messageQueueFactory;
            _logger = logger;
        }

        #endregion

        #region Private Methods

        #endregion

        #region INotificationProducerPipeline Members

        public Func<QueueMessage<NotificationRequest>, Task<PipelineMessageNotificationProducer>> BuilderPipelineMessageAsync()
        {
            return (
                async queueMessage =>
                {
                    ExceptionDispatchInfo exception = null;

                    PipelineMessageNotificationProducer result = null;

                    try
                    {
                        if (queueMessage != null && queueMessage.Item != null)
                        {
                            await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "NotificationProducer", "BuilderPipelineMessageAsync",
                                string.Format("Inicio BuilderPipelineMessageAsync. PipelineMessage: {0}", queueMessage.ToString()));

                            if (queueMessage.Item.Response != null && queueMessage.Item.Response is BaseResponse)
                            {
                                result = new PipelineMessageNotificationProducer
                                {
                                    Message = queueMessage,
                                    InsurerQuotationId = queueMessage.Item.InsurerQuotationId,
                                    CorrelationId = queueMessage.Item.CorrelationId,
                                    Response = queueMessage.Item.Response,
                                    Result = new OperationResult
                                    {
                                        Code = Code.Success,
                                        DescriptionCode = DescriptionCode.Successfully,
                                        Description = "Builder pipeline message successfully."
                                    }
                                };
                            }
                            else
                            {
                                throw new Exception("queueMessage.Item.Response invalido.");
                            }
                        }
                        else
                        {
                            throw new Exception("queueMessage vazia ou null.");
                        }

                        await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "NotificationProducer", "BuilderPipelineMessageAsync",
                            string.Format("BuilderPipelineMessageAsync executado com sucesso. PipelineMessage: {0}",
                           queueMessage.ToString()));
                    }
                    catch (Exception ex)
                    {
                        exception = ExceptionDispatchInfo.Capture(ex);

                        if (result == null)
                        {
                            result = new PipelineMessageNotificationProducer();
                        }

                        result.Result = ex.ToOperationResult(ex.Message);
                    }

                    if (exception != null)
                    {
                        await _logger.WriteErrorAsync("NotificationProducer", "BuilderPipelineMessageAsync", exception.SourceException.Message, exception.SourceException);
                    }

                    return result;
                });
        }

        public Func<PipelineMessageNotificationProducer, Task<PipelineMessageNotificationProducer>> GetCallNotificationParametersAsync()
        {
            return (
                async pipelineMessage =>
                {
                    string correlationId = string.Empty;

                    ExceptionDispatchInfo exception = null;

                    try
                    {
                        if (pipelineMessage.Result != null && pipelineMessage.Result.Code != Code.Failure)
                        {
                            using (var scope = _containerProvider.BeginScope())
                            {
                                var notificationProvider = _containerProvider.GetService<INotificationProvider>();
                                pipelineMessage.Notification = await notificationProvider.GetNotificationAsync(pipelineMessage.InsurerQuotationId);
                            }
                        }
                        else
                        {
                            return pipelineMessage;
                        }
                    }
                    catch (Exception ex)
                    {
                        exception = ExceptionDispatchInfo.Capture(ex);

                        if (pipelineMessage != null && pipelineMessage.Message != null)
                        {
                            correlationId = pipelineMessage.Message.CorrelationId;
                        }

                        if (pipelineMessage == null)
                        {
                            pipelineMessage = new PipelineMessageNotificationProducer();
                        }

                        pipelineMessage.Result = ex.ToOperationResult(string.Format("Erro ao obter o INotificationProvider. {0}.", ex.Message));
                    }

                    if (exception != null)
                    {
                        await _logger.WriteErrorAsync("NotificationProducer", 
                            "GetCallNotificationParametersAsync", exception.SourceException.Message, exception.SourceException);
                    }

                    return pipelineMessage;
                });
        }

        public async Task DeliveryNotificationRequestAsync(PipelineMessageNotificationProducer pipelineMessage)
        {
            string correlationId = string.Empty;

            ExceptionDispatchInfo exception = null;

            try
            {
                if (pipelineMessage.Result != null && pipelineMessage.Result.Code != Code.Failure)
                {
                    using (var scope = _containerProvider.BeginScope())
                    {
                        var notificationProvider = _containerProvider.GetService<INotificationProvider>();

                        var notificationRequest = new NotificationRequest
                        {
                            InsurerQuotationId = pipelineMessage.InsurerQuotationId,
                            CorrelationId = pipelineMessage.CorrelationId,
                            Response = pipelineMessage.Response
                        };

                        await notificationProvider.DeliveryNotificationAsync(pipelineMessage.Notification, notificationRequest);
                    }
                }
                else
                {
                    throw new Exception("Erro inesperado em DeliveryNotificationRequestAsync.");
                }
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);

                if (pipelineMessage != null && pipelineMessage.Message != null)
                {
                    correlationId = pipelineMessage.Message.CorrelationId;
                }

                if (pipelineMessage == null)
                {
                    pipelineMessage = new PipelineMessageNotificationProducer();
                }

                pipelineMessage.Result = ex.ToOperationResult(ex.Message);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("NotificationProducer", "DeliveryNotificationRequestAsync",
                    exception.SourceException.Message, exception.SourceException);
            }
        }

        public async Task StartAsync()
        {
            _isRunning = true;

            var unboundedConfig = new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = DataflowBlockOptions.Unbounded };

            _buffer = new BufferBlock<QueueMessage<NotificationRequest>>(new DataflowBlockOptions { BoundedCapacity = 1 });

            _builderPipelineMessageBlock = new TransformBlock<QueueMessage<NotificationRequest>, PipelineMessageNotificationProducer>((Func<QueueMessage<NotificationRequest>,
                Task<PipelineMessageNotificationProducer>>)BuilderPipelineMessageAsync(), new ExecutionDataflowBlockOptions
                {
                    MaxDegreeOfParallelism = DataflowBlockOptions.Unbounded,
                    BoundedCapacity = _notificationParameter.WindowSize
                });

            _getCallNotificationParametersBlock = new TransformBlock<PipelineMessageNotificationProducer, PipelineMessageNotificationProducer>((Func<PipelineMessageNotificationProducer,
                Task<PipelineMessageNotificationProducer>>)GetCallNotificationParametersAsync(), unboundedConfig);

            _deliveryNotificationRequestBlock = new ActionBlock<PipelineMessageNotificationProducer>((Func<PipelineMessageNotificationProducer, Task>)DeliveryNotificationRequestAsync, unboundedConfig);

            _links = new ConcurrentDictionary<int, PipelineLink<PipelineMessageNotificationProducer>>();

            _buffer.LinkTo(_builderPipelineMessageBlock);

            _builderPipelineMessageBlock.LinkTo(_getCallNotificationParametersBlock);
            _getCallNotificationParametersBlock.LinkTo(_deliveryNotificationRequestBlock);

            while (_isRunning)
            {
                QueueMessage<NotificationRequest> queueMessage = null;

                ExceptionDispatchInfo exception = null;

                string correlationId = DateTime.UtcNow.Ticks.ToString();

                try
                {
                    var inputQueue = _messageQueueFactory.Create<NotificationRequest>(_notificationParameter.NotificationQueueName);

                    queueMessage = await inputQueue.ReceiveAsync(_notificationParameter.ReceiveTimeoutMiliSec);

                    if (queueMessage != null)
                    {
                        await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "NotificationProducer", "StartAsync",
                             string.Format("Nova mensagem retirada da fila. Body: {0}", queueMessage.Item));

                        if (!(await _buffer.SendAsync<QueueMessage<NotificationRequest>>(queueMessage)))
                        {
                            await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "NotificationProducer", "StartAsync",
                                 string.Format("Mesagem negada pelo Pipeline - Body: {0}", queueMessage.Item));

                            await inputQueue.PushAsync(queueMessage);

                            await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "NotificationProducer", "StartAsync",
                               string.Format("Mensagem retorada para fila - Body: {0}", queueMessage.Item));
                        }
                        else
                        {
                            await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "NotificationProducer", "StartAsync",
                                   string.Format("Mensagem enviada para o pipeline - Body: {0}", queueMessage.Item));
                        }
                    }
                }
                catch (Exception ex)
                {
                    exception = ExceptionDispatchInfo.Capture(ex);

                    if (queueMessage != null)
                    {
                        correlationId = queueMessage.CorrelationId;
                    }
                }

                if (exception != null)
                {
                    await _logger.WriteErrorAsync("NotificationProducer", "StartAsync", exception.SourceException.Message, exception.SourceException);
                }
            }
        }

        public async Task StopAsync()
        {
            string correlationId = DateTime.Now.Ticks.ToString();

            await _logger.WriteInformationAsync(correlationId, "NotificationProducer", "StopAsync", "Begin stop engine.");

            if (_isRunning)
            {
                _isRunning = false;

                _buffer.Complete();
                await _buffer.Completion;

                await _logger.WriteVerboseAsync(correlationId, "NotificationProducer", "StopAsync", "Parando o servico.");

                _builderPipelineMessageBlock.Complete();
                await _builderPipelineMessageBlock.Completion;

                await _logger.WriteVerboseAsync(correlationId, "NotificationProducer", "StopAsync", "Builder pipeline message block completado.");

                _getCallNotificationParametersBlock.Complete();
                await _getCallNotificationParametersBlock.Completion;

                await _logger.WriteVerboseAsync(correlationId, "NotificationProducer", "StopAsync", "Get call notification parameters block completado.");

                _deliveryNotificationRequestBlock.Complete();
                await _deliveryNotificationRequestBlock.Completion;

                await _logger.WriteVerboseAsync(correlationId, "NotificationProducer", "StopAsync", "Delivery notification request block completado.");

                await _logger.WriteInformationAsync(correlationId, "NotificationProducer", "StopAsync", "Serviço parado com sucesso.");
            }
            else
            {
                await _logger.WriteVerboseAsync(correlationId, "NotificationProducer", "StopAsync", "Serviço já parado");
            }
        }

        public void Initializer(Alvac.Domain.Services.Notification.NotificationParameter parameters)
        {
            if (parameters == null)
            {
                throw new Exception("NotificationProducer - NotificationParameter vazio ou nulo.");
            }

            _notificationParameter = parameters;
        }

        #endregion
    }
}
