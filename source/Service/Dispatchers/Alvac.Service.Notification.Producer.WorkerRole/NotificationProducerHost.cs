﻿using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Connections;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Contract.Extensions;
using Alvac.Domain.Services.Notification;

namespace Alvac.Service.Notification.Producer.WorkerRole
{
    public class NotificationProducerHost
    {
        #region Private Fields

        private readonly IContainerProvider _containerProvider;
        private readonly IMessageQueueFactory _messageQueueFactory;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ILogger _logger;

        private readonly List<NotificationProducerPipeline> notificationProducerPipelineList;

        #endregion

        #region Contructor

        public NotificationProducerHost(IContainerProvider containerProvider, IMessageQueueFactory messageQueueFactory, IQueryProcessor queryProcessor, ILogger logger)
        {
            _containerProvider = containerProvider;
            _messageQueueFactory = messageQueueFactory;
            _queryProcessor = queryProcessor;
            _logger = logger;
            notificationProducerPipelineList = new List<NotificationProducerPipeline>();
        }

        #endregion

        #region IServiceHost Members

        /// <summary>
        /// Starts the asynchronous.
        /// </summary>
        /// <returns></returns>
        public async Task StartAsync()
        {
            ExceptionDispatchInfo exception = null;

            try
            {
                using (var scope = _containerProvider.BeginScope())
                {
                    var findConnectionsByActive = new FindConnectionsByActive
                    {
                        IsActive = true
                    };

                    var connections = await _queryProcessor.ProcessAsync(findConnectionsByActive);

                    foreach (var connection in connections)
                    {
                        ExceptionDispatchInfo connectionException = null;

                        try
                        {
                            var notificationParameter = new NotificationParameter
                            {
                                MaxWaitTimeSec = 60,
                                MaxThroughput = connection.MaxThroughput,
                                WindowSize = connection.WindowSize,
                                NotificationQueueName = connection.NotificationQueueName
                            };

                            string crrelationId = connection.ConnectionId.ToString();

                            await _logger.WriteVerboseAsync(crrelationId, "NotificationProducerHost", "StartAsync", string.Format("Criando {0} consumidore(s) para a conexão {1}",
                                connection.Amount, connection.Name));

                            for (int i = 0; i < connection.Amount; i++)
                            {
                                ExceptionDispatchInfo currentConnectionException = null;

                                try
                                {
                                    var notificationProducerPipeline = new NotificationProducerPipeline(_containerProvider, _messageQueueFactory, _logger);
                                    notificationProducerPipeline.Initializer(notificationParameter);

                                    notificationProducerPipelineList.Add(notificationProducerPipeline);

                                    await _logger.WriteInformationAsync(crrelationId, "NotificationProducerHost", "StartAsync", string.Format("Criado {0} consumidore(s) para a conexão {1}", 
                                        connection.Amount, connection.Name));

                                    notificationProducerPipeline.StartAsync();
                                }
                                catch (Exception ex)
                                {
                                    currentConnectionException = ExceptionDispatchInfo.Capture(ex);
                                }

                                if (currentConnectionException != null)
                                {
                                    await _logger.WriteErrorAsync("NotificationProducerHost", "StartAsync", "Erro inesperado na criação da conexão.", 
                                        currentConnectionException.SourceException);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            connectionException = ExceptionDispatchInfo.Capture(ex);
                        }

                        if (connectionException != null)
                        {
                            await _logger.WriteErrorAsync("DispatcherConsumerHost", "StartAsync", "Erro inesperado na criação da conexão", connectionException.SourceException);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("DispatcherConsumerHost", "StartAsync", "Erro inesperado na criação da conexão", exception.SourceException);
            }
        }

        public async Task StopAsync()
        {
            string correlationId = DateTime.UtcNow.Ticks.ToString();

            ExceptionDispatchInfo exception = null;

            try
            {
                //Stop all dispatchers simultaneously
                var stopTasks = notificationProducerPipelineList.Select(d => d.StopAsync().TimeoutAfter(20000));

                ExceptionDispatchInfo itemStopAsyncException = null;

                try
                {
                    //Aqui ele manda parar todas as TASK ao mesmo tempo
                    await Task.WhenAll(stopTasks.ToArray());
                }
                catch (Exception ex)
                {
                    itemStopAsyncException = ExceptionDispatchInfo.Capture(ex);
                }

                if (itemStopAsyncException != null)
                {
                    await _logger.WriteErrorAsync("NotificationProducerHost", "StopAsync", "Erro inesperado para parar o serviço", 
                        itemStopAsyncException.SourceException);
                }

            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("NotificationProducerHost", "StopAsync", "Erro inesperado para parar o serviço", 
                    exception.SourceException);
            }
        }

        #endregion
    }
}
