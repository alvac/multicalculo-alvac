﻿using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.ConnectionParameters;
using Alvac.Domain.Entities.Queries.Connections;
using Alvac.Domain.Entities.Queries.Dispatchers;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Contract.Extensions;
using Alvac.Domain.Services;
using Alvac.Domain.Entities.Queries.InsurerConfigurations;

namespace Alvac.Service.Dispatcher.Producer.WorkerRole
{
    public class DispatcherProducerHost
    {
        #region Private Fields

        private readonly IContainerProvider _containerProvider;
        private readonly IMessageQueueFactory _messageQueueFactory;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ILogger _logger;
        private readonly IDispatcherFactory _dispatcherFactory;

        private readonly List<DispatcherProducerPipeline> _dispatcherProducerPipelineList;

        #endregion

        #region Contructor

        public DispatcherProducerHost(IContainerProvider containerProvider, IMessageQueueFactory messageQueueFactory, IQueryProcessor queryProcessor,
            IDispatcherFactory dispatcherFactory, ILogger logger)
        {
            _containerProvider = containerProvider;
            _messageQueueFactory = messageQueueFactory;
            _queryProcessor = queryProcessor;
            _dispatcherFactory = dispatcherFactory;
            _logger = logger;
            _dispatcherProducerPipelineList = new List<DispatcherProducerPipeline>();
        }

        #endregion

        #region IDispatcherHost Members

        public async Task StartAsync()
        {
            ExceptionDispatchInfo exception = null;

            try
            {
                using (var scope = _containerProvider.BeginScope())
                {
                    var findConnectionsByActive = new FindConnectionsByActive
                    {
                        IsActive = true
                    };

                    var connections = await _queryProcessor.ProcessAsync(findConnectionsByActive);

                    foreach (var connection in connections)
                    {
                        ExceptionDispatchInfo connectionException = null;

                        try
                        {
                            string correlationId = connection.ConnectionId.ToString();

                            await _logger.WriteVerboseAsync(correlationId, "DispatcherProducerHost", "StartAsync", string.Format("Begin create dispatcher with {0} connection(s). Name: {1}",
                                connection.Amount, connection.Name));

                            var findDispatcherById = new FindDispatcherById
                            {
                                DispatcherId = connection.DispatcherId
                            };

                            var dispatcher = await _queryProcessor.ProcessAsync(findDispatcherById);

                            if (dispatcher != null)
                            {
                                if (dispatcher.IsActive)
                                {
                                    var dispatcherProducerParameter = new DispatcherParameter
                                    {
                                        Dispatchers = new List<IDispatcher>(),
                                        Technology = dispatcher.Technology,
                                        MaxWaitTimeSec = 60,
                                        MaxThroughput = connection.MaxThroughput,
                                        WindowSize = connection.WindowSize,
                                        InputQueueName = connection.InputQueueName,
                                        OutputQueueName = connection.OutputQueueName,
                                        NotificationQueueName = connection.NotificationQueueName
                                    };

                                    for (int i = 0; i < connection.Amount; i++)
                                    {
                                        ExceptionDispatchInfo createDispatcherException = null;

                                        try
                                        {

                                            var connectionParameters = await GetConnectionParametersAsync(connection.ConnectionId);

                                            var insurerConfigurations = await _queryProcessor.ProcessAsync(new FindInsurerConfigurationByInsurer
                                            {
                                                Insurer = connection.Dispatcher.InsurerId
                                            });

                                            var dispatcherInstance = _dispatcherFactory.Create(dispatcher.Name, connectionParameters, insurerConfigurations);

                                            dispatcherProducerParameter.Dispatchers.Add(dispatcherInstance);

                                            await _logger.WriteInformationAsync(correlationId, "DispatcherProducerHost", "StartAsync", string.Format("Created dispatcher with id {0} successfully. WindowSize: {1} MaxThroughput: {2}",
                                                dispatcher.DispatcherId, connection.WindowSize, connection.MaxThroughput));

                                            var dispatcherProducerPipeline = new DispatcherProducerPipeline(_containerProvider, _messageQueueFactory, _logger);
                                            dispatcherProducerPipeline.Initializer(dispatcherProducerParameter);

                                            _dispatcherProducerPipelineList.Add(dispatcherProducerPipeline);

                                            dispatcherProducerPipeline.StartAsync();
                                        }
                                        catch (Exception ex)
                                        {
                                            createDispatcherException = ExceptionDispatchInfo.Capture(ex);
                                        }

                                        if (createDispatcherException != null)
                                        {
                                            await _logger.WriteErrorAsync("DispatcherProducerHost", "StartAsync", "Unexpected error creating dispatcher.", createDispatcherException.SourceException);
                                        }
                                    }
                                }
                                else
                                {
                                    throw new Exception(string.Format("Dispatcher {0} inactive status", dispatcher.Name));
                                }

                            }
                            else
                            {
                                throw new Exception(string.Format("DispatcherId {0} not found", connection.DispatcherId));
                            }
                        }
                        catch (Exception ex)
                        {
                            connectionException = ExceptionDispatchInfo.Capture(ex);
                        }

                        if (connectionException != null)
                        {
                            await _logger.WriteErrorAsync("DispatcherProducerHost", "StartAsync", "Unexpected error create connection", connectionException.SourceException);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("DispatcherProducerHost", "StartAsync", "Unexpected error create connection", exception.SourceException);
            }
        }

        public async Task StopAsync()
        {
            string correlationId = DateTime.UtcNow.Ticks.ToString();

            ExceptionDispatchInfo exception = null;

            try
            {
                //Stop all dispatchers simultaneously
                var stopTasks = _dispatcherProducerPipelineList.Select(d => d.StopAsync().TimeoutAfter(20000));

                ExceptionDispatchInfo itemStopAsyncException = null;

                try
                {
                    //Aqui ele manda parar todas as TASK ao mesmo tempo
                    await Task.WhenAll(stopTasks.ToArray());
                }
                catch (Exception ex)
                {
                    itemStopAsyncException = ExceptionDispatchInfo.Capture(ex);
                }

                if (itemStopAsyncException != null)
                {
                    await _logger.WriteErrorAsync("DispatcherHost", "StopAsync", "Unexpected error in item stop", itemStopAsyncException.SourceException);
                }

            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("DispatcherHost", "StopAsync", "Unexpected error in method stop", exception.SourceException);
            }
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Carrega os parametros da conexão.
        /// </summary>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        private async Task<IDictionary<string, object>> GetConnectionParametersAsync(int connectionId)
        {
            IDictionary<string, object> result = null; ;

            var findConnectionParametersByConnectionId = new FindConnectionParametersByConnectionId
            {
                ConnectionId = connectionId
            };

            var connectionParameters = await _queryProcessor.ProcessAsync(findConnectionParametersByConnectionId);

            if (connectionParameters != null)
            {
                result = new Dictionary<string, object>();

                foreach (var connectionParameter in connectionParameters)
                {
                    result.Add(connectionParameter.Key, connectionParameter.Value);
                }
            }

            return result;
        }

        #endregion
    }
}
