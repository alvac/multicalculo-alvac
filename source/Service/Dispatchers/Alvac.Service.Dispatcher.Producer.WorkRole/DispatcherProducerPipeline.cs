﻿using Alvac.Domain.Contract;
using Alvac.Domain.Contract.Extensions;
using Alvac.Domain.Contract.Request;
using Alvac.Domain.Contract.Response;
using Alvac.Domain.Enum;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services.Logger;
using Alvac.Domain.Services.Pipeline.Producer;
using Alvac.Domain.Services.Queue;
using Alvac.Infrastructure.Util.RateLimiting;
using System;
using System.Runtime.ExceptionServices;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;

namespace Alvac.Service.Dispatcher.Producer.WorkerRole
{
    public class DispatcherProducerPipeline : IDispatcherProducerPipeline
    {
        #region Private Fields

        private readonly IContainerProvider _containerProvider;
        private readonly IMessageQueueFactory _messageQueueFactory;
        private readonly ILogger _logger;

        private DispatcherParameter _dispatcherParameter;
        private RateGate _engineRateGate;

        private bool _isRunning;

        protected BufferBlock<QueueMessage<InsurerQuotationRequest>> _buffer;
        protected TransformBlock<QueueMessage<InsurerQuotationRequest>, PipelineMessageDispatcherProducer> _builderPipelineMessageBlock;
        protected TransformBlock<PipelineMessageDispatcherProducer, PipelineMessageDispatcherProducer>[] _throughputLimiterBlock;
        protected TransformBlock<PipelineMessageDispatcherProducer, PipelineMessageDispatcherProducer>[] _processRequestBlock;
        protected ActionBlock<PipelineMessageDispatcherProducer> _enqueueResponseBlock;

        protected ActionBlock<PipelineMessageDispatcherProducer> _sendBackToQueueBlock;

        #endregion

        #region Contructor

        public DispatcherProducerPipeline(IContainerProvider containerProvider, IMessageQueueFactory messageQueueFactory, ILogger logger)
        {
            _containerProvider = containerProvider;
            _messageQueueFactory = messageQueueFactory;
            _logger = logger;
        }

        #endregion

        #region IDispatcherProducerPipeline Members

        public Func<QueueMessage<InsurerQuotationRequest>, Task<PipelineMessageDispatcherProducer>> BuilderPipelineMessageAsync()
        {
            return (
                async queueMessage =>
                {
                    ExceptionDispatchInfo exception = null;

                    PipelineMessageDispatcherProducer result = null;

                    try
                    {
                        if (queueMessage != null && queueMessage.Item != null)
                        {
                            await _logger.WriteVerboseAsync("DispatcherProducer", "BuilderPipelineMessageAsync",
                                string.Format("Inicio da construão do pipeline message. PipelineMessage: {0}", queueMessage.ToString()));

                            if (queueMessage.Item.Request != null && queueMessage.Item.Request is BaseRequest)
                            {
                                if (queueMessage.Item.Parameters != null)
                                {
                                    result = new PipelineMessageDispatcherProducer
                                    {
                                        Message = queueMessage,
                                        Parameters = queueMessage.Item.Parameters,
                                        Request = queueMessage.Item.Request,
                                        Result = new OperationResult
                                        {
                                            Code = Code.Success,
                                            DescriptionCode = DescriptionCode.Successfully,
                                            Description = "Builder pipeline message successfully."
                                        }
                                    };
                                }
                                else
                                {
                                    throw new Exception("BaseRequest invalido - Parâmetros seguradora não encontrado.");
                                }

                            }
                            else
                            {
                                throw new Exception("BaseRequest não encontrado ou invalido.");
                            }
                        }
                        else
                        {
                            throw new Exception("Queue message vazia ou nula.");
                        }

                        await _logger.WriteVerboseAsync("DispatcherProducer", "BuilderPipelineMessageAsync",
                            string.Format("Pipeline message construido com suesso. PipelineMessage: {0}",
                           queueMessage.ToString()));
                    }
                    catch (Exception ex)
                    {
                        exception = ExceptionDispatchInfo.Capture(ex);

                        if (result == null)
                        {
                            result = new PipelineMessageDispatcherProducer();
                        }

                        result.Result = ex.ToOperationResult("Erro inesperado ao contruir o pipelineMessage");
                    }

                    if (exception != null)
                    {
                        await _logger.WriteErrorAsync("DispatcherProducer", "BuilderPipelineMessageAsync", "Erro inesperado", exception.SourceException);
                    }

                    return result;
                });
        }

        public Func<PipelineMessageDispatcherProducer, Task<PipelineMessageDispatcherProducer>> CreateThroughputLimiterAsync()
        {
            return (
                async pipelineMessage =>
                {
                    ExceptionDispatchInfo exception = null;

                    try
                    {
                        await _logger.WriteVerboseAsync("DispatcherProducer", "CreateThroughputLimiterAsync",
                            string.Format("Inicio do throughputLimiter. PipelineMessage: {0}",
                            pipelineMessage.ToString()));

                        if (_engineRateGate != null)
                        {
                            if (!await _engineRateGate.WaitToProceedAsync(TimeSpan.FromSeconds(_dispatcherParameter.MaxWaitTimeSec)))
                            {
                                await _logger.WriteWarningAsync("DispatcherProducer", "CreateThroughputLimiterAsync",
                                    string.Format("Timeout esperando o controle de throughput. Considere reduzi o numero de conexões ou sua janela. TimeoutConfigured: {0}",
                                    _dispatcherParameter.MaxWaitTimeSec.ToString()));
                            }
                        }

                        await _logger.WriteVerboseAsync("DispatcherProducer", "CreateThroughputLimiterAsync",
                            string.Format("ThroughputLimiter executado. PipelineMessage: {0}",
                           pipelineMessage.ToString()));
                    }
                    catch (Exception ex)
                    {
                        exception = ExceptionDispatchInfo.Capture(ex);

                        if (pipelineMessage == null)
                        {
                            pipelineMessage = new PipelineMessageDispatcherProducer();
                        }

                        pipelineMessage.Result = ex.ToOperationResult("Erro inesperado na criação do Througthput.");
                    }

                    if (exception != null)
                    {
                        await _logger.WriteErrorAsync("DispatcherProducer", "CreateThroughputLimiterAsync",
                            "Erro inesperado", exception.SourceException);
                    }

                    return pipelineMessage;
                });
        }

        public Func<PipelineMessageDispatcherProducer, Task<PipelineMessageDispatcherProducer>> ProcessRequestAsync(IDispatcher dispatcher)
        {
            return (
                async pipelineMessage =>
                {
                    string correlationId = string.Empty;

                    ExceptionDispatchInfo exception = null;

                    try
                    {
                        await _logger.WriteVerboseAsync("DispatcherProducer", "ProcessRequestAsync",
                            string.Format("Inicio do processo do pedido. PipelineMessage: {0}",
                         pipelineMessage.ToString()));

                        if (pipelineMessage.Result != null && pipelineMessage.Result.Code != Code.Failure)
                        {
                            await _logger.WriteInformationAsync("DispatcherProducer", "ProcessRequestAsync",
                                string.Format("Enviando para Dispatcher. PipelineMessage: {0}", pipelineMessage.ToString()));

                            pipelineMessage.Response = await dispatcher.ProcessAsync(pipelineMessage.Request, pipelineMessage.Parameters);
                            pipelineMessage.Response.Identifier = pipelineMessage.Request.Identifier;

                            if (pipelineMessage.Response != null)
                            {
                                if (pipelineMessage.Response.Result != null)
                                {
                                    pipelineMessage.Result = pipelineMessage.Response.Result;
                                }
                            }
                            else
                            {
                                throw new Exception(string.Format("Resposta é vazia ou nula. Dispatcher '{0}'."));
                            }

                            await _logger.WriteVerboseAsync("DispatcherProducer", "ProcessRequestAsync",
                                string.Format("Messagem processada. Resposta do Dispatcher: {0}", pipelineMessage.Response.ToString()));
                        }
                        else
                        {
                            return pipelineMessage;
                        }
                    }
                    catch (Exception ex)
                    {
                        exception = ExceptionDispatchInfo.Capture(ex);

                        if (pipelineMessage != null && pipelineMessage.Message != null)
                        {
                            correlationId = pipelineMessage.Message.CorrelationId;
                        }

                        if (pipelineMessage == null)
                        {
                            pipelineMessage = new PipelineMessageDispatcherProducer();
                        }

                        pipelineMessage.Result = ex.ToOperationResult(ex.Message);
                    }

                    if (exception != null)
                    {
                        await _logger.WriteErrorAsync("DispatcherProducer", "ProcessRequestAsync",
                            "Erro inesperado", exception.SourceException);
                    }

                    return pipelineMessage;
                });
        }

        public async Task EnqueueResponseAsync(PipelineMessageDispatcherProducer pipelineMessage)
        {
            string correlationId = string.Empty;

            ExceptionDispatchInfo exception = null;

            bool enqueueResponse = true;

            try
            {
                await _logger.WriteVerboseAsync(pipelineMessage.Message.CorrelationId, "DispatcherProducer", "EnqueueResponseAsync",
                    string.Format("Inicio do processo para efileirar resposta. PipelineMessage: {0}", pipelineMessage.ToString()));

                var insurerQuotationResponse = GetInsurerQuotationResponse(pipelineMessage);

                if (_dispatcherParameter.Technology == Technology.Robotics)
                {
                    if(insurerQuotationResponse.Response.Result.Code == Code.Success)
                    {
                        enqueueResponse = false;
                        
                        var updateResponseFactory = _containerProvider.GetService<IUpdateResponseFactory>();
                        var updateResponse = updateResponseFactory.Create(pipelineMessage.Response.RequestType);
                        await updateResponse.UpdateStatusAsync(pipelineMessage.Response, Status.Waiting, DescriptionCode.Successfully, "Cálculo entregue ao robô com sucesso.");
                    }
                }

                if (enqueueResponse)
                {   
                    var queueMessage = _messageQueueFactory.Create<InsurerQuotationResponse>(_dispatcherParameter.OutputQueueName);
                    await queueMessage.PushAsync(pipelineMessage.Request.Identifier.ToString(), AlvacJson<InsurerQuotationResponse>.Serialize(insurerQuotationResponse));

                    await _logger.WriteVerboseAsync("DispatcherProducer", "EnqueueResponseAsync",
                        "Resposta enfileirada com sucesso.");
                }

            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);

                if (pipelineMessage != null && pipelineMessage.Message != null)
                {
                    correlationId = pipelineMessage.Message.CorrelationId;
                }

                if (pipelineMessage == null)
                {
                    pipelineMessage = new PipelineMessageDispatcherProducer();
                }

                pipelineMessage.Result = ex.ToOperationResult(ex.Message);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("DispatcherProducer", "EnqueueResponseAsync", exception.SourceException);
            }
        }
        
        public async Task SendBackToQueueAsync(PipelineMessageDispatcherProducer pipelineMessage)
        {
            string correlationId = string.Empty;

            ExceptionDispatchInfo exception = null;

            try
            {
                await _logger.WriteInformationAsync("DispatcherProducer", "SendBackToQueueAsync",
                    string.Format("Inicio do processo para efileirar retentativa. PipelineMessage: {0}", pipelineMessage.ToString()));

                var queueMessage = _messageQueueFactory.Create<InsurerQuotationRequest>(_dispatcherParameter.InputQueueName);
                await queueMessage.PushAsync(pipelineMessage.Request.Identifier.ToString(), AlvacJson<InsurerQuotationRequest>.Serialize(pipelineMessage.Message.Item));

                await _logger.WriteInformationAsync("DispatcherProducer", "EnqueueResponseAsync",
                    "Retentativa enfileirada com sucesso.");
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);

                if (pipelineMessage != null && pipelineMessage.Message != null)
                {
                    correlationId = pipelineMessage.Message.CorrelationId;
                }

                if (pipelineMessage == null)
                {
                    pipelineMessage = new PipelineMessageDispatcherProducer();
                }

                pipelineMessage.Result = ex.ToOperationResult(ex.Message);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("DispatcherProducer", "EnqueueResponseAsync", exception.SourceException);
            }
        }

        public void Initializer(DispatcherParameter parameters)
        {
            if (parameters == null)
            {
                throw new Exception("DispatcherProducer - DispatcherProducerParameter é vazio ou nulo.");
            }

            _dispatcherParameter = parameters;
        }

        public async Task StartAsync()
        {
            _isRunning = true;

            var unboundedConfig = new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = DataflowBlockOptions.Unbounded };

            _buffer = new BufferBlock<QueueMessage<InsurerQuotationRequest>>(new DataflowBlockOptions { BoundedCapacity = 1 });

            _builderPipelineMessageBlock = new TransformBlock<QueueMessage<InsurerQuotationRequest>, PipelineMessageDispatcherProducer>(BuilderPipelineMessageAsync(), new ExecutionDataflowBlockOptions
            {
                MaxDegreeOfParallelism = DataflowBlockOptions.Unbounded,
                BoundedCapacity = _dispatcherParameter.Dispatchers.Count * _dispatcherParameter.WindowSize
            });

            _throughputLimiterBlock = new TransformBlock<PipelineMessageDispatcherProducer, PipelineMessageDispatcherProducer>[_dispatcherParameter.Dispatchers.Count];

            _processRequestBlock = new TransformBlock<PipelineMessageDispatcherProducer, PipelineMessageDispatcherProducer>[_dispatcherParameter.Dispatchers.Count];

            _enqueueResponseBlock = new ActionBlock<PipelineMessageDispatcherProducer>(EnqueueResponseAsync, unboundedConfig);

            _sendBackToQueueBlock = new ActionBlock<PipelineMessageDispatcherProducer>(SendBackToQueueAsync, unboundedConfig);

            _buffer.LinkTo(_builderPipelineMessageBlock);

            if (_dispatcherParameter.MaxThroughput > 0)
            {
                _engineRateGate = new RateGate(_dispatcherParameter.MaxThroughput, TimeSpan.FromSeconds(1));
            }

            for (int i = 0; i < _dispatcherParameter.Dispatchers.Count; i++)
            {
                var inputConsumerOptions = new ExecutionDataflowBlockOptions
                {
                    BoundedCapacity = _dispatcherParameter.WindowSize,
                    MaxDegreeOfParallelism = _dispatcherParameter.WindowSize
                };

                _throughputLimiterBlock[i] = new TransformBlock<PipelineMessageDispatcherProducer, PipelineMessageDispatcherProducer>(CreateThroughputLimiterAsync(),
                    new ExecutionDataflowBlockOptions { BoundedCapacity = 1, MaxDegreeOfParallelism = DataflowBlockOptions.Unbounded });

                _processRequestBlock[i] = new TransformBlock<PipelineMessageDispatcherProducer,
                    PipelineMessageDispatcherProducer>(ProcessRequestAsync(_dispatcherParameter.Dispatchers[i]), inputConsumerOptions);

                _builderPipelineMessageBlock.LinkTo(_throughputLimiterBlock[i]);

                _throughputLimiterBlock[i].LinkTo(_processRequestBlock[i]);

                _processRequestBlock[i].LinkTo(_sendBackToQueueBlock, p => IsSendBackToQueue(p));
                _processRequestBlock[i].LinkTo(_enqueueResponseBlock, p => !IsSendBackToQueue(p));
            }

            var inputQueue = _messageQueueFactory.Create<InsurerQuotationRequest>(_dispatcherParameter.InputQueueName);

            while (_isRunning)
            {
                QueueMessage<InsurerQuotationRequest> queueMessage = null;

                ExceptionDispatchInfo exception = null;

                string correlationId = DateTime.UtcNow.Ticks.ToString();

                try
                {
                    queueMessage = await inputQueue.ReceiveAsync(_dispatcherParameter.ReceiveTimeoutMiliSec);

                    if (queueMessage != null)
                    {
                        await _logger.WriteVerboseAsync("DispatcherProducer", "StartAsync",
                            string.Format("Nova mensagem retirada da fila. Body: {0}", queueMessage.Item));

                        if (!(await _buffer.SendAsync(queueMessage)))
                        {
                            await _logger.WriteVerboseAsync("DispatcherProducer", "StartAsync",
                                string.Format("Mesagem negada pelo Pipeline - Body: {0}", queueMessage.Item));

                            await inputQueue.PushAsync(queueMessage);

                            await _logger.WriteVerboseAsync("DispatcherProducer", "StartAsync",
                                string.Format("Mensagem retorada para fila - Body: {0}", queueMessage.Item));
                        }
                        else
                        {
                            await _logger.WriteVerboseAsync("DispatcherProducer", "StartAsync",
                                string.Format("Mensagem enviada para o pipeline - Body: {0}", queueMessage.Item));
                        }
                    }
                }
                catch (Exception ex)
                {
                    exception = ExceptionDispatchInfo.Capture(ex);

                    if (queueMessage != null)
                    {
                        correlationId = queueMessage.CorrelationId;
                    }
                }

                if (exception != null)
                {
                    await _logger.WriteErrorAsync("DispatcherProducer", "StartAsync", "Erro inesperado", exception.SourceException);
                }
            }
        }

        public async Task StopAsync()
        {
            string correlationId = DateTime.Now.Ticks.ToString();

            await _logger.WriteInformationAsync("DispatcherProducer", "StopAsync", "Parando o servico");

            if (_isRunning)
            {
                _isRunning = false;

                _buffer.Complete();
                await _buffer.Completion;

                await _logger.WriteVerboseAsync("DispatcherProducer", "StopAsync", "Buffer block completado.");

                _builderPipelineMessageBlock.Complete();
                await _builderPipelineMessageBlock.Completion;

                await _logger.WriteVerboseAsync("DispatcherProducer", "StopAsync", "Builder pipeline message block completado.");

                foreach (var throughput in _throughputLimiterBlock)
                {
                    throughput.Complete();
                    await throughput.Completion;
                }

                await _logger.WriteVerboseAsync("DispatcherProducer", "StopAsync", "Throughput limiter block completado.");

                foreach (var item in _processRequestBlock)
                {
                    item.Complete();
                    await item.Completion;
                }

                await _logger.WriteVerboseAsync("DispatcherProducer", "StopAsync", "Process request block completado.");

                _enqueueResponseBlock.Complete();
                await _enqueueResponseBlock.Completion;

                await _logger.WriteVerboseAsync("DispatcherProducer", "StopAsync", "Enqueue response block completado.");

                if (_engineRateGate != null)
                {
                    _engineRateGate.Dispose();
                }

                await _logger.WriteInformationAsync("DispatcherProducer", "StopAsync", "Serviço parado com sucesso.");
            }
            else
            {
                await _logger.WriteVerboseAsync("DispatcherProducer", "StopAsync", "Serviço já parado");
            }
        }

        #endregion

        #region Private Methods

        private bool IsSendBackToQueue(PipelineMessageDispatcherProducer pipelineMessage)
        {
            if (pipelineMessage.Result != null && pipelineMessage.Result.Code == Code.Failure && (int)pipelineMessage.Result.DescriptionCode >= 400
                && (int)pipelineMessage.Result.DescriptionCode < 500)
            {
                if (pipelineMessage.Request.Retry != null && pipelineMessage.Request.Retry.Attempts <= pipelineMessage.Request.Retry.Max)
                {
                    return true;
                }
            }

            return false;
        }

        private InsurerQuotationResponse GetInsurerQuotationResponse(PipelineMessageDispatcherProducer pipelineMessage)
        {
            /// Se o Response for null, é criado um com os valores do result (se existir) 
            /// pois é obrigatório ter uma resposta a ser processada.
            if (pipelineMessage.Response == null)
            {
                if (pipelineMessage.Request != null)
                {
                    if (pipelineMessage.Result != null)
                    {
                        pipelineMessage.Response = pipelineMessage.Request.CreateResponse(pipelineMessage.Result.Code, pipelineMessage.Result.DescriptionCode,
                            pipelineMessage.Result.Description);
                    }
                    else
                    {
                        pipelineMessage.Response = pipelineMessage.Request.CreateResponse(Code.Failure, "Erro inesperado no dispatcher producer pipeline.");
                    }
                }
                else
                {
                    throw new Exception("Base request é vazio ou null.");
                }
            }

            var insurerQuotationResponse = new InsurerQuotationResponse
            {
                Notification = pipelineMessage.Request.Notification,
                Response = pipelineMessage.Response
            };

            return insurerQuotationResponse;
        }

        #endregion
    }
}
