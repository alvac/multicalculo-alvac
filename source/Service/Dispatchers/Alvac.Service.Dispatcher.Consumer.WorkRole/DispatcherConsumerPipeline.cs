﻿using Alvac.Domain.Contract.Response;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Logger;
using Alvac.Domain.Services.Queue;
using System;
using System.Collections.Concurrent;
using System.Threading.Tasks;
using System.Threading.Tasks.Dataflow;
using Alvac.Domain.Services.Pipeline.Consumer;
using System.Runtime.ExceptionServices;
using Alvac.Domain.Contract;
using Alvac.Domain.Enum;
using Alvac.Domain.Contract.Extensions;
using Alvac.Domain.Services.Billing;
using Alvac.Domain.Services.Interfaces;
using Alvac.Domain.Services;
using Alvac.Domain.Entities.Commands.InsurerQuotations;
using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities;
using Alvac.Domain.Contract.Exceptions;
using Alvac.Domain.Services.Pipeline;

namespace Alvac.Service.Dispatcher.Consumer.WorkerRole
{
    public class DispatcherConsumerPipeline : IDispatcherConsumerPipeline
    {
        #region Private Fields

        private readonly IContainerProvider _containerProvider;
        private readonly IMessageQueueFactory _messageQueueFactory;
        private readonly ILogger _logger;

        private DispatcherParameter _dispatcherParameter;
        private bool _isRunning;

        protected ConcurrentDictionary<int, PipelineLink<PipelineMessageDispatcherConsumer>> _links;

        protected BufferBlock<QueueMessage<InsurerQuotationResponse>> _buffer;

        protected TransformBlock<QueueMessage<InsurerQuotationResponse>, PipelineMessageDispatcherConsumer> _builderPipelineMessageBlock;
        protected TransformBlock<PipelineMessageDispatcherConsumer, PipelineMessageDispatcherConsumer> _registerTransactionBlock;
        protected TransformBlock<PipelineMessageDispatcherConsumer, PipelineMessageDispatcherConsumer> _updateResponseBlock;
        protected ActionBlock<PipelineMessageDispatcherConsumer> _createNotificationBlock;

        #endregion

        #region Contructor

        public DispatcherConsumerPipeline(IContainerProvider containerProvider, IMessageQueueFactory messageQueueFactory, ILogger logger)
        {
            _containerProvider = containerProvider;
            _messageQueueFactory = messageQueueFactory;
            _logger = logger;
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Verifica se o resultado do envio foi com sucesso para criar o link com o flow de registro de transação.
        /// </summary>
        /// <param name="pipelineMessage"></param>
        /// <returns></returns>
        private bool RouteToRegisterTransaction(PipelineMessageDispatcherConsumer pipelineMessage)
        {
            var result = false;

            try
            {
                if (pipelineMessage != null && pipelineMessage.Result != null && pipelineMessage.Result.Code == Code.Success)
                {
                    result = true;
                }
            }
            catch
            {
                result = false;
            }

            return result;
        }

        #endregion

        #region IDispatcherConsumerPipeline Members

        public Func<QueueMessage<InsurerQuotationResponse>, Task<PipelineMessageDispatcherConsumer>> BuilderPipelineMessageAsync()
        {
            return (
                async queueMessage =>
                {
                    ExceptionDispatchInfo exception = null;

                    PipelineMessageDispatcherConsumer result = null;

                    try
                    {
                        if (queueMessage != null && queueMessage.Item != null)
                        {
                            await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "DispatcherConsumer", "BuilderPipelineMessageAsync", 
                                string.Format("Inicio da criação do pipeline message. PipelineMessage: {0}", queueMessage.ToString()));

                            if (queueMessage.Item.Response != null && queueMessage.Item.Response is BaseResponse)
                            {
                                result = new PipelineMessageDispatcherConsumer
                                {
                                    Message = queueMessage,
                                    Notification = queueMessage.Item.Notification,
                                    Response = queueMessage.Item.Response,
                                    Result = new OperationResult { Code = Code.Success, DescriptionCode = DescriptionCode.Successfully, 
                                        Description = "pipeline message criado com sucesso." }
                                };
                            }
                            else
                            {
                                throw new Exception("Resposta invalida.");
                            }
                        }
                        else
                        {
                            throw new Exception("Queue message é vazio ou null.");
                        }

                        await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "DispatcherConsumer", "BuilderPipelineMessageAsync", 
                            string.Format("pipeline message criado com sucesso. PipelineMessage: {0}",
                           queueMessage.ToString()));
                    }
                    catch (Exception ex)
                    {
                        exception = ExceptionDispatchInfo.Capture(ex);

                        if (result == null)
                        {
                            result = new PipelineMessageDispatcherConsumer();
                        }

                        result.Result = ex.ToOperationResult("Erro inesperado.");
                    }

                    if (exception != null)
                    {
                        await _logger.WriteErrorAsync("DispatcherConsumer", 
                            "BuilderPipelineMessageAsync", "Erro inesperado", exception.SourceException);
                    }

                    return result;
                });
        }

        public Func<PipelineMessageDispatcherConsumer, Task<PipelineMessageDispatcherConsumer>> RegisterTransactionAsync()
        {
            return (
                async pipelineMessage =>
                {
                    string correlationId = string.Empty;

                    ExceptionDispatchInfo exception = null;

                    try
                    {
                        await _logger.WriteVerboseAsync(pipelineMessage.Message.CorrelationId, "DispatcherConsumer", "RegisterTransactionAsync", 
                            string.Format("RegisterTransactionAsync '{0}' inicio do processo.", pipelineMessage.ToString()));

                        if (pipelineMessage.Response != null)
                        {
                            if (pipelineMessage.Response.Result != null && pipelineMessage.Response.Result.Code == Code.Success)
                            {
                                using (var scope = _containerProvider.BeginScope())
                                {
                                    var billingService = _containerProvider.GetService<IBillingService>();

                                    var transactionId = await billingService.DebitAsync(pipelineMessage.Response.Identifier);

                                    await _logger.WriteVerboseAsync(pipelineMessage.Message.CorrelationId, "DispatcherConsumer", "RegisterTransactionAsync", 
                                        string.Format("Transaction '{0}' registrado com sucesso.", transactionId));
                                }
                            }
                            else
                            {
                                return pipelineMessage;
                            }
                        }
                        else
                        {
                            throw new AlvacException("Resposta é vazia ou null.", DescriptionCode.InternalServerError);
                        }
                    }
                    catch (Exception ex)
                    {
                        exception = ExceptionDispatchInfo.Capture(ex);

                        if (pipelineMessage != null && pipelineMessage.Message != null)
                        {
                            correlationId = pipelineMessage.Message.CorrelationId;
                        }

                        if (pipelineMessage == null)
                        {
                            pipelineMessage = new PipelineMessageDispatcherConsumer();
                        }

                        pipelineMessage.Result = ex.ToOperationResult(ex.Message);
                    }

                    if (exception != null)
                    {
                        await _logger.WriteErrorAsync("DispatcherConsumer", "RegisterTransactionrAsync",
                            "Erro inesperado", exception.SourceException);
                    }

                    return pipelineMessage;
                });
        }

        public Func<PipelineMessageDispatcherConsumer, Task<PipelineMessageDispatcherConsumer>> UpdateResponseAsync()
        {
            return (
                 async pipelineMessage =>
                 {
                     string correlationId = string.Empty;

                     ExceptionDispatchInfo exception = null;

                     try
                     {
                         using (var scope = _containerProvider.BeginScope())
                         {
                             if (pipelineMessage.Result != null && pipelineMessage.Result.Code != Code.Failure)
                             {
                                 var updateResponseFactory = _containerProvider.GetService<IUpdateResponseFactory>();

                                 var updateResponse = updateResponseFactory.Create(pipelineMessage.Response.RequestType);

                                 await updateResponse.UpdateAsync(pipelineMessage.Response);

                                 await _logger.WriteVerboseAsync(pipelineMessage.Message.CorrelationId, "DispatcherConsumer", "UpdateResponseAsync", 
                                     string.Format("Update quotation '{0}' registrado com sucesso.", pipelineMessage.Response.Identifier));
                             }
                             else
                             {
                                 if (pipelineMessage.Response != null)
                                 {
                                     pipelineMessage.Response.Result = pipelineMessage.Result;

                                     var updateStatusInsurerQuotationCommand = new UpdateStatusInsurerQuotation
                                     {
                                         InsurerQuotationId = pipelineMessage.Response.Identifier,
                                         Status = Status.Failure,
                                         OperationDescriptionCode = pipelineMessage.Result.DescriptionCode,
                                         OperationDescription = pipelineMessage.Result.Description
                                     };

                                     var commandProcessor = _containerProvider.GetService<ICommandProcessor>();
                                     await commandProcessor.ProcessAsync<InsurerQuotation>(updateStatusInsurerQuotationCommand);
                                 }

                                 return pipelineMessage;
                             }
                         }
                     }
                     catch (Exception ex)
                     {
                         exception = ExceptionDispatchInfo.Capture(ex);

                         if (pipelineMessage != null && pipelineMessage.Message != null)
                         {
                             correlationId = pipelineMessage.Message.CorrelationId;
                         }

                         if (pipelineMessage == null)
                         {
                             pipelineMessage = new PipelineMessageDispatcherConsumer();
                         }

                         pipelineMessage.Result = ex.ToOperationResult(ex.Message);
                     }

                     if (exception != null)
                     {
                         await _logger.WriteErrorAsync("DispatcherConsumer", "UpdateResponseAsync", "Erro inesperado",
                             exception.SourceException);
                     }

                     return pipelineMessage;
                 });
        }

        public async Task CreateNotificationAsync(PipelineMessageDispatcherConsumer pipelineMessage)
        {
            string correlationId = string.Empty;

            ExceptionDispatchInfo exception = null;

            try
            {
                if (pipelineMessage.Notification != null && pipelineMessage.Notification.CallType != CallType.None && !string.IsNullOrEmpty(pipelineMessage.Notification.Path))
                {
                    if (pipelineMessage.Response != null)
                    {
                        using (var scope = _containerProvider.BeginScope())
                        {
                            var notificationProvider = _containerProvider.GetService<INotificationProvider>();

                            var notificationRequest = pipelineMessage.Response.ToNotificationRequest(pipelineMessage.Notification.CorrelationId);

                            await notificationProvider.RegisterNotificationAsync(pipelineMessage.Notification, notificationRequest);

                            await notificationProvider.EnqueueAsync(notificationRequest, _dispatcherParameter.NotificationQueueName);
                        }
                    }
                    else
                    {
                        throw new AlvacException("Resposta é vazia ou null.", DescriptionCode.InvalidRequest);
                    }

                }
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);

                if (pipelineMessage != null && pipelineMessage.Message != null)
                {
                    correlationId = pipelineMessage.Message.CorrelationId;
                }

                if (pipelineMessage == null)
                {
                    pipelineMessage = new PipelineMessageDispatcherConsumer();
                }

                pipelineMessage.Result = ex.ToOperationResult(ex.Message);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("DispatcherConsumer", "CreateNotificationAsync",
                    "Erro inesperado", exception.SourceException);
            }
        }

        public async Task StartAsync()
        {
            _isRunning = true;

            var unboundedConfig = new ExecutionDataflowBlockOptions { MaxDegreeOfParallelism = DataflowBlockOptions.Unbounded };

            _buffer = new BufferBlock<QueueMessage<InsurerQuotationResponse>>(new DataflowBlockOptions { BoundedCapacity = 1 });

            _builderPipelineMessageBlock = new TransformBlock<QueueMessage<InsurerQuotationResponse>, PipelineMessageDispatcherConsumer>((Func<QueueMessage<InsurerQuotationResponse>,
                Task<PipelineMessageDispatcherConsumer>>)BuilderPipelineMessageAsync(), new ExecutionDataflowBlockOptions
                {
                    MaxDegreeOfParallelism = DataflowBlockOptions.Unbounded,
                    BoundedCapacity = _dispatcherParameter.WindowSize
                });

            _registerTransactionBlock = new TransformBlock<PipelineMessageDispatcherConsumer, PipelineMessageDispatcherConsumer>((Func<PipelineMessageDispatcherConsumer, Task<PipelineMessageDispatcherConsumer>>)RegisterTransactionAsync(), unboundedConfig);
            _updateResponseBlock = new TransformBlock<PipelineMessageDispatcherConsumer, PipelineMessageDispatcherConsumer>((Func<PipelineMessageDispatcherConsumer, Task<PipelineMessageDispatcherConsumer>>)UpdateResponseAsync(), unboundedConfig);

            _createNotificationBlock = new ActionBlock<PipelineMessageDispatcherConsumer>((Func<PipelineMessageDispatcherConsumer, Task>)CreateNotificationAsync, unboundedConfig);

            _links = new ConcurrentDictionary<int, PipelineLink<PipelineMessageDispatcherConsumer>>();

            _buffer.LinkTo(_builderPipelineMessageBlock);

            _builderPipelineMessageBlock.LinkTo(_registerTransactionBlock, p => RouteToRegisterTransaction(p));
            _builderPipelineMessageBlock.LinkTo(_updateResponseBlock, p => !RouteToRegisterTransaction(p));

            _registerTransactionBlock.LinkTo(_updateResponseBlock);

            _updateResponseBlock.LinkTo(_createNotificationBlock);

            while (_isRunning)
            {
                QueueMessage<InsurerQuotationResponse> queueMessage = null;

                ExceptionDispatchInfo exception = null;

                string correlationId = DateTime.UtcNow.Ticks.ToString();

                try
                {
                    var inputQueue = _messageQueueFactory.Create<InsurerQuotationResponse>(_dispatcherParameter.OutputQueueName);

                    queueMessage = await inputQueue.ReceiveAsync(_dispatcherParameter.ReceiveTimeoutMiliSec);

                    if (queueMessage != null)
                    {
                        await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "DispatcherConsumer", "StartAsync", 
                            string.Format("Nova mensagem recuperada da fila. Body: {0}", queueMessage.Item));

                        if (!(await _buffer.SendAsync<QueueMessage<InsurerQuotationResponse>>(queueMessage)))
                        {
                            await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "DispatcherConsumer", "StartAsync", 
                                string.Format("Mensagem negado pelo Pipeline - Body: {0}", queueMessage.Item));

                            await inputQueue.PushAsync(queueMessage);

                            await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "DispatcherConsumer", "StartAsync", 
                                string.Format("Mensagem retornada para fila - Body: {0}", queueMessage.Item));
                        }
                        else
                        {
                            await _logger.WriteVerboseAsync(queueMessage.CorrelationId, "DispatcherConsumer", "StartAsync", 
                                string.Format("Mensagem enviada para o Pipeline - Body: {0}", queueMessage.Item));
                        }
                    }
                }
                catch (Exception ex)
                {
                    exception = ExceptionDispatchInfo.Capture(ex);

                    if (queueMessage != null)
                    {
                        correlationId = queueMessage.CorrelationId;
                    }
                }

                if (exception != null)
                {
                    await _logger.WriteErrorAsync("DispatcherConsumer", "StartAsync", "Erro inesperado", exception.SourceException);
                }
            }
        }

        public async Task StopAsync()
        {
            string correlationId = DateTime.Now.Ticks.ToString();

            await _logger.WriteInformationAsync(correlationId, "DispatcherProducer", "StopAsync", "Parando o servico.");

            if (_isRunning)
            {
                _isRunning = false;

                _buffer.Complete();
                await _buffer.Completion;

                await _logger.WriteVerboseAsync(correlationId, "DispatcherProducer", "StopAsync", "Buffer block completado.");

                _builderPipelineMessageBlock.Complete();
                await _builderPipelineMessageBlock.Completion;

                await _logger.WriteVerboseAsync(correlationId, "DispatcherProducer", "StopAsync", "Builder pipeline message block completado.");

                _registerTransactionBlock.Complete();
                await _registerTransactionBlock.Completion;

                await _logger.WriteVerboseAsync(correlationId, "DispatcherProducer", "StopAsync", "Register transaction block completado.");

                _updateResponseBlock.Complete();
                await _updateResponseBlock.Completion;

                await _logger.WriteVerboseAsync(correlationId, "DispatcherProducer", "StopAsync", "Update response block completado.");

                _createNotificationBlock.Complete();
                await _createNotificationBlock.Completion;

                await _logger.WriteVerboseAsync(correlationId, "DispatcherProducer", "StopAsync", "Create notification block completado.");

                await _logger.WriteInformationAsync(correlationId, "DispatcherProducer", "StopAsync", "Servico parado com sucesso.");
            }
            else
            {
                await _logger.WriteVerboseAsync(correlationId, "DispatcherProducer", "StopAsync", "Servico já esta parado");
            }
        }

        public void Initializer(DispatcherParameter parameters)
        {
            if (parameters == null)
            {
                throw new Exception("DispatcherConsumer - DispatcherParameter vazio ou null.");
            }

            _dispatcherParameter = parameters;
        }

        #endregion
    }
}
