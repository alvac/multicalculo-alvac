﻿using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.ConnectionParameters;
using Alvac.Domain.Entities.Queries.Connections;
using Alvac.Domain.Services;
using Alvac.Domain.Services.Dispatchers;
using Alvac.Domain.Services.Factories;
using Alvac.Domain.Services.Logger;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.ExceptionServices;
using System.Text;
using System.Threading.Tasks;
using Alvac.Domain.Contract.Extensions;
using Alvac.Domain.Services.Interfaces;

namespace Alvac.Service.Dispatcher.Consumer.WorkerRole
{
    public class DispatcherConsumerHost
    {
        #region Private Fields

        private readonly IContainerProvider _containerProvider;
        private readonly IMessageQueueFactory _messageQueueFactory;
        private readonly IQueryProcessor _queryProcessor;
        private readonly ILogger _logger;

        private readonly List<DispatcherConsumerPipeline> dispatcherConsumerPipelineList;

        #endregion

        #region Contructor

        public DispatcherConsumerHost(IContainerProvider containerProvider, IMessageQueueFactory messageQueueFactory, IQueryProcessor queryProcessor, ILogger logger)
        {
            _containerProvider = containerProvider;
            _messageQueueFactory = messageQueueFactory;
            _queryProcessor = queryProcessor;
            _logger = logger;
            dispatcherConsumerPipelineList = new List<DispatcherConsumerPipeline>();
        }

        #endregion

        #region Private Methods

        /// <summary>
        /// Carrega os parametros da conexão.
        /// </summary>
        /// <param name="connectionId"></param>
        /// <returns></returns>
        private async Task<IDictionary<string, string>> GetConnectionParametersAsync(int connectionId)
        {
            IDictionary<string, string> result = null; ;

            var findConnectionParametersByConnectionId = new FindConnectionParametersByConnectionId
            {
                ConnectionId = connectionId
            };

            var connectionParameters = await _queryProcessor.ProcessAsync(findConnectionParametersByConnectionId);

            if (connectionParameters != null)
            {
                result = new Dictionary<string, string>();

                foreach (var connectionParameter in connectionParameters)
                {
                    result.Add(connectionParameter.Key, connectionParameter.Value);
                }
            }

            return result;
        }

        #endregion

        #region IServiceHost Members

        public async Task StartAsync()
        {
            ExceptionDispatchInfo exception = null;

            try
            {
                using (var scope = _containerProvider.BeginScope())
                {
                    var findConnectionsByActive = new FindConnectionsByActive
                    {
                        IsActive = true
                    };

                    var connections = await _queryProcessor.ProcessAsync(findConnectionsByActive);

                    foreach (var connection in connections)
                    {
                        ExceptionDispatchInfo connectionException = null;

                        try
                        {
                            var dispatcherProducerParameter = new DispatcherParameter
                                {
                                    MaxWaitTimeSec = 60,
                                    MaxThroughput = connection.MaxThroughput,
                                    WindowSize = connection.WindowSize,
                                    InputQueueName = connection.InputQueueName,
                                    OutputQueueName = connection.OutputQueueName,
                                    NotificationQueueName = connection.NotificationQueueName
                                };

                            string crrelationId = connection.ConnectionId.ToString();

                            await _logger.WriteVerboseAsync(crrelationId, "DispatcherConsumerHost", "StartAsync",
                                string.Format("Criando '{0}' conexões para {1}",
                                connection.Amount, connection.Name));

                            for (int i = 0; i < connection.Amount; i++)
                            {
                                ExceptionDispatchInfo currentConnectionException = null;

                                try
                                {

                                    var connectionParameters = await GetConnectionParametersAsync(connection.ConnectionId);

                                    var dispatcherConsumerPipeline = new DispatcherConsumerPipeline(_containerProvider, _messageQueueFactory, _logger);
                                    dispatcherConsumerPipeline.Initializer(dispatcherProducerParameter);

                                    dispatcherConsumerPipelineList.Add(dispatcherConsumerPipeline);

                                    await _logger.WriteInformationAsync(crrelationId, "DispatcherConsumerHost", "StartAsync", string.Format("Criado '{0}' conexões para {1}", connection.Amount, connection.Name));

                                    dispatcherConsumerPipeline.StartAsync();
                                }
                                catch (Exception ex)
                                {
                                    currentConnectionException = ExceptionDispatchInfo.Capture(ex);
                                }

                                if (currentConnectionException != null)
                                {
                                    await _logger.WriteErrorAsync("DispatcherConsumerHost", "StartAsync",
                                        "Erro inesperado.", currentConnectionException.SourceException);
                                }
                            }

                        }
                        catch (Exception ex)
                        {
                            connectionException = ExceptionDispatchInfo.Capture(ex);
                        }

                        if (connectionException != null)
                        {
                            await _logger.WriteErrorAsync("DispatcherConsumerHost", "StartAsync", "Erro inesperado",
                                connectionException.SourceException);
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("DispatcherConsumerHost", "StartAsync", "Erro inesperado", exception.SourceException);
            }
        }

        public async Task StopAsync()
        {
            string correlationId = DateTime.UtcNow.Ticks.ToString();

            ExceptionDispatchInfo exception = null;

            try
            {
                //Stop all dispatchers simultaneously
                var stopTasks = dispatcherConsumerPipelineList.Select(d => d.StopAsync().TimeoutAfter(20000));

                ExceptionDispatchInfo itemStopAsyncException = null;

                try
                {
                    //Aqui ele manda parar todas as TASK ao mesmo tempo
                    await Task.WhenAll(stopTasks.ToArray());
                }
                catch (Exception ex)
                {
                    itemStopAsyncException = ExceptionDispatchInfo.Capture(ex);
                }

                if (itemStopAsyncException != null)
                {
                    await _logger.WriteErrorAsync("DispatcherConsumerHost", "StopAsync", "Erro inesperado",
                        itemStopAsyncException.SourceException);
                }

            }
            catch (Exception ex)
            {
                exception = ExceptionDispatchInfo.Capture(ex);
            }

            if (exception != null)
            {
                await _logger.WriteErrorAsync("DispatcherConsumerHost", "StopAsync", "Erro inesperado", exception.SourceException);
            }
        }

        #endregion
    }
}
