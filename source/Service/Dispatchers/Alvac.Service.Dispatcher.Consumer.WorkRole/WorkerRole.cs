using System.Diagnostics;
using System.Net;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.WindowsAzure.ServiceRuntime;
using Alvac.Infrastructure.SimpleInjector;

namespace Alvac.Service.Dispatcher.Consumer.WorkerRole
{
    public class WorkerRole : RoleEntryPoint
    {
        private readonly CancellationTokenSource cancellationTokenSource = new CancellationTokenSource();
        private readonly ManualResetEvent runCompleteEvent = new ManualResetEvent(false);

        private DispatcherConsumerHost _dispatcherConsumerHost;

        public override void Run()
        {
            Trace.TraceInformation("Alvac.Service.Dispatcher.Consumer.WorkRole is running");

            try
            {
                var simpleInjectorContainer = DispatcherConsumerBootstrap.Register();

                _dispatcherConsumerHost = simpleInjectorContainer.GetService(typeof(DispatcherConsumerHost)) as DispatcherConsumerHost;
                _dispatcherConsumerHost.StartAsync();

                this.RunAsync(this.cancellationTokenSource.Token).Wait();
            }
            finally
            {
                this.runCompleteEvent.Set();
            }
        }

        public override bool OnStart()
        {
            // Set the maximum number of concurrent connections
            ServicePointManager.DefaultConnectionLimit = 12;

            // For information on handling configuration changes
            // see the MSDN topic at http://go.microsoft.com/fwlink/?LinkId=166357.

            bool result = base.OnStart();

            Trace.TraceInformation("Alvac.Service.Dispatcher.Consumer.WorkRole has been started");

            return result;
        }

        public override void OnStop()
        {
            Trace.TraceInformation("Alvac.Service.Dispatcher.Consumer.WorkRole is stopping");

            this.cancellationTokenSource.Cancel();
            this.runCompleteEvent.WaitOne();

            base.OnStop();

            Trace.TraceInformation("Alvac.Service.Dispatcher.Consumer.WorkRole has stopped");
        }

        private async Task RunAsync(CancellationToken cancellationToken)
        {
            // TODO: Replace the following with your own logic.
            while (!cancellationToken.IsCancellationRequested)
            {
                Trace.TraceInformation("Working");
                await Task.Delay(1000);
            }
        }
    }
}
