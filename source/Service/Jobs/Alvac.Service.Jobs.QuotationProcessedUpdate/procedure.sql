﻿CREATE PROCEDURE [dbo].[QuotationProcessedUpdate]
AS 
	UPDATE [dbo].[Quotations] 
	SET Processed = 1
	WHERE Processed = 0 AND QuotationId NOT IN
	(
		SELECT A.QuotationId FROM [dbo].[Quotations] AS A
		JOIN [dbo].[InsurerQuotations] AS B
		ON A.QuotationId = B.QuotationId
		WHERE B.[Status] <= 2 
		GROUP BY A.QuotationId
	)