﻿using Alvac.Domain.Entities.Commands;
using Alvac.Domain.Entities.Commands.Invoices;
using Alvac.Domain.Entities.Queries;
using Alvac.Domain.Entities.Queries.Invoices;
using Alvac.Domain.Entities.Queries.Transactions;
using Alvac.Domain.Enum;
using Alvac.Domain.Services;
using Alvac.Infrastructure.SimpleInjector;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alvac.Service.Jobs.Invoice
{
    internal class Program
    {
        private static void Main(string[] args)
        {
            var simpleInjectorContainer = InvoiceJobBootstrap.Register();
            var containerProvider = simpleInjectorContainer.GetService<IContainerProvider>();

            try
            {
                using (var scope = containerProvider.BeginScope())
                {
                    var commandProcessor = simpleInjectorContainer.GetService<ICommandProcessor>();
                    var queryProcessor = simpleInjectorContainer.GetService<IQueryProcessor>();

                    var invoices = queryProcessor.ProcessAsync(new GetInvoicesByStatusClosing
                    {
                        Closing = DateTime.UtcNow,
                        Status = Alvac.Domain.Enum.InvoiceStatus.Open
                    }).Result;

                    foreach (var item in invoices.Take(int.Parse(ConfigurationManager.AppSettings["invoiceTake"])))
                    {
                        var closeInvoiceCommand = new CloseInvoice
                        {
                            InvoiceId = item.InvoiceId
                        };

                        var closedInvoices = commandProcessor.ProcessAsync(closeInvoiceCommand).Result;

                        if (closedInvoices)
                        {
                            var subscription = item.Subscription;
                            var plan = item.Subscription.Plan;

                            var minInvoiceValue = plan.Amount * plan.Value;
                            decimal currentValue = 0;

                            try
                            {
                                var totalQuotations = item.Transactions.Count;

                                if (totalQuotations > plan.Amount)
                                {
                                    currentValue = item.Transactions.Sum(x => (decimal?)x.Value) ?? 0;
                                }
                            }
                            catch
                            {
                            }

                            if (subscription.Type == SubscriptionType.Normal)
                            {
                                item.Value = currentValue <= minInvoiceValue ? minInvoiceValue : currentValue;
                            }


                            var newInvoice = new CreateInvoice
                            {
                                SubscriptionId = item.SubscriptionId,
                                Closing = plan.RecurrenceType == PlanRecurrenceType.Month ? item.Closing.AddMonths(plan.Recurrence) : item.Closing.AddDays(plan.Recurrence)
                            };

                            commandProcessor.ProcessAsync(newInvoice).Wait();

                            System.Diagnostics.Trace.TraceInformation(string.Format("Invoice '{0}' fechada com sucesso. Date: {0} Closing: {1} - Status: {2} - Value: {3}",
                                    item.InvoiceId, item.Closing, item.Status, item.Value));
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                System.Diagnostics.Trace.TraceError(ex.Message);
                throw;
            }
        }
    }
}